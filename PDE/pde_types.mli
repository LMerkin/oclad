(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*         Diffusion and Numerical Method Types for the PDE Solver:          *)
(*===========================================================================*)
(*-----------------*)
(* Calendars:      *)
(*-----------------*)
(* Date in Excel LFDATE format, similar to JDT but with the Epoch at
   1899-12-30.0; must be less than 2199-03-01.0:
*)
type  jdt    = 'a;

(* Conversions to/from "float" are provided, but must be done explicitly:    *)
value jdt_to_float: jdt    -> float;
value jdt_of_float: float  -> jdt;

(* Time as a year fraction, according to the standard FX Vols convention:    *)
type  yft    = 'a;

value yft_to_float: yft    -> float;
value yft_of_float: float  -> yft;

(* Throughout the PDE solver, the FX Vols day count ( year fraction ) conv is
   used -- even in interpolating the interest rates ( since the  Ccy-specific
   conventions are already embedded in the discount factors provided ):
*)
value fxvols_yft: jdt -> jdt -> yft;

(*-----------------*)
(* Rates:          *)
(*-----------------*)
(* Normally, the interest rates are inferred from an array of LFDATEs and
   Discount Factors. For testing purposes, constant rates can be provided:
*)
type rate_info =
[
  ConstRate of float
| DF_Table  of array jdt and array float
];

(*-----------------*)
(* Spatial Axes:   *)
(*-----------------*)
(* Both uniform and non-uniform axes can in general be used: *)
type spatial_axis  =
[
  Uniform_Axis    of array float and float    (* All Points, Fixed Step      *)
| NonUniform_Axis of array float              (* All Points                  *)
];

(*===========================================================================*)
(* PDE Solution Spec: To be passed to the Backward Induction Engine:         *)
(*===========================================================================*)
(*------------------------------------------*)
(* PDE Spec for 2D Diffusions, e.g. SLV:    *)
(*------------------------------------------*)
module Pde2d:
sig
  (*---------------------*)
  (* 2D Diffusions:      *)
  (*---------------------*)
  (* SLV 2D Diffusion: LocalVol + StocVol of SABR type.
     The LocalVol factor is a general Lambda expr:   Spot ->  t -> LocalVol.
     All time arguments are year fractions following the FX Vols convention:
  *)
  type slv =
  {
    slv_r_dom:      yft   -> float;         (* Domestic fwd rate    *)
    slv_r_for:      yft   -> float;         (* Foreign  fwd rate    *)
    slv_local_vol:  float -> yft -> float;  (* LocalVol( Spot, t )  *)
    slv_alpha:      yft   -> float;         (* SABR coeff           *)
    slv_corr:       yft   -> float          (* Corr( Spot, Vol )    *)
  };

  type heston =
  {
    heston_r_dom:   yft   -> float;         (* Domestic fwd rate    *)
    heston_r_for:   yft   -> float;         (* Foreign  fwd rate    *)
    heston_kappa:   yft   -> float;         (* Mean Rev Rate        *)
    heston_theta:   yft   -> float;         (* The Mean             *)
    heston_volvol:  yft   -> float;         (* The  Vol of Vol      *)
    heston_corr :   yft   -> float          (* Corr( Spot, Vol )    *)
  };

  type diffusion =
  [
    SLV    of slv
  | Heston of heston
  ];

  (*---------------------*)
  (* Boundary Conds:     *)
  (*---------------------*)
  (* -- 1st dim of the Diffusion is always the Spot.
     -- 2nd dim is typically a StocVol ( for 2D Diffusions ).
     Asian extensions are possible but not implemented yet...

     Dirichlet boundary cond: a Lambda which produces a value at the boundary:
     -- 1st arg is in the "SpaceVar" opposite to the "boundary" one ( eg if it
        is a Spot boundary, Arg1 is StocVol; for a StocVol boundary, it's a
        Spot );
     -- 2nd arg is time;

     Neumann   bondary cond: a Lambda which produces a derivative wrt the
        bondary "SpaceVar" as a function of the opposite "SpaceVar" and time.
        Same args as for Dirichlet:
  *)
  type boundary_cond =
  [
    Free                            
  | Dirichlet of float -> yft -> float
  | Neumann   of float -> yft -> float
  ];

  (*---------------------*)
  (* BI Method & Engine: *)
  (*---------------------*)
  type bi_method =
  [
    (* Implicit-Explicit Splitting Method, O( tau ) temporal convergence,
       3-point spatial grid:
    *)
    Yanenko3

    (* As above, but with Exponential Fitting:  *)
  | Yanenko3_ExpFit

    (* As above, but with 5-point spatial grid: *)
  | Yanenko5
  | Yanenko5_ExpFit
  ];

  (* The engine which actually performs Backward Induction, updating the state
     of the grid:
  *)
  type bi_engine = pde_spec -> unit

  (*---------------------*)
  (* PDE Spec:           *)
  (*---------------------*)
  and pde_spec =
  {
    diffusion   : diffusion;    (* Diffusion used                            *)
    bi_engine   : bi_engine;    (* A 2D Backward Induction Engine            *)

    (* Initial spot at which the price is to be computed. It may or may not be
       on the grid; "i0" is its grid position ( None if not on the grid ):
    *)
    spot0       : float;        (* Always > 0                                *)
    vol0        : float;        (* Normally 1.0                              *)
  
    (* The temporal axis, of ( time_k+1 ) points, in FX Vol year fractions. The
       initial ( spot ) point is at 0.0  and the last one corresponds to the
       pay- off time. In general the temporal grid does not need to be uniform:
    *)
    time_k      : int;          (* Number of Time intervals, >= 1            *)
    ts          : array yft;    (* Of size ( time_k + 1 )                    *)

    (* The spatial axes: *)
    spot_n      : int;          (* Number of intervals wrt Spot,    >= 1     *)
    spots       : spatial_axis;

    vol_m       : int;          (* Number of intervals wrt StocVol, >= 1     *)
    vols        : spatial_axis;

    (* Boundary conds at all boundaries: *)
    df_dom      : yft -> float; (* Domestic and foreign Discount Factors     *)
    df_for      : yft -> float;

    spot_low_bc : boundary_cond;
    spot_up_bc  : boundary_cond;
    vol_low_bc  : boundary_cond;
    vol_up_bc   : boundary_cond;

    (* The Solution Space initialised to the option-specific termnl pay-off: *)
    ys          : array ( array float );

    (* Workspace for the backward Induction engine, to be initialised by the
       latter. Currently viewed as an arbitrary collection of 2D states:
    *)
    threads     : int;          (* Multi-Threading operation if >= 2         *)
    debug_dir   : string        (* Where to output the debug info during BI  *)
  };
end;

(*---------------------------------*)
(* Top-Level Diffusion&PDE Types:  *)
(*---------------------------------*)
type fx_diffusion_type =
[
  T_SLV_2D      (* Corresponds to Pde2d.SLV     *)
| T_Heston_2D   (* Corresponds to Pde2d.Heston  *)
];

type pde_spec =
[
  PDE_2D of Pde2d.pde_spec
];

(*===========================================================================*)
(* PDE Pricer: On evaluation, returns the Price by PDE solving:              *)
(*===========================================================================*)
(* In the simplest cases, the "pde_pricer" runs the Backward Induction Engine
   over a single pre-configured "pde_spec". In more complex cases ( eg, KIO ),
   multiple "pde_spec"s  are created and several backward induction processes
   are run:
*)
type pde_pricer = unit -> float;

