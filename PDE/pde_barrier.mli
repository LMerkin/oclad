(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*         Refinement of the PDE Spec According to the Option Type           *)
(*                  and Construction of the PDE Pricer:                      *)
(*                             Barrier Options                               *)
(*===========================================================================*)
value mk_barrier_pricer:
      Barrier_option.fx_barrieroption ->
      Options_utils.option_parameters ->
      Pde_types.pde_spec              ->
      Pde_types.pde_pricer;

