(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*         Refinement of the PDE Spec According to the Option Type           *)
(*                  and Construction of the PDE Pricer:                      *)
(*                             Simple Options                                *)
(*===========================================================================*)
module T  = Pde_types;
module T2 = Pde_types.Pde2d;
module U  = Pde_utils;
module BU = Pde_bi_utils;
module OU = Options_utils;
module A  = Argument;

(*===========================================================================*)
(* OE:                                                                       *)
(*===========================================================================*)
value mk_pde2d_oe: OU.option_parameters -> T2.pde_spec -> T.pde_pricer =

fun o_params pde_spec0 ->
  (*------------------------------*)
  (* Spot Axis:                   *)
  (*------------------------------*)
  (* XXX: In general, the upper boundary should be computed according to the
     average vol and the period to the option expiration, but for the moment,
     we just use a constant multiple of the max( S0, K ):
  *)
  let strike  = U.get_req_strike o_params             in
  let spot0   = pde_spec0.T2.spot0                    in
  let spot_up = 7.0 *. ( max spot0 strike )           in
  let spot_n  = pde_spec0.T2.spot_n                   in

  let use_uniform_axes = U.get_uniform o_params       in
  let spots   =
    if   use_uniform_axes
    then U.mk_uniform_axis 0.0 spot_up spot_n
    else failwith "ERROR: mk_pde2d_oe: Non-uniform axes not implemented yet"
  in
  (*------------------------------*)
  (* Pay-Off and Spot BoundConds: *)
  (*------------------------------*)
  let cp     = U.get_req_cp o_params                  in
  let ts     = pde_spec0.T2.ts                        in
  let tt     = ts.( Array.length ts - 1 )             in
  let dfd    = pde_spec0.T2.df_dom                    in
  let dff    = pde_spec0.T2.df_for                    in
  let dff_tt = dff tt                                 in
  let stk_tt = strike *. ( dfd tt )                   in

  let payoff =
    if cp = A.Call
    then fun s -> max ( s -. strike ) 0.0
    else fun s -> max ( strike -. s ) 0.0
  in
  let spot_low_bc =
    if cp = A.Call
    then T2.Dirichlet ( fun _ _ -> 0.0 )
    else T2.Dirichlet ( fun _ t -> stk_tt /. ( dfd t ) )
  in
  let spot_up_bc  =
    if cp = A.Call
    then T2.Neumann   ( fun _ t -> dff_tt /. ( dff t ) )
    else T2.Dirichlet ( fun _ _ -> 0.0   )
  in
  (*------------------------------*)
  (* StocVol BoundConds:          *)
  (*------------------------------*)
  (* The Low Vol BC is from the hyperbolic PDE, the Upper one is obtained from
     the BSM formula, and for Put, it's actually the same as for StocVol=oo:
  *)
  let vol_low_bc    = BU.mk_vol_low_bc_2d pde_spec0 payoff in
  let vol_up_bc     =
    if cp = A.Call
    then T2.Dirichlet ( fun s t -> s *. dff_tt /. ( dff t ) )
    else spot_low_bc
  in
  (*------------------------------*)
  (* The final PDE Spec:          *)
  (*------------------------------*)
  let pde_spec1 =
  {
    ( pde_spec0 )  with
    T2.spots       = spots;
    T2.spot_low_bc = spot_low_bc;
    T2.spot_up_bc  = spot_up_bc;
    T2.vol_low_bc  = vol_low_bc;
    T2.vol_up_bc   = vol_up_bc
  }
  in
  (* Set up the terminal pay-off -- depends on the Spot only:       *)
  let pde_spec2 = BU.set_payoff_2d pde_spec1 payoff in

  (*------------------------------*)
  (* The Pricer:                  *)
  (*------------------------------*)
  (* The pricer is generic -- just a single backward-induction run: *)
  BU.mk_pricer2d_gen pde_spec2;
  

(*===========================================================================*)
(* Simple Options Top-Level:                                                 *)
(*===========================================================================*)
(* 2-level de-multiplexing: by Option Sub-Type and PDE Dimensionality:       *)

value mk_simple_pricer:
  Simple_option.fx_simpleoption ->
  OU.option_parameters          ->
  T.pde_spec                    ->
  T.pde_pricer                  =

fun simple o_params pde_spec0   ->
  (* Construct the PDE Spec for the given option sub-type: *)
  match simple with
  [
    (*---------*)
    (* OE:     *)
    (*---------*)
    Simple_option.OE _ _ ->
      match pde_spec0 with
      [
        T.PDE_2D pde_spec2d -> mk_pde2d_oe o_params pde_spec2d
      ]
    (*---------*)
    (* Others: *)
    (*---------*)
  | _ -> failwith ( "ERROR: mk_pde_simple: Unsupported option sub-type: "^
                    Simple_option.string_of_fx_simpleoption simple )
  ];

