(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                         Backward-Induction Engine                         *)
(*===========================================================================*)
(*--------------------------*)
(* Generic 2D Pricer Gener: *)
(*--------------------------*)
value mk_pricer2d_gen: Pde_types.Pde2d.pde_spec -> Pde_types.pde_pricer;

(*--------------------------*)
(* Pay-Off and BoundConds:  *)
(*--------------------------*)
(* "mk_vanilla_payoff":
   Vanilla European Call / Put Pay-Off.  Arg2 is the Strike:
*)
value mk_std_payoff: Argument.call_put -> float -> ( float -> float );

(* "set_payoff_2d":
   Pay-off depends on the Spot only, the PDE is 2D -- most common case:
*)
value set_payoff_2d:
  Pde_types.Pde2d.pde_spec -> ( float -> float ) ->
  Pde_types.Pde2d.pde_spec;

(* "mk_vol_low_bc_2d":
   BC at Low StocVol in 2D, via a degenerate ( hyperbolic ) PDE. Arg2 is the
   pay-off function:
*)
value mk_vol_low_bc_2d:
  Pde_types.Pde2d.pde_spec -> ( float -> float ) ->
  Pde_types.Pde2d.boundary_cond;

(*--------------------------*)
(* Low-Level BI Support:    *)
(*--------------------------*)
(* "apply_bcs_2d_3diag":    *)

value apply_bcs_2d_3diag:
  ( Pde_types.Pde2d.boundary_cond * Pde_types.Pde2d.boundary_cond *
  (* low_bc                         up_bc                  *)

    float        *  int * float   * Pde_types.yft
  (* other_coord    nm    this_h    next_t                 *)
  )
  -> array float -> array float -> array float -> array float    -> unit;
  (* diag1          diag2          diag3          rhs      *)

(* "tri_diag_solver":
   Solving 3-diagonal linear systems. The args are over-written, the result
   comes into the last array:
*)
value tri_diag_solver:
  array float -> array float -> array float -> array float -> unit;
  (* Lower Diag  Main Diag      Upper Diag     RHS & Res   *)
  (* Size( n-1 ) Size( n )      Size( n-1 )    Size( n )   *)

(* "interpolate_price_2d":
   Find the initial condition on the grid and interpolate the price ( if it is
   not exactly in a grid node ). Currently, linear interpolation is used:
*)
value interpolate_price_2d: Pde_types.Pde2d.pde_spec  -> float;

(* "output_state_2d":
   For debugging and visualisation only:
*)
value output_state_2d:
  ?half_step:bool -> Pde_types.Pde2d.pde_spec -> Pde_types.yft ->
  array ( array float  ) -> unit;

(*--------------------------*)
(* Multi-Threading Support: *)
(*--------------------------*)
(* "run_sub_steps":
   Each of the "mn" steps is to be processed by "thrds" concurrent threads;
   each thraed processes a block of steps via the "do_block" function with
   the args: [ step_from step_to thread_id ]; the last "()"  arg is to get
   the deferred evaluation:
*)
value run_sub_steps:
  ~do_block:( int -> int -> int -> unit -> unit ) -> ~mn:int -> ~thrds:int ->
  unit;

