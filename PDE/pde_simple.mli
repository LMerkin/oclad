(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*         Refinement of the PDE Spec According to the Option Type           *)
(*                  and Construction of the PDE Pricer:                      *)
(*                              Simple Options                               *)
(*===========================================================================*)
value mk_simple_pricer:
      Simple_option.fx_simpleoption   ->
      Options_utils.option_parameters ->
      Pde_types.pde_spec              ->
      Pde_types.pde_pricer;

