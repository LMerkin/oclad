(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*       N.N.Yanenko Backward-Induction Engines, 3-Point Spatial Grids       *)
(*===========================================================================*)
module T  = Pde_types;
module T2 = Pde_types.Pde2d;
module U  = Pde_utils;
module BU = Pde_bi_utils;

(*===========================================================================*)
(*                                    Utils:                                 *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* Computation of the Difference for the Cross-Derivative ( Unifirm Grid ):  *)
(*---------------------------------------------------------------------------*)
(* Explicit approximation:
   in the "yanenko_2d_uni" below, it is called with teh following args:
      for the Spot sub-step, ( y, ij, ji, mn ) = ( ys, i, j, m );
      for the Vol  sub-step, ( y, ij, ji, mn ) = ( yh, j, i, n ):
*)
value xder_uni: array ( array float ) -> int -> int -> int -> float =
fun  y ij ji mn ->
  if ji = 0
  then
    (* One-sided derivative wrt StocVol: *)
    2.0 *. ( y.( ij+1 ).( 1 )    -. y.( ij-1 ).( 1 ) -.
             y.( ij+1 ).( 0 )    +. y.( ij-1 ).( 0 ) )
  else
  if ji = mn
  then
    (* One-sided derivative wrt StocVol: *)
    2.0 *. ( y.( ij+1 ).( mn )   -. y.( ij-1 ).( mn ) -.
             y.( ij+1 ).( mn-1 ) +. y.( ij-1 ).( mn-1 ) )
  else
    (* Generic case: 2-sided derivative: *)
    y.( ij+1 ).( ji+1 ) -. y.( ij-1 ).( ji+1 ) -.
    y.( ij+1 ).( ji-1 ) +. y.( ij-1 ).( ji-1 );


(*===========================================================================*)
(*    SLV 2D Diffusion, Uniform Grid, O( tau ), Exp Fitting Available:       *)
(*===========================================================================*)
value bi_slv2d_uni: ~with_exp_fit:bool -> T2.pde_spec -> unit =
fun  ~{ with_exp_fit }  pde_spec       ->

  (*-------------------------------------------------------------------------*)
  (* The Grid and Diagonals for the Implicit 2D Solver:                      *)
  (*-------------------------------------------------------------------------*)
  (* XXX: This section can be factored out, but passing so many params as func
     args is error-prone. Copy it over when implementing other diffusion types:
  *)
  let n     = pde_spec.T2.spot_n                                        in
  let m     = pde_spec.T2.vol_m                                         in
  let k     = pde_spec.T2.time_k                                        in

  let ( spots, hs ) = U.get_axis_uni pde_spec.T2.spots                  in
  let ( vols,  hv ) = U.get_axis_uni pde_spec.T2.vols                   in
  let ts    = pde_spec.T2.ts                                            in

  (* NB: "ys" will be over-written during the BI process: *)
  let ys    = pde_spec.T2.ys                                            in
  let thrds = pde_spec.T2.threads                                       in

  (* The Spot diagonals and RHSs:                         *)
  let ds1   = Array.init ( thrds ) ( fun _ -> Array.make ( n   ) nan )  in
  let ds2   = Array.init ( thrds ) ( fun _ -> Array.make ( n+1 ) nan )  in
  let ds3   = Array.init ( thrds ) ( fun _ -> Array.make ( n   ) nan )  in

  (* Solution space for the intermediate step:            *)
  let yh    = Array.init (  m+1  ) ( fun _ -> Array.make ( n+1 ) nan )  in

  (* The Vol  diagonals and RHSs: *)
  let dv1   = Array.init ( thrds ) ( fun _ -> Array.make ( m   ) nan )  in
  let dv2   = Array.init ( thrds ) ( fun _ -> Array.make ( m+1 ) nan )  in
  let dv3   = Array.init ( thrds ) ( fun _ -> Array.make ( m   ) nan )  in

  (* Interest rates -- available for any diffusion:       *)
  let r_dom = U.get_dom_rate_2d  pde_spec.T2.diffusion                  in
  let r_for = U.get_for_rate_2d  pde_spec.T2.diffusion                  in

  (* Useful constants ( any diffusion ):                  *)
  let _2hs  = 2.0 *. hs                                                 in
  let _2hv  = 2.0 *. hv                                                 in
  let _8hsv = 8.0 *. hs *. hv                                           in
  let _hss  = hs  *. hs                                                 in
  let _2hvv = 2.0 *. hv *. hv                                           in

  (* Extract the diffusion, must be SLV:                  *)
  let slv   =
    match pde_spec.T2.diffusion with
    [
      T2.SLV slv' -> slv'
    | _           -> invalid_arg "ERROR: bi_slv2d_uni: Not an SLV diffusion"
    ]
  in
  (*-------------------------------------------------------------------------*)
  (* Make all BI steps; q=0 corresp to the terminal pay-off, q=k to price:   *)
  (*-------------------------------------------------------------------------*)
  for q = 1 to k
  do{
    (* ( Possibly ) debugging output -- initial state before the BI process: *)
    if q = 1
    then BU.output_state_2d ~{ half_step=False } pde_spec ts.( k ) ys
    else ();

    (* Times and intetrest rates -- diffusion-independent:      *)
    let t0   = ts.( k-q+1 )                                             in
    let t1   = ts.( k-q   )                                             in
    let t0f  = T.yft_to_float t0                                        in
    let t1f  = T.yft_to_float t1                                        in
    let th   = T.yft_of_float ( ( t0f +. t1f ) /. 2.0 )                 in
    let itau = 1.0  /. ( t1f -. t0f ) (* Inverse time step, < 0 *)      in
    let rdh  = r_dom th                                                 in
    let rfh  = r_for th                                                 in

    (* Optimisation Params -- diffusion-specific: *)

    let alpha0    = slv.T2.slv_alpha t0                                 in
    let rho0      = slv.T2.slv_corr  t0                                 in
    let alphah    = slv.T2.slv_alpha th                                 in
    let rhoh      = slv.T2.slv_corr  th                                 in
    let alpha1    = slv.T2.slv_alpha t1                                 in
    let alr0      = alpha0 *. rho0                                      in
    let alrh      = alphah *. rhoh                                      in
    let alph12    = alpha1 *. alpha1                                    in
    (* Pre-evaluate the Local Vols at "t0" and "th" for all Spots: *)
    let lv0       = Array.init ( n+1 )
                               ( fun i -> slv.T2.slv_local_vol spots.( i ) t0 )
    in
    let lvh       = Array.init ( n+1 )
                               ( fun i -> slv.T2.slv_local_vol spots.( i ) th )
    in
    (*-----------------------------------------------------------------------*)
    (* Spot ( #0 ) and StocVol ( #1 ) Sub-Steps:                             *)
    (*-----------------------------------------------------------------------*)
    for step = 0 to 1
    do{
      let  ( is_spot,  d1,  d2,  d3, rhs, nm, mn ) =
        if step = 0
        then ( True,  ds1, ds2, ds3,  yh,  n,  m )
        else ( False, dv1, dv2, dv3,  ys,  m,  n )
      in
      (*---------------------------------------------------------------------*)
      (* Make a sub-step from "t0" to "th" ( step=0 ) or from "th" to "t1"r  *)
      (*  ( step=1 ), implicitly in Spot ( resp StocVol ), explicitly in the *)
      (*  other co-ord, for all values of the other co-ord in the range of   *)
      (*  "ji"s, in the thread given by "p":                                 *)
      (*---------------------------------------------------------------------*)
      let do_block: int -> int -> int -> unit -> unit =
      fun   ji_from ji_to p () ->
        for ji = ji_from to ji_to
        do{
          (*-----------------------------------------------------------------*)
          (* Form the Diagonals -- Inner Points:                             *)
          (*-----------------------------------------------------------------*)
          (* Diffusion-specific parts: *)
          for ij = 1 to nm-1
          do{
            if is_spot
            then
              (*-------------*)
              (* Spot Diags: *)
              (*-------------*)
              let  i  = ij                              in
              let  j  = ji                              in
              let si  = spots.( i )                     in
              let ah  = ( rdh -. rfh ) *. si            in
              let a   = ah /. _2hs                      in
              let svv = si  *. lvh.( i ) *. vols.( j )  in
              let bh  = 0.5 *. svv *. svv               in
              let b   =
                (* Use exponential fitting?   *)
                if with_exp_fit
                then
                  let mu = ah *. hs /. 2.0    in
                  mu /. tanh( mu /. bh ) /. _hss
                else
                  bh /. _hss
              in
              do{
                (* Coeffs @ yh.( j ){ i-1, i, i+1 }: *)
                d1.( p ).( i-1 ):= b -. a;
                d2.( p ).( i )  := itau -. 2.0 *. b -. rdh;
                d3.( p ).( i )  := a +. b;

                (* The RHS: *)
                rhs.( j ).( i ) := ys.( i ).( j ) *. itau  -.
                                   alr0 *. si *. lv0.( i ) *.
                                   vols.( j ) *. vols.( j ) /. _8hsv *.
                                   ( xder_uni ys i j m )
              }
            else
              (*-------------*)
              (* Vol  Diags: *)
              (*-------------*)
              let i    = ji                             in
              let j    = ij                             in
              let sgm2 = vols.( j ) *. vols.( j )       in
              let b    = alph12     *. sgm2 /. _2hvv    in
              (* No exp fitting here, since there is no convective term *)
              do{
                (* Coeffs @ ys.( i ).{ j-1, j, j+1 }: *)
                d1.( p ).( j-1 ):= b;
                d2.( p ).( j )  := itau -. 2.0 *. b;
                d3.( p ).( j )  := b;

                (* The RHS: *)
                rhs.( i ).( j ) := yh.( j ).( i ) *. itau -.
                                   alrh *. spots.( i ) *. lvh.( i ) *.
                                   sgm2 /. _8hsv  *. ( xder_uni yh j i n )
              }
          };
          (*-----------------------------------------------------------------*)
          (* Apply the Boundary Conds and solve the 3-diag system:           *)
          (*-----------------------------------------------------------------*)
          (* This part is diffusion-independent: *)
          let bc_params =
            if is_spot
            then     (* ThisCoord = Spot, OtherCoord = Vol,  j = ji, t = th: *)
            (
              pde_spec.T2.spot_low_bc,
              pde_spec.T2.spot_up_bc,
              vols.( ji ),  n, hs, th
                
            )
            else     (* ThisCoord = Vol,  OtherCoord = Spot, i = ji, t = t1: *)
            (
              pde_spec.T2.vol_low_bc,
              pde_spec.T2.vol_up_bc,
              spots.( ji ), m, hv, t1
            )
          in
          BU.apply_bcs_2d_3diag bc_params d1.( p ) d2.( p ) d3.( p ) rhs.( ji );

          BU.tri_diag_solver              d1.( p ) d2.( p ) d3.( p ) rhs.( ji )
        }
      in
      (*---------------------------------------------------------------------*)
      (* Run the sub-steps, possibly in parallel:                            *)
      (*---------------------------------------------------------------------*)
      BU.run_sub_steps ~{ do_block=do_block } ~{ mn=mn } ~{ thrds=thrds };

      (* ( Possibly ) debugging output at the end of the fractional step:    *)
      if is_spot
      then BU.output_state_2d ~{ half_step=True  } pde_spec th yh
      else BU.output_state_2d ~{ half_step=False } pde_spec t1 ys
    }
  };
  (* Backward induction done! *)

