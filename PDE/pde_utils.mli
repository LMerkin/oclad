(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*          PDE Utils: Misc Functions for PDE Generation and Solving         *)
(*===========================================================================*)
(*--------------------------*)
(* Search:                  *)
(*--------------------------*)
(* "bin_search":
    Assumes that the elements of "arg1" are monotonically increasing. Returns
    the index "ia" such that
    arg1.( ia ) <= arg2 < arg1.( ia+1 )
    or raises "Failure" if "arg2" cannot be bracketed by the "arg1" elements:
*)
value bin_search: array float -> float -> int;

(*--------------------------*)
(* Stateful Func Objects:   *)
(*--------------------------*)
(* "fun1":
   1-Arg Funcs over tabulated "float" values, with linear interpolation and
   memoisation of the last arg.    Such funcs can be constructed by partial
   application of "fun1":
*)
type  interp_type = [ Step | Linear | Exp ];
type  fun1_table  = 'a;

value mk_fun1_table: array float -> array float -> interp_type -> fun1_table;
value fun1:          fun1_table  -> float       -> float;

(*--------------------------*)
(* Retriving Option Params: *)
(*--------------------------*)
value get_req_expiry_date:
  Options_utils.option_parameters -> Pde_types.jdt;

value get_diffusion_type :
  Options_utils.option_parameters -> Pde_types.fx_diffusion_type;

value get_bi_method_2d   :
  Options_utils.option_parameters -> Pde_types.Pde2d.bi_method;

value get_spot_n         : Options_utils.option_parameters -> int;
value get_time_step      : Options_utils.option_parameters -> float;
value get_vol_m          : Options_utils.option_parameters -> int;
value get_vol_up         : Options_utils.option_parameters -> float;
value get_threads_no     : Options_utils.option_parameters -> int;
value get_uniform        : Options_utils.option_parameters -> bool;

value get_req_bar1       : Options_utils.option_parameters -> float;
value get_req_bar2       : Options_utils.option_parameters -> float;
value get_req_cp         : Options_utils.option_parameters -> Argument.call_put;
value get_req_strike     : Options_utils.option_parameters -> float;

(* Access to the PDE Specs: *)

value get_dom_rate_2d    : Pde_types.Pde2d.diffusion ->
                         ( Pde_types.yft -> float );

value get_for_rate_2d    : Pde_types.Pde2d.diffusion ->
                         ( Pde_types.yft -> float );

value get_axis           : Pde_types.spatial_axis    ->   array float;
value get_axis_uni       : Pde_types.spatial_axis    -> ( array float * float );

(*--------------------------*)
(* Grids generation:        *)
(*--------------------------*)
(* Temporal Axis:           *)
value mk_time_axis:
  ~start:    Pde_types.jdt ->
  ~expir:    Pde_types.jdt ->
  ~time_step:float         ->
  array Pde_types.yft;

(* Uniform Spatial Axes:    *)
value mk_uniform_axis: float -> float -> int -> Pde_types.spatial_axis;

