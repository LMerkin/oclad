(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*         Diffusion and Numerical Method Types for the PDE Solver:          *)
(*===========================================================================*)
(*-----------------*)
(* Calendars:      *)
(*-----------------*)
type  jdt  = float;

value jdt_to_float: jdt    -> float = fun x -> x;

value jdt_of_float: float  -> jdt   =
fun x ->
  if ( classify_float x = FP_nan ) || ( x >= 0.0 && x < 109269.0 )
  then x
  else invalid_arg
       ( Printf.sprintf "ERROR: jdt_of_float: arg=%f is out of range" x );

type  yft  = float;

value yft_to_float: yft    -> float = fun x -> x;

value yft_of_float: float  -> yft   =
fun x ->
  (* XXX: We currently assume that the Year Fractions cannot be negative, and
     cannot exceed some maximum value constrained by the length of the epoch.
     NB: The arg can also be a "nan"! 
  *)
  if   ( classify_float x = FP_nan ) || ( x >= 0.0 && x < 200.0 )
  then x
  else invalid_arg
       ( Printf.sprintf "ERROR: yft_of_float: arg=%f is out of range" x );

(* "fxvols_yft":   *)

value fxvols_yft: jdt -> jdt -> yft =
fun jdt1 jdt2 ->
  yft_of_float( ( ( jdt_to_float jdt2 ) -. ( jdt_to_float jdt1 ) ) /. 365.0 );

(*-----------------*)
(* Rates:          *)
(*-----------------*)
type rate_info =
[
  ConstRate of float
| DF_Table  of array jdt and array float
];

(*-----------------*)
(* Spatial Axes:   *)
(*-----------------*)
type spatial_axis  =
[
  Uniform_Axis    of array float and float
| NonUniform_Axis of array float
];

(*===========================================================================*)
(* PDE Solution Spec: To be passed to the Backward Induction Engine:         *)
(*===========================================================================*)
(*------------------------------------------*)
(* PDE Spec for 2D Diffusions, e.g. SLV:    *)
(*------------------------------------------*)
module Pde2d =
struct
  type slv =
  {
    slv_r_dom:      yft   -> float;
    slv_r_for:      yft   -> float;
    slv_local_vol:  float -> yft -> float;
    slv_alpha:      yft   -> float;
    slv_corr:       yft   -> float
  };

  type heston =
  {
    heston_r_dom:   yft   -> float;
    heston_r_for:   yft   -> float;
    heston_kappa:   yft   -> float;
    heston_theta:   yft   -> float;
    heston_volvol:  yft   -> float;
    heston_corr :   yft   -> float
  };

  type diffusion =
  [
    SLV    of slv
  | Heston of heston
  ];

  (*-----------------*)
  (* Boundary Conds: *)
  (*-----------------*)
  type boundary_cond =
  [
    Free                            
  | Dirichlet of float -> yft -> float
  | Neumann   of float -> yft -> float
  ];

  (*-----------------*)
  (* BI Method:      *)
  (*-----------------*)
  type bi_method =
  [
    Yanenko3
  | Yanenko3_ExpFit
  | Yanenko5
  | Yanenko5_ExpFit
  ];

  (*-----------------*)
  (* PDE Spec:       *)
  (*-----------------*)
  type bi_engine = pde_spec -> unit
  and  pde_spec  =
  {
    diffusion   : diffusion;
    bi_engine   : bi_engine;

    spot0       : float;
    vol0        : float;
    time_k      : int;
    ts          : array yft;

    spot_n      : int;
    spots       : spatial_axis;
    vol_m       : int;
    vols        : spatial_axis;

    df_dom      : yft -> float;
    df_for      : yft -> float;

    spot_low_bc : boundary_cond;
    spot_up_bc  : boundary_cond;
    vol_low_bc  : boundary_cond;
    vol_up_bc   : boundary_cond;

    ys          : array ( array float );

    threads     : int;
    debug_dir   : string 
  };
end;

(*---------------------------------*)
(* Top-Level Diffusion&PDE Types:  *)
(*---------------------------------*)
type fx_diffusion_type =
[
  T_SLV_2D
| T_Heston_2D
];

type pde_spec =
[
  PDE_2D of Pde2d.pde_spec
];

(*===========================================================================*)
(* PDE Pricer: On evaluation, returns the Price by PDE solving:              *)
(*===========================================================================*)
type pde_pricer = unit -> float;

