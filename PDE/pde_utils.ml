(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*          PDE Utils: Misc Functions for PDE Generation and Solving         *)
(*===========================================================================*)
module T  = Pde_types;
module T2 = Pde_types.Pde2d;

(*------------------*)
(* "bin_search":    *)
(*------------------*)
(* Assumes that "args" are monotonically increasing. Returns the index "ia" st
   args.( ia ) <= x < args.( ia+1 )
   or raises "Failure" if "x" cannot be bracketed by the "args":
*)
value bin_search: array float -> float -> int =
fun args x ->
  let n = Array.length args in
  if  n < 2 || x < args.( 0 ) || x >= args.( n-1 )
  then
    invalid_arg( Printf.sprintf
                 "ERROR: bin_search: x=%f out of range [ %f, %f ]"
                 x  args.( 0 ) args.( n-1 ) )
  else
  let rec bin_search_rec: int -> int -> int =
  fun  ia ib ->
    let a = args.( ia ) in
    let b = args.( ib ) in
    do{
      assert ( ia < ib && ia >= 0 && ib < n && x >= a && x < b );
      if ib = ia + 1
      then ia (* "x" has been bracketed *)
      else
      let  ic = ( ia + ib ) / 2    in
      let  c  = args.( ic ) in
      do{
        assert( ic > ia && ic < ib );
        if x < c
        then bin_search_rec ia ic
        else bin_search_rec ic ib
      }
    }
  in
  bin_search_rec 0 ( n-1 );

(*------------------*)
(* "rel_search":    *)
(*------------------*)
(* Find the new bracket for "x" which has moved not much from its old value
   ( "x0" ); "i0" is the old left bracket index.   Returns same "ia" as for
   "bin_search":
*)
value rel_search: array float -> float -> int -> float -> int =
fun  args x0 i0 x ->
  let n = Array.length args   in
  let rec left_search: int -> int =
    fun  i ->
      if i < 0
      then failwith "ERROR: rel_search: arg beyond the left boundary"
      else
      if x >= args.( i )
      then i  (* Found the new left boundary *)
      else left_search ( i-1 )
  in
  let rec right_search: int -> int =
    fun  i ->
      if i >= ( n-1 )
      then failwith "ERROR: rel_search: arg beyond the right boundary"
      else
      if x < args.( i+1 )
      then i  (* Found the new left boundary *)
      else right_search ( i+1 )
  in
  if x = x0
  then   i0   (* Same bracket *)
  else
    if x < x0
    then   left_search  i0
    else   right_search i0;

(*===========================================================================*)
(* 1-Arg Funcs:                                                              *)
(*===========================================================================*)
(* E.g. interest rates: 1-arg functions of time.  Currently implemented as an
   array of data with interpolation between the nodes. Since  the data are ty-
   pically retrieved in the sequential order, the last arg is memoised. Hence
   the following "fun1_table" type:
*)
type interp_type = [ Step | Linear | Exp ];

type fun1_table  =
{
  (* The following arrays must be of the same length ( "n1", >= 1 ):      *)
  n1           : int;
  args1        : array float;
  vals1        : array float;
  slopes1      : array float;      (* Shorter by 1 than "args" and "vals" *)
  interp1      : interp_type;
  last_x1      : mutable float;    (* Last "x"                            *)
  last_ia1     : mutable int       (* Left bracket index for the last "x" *)
};

(*------------------*)
(* "mk_fun1_table": *)
(*------------------*)
(* Safe way of creating the 1D data table. For the sake of generality, "args"
   can be an arary of any types convertible to the "fun1" arg type, and same
   for "vals":
*)
value mk_fun1_table: array float -> array float -> interp_type -> fun1_table =

fun args vals interp_type ->
  let na = Array.length args in
  let nv = Array.length vals in
  if  na <> nv || na < 1
  then
    invalid_arg "ERROR: mk_fun1_table: Invalid array size(s)"
  else
  do{
    (* Verify that "args" are monotonically increasing: *)
    for i = 1 to ( na-1 )
    do{
      if args.( i-1 ) >= args.( i )
      then invalid_arg "ERROR: mk_fun1_table: Non-increasing args"
      else ();
    };
    (* For the Exp-type interpolation, we currently require that all vals are
       of the same sign:
    *)
    if interp_type = Exp
    then
      for i = 1 to ( na-1 )
      do{
        if vals.( i ) *. vals.( 0 ) <= 0.0
        then invalid_arg
             "ERROR: mk_fun1_table: Exp interp requires same-sign vals"
        else ()
      }
    else ();

    (* Create the "slopes"; if interp type is Exp, it's log slopes: *)
    let slopes =
      if interp_type <> Exp
      then
        Array.init ( na-1 ) ( fun i -> ( vals.( i+1 ) -. vals.( i ) ) /.
                                       ( args.( i+1 ) -. args.( i ) ) )
      else
        Array.init ( na-1 ) ( fun i -> ( log  ( vals.( i+1 ) /. vals.( i ) ) )
                                       /.     ( args.( i+1 ) -. args.( i ) ) )
    in
    (* The resulting object: *)
    {
      n1        = na;
      args1     = args;
      vals1     = vals;
      slopes1   = slopes;
      interp1   = interp_type;
      last_ia1  = -1;                     (* No evaluations yet... *)
      last_x1   = nan
    }
  };

(*------------------*)
(* "fun1":          *)
(*------------------*)
(* The 1-arg function with vals interpolation and arg memoisation. The actual
   functions will be produced by partial application of "fun1":
*)
value fun1: fun1_table -> float -> float =
fun   tab   x ->
  let n = tab.n1 in
  if  n = 1
  then
    (* The function is a const, so the arg is irrelevant: *)
    tab.vals1.( 0 )
  else
    (* Generic case:  n >= 2: *)
    let a = tab.args1.( 0 )   in
    let b = tab.args1.( n-1 ) in
    if  x < a || x > b
    then
      invalid_arg( Printf.sprintf "ERROR: fun1: Arg=%f out of range=[ %f, %f ]"
                   x a b )           (* No extrapolation! *)
    else
    if  x = b
    then do{
      (* Special case: "x" at the right boundary, so it cannot be found by
         "bin_search"; treat it explicitly. Memoise the node which is 1 step
         to teh left, but the actual "x":
      *)
      tab.last_ia1 := n-2;
      tab.last_x1  := x;
      tab.vals1.( n-1 )
    }
    else
      (* "Generic generic case": x in [ a; b ). Find the localisation bracket
          for "x" such that      args.( ia ) <= x < args.( ia+1 ):
      *)
      let ia =
        if tab.last_ia1 < 0
        then
          (* First call -- need to bracket "x", use binary search:     *)
          bin_search tab.args1 x
        else
          (* There was a previous call, need to adjust the bracket:    *)
          rel_search tab.args1 tab.last_x1 tab.last_ia1 x
      in
      do{
        (* Memoise the last point:            *)
        assert( 0 <= ia && ia < n-1 );
        tab.last_ia1   := ia;
        tab.last_x1    := x;

        (* If "x" corresponds to the "ia" node, no interp is required: *)
        let xa = tab.args1.( ia ) in
        let fa = tab.vals1.( ia ) in
        if  x  = xa
        then fa
        else
          (* Interpolate the func according to the mode configured:    *)
          match tab.interp1 with
          [
            Step   -> fa
          | Linear -> fa  +.      tab.slopes1.( ia ) *. ( x -. xa )
          | Exp    -> fa  *. exp( tab.slopes1.( ia ) *. ( x -. xa ) )
          ]
        };

(*===========================================================================*)
(* Retrieving Option Params:                                                 *)
(*===========================================================================*)
module A  = Argument;
module OU = Options_utils;

(*------------------------*)
(* "get_req_expiry_date": *)
(*------------------------*)
value get_req_expiry_date: OU.option_parameters -> T.jdt =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_ExpiryDate   in
    match arg with
    [
      A.Required ( A.ExpiryDate ( A.DateValue expir ) ) ->
        T.jdt_of_float expir

    | A.Required ( A.ExpiryDate ( A.DateString _ ) )    ->
        failwith "ERROR: get_req_expiry_date: str dates not supported yet"

    | _ -> invalid_arg( "ERROR: get_expiry_date: Not an ExpiryDate: "^
                    ( A.string_of_argument arg ) )
    ]
  with[ Not_found -> failwith "ERROR: get_req_expiry_date: Missing" ];


(*------------------------*)
(* "get_diffusion_type":  *)
(*------------------------*)
value get_diffusion_type: OU.option_parameters -> T.fx_diffusion_type =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_DiffusionType  in
    match arg with
    [
      A.Optional ( A.DiffusionType diff_type ) ->
        match String.lowercase diff_type with
        [
          "slv"    -> T.T_SLV_2D
        | "heston" -> T.T_Heston_2D
        | hmm      -> failwith
                      ( "ERROR: get_diffusion_type: Unrecognised: "^hmm )
        ]

      | _ -> invalid_arg( "ERROR: get_diffusion_type: Not a DiffusionType: "^
                        ( A.string_of_argument arg ) )
    ]
  (* Default Diffusion: *)
  with[ Not_found -> T.T_SLV_2D ];


(*------------------------*)
(* "get_bi_method_2d":    *)
(*------------------------*)
value get_bi_method_2d: OU.option_parameters -> T2.bi_method =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_BIMethod in
    match arg with
    [
      A.Optional ( A.BIMethod mtd ) ->
        match String.lowercase mtd with
        [
          "yanenko3"        -> T2.Yanenko3
        | "yanenko3_expfit" -> T2.Yanenko3_ExpFit
        | "yanenko5"        -> T2.Yanenko5
        | "yanenko5_expfit" -> T2.Yanenko5_ExpFit

        | _ -> failwith ( "ERROR: get_bi_method_2d: Unrecognised method: "^mtd )
        ]

    | _   -> invalid_arg( "ERROR: get_bi_method_2d: Not a BIMethod: "^
                        ( A.string_of_argument arg ) )
    ]
  (* Default BI Method: *)
  with[ Not_found -> T2.Yanenko3 ];


(*------------------------*)
(* "get_uniform":         *)
(*------------------------*)
value get_uniform: OU.option_parameters -> bool =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_UseUniformAxes in
    match arg with
    [
      A.Optional ( A.UseUniformAxes b ) -> b

    | _ -> invalid_arg( "ERROR: get_bi_method_2d: Not a UseUniformAxes: "^
                      ( A.string_of_argument arg ) )
    ]
  (* Default: Uniform: *)
  with[ Not_found -> True ];


(*------------------------*)
(* "get_time_step":       *)
(*------------------------*)
value get_time_step: OU.option_parameters -> float =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_TimeStepDays in
    match arg with
    [
      A.Optional ( A.TimeStepDays tau_days ) ->
        if tau_days > 0.0
        then
          tau_days
        else
          failwith( Printf.sprintf
          "ERROR: get_tau_days: Invalid time step: %f, must be > 0" tau_days )

    | _ -> invalid_arg( "ERROR: get_tau_days: Not a TimeStepDats: "^
                      ( A.string_of_argument arg ) )
    ]
  (* Default Time Step: *)
  with[ Not_found -> 5.0 ];


(*------------------------*)
(* "get_spot_n":          *)
(*------------------------*)
value get_spot_n: OU.option_parameters -> int =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_SpotN in
    match arg with
    [
      A.Optional ( A.SpotN spot_n ) ->

        if   ( spot_n >= 1.0 ) && ( spot_n = floor spot_n )
        then
          int_of_float spot_n
        else
          failwith( Printf.sprintf "ERROR: get_spot_n: Invalid N: %f" spot_n )

    | _ -> invalid_arg( "ERROR: get_spot_n: Not a SpotN: "^
                      ( A.string_of_argument arg ) )
    ]
  (* Default SpotN: *)
  with[ Not_found -> 500 ];


(*------------------------*)
(* "get_vol_m":           *)
(*------------------------*)
value get_vol_m: OU.option_parameters -> int =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_VolM in
    match arg with
    [
      A.Optional ( A.VolM  vol_m ) ->

        if   ( vol_m >= 1.0 ) && ( vol_m = floor vol_m )
        then
          int_of_float vol_m
        else
          failwith( Printf.sprintf "ERROR: get_vol_m: Invalid M: %f" vol_m )

    | _ -> invalid_arg( "ERROR: get_vol_m: Not a VolM: "^
                      ( A.string_of_argument arg ) )
    ]
  (* Default Vol M: *)
  with[ Not_found -> 300 ];


(*------------------------*)
(* "get_vol_up":          *)
(*------------------------*)
value get_vol_up: OU.option_parameters -> float =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_VolUp in
    match arg with
    [
      A.Optional ( A.VolUp vol_up ) ->
        if   vol_up > 0.0
        then vol_up
        else failwith( Printf.sprintf
             "ERROR: get_vol_up: Invalid upper vol bound: %f,  must be > 0"
             vol_up )

    | _ -> invalid_arg( "ERROR: get_vol_up: Not a VolUp: "^
                      ( A.string_of_argument arg ) )
    ]
  (* Default Vol Upper Boundary: *)
  with[ Not_found -> 5.0 ];


(*------------------------*)
(* "get_threads_no":      *)
(*------------------------*)
value get_threads_no: OU.option_parameters -> int =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_ThreadsNo in
    match arg with
    [
      A.Optional ( A.ThreadsNo thrds ) ->
        if ( thrds >= 1.0 ) && ( thrds = floor thrds )
        then
          int_of_float thrds
        else
          failwith( Printf.sprintf
                    "ERROR: get_threads_no: Invalid threads no.: %f" thrds )

    |  _ -> invalid_arg( "ERROR: get_threads_no: Not a ThreadsNo: "^
                       ( A.string_of_argument arg ) )
    ]
  (* Default Threads No:  *)
  with[ Not_found -> 1 ];


(*------------------------*)
(* "get_req_bar1":        *)
(*------------------------*)
value get_req_bar1: OU.option_parameters -> float =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_Bar1 in
    match arg with
    [
      A.Required ( A.Bar1 bar1 ) ->
        (* NB: Unlike the C++ part, negative "bar1" is NOT allowed here: *)
        if   bar1 >= 0.0
        then bar1
        else failwith( Printf.sprintf
                     "ERROR: get_req_bar1: Invalid bar1: %f, must be >=0" bar1 )

    | _ -> invalid_arg( "ERROR: get_req_bar1: Not a Bar1: "^
                      ( A.string_of_argument arg ) )
    ]
  with[ Not_found -> failwith "ERROR: get_req_bar1: Missing" ];


(*------------------------*)
(* "get_req_bar2":        *)
(*------------------------*)
value get_req_bar2: OU.option_parameters -> float =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_Bar2 in
    match arg with
    [
      A.Required ( A.Bar2 bar2 ) ->
        if   bar2 > 0.0
        then bar2
        else failwith( Printf.sprintf
                     "ERROR: get_req_bar2: Invalid bar2: %f, must be > 0" bar2 )

    | _ -> invalid_arg( "ERROR: get_req_bar2: Not a Bar2: "^
                      ( A.string_of_argument arg ) )
    ]
  with[ Not_found -> failwith "ERROR: get_req_bar2: Missing" ];


(*------------------------*)
(* "get_req_cp":          *)
(*------------------------*)
value get_req_cp: OU.option_parameters -> Argument.call_put =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_CP in
    match arg with
    [
      A.Required( A.CP cp ) -> cp

    | _ -> invalid_arg( "ERROR: get_req_cp: Not a CP: "^
                      ( A.string_of_argument arg ) )
    ]
  with[ Not_found -> failwith "ERROR: get_req_cp: Missing" ];


(*------------------------*)
(* "get_req_strike":      *)
(*------------------------*)
value get_req_strike: OU.option_parameters -> float =
fun o_params ->
  try
    let   arg = Hashtbl.find o_params.OU.parameters A.T_Strike in
    match arg with
    [
      A.Required( A.Strike strike ) -> strike

    | _ -> invalid_arg( "ERROR: get_req_strike: Not a Strike: "^
                      ( A.string_of_argument arg ) )
    ]
  with[ Not_found -> failwith "ERROR: get_req_strike: Missing" ];


(*------------------------*)
(* "get_axis":            *)
(*------------------------*)
value get_axis: T.spatial_axis -> array float =
fun axis ->
  match axis with
  [
    T.Uniform_Axis    points _ -> points
  | T.NonUniform_Axis points   -> points
  ];

(*------------------------*)
(* "get_axis_uni":        *)
(*------------------------*)
(* As "get_axis" above, but also returns the uniform step: *)

value get_axis_uni: T.spatial_axis -> ( array float * float ) =
fun axis ->
  match axis with
  [
    T.Uniform_Axis  points step -> ( points, step )

  | _ -> invalid_arg( "ERROR: get_axis_uni: Not a UniformAxis" )
  ];

(*------------------------*)
(* "get_*_rate_2d":       *)
(*------------------------*)
(* Dom and For Rates must be present in every diffusion:   *)

value get_dom_rate_2d: T2.diffusion -> ( T.yft -> float ) =
fun diffusion ->
  match diffusion with
  [
    T2.SLV    slv    -> slv.T2.slv_r_dom
  | T2.Heston heston -> heston.T2.heston_r_dom
  ];

value get_for_rate_2d: T2.diffusion -> ( T.yft -> float ) =
fun diffusion ->
  match diffusion with
  [
    T2.SLV    slv    -> slv.T2.slv_r_for
  | T2.Heston heston -> heston.T2.heston_r_for
  ];


(*===========================================================================*)
(* Grids Generation:                                                         *)
(*===========================================================================*)
(*---------------------------------*)
(* Temporal Grid:                  *)
(*---------------------------------*)
(* "mk_time_axis":
   Construct the temporal grid ( axis ) with the specified step. The step is
   kept constant from right to left, except possibly for the left-most interval:
*)
value mk_time_axis:
  ~start: T.jdt -> ~expir: T.jdt -> ~time_step: float -> array T.yft = 

fun ~{ start } ~{ expir } ~{ time_step }  ->
  (* Number of intervals: *)
  let tau = abs_float time_step           in
  if  tau <= 0.0 || expir < start
  then
    invalid_arg "ERROR: mk_time_axis"
  else
  let t0 = T.jdt_to_float start           in
  let tt = T.jdt_to_float expir           in
  let kf = ceil ( ( tt -. t0 ) /. tau )   in
  let k  = int_of_float kf                in

  (* The time axis. The standard FX Vols calendar is used to compute the Year
     Fractions:
  *)
  let ts  = Array.make ( k+1 ) ( T.yft_of_float nan )  in
  do{
    for i = 0 to k
    do{
      (* All steps are equal except possibly the last one ( before "tt" ): *)
      let t: T.jdt =
        if i < k
        then T.jdt_of_float ( t0 +. ( float_of_int i ) *. tau )
        else expir
      in
      ts.( i ) := T.fxvols_yft start t
    };
    (* The resulting time axis:    *)
    ts
  };

(*---------------------------------*)
(* Spatial Grid:                   *)
(*---------------------------------*)
(* "mk_uniform_axis":              *)

value mk_uniform_axis: float -> float -> int -> T.spatial_axis =

fun x0 x1 n ->
  if x0 >= x1 || n <= 0
  then invalid_arg "ERROR: mk_unform_axis" 
  else
  let h = ( x1 -. x0 ) /. ( float_of_int n ) in

  T.Uniform_Axis
    ( Array.init ( n+1 )( fun j -> x0 +. h *. ( float_of_int j ) ) ) h;

