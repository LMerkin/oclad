(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*          BI Utils, Mostly Independent of the Actual BI Method             *)
(*===========================================================================*)
module T  = Pde_types;
module T2 = Pde_types.Pde2d;
module U  = Pde_utils;
module A  = Argument;

(*===========================================================================*)
(*                         Generic Pricer Generator:                         *)
(*===========================================================================*)
(*-------------------------*)
(* "interpolate_price_2d": *)
(*-------------------------*)
(* Find the initial condition on the grid and interpolate the price ( if it is
   not exactly in a grid node ). Currently, linear interpolation is used:
*)
value interpolate_price_2d: T2.pde_spec -> float =
fun pde_spec ->
  let n      = pde_spec.T2.spot_n             in
  let spots  = U.get_axis pde_spec.T2.spots   in
  let spot0  = pde_spec.T2.spot0              in

  let m      = pde_spec.T2.vol_m              in
  let vols   = U.get_axis pde_spec.T2.vols    in
  let vol0   = pde_spec.T2.vol0               in

  (* Localise the inituial condition on the Grid: *)
  let i0     =
    if spot0 = spots.( n )
    then n
    else
      try U.bin_search spots spot0
      with
      [ _ ->
        failwith ( Printf.sprintf
          "ERROR: interpolate_price_2d: Spot0=%f is outside Grid=[ %f .. %f ]"
          spot0 spots.( 0 ) spots.( n ) )
      ]
  in
  let j0      =
    if vol0   = vols.( m )
    then m
    else
      try U.bin_search vols  vol0
      with
      [ _ ->
        failwith ( Printf.sprintf
          "ERROR: interpolate_price_2d: Vol0 =%f is outside Grid=[ %f .. %f ]"
          vol0 vols.( 0 ) vols.( m ) )
      ]
  in
  (* Do linear interpolation: *)

  let ys    = pde_spec.T2.ys in
  let y0    = ys.( i0 ).( j0 )            in

  let ( dspot,  dy_spot ) =
    if i0 = n
    then ( 1.0,     0.0 )
    else ( spots.( i0+1 ) -. spots.( i0 ),  ys.( i0+1 ).( j0 ) -. y0 )
  in
  let ( dvol,   dy_vol  ) =
    if j0 = m
    then ( 1.0,     0.0 )
    else ( vols.(  j0+1 ) -. vols.( j0 ),   ys.( i0 ).( j0+1 ) -. y0 )
  in
  y0 +. ( spot0 -. spots.( i0 ) ) /. dspot *. dy_spot +.
        ( vol0  -. vols.(  j0 ) ) /. dvol  *. dy_vol ;


(*-------------------------*)
(* "mk_pricer2d_gen":      *)
(*-------------------------*)
(* Constructs the generic 2D pricer which obtains the price in one BI run; not
   suitable for e.g. knock-in options:
*)
type pricer_memo =
{
  price: mutable option float
};

value mk_pricer2d_gen: T2.pde_spec -> T.pde_pricer =
fun pde_spec ->
  (* A pricing engine with single backward induction run, interpolation and
     price memoisation:
  *)
  let pmemo = { price = None } in

  (* The constructed Pricer to return: *)
  fun () ->
    match pmemo.price with
    [
      None ->
      do{
        (* Run the BI Engine -- NB: it is obtained from the "pde_spec" and
           runs on the same spec, like an OO method:
        *)
        pde_spec.T2.bi_engine pde_spec;

        (* Localise the initial condition on the grid and interpolate the
           price:
        *)
        let price = interpolate_price_2d pde_spec in
        do{
          pmemo.price := Some price;
          price
        }
      }
    | Some price -> price (* Return memoised price *)
    ];

(*===========================================================================*)
(* Pay-Off and BoundConds Utils:                                             *)
(*===========================================================================*)
(*-------------------------*)
(* "mk_vanilla_payoff":    *)
(*-------------------------*)
(* Vanilla European Call / Put Pay-Off: *)

value mk_std_payoff: A.call_put -> float -> ( float -> float ) =
fun  cp strike ->
  if cp = A.Call
  then fun s -> max ( s -. strike ) 0.0
  else fun s -> max ( strike -. s ) 0.0;

(*=========================*)
(* "set_payoff*":          *)
(*=========================*)
(* These call-backs are invoked by option type-specific PDE generators. Before
   starting the Backward Induction engine, they initialise the solution space
   using the given pay-off function.
*)
(*-------------------------*)
(* "set_payoff_2d":        *)
(*-------------------------*)
(* Pay-off depends on the Spot only, the PDE is 2D -- most common case:      *)

value set_payoff_2d: T2.pde_spec -> ( float -> float ) -> T2.pde_spec =
fun   pde_spec0 payoff     ->
  (*-------------------------------------------*)
  (* Grid Geometry:                            *)
  (*-------------------------------------------*)
  let n     = pde_spec0.T2.spot_n             in
  let m     = pde_spec0.T2.vol_m              in
  let k     = pde_spec0.T2.time_k             in

  let spots = U.get_axis  pde_spec0.T2.spots  in
  let vols  = U.get_axis  pde_spec0.T2.vols   in
  let ts    = pde_spec0.T2.ts                 in

  (*-------------------------------------------*)
  (* Allocate and initialise the 2D grid:      *)
  (*-------------------------------------------*)
  let pde_spec1 =
  {
    ( pde_spec0 ) with

      T2.ys = Array.init ( n+1 )
              ( fun i -> Array.make ( m+1 ) ( payoff spots.( i ) ) )
  }
  in
  (*-------------------------------------------*)
  (* Boundary conds may override the "payoff": *)
  (*-------------------------------------------*)
  do{
    assert( Array.length spots = n+1 && Array.length vols = m+1 &&
            Array.length ts    = k+1 );

    (*-----------*)
    (* Spot LOW: *)
    (*-----------*)
    match pde_spec1.T2.spot_low_bc with
    [
      T2.Dirichlet f ->
        for j = 0 to m
        do{
          let y = f vols.( j ) ts.( k ) in
          pde_spec1.T2.ys.( 0 ).( j ) := y
        }

    | T2.Neumann   f ->
        (* The following works for a non-uniform grid as well: *)
        for j = 0 to m
        do{
          let c  = f  vols.( j ) ts.( k )              in
          let d0 = ( spots.( 1 ) -. spots.( 0 ) ) *. c in

          pde_spec1.T2.ys.( 0 ).( j ) :=
          pde_spec1.T2.ys.( 1 ).( j ) -. d0
        }

    | T2.Free -> ()
    ];
    (*-----------*)
    (* Spot UP:  *)
    (*-----------*)
    match pde_spec1.T2.spot_up_bc with
    [
      T2.Dirichlet f ->
        for j = 0 to m
        do{
          let y = f  vols.( j ) ts.( k ) in
          pde_spec1.T2.ys.( n ).( j ) := y
        }

    | T2.Neumann   f ->
        (* The following works for a non-uniform grid as well: *)
        for j = 0 to m
        do{
          let c  = f  vols.( j ) ts.( k )                in
          let dn = ( spots.( n ) -. spots.( n-1 ) ) *. c in

          pde_spec1.T2.ys.( n ).(   j ) :=
          pde_spec1.T2.ys.( n-1 ).( j ) +. dn
        }

    | T2.Free -> ()
    ];
    (*-----------*)
    (* Vol LOW:  *)
    (*-----------*)
    match pde_spec1.T2.vol_low_bc with
    [
      T2.Dirichlet f  ->
        for i = 0 to n
        do{
          let y = f spots.( i ) ts.( k ) in
          pde_spec1.T2.ys.( i ).( 0 ) := y
        }

    | T2.Neumann   f ->
        (* The following works for a non-uniform grid as well: *)
        for i = 0 to n
        do{
          let c  = f spots.( i ) ts.( k )             in
          let d0 = (  vols.( 1 ) -. vols.( 0 ) ) *. c in

          pde_spec1.T2.ys.( i ).( 0 ) :=
          pde_spec1.T2.ys.( i ).( 1 ) -. d0
        }

    | T2.Free -> ()
    ];
    (*-----------*)
    (* Vol UP:   *)
    (*-----------*)
    match pde_spec1.T2.vol_up_bc with
    [
      T2.Dirichlet f  ->
        for i = 0 to n
        do{
          let y = f spots.( i ) ts.( k ) in
          pde_spec1.T2.ys.( i ).( m ) := y
        }

    | T2.Neumann   f ->
        (* The following works for a non-uniform grid as well: *)
        for i = 0 to n
        do{
          let c  = f spots.( i ) ts.( k )               in
          let dm = (  vols.( m ) -. vols.( m-1 ) ) *. c in

          pde_spec1.T2.ys.( i ).( m ) :=
          pde_spec1.T2.ys.( i ).( m-1 ) +. dm
        }

    | T2.Free -> ()
    ];
    (*-------------------------------------------*)
    (* The result:                               *)
    (*-------------------------------------------*)
    pde_spec1
  };

(*-------------------------*)
(* "mk_vol_low_bc_2d":     *)
(*-------------------------*)
(* The BC at Low StocVol in 2D diffusions is obtained by solving a degenerate
   ( hyperbolic ) PDE with the terminal condition given by the option pay-off:
*)
value mk_vol_low_bc_2d:
      T2.pde_spec   -> ( float -> float ) -> T2.boundary_cond =

fun pde_spec payoff ->
  let ts       = pde_spec.T2.ts              in
  let dfd      = pde_spec.T2.df_dom          in
  let dff      = pde_spec.T2.df_for          in
  let tt       = ts.( Array.length ts - 1 )  in
  let dfd_tt   = dfd tt                      in
  let dff_tt   = dff tt                      in
  let ratio_tt = dff_tt /. dfd_tt            in

  T2.Dirichlet
  (
    fun s t ->
      let dfd_t = dfd t in
      let dff_t = dff t in
       payoff( s *. ratio_tt *. dfd_t /. dff_t ) *. dfd_tt /. dfd_t
  );

(*===========================================================================*)
(* Low-Level BI Support:                                                     *)
(*===========================================================================*)
(*-------------------------*)
(* "apply_bcs_2d_3diag":   *)
(*-------------------------*)
(* BCs are actually diffusion-specific, but they are already compiled into the
   corresp Lambdas, so this function is generic:
*)
value apply_bcs_2d_3diag:
  ( T2.boundary_cond * T2.boundary_cond * float * int * float * T.yft ) ->
  array float -> array float -> array float -> array float    -> unit   =

fun ( low_bc, up_bc, other_coord, nm, this_h, next_t ) d1 d2 d3 rhs     ->
do{
  (*-----------------*)
  (* Lower Boundary: *)
  (*-----------------*)
  match low_bc with
  [
    T2.Dirichlet f ->
    (* Const solution at the boundary, evaluated at "t":       *)
    do{
      d2.(  0 ) := 1.0;
      d3.(  0 ) := 0.0;
      rhs.( 0 ) := f other_coord next_t
      }
  | T2.Neumann   f ->
    (* One-sided derivative at the boundary, evaluated at "t": *)
    do{
      d2.(  0 ) := -1.0;
      d3.(  0 ) :=  1.0;
      rhs.( 0 ) := ( f other_coord next_t ) *. this_h
  }
  | T2.Free ->
      failwith "ERROR: apply_bcs_2d: Free BoundCond not allowed"
  ];
  (*-----------------*)
  (* Upper Boundary: *)
  (*-----------------*)
  match up_bc with
  [
    T2.Dirichlet f ->
    (* Const solution at the boundary, evaluated at "t":       *)
    do{
      d1.(  nm-1 ) := 0.0;
      d2.(  nm   ) := 1.0;
      rhs.( nm   ) := f other_coord next_t
    }
  | T2.Neumann   f ->
    (* One-sided derivative at the boundary, evaluated at "t": *)
    do{
      d1.(  nm-1 ) := -1.0;
      d2.(  nm   ) :=  1.0;
      rhs.( nm   ) := ( f other_coord next_t ) *. this_h
    }
  | T2.Free ->
      failwith "ERROR: apply_bcs_2d: Free BoundCond not allowed"
  ]
};

(*-------------------------*)
(* "tri_diag_solver":      *)
(*-------------------------*)
(* NB: This procedure is destructive to the original data.
   "d1" -- lower diag, "d2" -- main diag, "d3" -- upper diag, "rhs" is the RHS:
*)
value tri_diag_solver:
  array float -> array float -> array float -> array float -> unit =

fun d1 d2 d3 rhs ->

  let n = Array.length d2  in
  if  n < 1 || ( Array.length d1  <> n-1 ) || ( Array.length d3 <> n-1 ) ||
               ( Array.length rhs <> n   )
  then
    failwith "ERROR: tri_diag_solver: Invalid diag(s)"
  else
  do{
    (* Eliminate the lower diag: *)
    for i = 1 to n-1
    do{
      let c =
        if   d2.( i-1 ) = 0.0
        then failwith "ERROR: tri_diag_solver: main diag element = 0 (1)"
        else d1.( i-1 ) /. d2.( i-1 )
      in
      do{
        (* d1.( i-1 )  becomes 0.0 *)
        d2.(  i   ) := d2.(  i ) -. c *. d3.(  i-1 );
        rhs.( i   ) := rhs.( i ) -. c *. rhs.( i-1 )
      }
    };
    (* Back-track; the result comes into the "rhs": *)
    rhs.( n-1 ) :=
      if d2.( n-1 ) = 0.0
      then failwith "ERROR: tri_diag_solver: main diag element = 0 (2)"
      else rhs.( n-1 ) /. d2.( n-1 );

    for j = 2 to n
    do{
      let i = n-j  in

      if d2.( i ) = 0.0
      then failwith "ERROR: tri_diag_solver: main diag element = 0 (3)"
      else rhs.( i ) := ( rhs.( i ) -. d3.( i ) *. rhs.( i+1 ) ) /. d2.( i )
    }
  };

(*-------------------------*)
(* "penta_diag_solver":    *)
(*-------------------------*)

(*-------------------------*)
(* "output_state_2d":      *)
(*-------------------------*)
(* Outputs the current solution space into a file in a GNUPlot-compatible
   format, if "debug_dir" is set:
*)
value output_state_2d:
  ?half_step:bool -> T2.pde_spec -> Pde_types.yft ->
  array ( array float  ) -> unit =

fun ?{ half_step=False } pde_spec t ys ->
  if pde_spec.T2.debug_dir <> ""
  then
    (* Output the current 2D state in the GNUPlot-compatible format. NB: as
       half-steps are possible, "ys" are passed in explicitly,  not via the
       "pde_spec":
    *)
    let spots = U.get_axis pde_spec.T2.spots                              in
    let vols  = U.get_axis pde_spec.T2.vols                               in
    let n     = pde_spec.T2.spot_n                                        in
    let m     = pde_spec.T2.vol_m                                         in

    (* Open the file in the current dir with the name given by the curr
       time stamp:
    *)
    let hmark = if half_step then "-H" else ""                            in
    let fname = Printf.sprintf "%s/PDE-%.5f%s.txt"
                pde_spec.T2.debug_dir ( T.yft_to_float t ) hmark          in
    let out   = open_out fname                                            in
    do{
      assert( Array.length spots = n+1 && Array.length vols = m+1 );

      (* The layout of "ys" depends on whether it is a half-step or not: *)
      for i = 0 to n
      do{
        for j = 0 to m
        do{
          let y = if half_step then ys.( j ).( i )   else ys.( i ).( j )  in
          Printf.fprintf out "%f\t%f\t%f\n" spots.( i ) vols.( j ) y
        };
        Printf.fprintf out "\n"
      };
      close_out out
    }
  else ();

(*===========================================================================*)
(* Multi-Threading Support:                                                  *)
(*===========================================================================*)
(*-------------------------*)
(* "run":                  *)
(*-------------------------*)
(* Run a given closure in a separate thread:    *)

value run: int -> ( unit -> unit ) -> unit =
fun  _ f   ->
  (* XXX: multi-threading is not supported yet: *)
  f ();

(*-------------------------*)
(* "run_sub_steps":        *)
(*-------------------------*)
(* See the description in the module interface: *)

value run_sub_steps:
  ~do_block:( int -> int -> int -> unit -> unit ) -> ~mn:int -> ~thrds:int ->
  unit =

fun ~{ do_block } ~{ mn } ~{ thrds } ->
  if thrds <= 0
  then
    invalid_arg "ERROR: run_sub_steps: Invalid Threads No., must be >= 1"
  else
  if thrds = 1
  then
    (* No multi-threading, run the steps sequentially: *)
    do_block 0 mn 0 ()
  else
    (* Multi-threading mode. Each thread processes a block of at most "bsz"
       sub-steps wrt the other co-ord:
    *)
    let bsz = int_of_float
              ( ceil ( ( float_of_int ( mn+1 ) ) /. ( float_of_int thrds ) ) )
    in
    do{
      assert ( bsz >= 1 );

      for p = 0 to thrds-1
      do{
        let ji_from = p * bsz                   in
        let ji_to   = min ( ( p+1 ) * bsz  ) mn in    (* Inclusive! *)
        do{
          assert ( 0 <= ji_from && ji_from <= ji_to && ji_to <= mn );

          (* Start the threads:  *)
          run thrds ( do_block ji_from ji_to p )
        }
      };
      (* Wait on the threads to terminate: *)
    };

