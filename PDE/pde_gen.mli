(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*     PDE Construction From Option Info: Generic Top-Level Mechanisms       *)
(*===========================================================================*)
(* Top-Level PDE Env Generator: *)

value mk_option_pricer:
  (* Market Data:  *)
  ~spot:      float               ->
  ~rd_info:   Pde_types.rate_info ->
  ~rf_info:   Pde_types.rate_info ->
  ~v0:        float               ->
  (* Model Params: *)
  ~alpha:     ( Pde_types.yft     -> float ) ->
  ~corr:      ( Pde_types.yft     -> float ) ->
  ?debug_dir: string              ->
  (* Option Spec:  *)
  Options.fx_option               ->
  (* Res: Pricer:  *)
  Pde_types.pde_pricer;

