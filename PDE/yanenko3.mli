(*===========================================================================*)
(*       N.N.Yanenko Backward Induction Engines, 3-Point Spatial Grids:      *)
(*===========================================================================*)
(* "bi_slv2d_uni":
   2D SLV Diffusion, Uniform Grid with 3-point spatial approximation:
*)
value bi_slv2d_uni:
  ~with_exp_fit:bool        ->
  Pde_types.Pde2d.pde_spec  ->
  unit;

