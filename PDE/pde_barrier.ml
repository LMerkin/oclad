(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*         Refinement of the PDE Spec According to the Option Type           *)
(*                  and Construction of the PDE Pricer:                      *)
(*                             Barrier Options                               *)
(*===========================================================================*)
module T  = Pde_types;
module T2 = Pde_types.Pde2d;
module U  = Pde_utils;
module BU = Pde_bi_utils;
module OU = Options_utils;

(*===========================================================================*)
(* DKO:                                                                      *)
(*===========================================================================*)
value mk_pde2d_dko: OU.option_parameters -> T2.pde_spec -> T.pde_pricer =

fun o_params pde_spec0  ->
  (*------------------------------*)
  (* Barriers:                    *)
  (*------------------------------*)
  (* NB: Negative "bar1" is not allowed, unlike in the C++ BarOpt definition: *)
  let bar1   = U.get_req_bar1 o_params in
  let bar2   = U.get_req_bar2 o_params in
  if  bar2  <= bar1
  then
    failwith ( Printf.sprintf "DKO: Invalid: bar1=%f, bar2=%f" bar1 bar2 )
  else
  (* If the initial spot is ouside the barriers, the price is immediately 0:  *)
  if  pde_spec0.T2.spot0 <= bar1 ||
      pde_spec0.T2.spot0 >= bar2
  then
    fun _ -> 0.0
  else
  (* Spot grid -- XXX: Uniform as yet: *)
  let spot_n = pde_spec0.T2.spot_n                        in

  let use_uniform_axes = U.get_uniform o_params           in
  let spots  =
    if use_uniform_axes
    then U.mk_uniform_axis bar1 bar2 spot_n
    else failwith "ERROR: mk_pde2d_dko: Non-uniform axes not implemented yet"
  in
  (*------------------------------*)
  (* Pay-Off and Spot BoundConds: *)
  (*------------------------------*)
  let cp     = U.get_req_cp      o_params                 in
  let strike = U.get_req_strike  o_params                 in
  let payoff = BU.mk_std_payoff cp strike                 in

  (* Boundary conds at Spot barriers: *)
  let spot_low_bc = T2.Dirichlet ( fun _ _ -> 0.0 )       in
  let spot_up_bc  = T2.Dirichlet ( fun _ _ -> 0.0 )       in

  (* The StocVol BCs:
     The LowBC is constructed as solution to the degenerate PDE;
     the Up BC is 0.0 since the price of a KO barrier option -> 0 as vol -> oo,
     ( as the probability of hitting the barrier -> 1 in any finite time ):
  *)
  let vol_low_bc  = BU.mk_vol_low_bc_2d pde_spec0 payoff  in
  let vol_up_bc   = T2.Dirichlet ( fun _ _ -> 0.0 )       in

  (*------------------------------*)
  (* The final PDE Spec:          *)
  (*------------------------------*)
  let pde_spec1 =
  {
    ( pde_spec0 )  with
    T2.spots       = spots;
    T2.spot_low_bc = spot_low_bc;
    T2.spot_up_bc  = spot_up_bc;
    T2.vol_low_bc  = vol_low_bc;
    T2.vol_up_bc   = vol_up_bc
  }
  in
  (* Set up the terminal pay-off -- depends on the Spot only:       *)
  let pde_spec2 = BU.set_payoff_2d pde_spec1 payoff in

  (*------------------------------*)
  (* The Pricer:                  *)
  (*------------------------------*)
  (* The pricer is generic -- just a single backward-induction run: *)
  BU.mk_pricer2d_gen pde_spec2;


(*===========================================================================*)
(* Barrier Option Top-Level:                                                 *)
(*===========================================================================*)
(* 2-level de-multiplexing: by Option Sub-Type and PDE Dim:                  *)

value mk_barrier_pricer:
  Barrier_option.fx_barrieroption ->
  OU.option_parameters            ->
  T.pde_spec                      ->
  T.pde_pricer                =

fun baropt o_params pde_spec0 ->
  (* Construct the PDE Spec for the given option sub-type: *)
  match baropt with
  [
    (*---------*)
    (* DKO:    *)
    (*---------*)
    Barrier_option.DKO _ _ ->
      match pde_spec0 with
      [
        T.PDE_2D pde_spec2d -> mk_pde2d_dko o_params pde_spec2d
      ]
    (*---------*)
    (* Others: *)
    (*---------*)
  | _ -> failwith ( "ERROR: mk_pde_barrier: Unsupported option sub-type: "^
                    Barrier_option.string_of_fx_barrieroption baropt )
  ];

