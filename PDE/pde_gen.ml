(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*     PDE Construction From Option Info: Generic Top-Level Mechanisms       *)
(*===========================================================================*)
module T  = Pde_types;
module T2 = Pde_types.Pde2d;
module U  = Pde_utils;

(*---------------------------------*)
(* "mk_rate_df":                   *)
(*---------------------------------*)
(* Creates ( ShortRate_Func, DF_Func ): *)

value mk_rate_df:
  T.rate_info ->
  T.jdt       ->
  T.jdt       ->
  ( ( T.yft -> float ) * ( T.yft -> float ) ) =

fun rinfo start expir ->
  match rinfo with
  [
    T.ConstRate r ->
      (* The rate is given explicitly, and is constant: *)
      let r_fun  = fun _ -> r                                     in
      let df_fun = fun t -> exp ( -. r *. ( T.yft_to_float t ) )  in
      ( r_fun, df_fun )

  | T.DF_Table dates dfs ->
      (* The DFs must all be given relative to the "start" date:  *)
      let n = Array.length dates in
      if  n < 2 || dates.( 0 ) <> start || dates.( n-1 ) < expir ||
          Array.length dfs     <> n     || dfs.( 0 )     <> 1.0
      then
        invalid_arg "ERROR: mk_rate_df: Invalid Dates / DFs"
      else
      (* Year Fractions and Rates: Ccy-specific day count conventions are dis-
         regarded since they are already incorporated into DFs;  step-wise in-
         terpolation used for Rates, exponential for DFs:
      *)
      let yffs0 =
          Array.init n
            ( fun i -> T.yft_to_float ( T.fxvols_yft start dates.( i ) ) )
      in
      let yffs1 = Array.sub yffs0 0 ( n-1 )                       in
      let rs    =
          Array.init ( n-1 )
              ( fun i -> ( log ( dfs.( i ) /.   dfs.( i+1 ) ) )  /.
                         (   yffs0.( i+1 ) -. yffs0.( i ) )
              )
      in
      (* NB: Th egeneric "fun1" is a "float -> float" function, but for rates,
         we need "T.yft -> float", hence the following complication:
      *)
      let rs_tab  = U.mk_fun1_table yffs1 rs  U.Step                in
      let dfs_tab = U.mk_fun1_table yffs0 dfs U.Exp               in

      let rs_fun  = fun t -> U.fun1 rs_tab  ( T.yft_to_float t )  in
      let dfs_fun = fun t -> U.fun1 dfs_tab ( T.yft_to_float t )  in

      ( rs_fun, dfs_fun )
  ];

(*===========================================================================*)
(*                             BI Engine Selector:                           *)
(*===========================================================================*)
(*---------------------------------*)
(* "select_bi_engine_2d":          *)
(*---------------------------------*)
(* De-multiplexr which selects the appropriate BI engine given a 2D Diffusion,
   the Grid Geometry ( XXX: currently only Uniform grids are supported ), and
   the configured BI Engine Type:
*)
value select_bi_engine_2d:
  T.fx_diffusion_type ->
  T2.bi_method        ->
  bool                ->
  T2.bi_engine        =

fun diff_type bi_method use_uniform          ->
  match ( diff_type, bi_method, use_uniform ) with
  [
    ( T.T_SLV_2D, T2.Yanenko3, True )        ->
      Yanenko3.bi_slv2d_uni ~{ with_exp_fit=False }

  | ( T.T_SLV_2D, T2.Yanenko3_ExpFit, True ) ->
      Yanenko3.bi_slv2d_uni ~{ with_exp_fit=True  }

  | _ ->
    failwith "ERROR: select_bi_engine_2d: Unsupported configuration"
  ];

(*===========================================================================*)
(* Top-Level PDE Spec Generator:                                             *)
(*===========================================================================*)
value mk_option_pricer:
  (* Market Data:  *)
  ~spot:      float              ->
  ~rd_info:   T.rate_info        ->
  ~rf_info:   T.rate_info        ->
  ~v0:        float              ->
  (* Model Params: *)
  ~alpha:     ( T.yft -> float ) ->
  ~corr:      ( T.yft -> float ) ->
  ?debug_dir: string             ->
  (* Option Spec:  *)
  Options.fx_option              ->
  (* Res: Pricer:  *)
  T.pde_pricer                   =

fun ~{ spot } ~{ rd_info } ~{ rf_info } ~{ v0 } ~{ alpha } ~{ corr }
    ?{ debug_dir = "" }    opt   ->

  (*------------------------------*)
  (* Market Data:                 *)
  (*------------------------------*)
  let o_params          = Options.option_params opt                         in

  (* Start and Expiry Dates:      *)
  let start_date        = T.jdt_of_float
                          ( float_of_int o_params.Options_utils.startdate ) in
  let expir_date        = U.get_req_expiry_date o_params                    in

  (* Rates and Discount Factors:  *)
  let ( r_dom, df_dom ) = mk_rate_df rd_info start_date expir_date          in
  let ( r_for, df_for ) = mk_rate_df rf_info start_date expir_date          in

  (*------------------------------*)
  (* Diffusion-Independent Parms: *)
  (*------------------------------*)
  (* Optional params and their default values.  The following params should be
     present for all Diffusion Types:
  *)
  (* Diffusion Type: *)
  let diff_type = U.get_diffusion_type o_params                             in

  (* Number of intervals in the Spot dim:   *)
  let spot_n    = U.get_spot_n o_params                                     in

  (* Time step, initially in days:          *)
  let tau_days  = U.get_time_step o_params                                  in

  (* Construct the Time Axis ( Time Grid ): *)
  let ts =
    U.mk_time_axis ~{ start=start_date }    ~{ expir=expir_date }
                   ~{ time_step=tau_days }  in

  (* Number of Threads for the BI Engine:   *)
  let threads   = U.get_threads_no o_params                                 in

  (*------------------------------*)
  (* Diffusion-Dependent Params   *)
  (* ( but Option Type-Indep. ):  *)
  (*------------------------------*)
  let pde_spec0  =
    match diff_type with
    [
      (*--------------------------*)
      (* 2D Diffusions:           *)
      (*--------------------------*)
      T.T_SLV_2D
    | T.T_Heston_2D ->
        let diffusion =
          match diff_type with
          [
            T.T_SLV_2D ->
              (*-------------------------*)
              (* The SLV 2D Diffusion:   *)
              (*-------------------------*)
              let slv =
              {
                T2.slv_r_dom      = r_dom;
                T2.slv_r_for      = r_for;
                T2.slv_local_vol  = fun _ _ -> v0;
                T2.slv_alpha      = alpha;
                T2.slv_corr       = corr
              }
            in
            T2.SLV slv

          | T.T_Heston_2D ->
              (*-------------------------*)
              (* Heston 2D diffusion:    *)
              (*-------------------------*)
              failwith
              "ERROR: mk_option_pricer: Unimplemented: Heston Diffusion"
          ]
        in
        (* Number of intervals in the StocVol dim: *)
        let vol_m  = U.get_vol_m o_params                                 in

        (* Upper boundary for the StocVol:         *)
        let vol_up = U.get_vol_up o_params                                in

        (* The Vol Axis is constructed here whereas the Spot one is option
           type-specific.  The axes can be uniform or non-uniform:
        *)
        let use_uniform_axes = U.get_uniform o_params                     in
        let vols   =
          if use_uniform_axes
          then U.mk_uniform_axis 0.0 vol_up vol_m
          else failwith
               "ERROR: mk_option_pricer: Non-uniform axes not implemented yet"
        in
        (* The Backward Induction Method for 2D:   *)
        let bi_method = U.get_bi_method_2d o_params                       in

        (* Select a 2D BI Engine:                  *)
        let bi_engine =
          select_bi_engine_2d diff_type bi_method use_uniform_axes        in

        (*-------------------------*)
        (* The PDE 2D BoilerPlate: *)
        (*-------------------------*)
        T.PDE_2D
        {
          T2.diffusion    = diffusion;
          T2.bi_engine    = bi_engine;
          T2.spot0        = spot;
          T2.vol0         = 1.0;          (* Unless overwritten...         *)

          (* Create the Temporal and Spatial Axes, and the 3-diag space:   *)
          T2.time_k       = Array.length ts - 1;
          T2.ts           = ts;

          (* The Spot grid is initialised later, vols -- now: *)
          T2.spot_n       = spot_n;
          T2.spots        = vols;         (* Later! *)
          T2.vol_m        = vol_m;
          T2.vols         = vols;

          (* Boundary Conds: *)
          T2.df_dom       = df_dom;
          T2.df_for       = df_for;

          T2.spot_low_bc  = T2.Free;      (* Later! *)
          T2.spot_up_bc   = T2.Free;      (* Later! *)
          T2.vol_low_bc   = T2.Free;      (* Later! *)
          T2.vol_up_bc    = T2.Free;      (* Later! *)

          (* Terminal payoff: Later! *)
          T2.ys           = [||];
          T2.threads      = threads;
          T2.debug_dir    = debug_dir
        }
    ]
  in
  (*------------------------------*)
  (* PDE Pricer by Option Type:   *)
  (*------------------------------*)
  match opt with
  [
    Options.BarrierOption baropt ->
      Pde_barrier.mk_barrier_pricer baropt o_params pde_spec0

  | Options.SimpleOption  simple ->
      Pde_simple.mk_simple_pricer   simple o_params pde_spec0

  | _ -> failwith "ERROR: mk_option_pde: Unimplemented option type"
  ];

