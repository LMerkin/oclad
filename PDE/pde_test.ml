(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                                 "pde_test":                               *)
(*===========================================================================*)
Printexc.record_backtrace True;

(*---------------------------------*)
(* Command-Line Params:            *)
(*---------------------------------*)
(* Default values: *)
value opt_type    = ref "OE";
value bar1        = ref 0.0;
value bar2        = ref 0.0;
value strike      = ref 1.0;
value spot0       = ref 1.0;
value start_date  = 0;
value cp          = ref 1;
value expiry_date = ref 365;
value r_dom       = ref 0.05;
value r_for       = ref 0.0;
value alpha       = ref 0.0;
value rho         = ref 0.0;
value local_vol   = ref 0.2;

value n           = ref 200;
value m           = ref 200;
value tau         = ref 2.0;
value vol_up      = ref 6.0;
value diffusion   = ref "SLV";
value bi_method   = ref "Yanenko3";
value debug_dir   = ref "";

value arg_specs   =
[
  ( "-type",        Arg.Set_string opt_type,    "Option Type ( OE|DKO|... )"  );
  ( "-bar1",        Arg.Set_float  bar1,        "Lower Barrier" );
  ( "-bar2",        Arg.Set_float  bar2,        "Upper Barrier" );
  ( "-strike",      Arg.Set_float  strike,      "Strike"        );
  ( "-spot",        Arg.Set_float  spot0,       "Spot at t=0"   );
  ( "-cp",          Arg.Set_int    cp,          "Call if > 0, Put otherwise"  );
  ( "-expiry-date", Arg.Set_int    expiry_date, "Expiry date in JDs from t=0" );
  ( "-r-dom",       Arg.Set_float  r_dom,       "Domestic interest rate"      );
  ( "-r-for",       Arg.Set_float  r_for,       "Foreign  interest rate"      );
  ( "-n",           Arg.Set_int    n,           "Number of Spot    intervals" );
  ( "-m",           Arg.Set_int    m,           "Number of StocVol intervals" );
  ( "-tau",         Arg.Set_float  tau,         "Time step in JDs"            );
  ( "-vol-up",      Arg.Set_float  vol_up,      "Upper StocVol boundary"      );
  ( "-diffusion",   Arg.Set_string diffusion,   "Diffusion type: SLV|Heston"  );
  ( "-alpha",       Arg.Set_float  alpha,       "Vol of Vol"    );
  ( "-rho",         Arg.Set_float  rho,         "Corr < Spot, StocVol >"      );
  ( "-local-vol",   Arg.Set_float  local_vol,   "LocalVol factor"             );
  ( "-bi-method",   Arg.Set_string bi_method,   "BI Method: Yanenko3[_ExpFit]");
  ( "-debug-dir",   Arg.Set_string debug_dir,   "Debug info dir if non-\"\""  )
];

Arg.parse arg_specs ( fun _ -> () ) "USAGE:\n";

(*---------------------------------*)
(* Option:                         *)
(*---------------------------------*)
(* Common Required Params: *)
value ccy_pair: Ccys.ccypair = Ccys.make_ccypair Ccys.GBP Ccys.USD;

value arg_cp      = Argument.CP
                    ( if cp.val > 0 then Argument.Call
                      else
                      if cp.val < 0 then Argument.Put
                      else failwith "CP must not be 0"
                    );

value arg_expdate = Argument.ExpiryDate
                    ( Argument.DateValue ( float_of_int expiry_date.val ) );

(* Optional Params for PDE Solver: *)

value opt_args: list Argument.optional_argument =
[
  Argument.DiffusionType diffusion.val;
  Argument.BIMethod      bi_method.val;
  Argument.SpotN         ( float_of_int n.val );
  Argument.VolM          ( float_of_int m.val );
  Argument.VolUp         vol_up.val;
  Argument.TimeStepDays  tau.val
];

(* The Option itself: *)

value uopt_type = String.uppercase opt_type.val;

value opt: Options.fx_option =
  match uopt_type with
  [
    (* The DKO Barrier Option:         *)
    "DKO" ->
      let req_args: Barrier_option.dko_required=
      {
        Barrier_option.dko_ccypair    = ccy_pair;
        Barrier_option.dko_startdate  = start_date;
        Barrier_option.dko_bar1       = Argument.Bar1       bar1.val;
        Barrier_option.dko_bar2       = Argument.Bar2       bar2.val;
        Barrier_option.dko_cp         = arg_cp;
        Barrier_option.dko_strike     = Argument.Strike     strike.val;
        Barrier_option.dko_expirydate = arg_expdate
      }
      in
      let baropt: Barrier_option.fx_barrieroption =
        Barrier_option.DKO req_args opt_args
      in
      Options.BarrierOption baropt

    (* The OE Vanilla:                 *)
  | "OE" ->
      let req_args: Simple_option.oe_required =
      {
        Simple_option.oe_ccypair      = ccy_pair;
        Simple_option.oe_startdate    = start_date;
        Simple_option.oe_cp           = arg_cp;
        Simple_option.oe_strike       = Argument.Strike     strike.val;
        Simple_option.oe_expirydate   = arg_expdate
      }
      in
      let sopt: Simple_option.fx_simpleoption =
        Simple_option.OE req_args opt_args
      in
      Options.SimpleOption sopt

  | _ -> failwith( "ERROR: Unsupported option type: "^ uopt_type )
  ];

(*---------------------------------*)
(* "Market Data":                  *)
(*---------------------------------*)
value rlen  = expiry_date.val + 2;

value dates : array Pde_types.jdt =
  Array.init  rlen ( fun i -> Pde_types.jdt_of_float ( float_of_int i ) );

value df_dom: array float =
  Array.init  rlen
              ( fun i -> exp( -. r_dom.val *. ( float_of_int i ) /. 365.0 ) );

value df_for: array float =
  Array.init  rlen
              ( fun i -> exp( -. r_for.val *. ( float_of_int i ) /. 365.0 ) );

(*---------------------------------*)
(* PDE Pricer:                     *)
(*---------------------------------*)
value pricer: Pde_types.pde_pricer =
  Pde_gen.mk_option_pricer
    ~{ spot      = spot0.val                           }
    ~{ rd_info   = ( Pde_types.DF_Table dates df_dom ) }
    ~{ rf_info   = ( Pde_types.DF_Table dates df_for ) }
    ~{ v0        = local_vol.val                       }
    ~{ alpha     = ( fun _   -> alpha.val )            }
    ~{ corr      = ( fun _   -> rho.val   )            }
    ~{ debug_dir = debug_dir.val                       }
    opt;

(*---------------------------------*)
(* Run the Pricer:                 *)
(*---------------------------------*)
value pde_price: float =
  try
    pricer ()
  with
  [ any ->
    do{
      print_endline ( "ERROR: "^( Printexc.to_string any ) );
      Printexc.print_backtrace stdout;
      flush stdout;
      exit 1
    }
  ];

Printf.printf "\nPDE Price = %f\n" pde_price;
flush stdout;
exit 0;

