#! /bin/bash -e
#=============================================================================#
# USER-LEVEL CONFIGURATION:                                                   #
#=============================================================================#
# NB: All paths must be absolute:
#
TOOLS_TOP="$HOME"/BuildEnv

#=============================================================================#
# Command-Line Params:                                                        #
#=============================================================================#
DoConfig=0
DoBuild=0
DoInstall=0
DRMode="Release"
VQMode=""
Branch="Trunk"
INSTALL_TOP=""

while getopts cbi:dvqr: opt
do
	case $opt in
		c) DoConfig=1;;
		b) DoBuild=1;;
		i) DoInstall=1; INSTALL_TOP="$OPTARG";;
		d) DRMode="Debug";;
		q) VQMode="-q";;
		v) VQMode="-v";;
		r) Branch="$OPTARG";;
		\?)
			echo "ERROR: Invalid Parameter: $opt" >&2
			exit 1 ;;
	esac
done

# Now the actual directories layout:
# Automatically determine the "PROJECT_TOP":
cd `dirname $0`/..
PROJECT_TOP=`pwd`
cd -

BUILD_TOP="$PROJECT_TOP"/Objs/"$Branch"-"$DRMode"
SRC_TOP="$PROJECT_TOP"/"$Branch"

export PATH="$TOOLS_TOP"/OCaml/Current/bin:"$PATH"

#=============================================================================#
# Configure: Build the "ocmake" utility and run it with "-config":            #
#=============================================================================#
if [ $DoConfig -ne 0 ]
then
	# Erase and re-create the "BUILD_TOP":
	rm   -fr "$BUILD_TOP"
	mkdir -p "$BUILD_TOP"

	# Run OCMake it in the "-config" mode:
	cd "$SRC_TOP"
	ocmake -config $VQMode 2>&1 | tee "$BUILD_TOP"/CCC.log
fi

#=============================================================================#
# Build the Whole Project:                                                    #
#=============================================================================#
if [ $DoBuild -ne 0 ]
then
	cd "$SRC_TOP"
	ocmake -build $VQMode 2>&1 | tee "$BUILD_TOP"/MMM.log
fi

#=============================================================================#
# Install the Binaries:                                                       #
#=============================================================================#
if [ $DoInstall -ne 0 ]
then
	cd "$SRC_TOP"
	ocmake -install $VQMode 2>&1 | tee "$BUILD_TOP"/III.log
fi

