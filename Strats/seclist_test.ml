(* vim:ts=2:syntax=ocaml
*)
module FT    = Fix_types;
module FC    = Fix_configs;
module FE    = Fix_engine;
module FA    = Fix_api;
module TC    = Tests_common;

Printexc.record_backtrace True;

(*---------------------------------------------------------------------------*)
(* Params:                                                                   *)
(*---------------------------------------------------------------------------*)
value time_out = 0.001;
value
(
  ( q_config, _, _, _, _, _, _, _, _, _, _ ), _, debug, _
)
= TC.get_test_config ();

value e = FA.mk_fix_engine  ~{ debug=debug } q_config;

(*---------------------------------------------------------------------------*)
(* "state", "init":                                                          *)
(*---------------------------------------------------------------------------*)
type state   =
{
  n:      mutable int;
  i:      mutable int;
  req_id: string
};

value init: FA.fix_reactor_conf state -> state -> state =
fun rconf state ->
  if state.req_id = ""
  then
    (* Send up the SecList Request:   *)
    let ( req_id, _ ) =
      FA.req_securities_list
         ~{ req_type=FT.ReqListSecurities }
         rconf
         e
    in
    { ( state ) with req_id = req_id }
  else  state;

(*---------------------------------------------------------------------------*)
(* "call_back":                                                              *)
(*---------------------------------------------------------------------------*)
value call_back:
  FA.fix_reactor_conf state -> FE.fix_engine -> state -> FT.fix_msg -> state =

fun _rconf _e state msg ->
  match msg with
  [
    FT.SecurityDef def when def.FT.security_req_id_d  = state.req_id ->
    do{
      (* Print the response received: *)
      Printf.printf "\n%s\n%!" ( FT.string_of_security_def def );

      (* Update the state:            *)
      if   state.n <= 0
      then state.n := def.FT.total_securities_count_d
      else
        if state.n <> def.FT.total_securities_count_d
        then
        do{
          Printf.printf "\nWARNING: TotalSecuritiesCount changed: %d -> %d\n%!"
                        state.n def.FT.total_securities_count_d;
          state.n    := def.FT.total_securities_count_d
        }
        else ();
      state.i := state.i + 1;

      if state.i >= state.n
      then raise Exit (* All done!                        *)
      else state      (* Continue with the updated state  *)
    }
  | _ -> state
  ];

(*---------------------------------------------------------------------------*)
(* Top-Level:                                                                *)
(*---------------------------------------------------------------------------*)
(* Create the Reactor:               *)
value rconf   = FA.new_reactor_conf
                  ~{ engines = [ e ]     }
                  ~{ time_cb = init      }
                  ~{ period  = time_out  }
                  ~{ main_cb = call_back };

FA.reactor rconf { n = 0; i = 0; req_id = "" };
exit 0;
