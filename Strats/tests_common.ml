(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                     Common Utils for Strategy Tests                       *)
(*===========================================================================*)
module FC = Fix_configs;
module FT = Fix_types;
open Tywith;

(*---------------------------------------------------------------------------*)
(* Parse the Command Line and the Config File:                               *)
(*---------------------------------------------------------------------------*)
type test_config =
(
  FC.fix_config_id *          (* FIX Config for Receiving  Quotes *)
  FC.fix_config_id *          (* FIX Config for Submitting Orders *)
  FT.exchange  *
  FT.currency  *
  FT.side      *
  FT.id_source *              (* Source for SecurityIDs used      *)
  string       *              (* CFI Code                         *)
  ( string * string ) *       (* ( Symbol, SecID ) to Order       *)
  int          *              (* Qty   ( shares or lots    )      *)
  float        *              (* Price ( of a share or lot )      *)
  list ( string * string )    (* Extra Symbols / SecIDs to Subscr *)
)
with parse;

value debug      : ref bool                   = ref False;
value n_iters    : ref int                    = ref 1;
value bolt_config: ref string                 = ref "bolt.conf";
value config     : ref ( option test_config ) = ref None;
value order_id   : ref string                 = ref "";

value cmd_args =
[
  ( "-debug",   Arg.Set debug,
    "Debug mode: print all FIX msgs (default: False)"
  );
  ( "-bolt",    Arg.Set_string bolt_config,
    "Config file for the Bolt Logger"
  );
  ( "-n",       Arg.Set_int n_iters,
    "Number of test iterations (default: 1)"
  );
  ( "-ord-id",  Arg.Set_string order_id,
    "OrderID (if known)"
  )
];

(* "read_term_file":
   This function is borrowed from OCMake's module U, but we rather copy and
   paste it than introduce complex external dependencies:
*)
value comms_re       =
  Pcre.regexp ~{ study=True } ~{ flags=[ `DOTALL ] } "\\(\\*.*?\\*\\)";

value read_term_file: string -> string =
fun fname ->
  let inch =
    try open_in_bin fname
    with[ _ -> failwith ( "ERROR: Cannot read file: "^fname ) ]
  in
  try
    let len  = in_channel_length inch in
    let cont = String.create len      in
    do{
      really_input inch cont 0 len;
      close_in inch;
      Pcre.replace ~{ rex=comms_re } ~{ templ="" } cont
    }
  with
  [ _ ->
    do{
      close_in_noerr inch;
      failwith ( "ERROR: Cannot read file: "^fname )
    }
  ];

(* "parse_config_file": *)

value parse_config_file: string -> unit =
fun fn ->
  match config.val with
  [
    None ->
      (* First occurrence of the config file name in the command line, so
         actually parse the config file:
      *)
      let strm    = Stream.of_string ( read_term_file fn ) in
      config.val := Some ( parse_test_config strm )
  | _ ->
    do{
      Arg.usage cmd_args "ERROR: Duplicate ConfigFile:";
      exit 1
    }
  ];

Arg.parse cmd_args parse_config_file "PARAMETERS:";

value get_test_config: unit -> ( test_config * int * bool * string ) =
fun () ->
  match config.val with
  [
    None   ->
    do{
      Arg.usage cmd_args "ERROR: ConfigFile not specified";
      exit 1
    }
  | Some c -> ( c, n_iters.val, debug.val, order_id.val )
  ];

(* Initialise the Bolt Logger: *)
if bolt_config.val <> ""
then
  Unix.putenv "BOLT_FILE" bolt_config.val
else
  ();

