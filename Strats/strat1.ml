value rts_instr =
{
  GS.i_exchange    = FT.FORTS;
  GS.i_cfi_code    = "FXXXXX";
  GS.i_symbol      = "RTS-3.11";
  GS.i_sec_id      = "RIH1";
  GS.i_id_src      = FT.Exchange_Symbol;
  GS.i_ccy         = FT.RUB;
  GS.i_conf_q      = FC.OSL_Quotes_All;
  GS.i_conf_o      = FC.RTS_Direct_Orders;
  GS.i_account     = ""
};

(* Params---------------------------------------------------------------------*)
value dstrike = 5000.;
value tick = 5.;
value xmax = log (187500. /. 180000.);
value xmin = (-xmax);
value slippage_opt = 15.;
value slippage_fut = 10.;
value ba_c = 300.;
value vol_koef = 1e-8;
value equity = 5e6;
value equity_num_strikes = 3.;
value v_quoting = 10;
value ba_start = 1000.;
value max_delta = 1.;
value dy_dx = 1. /. 3.;
(*----------------------------------------------------------------------------*)
value rts_symbol  = rts_instr.GS.i_symbol;

(*XXX To define: normcdf: float -> float *)
(* need to be cheched *)
value blsprice:
  float -> float -> float -> float -> (float -> float) -> bool -> float =
  fun spot strike rate time vol_smile iscall->
  do{
    assert (spot > 0.);
    assert (strike > 0.);
    assert (time > 0.);
    assert (vol > 0.);
    }
  let sqrt_time = sqrt time in
  let vol  = vol_smile (log (strike /. spot)) in
  let d1 = (log (spot /. strike) +. (rate +. vol *. vol /. 2) *. time) /.
    vol /. sqrt_time in
  let d2 = d1 -. vol *. (sqrt_time) in
  let call = 
    spot *. normcdf d1 -. strike *. exp (-rate *. time) *. normcdf d2 in
  if iscall then
    call
  else
    call +. strike *. exp (-rate *. time) -. spot 
;

(* need to be cheched *)
value blsdelta:
  float -> float -> float -> float -> (float -> float) -> bool -> float =
  fun spot strike rate time vol_smile iscall ->
  
  let eps = 1e-5 *. spot in
  let vol  = vol_smile (log (strike /. spot)) in
  ( (blsprice (spot +. eps) strike rate time vol iscall) -.
  (blsprice (spot -. eps) strike rate time vol iscall) ) /. 2. /. eps
;


(* need to be cheched *)
value blsvega:
  float -> float -> float -> float -> (float -> float) -> bool -> float =
  fun spot strike rate time vol_smile iscall->
  let eps = 1e-5 *. (vol_smile 0.) in
  let vol_smile_posit x  = (vol_smile x) +. eps in
  let vol_smile_negat x  = (vol_smile x) -. eps in
  ( (blsprice spot strike rate time vol_smile_posit iscall) -.
  (blsprice spot strike rate time vol_smile_negat iscall) ) /. 2. /. eps
;

(*XXX To define: koefs for vol smile curve, may be read from a file *)
(* need array of 5 koefs: vol_smile_koefs array *)
(* x = log (strike / spot) *)
(* vol_smile at the beginning of the day *)
value vol_smile_initial:
  float -> float =
  fun x ->
    let a = vol_smile_koefs.(0) in
    let b = vol_smile_koefs.(1) in
    let c = vol_smile_koefs.(2) in
    let d = vol_smile_koefs.(3) in
    let e = vol_smile_koefs.(4) in 
    a +. b *. (1. -. exp(-c *. x *. x)) +. d *. atan(e *. x) /. e
;

(* XXX to define: hasht_symb 
  string -> float*bool (strike and is_call) *)


(* XXX to define: tnow = time now as a fraction of 24 hours,
  12:00:00 = 0.5 *)

(* Set time restrictions *)
value time_ok:
  float -> bool =
  fun t_now ->
  let fract_of_time hh mm ss =
    (float_of_int hh) /. 24. +.
    (float_of_int mm) /. 1440. +.
    (float_of_int ss) /. 86400.
  in
  let pair_float tp = 
    match tp with ( (hh1,mm1,ss1), (hh2,mm2,ss2) ) ->
     (fract_of_time hh1 mm1 ss1),  (fract_of_time hh2 mm2 ss2)
  do{
    let ti = ( (13, 59, 40), (14, 03, 20) ) in
    let list_restr = (pair_float ti) :: list_restr in

    let ti = ( (18, 44, 40), (19, 00, 20) ) in
    let list_restr = (pair_float ti) :: list_restr in
    (* and so on *)
    }
  let pred t el = (el < fst t) || (el > snd t) in
  let pred_cor t = pred t t_now in
  List.for_all pred_cor list_restr
;

value round:
  float -> int =
  fun x ->

  int_of_float ( floor (x +. 0.5) )
;

value opt_pred aos =
  let symbol = aos.GS.aos_symbol in
    if symbol = rts_symbol then
      false
    else
      true
;
value fut_pred aos =
  let symbol = aos.GS.aos_symbol in
    if symbol = rts_symbol then
      true
    else
      false
;

(*XXX To define: if aos is in delete state *)
value aos_in_del:
  GS.active_order_status -> bool =
  fun aos ->
    aos.GS.aos_cancel <> GS.CancelNone;
;

(*XXX To define: if aos is confirmed *)
value aos_conf:
  GS.active_order_status -> bool =
  fun aos ->
  XXX
;

(*XXX To define: time_of_start *)
(* ba is narrowing during first 1min from strategy launch: *)
value ba_t t =
  let one_min = 1. /. 24. /. 60. in
  if t < time_of_start +. one_min then
    let al = (t -. time_of_start) /. one_min in
    ba_start *. (1. - al) + ba_c *. al
  else
    ba_c
;

(* _/~ Restr_function *)
value restr_f:
  float -> float =
  fun x x0 ->

  let () = assert (x0 > 0.) in
  if x < (-x0) then
    (-x0)
  else
    if x > x0 then
      x0
    else
      x
;

(*XXX On start dvol_shift_x(x) -> 0 and vol_shift(x) -> 0,
  hasht_pmax: (strike, iscall, isbuy = true) -> infinity,
              (strike, iscall, isbuy = false) -> 0 *)


(*---------------------------------------------------------------------------*)
(* OnChange: *)
(*---------------------------------------------------------------------------*)

value on_change:
....................

let md = GS.get_mdata s0 in
let rts_md = Hashtbl.find md rts_symbol in
let market_data_ok = rts_md.GS.md_bids <> [| |] && rts_md.GS.md_asks <> [| |] in
let (rts_spot, rts_ba) =
  if market_data_ok then 
    ( 0.5 *. ( fst( rts_md.GS.md_bids.( 0 ) ) +.
                fst( rts_md.GS.md_asks.( 0 ) ) ), 
      ( -fst( rts_md.GS.md_bids.( 0 ) ) +.
                fst( rts_md.GS.md_asks.( 0 ) ) ) 
    )
    else
       (0., 0.)
in

let vol_smile x = 
  (vol_smile_initial x) +. (vol_shift x) +. (dvol_shift x)
in

(* Update Vega (state) *)
let dvega =
  let f_mydeals key el cur =
    let vega =
      if (String.compare key rts_symbol) = 0 then
        0.
      else
        let pair = Hashtbl.find hasht_symb key in
        vega_of_pair pair
    in
    cur +. vega *. (float_of_int (snd el))
  in
  Hastbl.fold f_mydeals hasht_mydeals 0.
in


if dvega <> 0 then
  (*  *)
  
else




let poss = GS.get_poss s0 in


(* XXX to define: tnow = time now as a fraction of 24 hours,
  12:00:00 = 0.5 *)
let ba_cur = ba_t tnow in

(*XXX dim_matrix indexing starts from 1 *)
(* Position shift *)
let dp_pos strike = 
  (* (strike, iscall) -> pos *)
  let f (strike, iscall) =
    let symbol = Hashtbl.find hasht_symb_1 (strike, iscall) in
    Hashtbl.find poss symbol
  in
  let pos_sum = f (strike, true) +. f (strike, false) in
  let im = 
    let ind = int_of_float (strike /. dstrike) in
    if pos_sum > 0. then
      dim_matrix.(ind).(0)
    else
      dim_matrix.(ind).(1)
  in  
  (-ba_cur) *. im /. (equity /. equity_num_strikes) *. pos_sum
in

(*XXX calc time_mat*)

(* calc theoretical option prices with vega_shift and pos shift *)
(* Warning! The function makes 4 calcs for each strike! *)
(*XXX Define dp_pos and vol_shift *)
let price_strike_iscall_isbuy strike iscall isbuy =
  let spot = 
    if isbuy then
      rts_spot -. rts_ba /. 2.
    else
      rts_spot +. rts_ba /. 2.
  in
  let p = ( blsprice spot strike 0. time_mat vol_smile iscall ) in
  let p = p +. dp_pos strike in
  let p =
    if isbuy then
      p -. ba_cur /. 2.
    else
      p +. ba_cur /. 2.
  in
  tick *. round (p /. tick)
in

(* calc delta and vega *)
let delta_of_pair (strike, iscall) =
  blsdelta rts_spot strike 0. time_mat vol_smile iscall
in

let vega_of_pair (strike, iscall) =
  blsvega rts_spot strike 0. time_mat vol_smile iscall
in

(*XXX it's supposed that all positions are in VF1 only, there are only options
 and rts*)

(* Calc portfolio delta: *)
let port_delta = 
  let f_poss key el cur =
    let delta =
      if (String.compare key rts_symbol) = 0 then
        1.
      else
        let pair = Hashtbl.find hasht_symb key in
        delta_of_pair pair
    in
    cur +. delta *. (float_of_int el)
  in
  Hastbl.fold f_poss poss 0.
in

(* XXX to define hasht_alldeals: symbol -> price*vol, (>0), meaning
 all exchange deals in the symbols the strategy is subscribed;
 hasht_mydeals: symbol -> price*dpos, (<>0) *)


(*XXX update vol_shift *)

(*XXX It's supposed that on_change is called once after all messages were
   transmitted into GS, discuss *)

let curr_vega = ref (GS.get_ustate s0) in
do{
  curr_vega.val :=  curr_vega.val +. dvega;
  }


(* function list of all orders -> 
  (list of orders to delete, list of orders to move with new prices,
    list of existing (strike*iscall*isbuy )) *)

let get_opt_lists:
    GS.active_order_status list -> 
  ( GS.active_order_status list,
  ( GS.active_order_status * float ) list,
  (float * bool * bool) list ) = 
  fun l0 -> 
  
  let rec f l l1 l2 l3 =
  if l = [] then
    (l1, l2, l3)
  else
    let aos = List.hd l in
    let symbol = aos.GS.aos_symbol in
    let (strike, iscall) = Hashtbl.find hasht_symb symbol in
    let x = log (strike /. rts_spot) in
(*XXX define get_newest_qx and px *)
    let qx = get_newest_qx aos in
    let px = get_newest_px aos in
    let isbuy = if (aos.GS.aos_side = FT.Buy) then true else false in
    let p_theor = price_strike_iscall_isbuy strike iscall isbuy 
    (* Mark it as existing *)
    let l3_upd = ( (strike, iscall, isbuy)::l3 ) in
    
    if aos_in_del aos then
      (* Do nothing with it *)
      f (List.tl l) l1 l2 l3_upd
    else
      if (not (x > xmin && x < xmax)) || (qx < v_quoting)  then
        (* Delete it *)
        f (List.tl l) (aos::l1) l2 l3_upd
      else
        if abs_float (px - p_theor) >= slippage_opt then
          (* Move it *)
          f (List.tl l) l1 ( (aos, p_theor)::l2 ) l3_upd
        else
          f (List.tl l) l1 l2 l3_upd  
  in
  let (l1_final, l2_final, l3_del) = f l0 [] [] [] in
  let l3_final =
    let rec f l strike =
      if strike > rts_spot *. exp(xmax) then
        l
      else
        let f1 l0 tr =
          (* if l3_del doesn't contain el, add it *)
          let pred el = 
            (el = tr)
          in
          if (List.find_all pred l3_del) = [] then
            (* Add *)
            tr::l0
          else
            l0
        in
        let l = f1 l (strike, true, true) in
        let l = f1 l (strike, true, false) in
        let l = f1 l (strike, false, true) in
        let l = f1 l (strike, false, false) in
        (* Add all allowed triples with given strike *)
        f l (strike +. dstrike)
    in
    f [] (dstrike *. ceil (rts_spot *. exp(xmin) /. dstrike)) 
  in
  let f (strike, iscall, isbuy) =
    let p = price_strike_iscall_isbuy strike iscall isbuy in
    (p, strike, iscall, isbuy)
  in
  
  (l1_final, l2_final, (List.map f l3_final) )
in

(*XXX discuss aos confirmation state: 
  in place-confirmed; in move-confirmed; in delete-_ *)

(* Futures: fut_oass list ->
   (delete [aos], move [(aos, price)], place [(px, qx, isbuy))] *)
let get_fut_lists:
  GS.active_order_status list ->
  ( GS.active_order_status list,
  ( GS.active_order_status * float ) list,
  (float * int * bool) list ) =
  fun l0 ->

    (* Hedge if needed *)   
    if (abs_float port_delta) > max_delta then
      let isbuy = port_delta < 0. in      
      (* it's supposed that all orders belong to VF1 *)       
      let vv = abs (round port_delta) in
      let p_theor = 
        if isbuy then
          rts_spot -. rts_ba /. 2.
        else
          rts_spot +. rts_ba /. 2.
      in

      let place = [ (p_theor, vv, isbuy) ] in
      
      if l0 = [] then
        (* Place hedge order *)
        ([], [], place)
      else
        let aos = List.hd l0 in
        let px = get_newest_px aos in
        let aos_isbuy = if (aos.GS.aos_side = FT.Buy) then true else false in
        if aos_in_del aos then
          (* Do nothing *)
          ([], [], [])
        else
          if aos_isbuy = isbuy then
            if abs_float (px - p_theor) >= slippage_fut then
              (* Move *)  
              ([], [(aos, p_theor)], [])
            else
              ([], [], [])
          else
            (* Delete it *)
            ([aos], [], [])
    else
      if l0 = [] then
        ([], [], [])
      else
        (* Move until filled *)
        let aos = List.hd l0 in
        let px = get_newest_px aos in
        let aos_isbuy = if (aos.GS.aos_side = FT.Buy) then true else false in
        let p_theor = 
          if aos_isbuy then
            ( rts_spot -. rts_ba /. 2. )
          else
            ( rts_spot +. rts_ba /. 2. )
        in
        if aos_in_del aos then
          (* Do nothing *)
          ([], [], [])
        else
          if abs_float (px - p_theor) >= slippage_fut then
            (* Move *)
            ([], [(aos, p_theor)], [])
          else
            ([], [], [])
in        

    


    
(* Main module: lists of (delete, move, place) *) 
(* XXX to define: tnow = time now as a fraction of 24 hours,
  12:00:00 = 0.5 *) 
let opt_fut_lists =
  let opt_aoss = GS.find_aoss s0 opt_pred in
  let fut_aoss = GS.find_aoss s0 fut_pred in
  let () = assert ( (List.length fut_aoss) <= 1 ) in
  (* Check time restrictions *)
  if time_ok tnow && market_data_ok then
    let opt_lists = get_opt_lists opt_aoss in
    let fut_lists = get_fut_lists fut_aoss in
    (opt_lists, fut_lists)
  else
    (* delete all orders which are not in delete state *)
    let pred aos = (not (aos_in_del aos)) in
    let opt_lists = ( (List.find_all pred opt_aoss), [], [] ) in
    let fut_lists = ( (List.find_all pred fut_aoss), [], [] ) in
    (opt_lists, fut_lists)
in  

(*---------------------------------------------------------------------------*)
(* Order management *)
let del_f aos = GS.cancel_order
  aos
  rconf
  s0
  in
let move_f (aos, px) = GS.modify_order
  aos
  ~{ price = px }
  rconf
  s0
in
let place_f (symbol, side, px, qx) = GS.send_limit_order
  ~{ symbol = symbol }
  ~{ side   = side   }
  ~{ price  = px     }
  ~{ qty    = qx     }
  rconf
  s0
in

let ( (o_del, o_mov, o_pl), (f_del, f_mov, f_pl) ) = opt_fut_lists in
let all_del = List.concat [o_del; f_del] in
let all_mov = List.concat [o_mov; f_mov] in
(*XXX Define hasht_symb_1 = hasht: (strike, iscall) -> symbol *)

let fun_opt (p, strike, iscall, isbuy) =
  let symbol = Hashtbl.find hasht_symb_1 (strike, iscall) in
  let side = if isbuy then FT.Buy else FT.Sell in
  (symbol, side, p, v_quoting)
in
let o_pl = List.map fun_opt o_pl in
  let fun_fut (p, vv, isbuy) =
  let side = if isbuy then FT.Buy else FT.Sell in
  (rts_symbol, side, p, vv)
in
let f_pl = List.map fun_fut f_pl in

let all_pl = List.concat [o_pl; f_pl] in
(* Delete, move, place *)
do
{
  List.iter del_f all_del;
  List.iter move_f all_mov;
  List.iter place_f all_pl;
}


(*XXX It's supposed that aoss list was just updated*)
(*XXX check! *)
let opt_aoss = GS.find_aoss s0 opt_pred in

      let f aos = 
        let symbol = aos.GS.aos_symbol in
        let (strike, iscall) = Hashtbl.find hasht_symb symbol in
        let aos_isbuy = if (aos.GS.aos_side = FT.Buy) then true else false in
        let px = get_newest_px aos in
        if level_conf then
          Hashtbl.add hasht_pmax (strike, iscall, aos_isbuy) px
        else
          (* Chech price *)
          let p_hasht_max = 
            Hashtbl.find hasht_pmax (strike, iscall, aos_isbuy)
          in
          if aos_isbuy then
            if px > p_hasht_max then
              Hashtbl.add hasht_pmax (strike, iscall, aos_isbuy) px
            else
              ()
          else
            
      in
      List.iter opt_aoss f












let f_poss key el cur =
    let delta =
      if (String.compare key rts_symbol) = 0 then
        1.
      else
        let pair = Hashtbl.find hasht_symb key in
        delta_of_pair pair
    in
    cur +. delta *. (float_of_int el)
  in








