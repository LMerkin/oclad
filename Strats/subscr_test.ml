module FT = Fix_types;
module FC = Fix_configs;
module FE = Fix_engine;
module FA = Fix_api;
module TC    = Tests_common;

Printexc.record_backtrace True;

(* Parse the Config File:    *)
value time_out = 0.001;
value
(
  ( q_config, _, exchange, currency, _, id_src, _, _, _, _, symbols ),
  _, debug, _
)
= TC.get_test_config ();

value e    = FA.mk_fix_engine ~{ debug=True } q_config;

type state =
[
  Init
| SubscrSent
];

(* Initial Call-Back: Subscribe to all of the above symbols: *)
value init: FA.call_back0 state  =
fun rconf s0 ->
  match s0 with
  [
    Init ->
    do{
      List.iter
      (
        fun ( symbol, sec_id ) ->
          let ( req_id, _ ) =
            FA.subscr_market_data
              ~{ exchange     = exchange               }
              ~{ symbol       = symbol                 }
              ~{ security_id  = sec_id                 }
              ~{ id_src       = id_src                 }
              ~{ entry_types  = [| FT.Bid; FT.Offer |] }
              ~{ depth        = FT.FullBook            }
              ~{ incr_refresh = True                   }
              rconf
              e
          in
          Printf.printf "SUBSCRIBED: %s\t%s\t%s\n%!" symbol sec_id req_id
      )
      symbols;

      SubscrSent
    }
  | _ -> s0
  ];

(* Main Call-Back:                          *)
value test_print: FA.call_back1 state =
fun _rconf _e state msg ->
do{
  match msg with
  [
    FT.MarketData  x ->
      Printf.printf "===== %s: %f =====\n%!" 
                    x.FT.symbol_W x.FT.md_entries_W.( 0 ).FT.md_entry_px_MDE

  | FT.MarketDataIncr x ->
      Printf.printf "----- %s\n-----\n%!" ( FT.string_of_market_data_incr x )

  | _ -> ()
  ];
  state
};

(* Construct the Reactor Conf: *)
value rconf = FA.new_reactor_conf
                 ~{ engines = [ e ]      }
                 ~{ time_cb = init       }
                 ~{ period  = time_out   }
                 ~{ main_cb = test_print };

(* Run the reactor: *)
FA.reactor rconf Init;

