(* vim:ts=2:syntax=ocaml
*)
Printexc.record_backtrace True;

module FT    = Fix_types;
module FC    = Fix_configs;
module FE    = Fix_engine;
module FA    = Fix_api;
module TC    = Tests_common;

(*---------------------------------------------------------------------------*)
(* Parse the Config File:                                                    *)
(*---------------------------------------------------------------------------*)
value time_out = 0.001;
value
(
  ( _,
    o_config,
    exchange,
    currency,
    side,
    id_src,
    cfi_code,
    symbol_o,
    qty,
    price,
    _
  ),
  _,
  debug,
  _
)
= TC.get_test_config ();

(*---------------------------------------------------------------------------*)
(* Create the Engine:                                                        *)
(*---------------------------------------------------------------------------*)
(* FIX Engine for submitting Orders: *)
value eo       = FA.mk_fix_engine ~{ debug=debug } o_config;

value mde_side = if side = FT.Buy then FT.Bid else FT.Offer;

(*---------------------------------------------------------------------------*)
(* State of the Main CallBack:                                               *)
(*---------------------------------------------------------------------------*)
type state =
[
  Init
| OrderSent of string
];

(*---------------------------------------------------------------------------*)
(* The Time-Driven Call-Back:                                                *)
(*---------------------------------------------------------------------------*)
value init: FA.fix_reactor_conf state -> state -> state =
fun rconf state ->
  match state with
  [ Init ->
    let ( ord_id, _ ) =
      FA.send_order   (* NB: market order! *)
        ~{ exchange    = exchange         }
        ~{ cfi_code    = cfi_code         }
        ~{ symbol      = ( fst symbol_o ) }
        ~{ security_id = ( snd symbol_o ) }
        ~{ id_src      = id_src           }
        ~{ side        = side             }
        ~{ price       = price            }
        ~{ currency    = currency         }
        ~{ qty         = qty              }
        rconf
        eo
    in
    OrderSent ord_id

  | _    -> state
  ];

(*---------------------------------------------------------------------------*)
(* The Main Call-Back:                                                       *)
(*---------------------------------------------------------------------------*)
value call_back:
  FA.fix_reactor_conf state -> FE.fix_engine -> state -> FT.fix_msg -> state =

fun _rconf _e state msg  ->
  match state with
  [
    (*--------------------*)
    Init             ->
    (*--------------------*)
      assert False

    (*--------------------*)
  | OrderSent ord_id ->
    (*--------------------*)
    (* Wait for completion *)
      match msg with
      [
        FT.ExecReport rep when
          (  rep.FT.exec_type_8  = FT.Traded || rep.FT.exec_type_8 = FT.Filled )
          && rep.FT.leaves_qty_8 = 0
          && rep.FT.cl_order_id_8= ord_id
        ->
          raise Exit
      | _ -> state
      ]
  ];

(* Create the Reactor Config:          *)
value rconf   = FA.new_reactor_conf
                  ~{ engines = [ eo ]    }
                  ~{ time_cb = init      }
                  ~{ period  = time_out  }
                  ~{ main_cb = call_back };

(* Run the reactor in the main thread: *)
FA.reactor rconf Init;
exit 0;
