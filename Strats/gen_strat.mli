(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                        "Generic Trading Strategy"                         *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* Data Types:                                                               *)
(*---------------------------------------------------------------------------*)
(* A Traded Instrument:                   *)
type instrument = 
{
  (* Instrument params proper:            *)
  i_exchange: Fix_types.exchange;
  i_cfi_code: string;
  i_symbol  : string;
  i_sec_id  : string;
  i_id_src  : Fix_types.id_source;
  i_ccy     : Fix_types.currency;

  (* Instrument quoting and trading params. Different Engines ( as specified by
     their Configs ) can be provided for receiving Quotes and submitting Orders
     for this Instrument:
  *)
  i_conf_q  : Fix_configs.fix_config_id;
  i_conf_o  : Fix_configs.fix_config_id;
  i_account : string    (* "" for default *)
};

(* Current market data for a given instrument:                               *)
type market_data =
{
  md_bids: mutable array ( float * int );         (*  [| ( Price, Qty ) |]   *)
  md_asks: mutable array ( float * int )          (*  [| ( Price, Qty ) |]   *)
};

(* Status of a current ( not yet fulfilled ) order;  "aos_order_phase" is for
   BOTH Orders and Modification Reqs (because the latter are essentially like
   new orders), "aos_cancel_phase" is for Cancel Reqs:
*)
type aos_order_phase  =                           (* OrdID, TimeStamp, Price *)
[
  OrderSent       of string and Fix_types.date_time and float
| OrderPending    of string and Fix_types.date_time and float
| OrderPartFilled of string and Fix_types.date_time and float
];

type aos_cancel_phase =
[
  CancelNone
| CancelSent    of string and Fix_types.date_time (* ReqID, TimeStamp        *)
| CancelPending of string and Fix_types.date_time (* ReqID, TimeStamp        *)
];

type active_order_status =
{
  aos_track   : string;           (* Unique ID for internal tracking         *)
  aos_symbol  : string;           (* Symbol being traded                     *)
  aos_side    : Fix_types.side;
  aos_orig_qty: int;              (* Qty as in the original order, > 0       *)
  aos_rem_qty : mutable int;      (* Unfilled qty in [ 0 .. orig_qty ]       *)
  aos_moves   : mutable list aos_order_phase;
  aos_cancel  : mutable aos_cancel_phase
};
(* Semantics of "aos_moves" above: it relates to placing of the original order
   and subsequent price moves. Always non-[]. Head is the last confirmed order
   (unless it was just submitted), newer orders are in the tail of the list.
*)

(* The over-all GS state, including the user-defined polymorphic state.    NB:
   hash tables are mutable by themselves, and will be updated in-place; user-
   level call- back is supposed to do the same, and return the same block ptr:
*)
type position = int;      (* Positive (Long) or Negative (Short) Qty         *)

type gstate 'u = 'a;

type uact  'u =
  Fix_api.fix_reactor_conf ( gstate 'u ) -> Fix_engine.fix_engine ->
  gstate 'u -> Fix_types.fix_msg -> 'u;


(*---------------------------------------------------------------------------*)
(* FIX API Extensions:                                                       *)
(*---------------------------------------------------------------------------*)
(* Access to parts of the State from the user call-back:                     *)

value get_mdata:         gstate 'u -> Hashtbl.t string market_data;
value get_active_orders: gstate 'u -> Hashtbl.t string active_order_status;
value get_poss:          gstate 'u -> Hashtbl.t string position;
value get_ustate:        gstate 'u -> 'u;
value get_engines:       gstate 'u -> list Fix_engine.fix_engine;

(* Extracting info from Bids/Asks order queues (Arg1).The function goes from
   the top of the queue into the depth and picks up the volume at each price
   level until the whole volume given by Arg2 is accommodated;   returns the
   AvgPrice and the Min/Max Price at which the instrument can be Sold/Purcsd
   ( that Min/Max price corresponds to the deepest queue level used ):
*)
value pxs_from_q:
  ~order_q: array ( float * int ) -> ~total_vol: int -> ( float * float );

(* Search for AOS(es) satisfying a given binary predicate:                   *)
value find_aoss:
  gstate 'u -> ( active_order_status -> bool ) -> list active_order_status;

(* Other Utils:    *)
value get_aosp_ord_id: aos_order_phase -> string;
value get_aosp_dt:     aos_order_phase -> Fix_types.date_time;
value get_aosp_price:  aos_order_phase -> float;

(* Testing output: *)
value string_of_market_data: market_data  -> string;

(* FIX Order Management, to be used from within the User Call-Back of GS:    *)

value send_limit_order:
  ~symbol:        string                  ->
  ~side:          Fix_types.side          ->
  ~price:         float                   ->
  ~qty:           int                     ->
  ?time_in_force: Fix_types.time_in_force ->
  Fix_api.fix_reactor_conf ( gstate 'u )  ->
  ( gstate 'u )                           ->
  unit;

value cancel_order:
  active_order_status                     ->
  Fix_api.fix_reactor_conf ( gstate 'u )  ->
  ( gstate 'u )                           ->
  unit;

value modify_order:
  active_order_status                     ->
  ~new_price:  float                      ->
  Fix_api.fix_reactor_conf ( gstate 'u )  ->
  ( gstate 'u )                           ->
  unit;

(*---------------------------------------------------------------------------*)
(* Creating a concrete Strategy:                                             *)
(*---------------------------------------------------------------------------*)
(* The Generic Strategy is hidden inside the closure created:                *)
value mk_strat:
  ?debug:       bool                            ->
  ?engines:     list Fix_engine.fix_engine      ->
  ~instrs:      array instrument                ->
  ~user_cb:     uact 'u                         ->
  ~init_ustate: 'u                              ->
  ( unit -> unit );

