(* vim:ts=2:syntax=ocaml
*)
module FT    = Fix_types;
module FC    = Fix_configs;
module FE    = Fix_engine;
module FA    = Fix_api;
module TC    = Tests_common;

Printexc.record_backtrace True;
(*---------------------------------------------------------------------------*)
(* Params:                                                                   *)
(*---------------------------------------------------------------------------*)
value time_out = 0.001;
value
(
  ( _,
    o_config,
    exchange,
    currency,
    side,
    id_src,
    cfi_code,
    symbol_o,
    orig_qty,
    price,
    _
  ),
  _,
  debug,
  ord_id
)
= TC.get_test_config ();

(* Config and Engine for submitting Orders:  *)
value eo       = FA.mk_fix_engine  ~{ debug=debug } o_config;
value time0    = Unix.gettimeofday ();

type state     =
[
  Init of string
| Sent of string
];

value get_id: state -> string =
fun
[ Init s -> s
| Sent s -> s
];

(*---------------------------------------------------------------------------*)
(* Main "call_back":                                                         *)
(*---------------------------------------------------------------------------*)
value rec call_back:
  FA.fix_reactor_conf state -> FE.fix_engine -> state -> FT.fix_msg -> state =
fun rconf _e state msg      ->
  let time1   = Unix.gettimeofday ()  in
  let cord_id = get_id state          in
  match msg with
  [
    (* Cancellation Request Received: *)
    FT.ExecReport rep when
      rep.FT.cl_order_id_8      = cord_id          &&
      rep.FT.order_status_8     = FT.PendingCancel &&
      rep.FT.exec_type_8        = FT.PendingCancel &&
      rep.FT.orig_cl_order_id_8 = Some ord_id ->
    do{
      Printf.printf
        "\nORDER CANCEL PENDING @ t0+%f: OrdID=%s, CancelID=%s\n%s\n%!"
        ( time1 -. time0 ) ord_id cord_id ( FT.string_of_fix_msg msg );
      (* Stay in the same state: *)
      state
    }

    (* Cancellation Request Rejected. This can happen, in particular, if the
       order has already been completed or cancelled:
    *)
  | FT.OrderCancelReject crej when
      crej.FT.orig_cl_order_id_9 = ord_id  &&
      crej.FT.cl_order_id_9      = cord_id ->
      match crej.FT.order_status_9 with
      [
        FT.Canceled   ->
        do{
          Printf.printf "\nORDER ALREADY CANCELLED:@ t0+%f: OrdID=%s\n%s\n%!"
                        ( time1 -. time0 ) ord_id ( FT.string_of_fix_msg msg );
          raise Exit
        }

      | FT.Filled     ->
        do{
          Printf.printf "\nTOO LATE: ORDER FILLED:@ t0+%f: OrdID=%s\n%s\n%!"
                        ( time1 -. time0 ) ord_id ( FT.string_of_fix_msg msg );
          raise Exit
        }

      | FT.PendingNew ->
        do{
          (* Unknown order? Invalid OrderID? *)
          Printf.printf "\nINVALID OrderID?@ t0+%f: OrdID=%s\n%s\n%!"
                        ( time1 -. time0 ) ord_id ( FT.string_of_fix_msg msg );
          raise Exit
        }

      | _ ->
        do{
          Printf.printf
            "\nORDER CANCEL REJECTED @ t0+%f: OrdID=%s, CancelID=%s\n%s\n%!"
            ( time1 -. time0 ) ord_id cord_id ( FT.string_of_fix_msg msg );
          (* Try to cancel again:         *)
          let  cord_id' = send_cancel_req rconf ord_id in
          Sent cord_id'
        }
      ]

    (* Order Really Cancelled: *)
  | FT.ExecReport rep when
      rep.FT.cl_order_id_8      = cord_id     &&
      rep.FT.order_status_8     = FT.Canceled &&
      rep.FT.exec_type_8        = FT.Canceled &&
      rep.FT.orig_cl_order_id_8 = Some ord_id ->
    do{
      Printf.printf "\nORDER CANCEL CONFIRMED @ t0+%f: OrdID=%s\n%s\n%!"
                    ( time1 -. time0 ) ord_id ( FT.string_of_fix_msg msg );
      raise Exit
    }

  | _ -> state
  ]
(*---------------------------------------------------------------------------*)
(* "send_cancel_req":                                                        *)
(*---------------------------------------------------------------------------*)
and send_cancel_req: FA.fix_reactor_conf state -> string -> string =
fun rconf ord_id ->
  let ( cord_id, _ ) =
    FA.cancel_order
      ~{ exchange    = exchange         }
      ~{ ord_id      = ord_id           }
      ~{ symbol      = ( fst symbol_o ) }
      ~{ security_id = ( snd symbol_o ) }
      ~{ id_src      = id_src           }
      ~{ cfi_code    = cfi_code         }
      ~{ side        = side             }
      ~{ orig_qty    = orig_qty         }
      rconf
      eo
  in
  let time1 = Unix.gettimeofday ()     in
  do{
    Printf.printf "\nORDER CANCEL SENT @ %f for OrdID=%s: \n%!"
                  ( time1 -. time0 ) ord_id;
    cord_id
  };

(*---------------------------------------------------------------------------*)
(* Time-Drive Call-Back:                                                     *)
(*---------------------------------------------------------------------------*)
value init: FA.fix_reactor_conf state -> state -> state =
fun rconf state ->
  match state with
  [
    Init ord_id -> Sent ( send_cancel_req rconf ord_id )
  | _           -> state
  ];

(*---------------------------------------------------------------------------*)
(* Top-Level:                                                                *)
(*---------------------------------------------------------------------------*)
(* Create the Reactor:              *)
value rconf   = FA.new_reactor_conf
                  ~{ engines = [ eo ]    }
                  ~{ time_cb = init      }
                  ~{ period  = time_out  }
                  ~{ main_cb = call_back };

FA.reactor rconf ( Init ord_id );
exit 0;
