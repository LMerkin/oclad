(* vim:ts=2:syntax=ocaml
*)
Printexc.record_backtrace True;

module FT    = Fix_types;
module FC    = Fix_configs;
module FE    = Fix_engine;
module FA    = Fix_api;
module TC    = Tests_common;

(*---------------------------------------------------------------------------*)
(* Parse the Config File:                                                    *)
(*---------------------------------------------------------------------------*)
value time_out = 0.001;
value
(
  ( q_config,
    o_config,
    exchange,
    currency,
    side,
    id_src,
    cfi_code,
    symbol_o,
    qty,
    price,
    symbols_q
  ),
  n_iters,
  debug,
  _
)
= TC.get_test_config ();

(*---------------------------------------------------------------------------*)
(* Create the Engines:                                                       *)
(*---------------------------------------------------------------------------*)
(* FIX Engine for receiving Quotes:  *)
value eq      = FA.mk_fix_engine ~{ debug=debug } q_config;

(* FIX Engine for submitting Orders: *)
value eo      =
  if o_config = q_config
  then
    (* Do not create yet another engine, use the Q one: *)
    eq
  else
    FA.mk_fix_engine ~{ debug=debug } o_config;

value mde_side = if side = FT.Buy then FT.Bid else FT.Offer;

(*---------------------------------------------------------------------------*)
(* State of the Main CallBack:                                               *)
(*---------------------------------------------------------------------------*)
type state =
[
  Init                                            (* The very initial state  *)
| SubscribedO                                     (* Subscribed to SymbolO   *)
| SubscribedQs       of int                       (* Subscribed to SymbolsQ  *)
| OrderSent          of int and float and string  (* Sent a limit order      *)
| OrderAcknowledged  of int and float and string  (* Received a response     *)
| CancelSent         of int and float and string and string
                                                  (* Sent a Cancellation Req *)
| CancelAcknowledged of int and float and string  (* Got a Cancel response   *)
];

(*---------------------------------------------------------------------------*)
(* The Initial Call-Back:                                                    *)
(*---------------------------------------------------------------------------*)
value init: FA.fix_reactor_conf state -> state -> state =
fun rconf state ->
do{
  assert ( state = Init );

  (* Subscribe to the initial symbol: *)
  let ( orig_req_id, _ ) =
    FA.subscr_market_data
      ~{ exchange     = exchange               }
      ~{ symbol       = ( fst symbol_o )       }
      ~{ security_id  = ( snd symbol_o )       }
      ~{ id_src       = id_src                 }
      ~{ entry_types  = [| FT.Bid; FT.Offer |] }
      ~{ depth        = FT.FullBook            }
      ~{ incr_refresh = True                   }
      rconf
      eq
  in
  Printf.printf "SUBSCRIBED: %s %s %s\n%!"
    ( fst symbol_o ) ( snd symbol_o ) orig_req_id;

  SubscribedO;
};

(*---------------------------------------------------------------------------*)
(* The Main Call-Back:                                                       *)
(*---------------------------------------------------------------------------*)
value call_back:
  FA.fix_reactor_conf state -> FE.fix_engine -> state -> FT.fix_msg -> state =

fun rconf _e state msg ->
  (*--------------------*)
  (* "send_cancel_req": *)
  (*--------------------*)
  let send_cancel_req: int -> float -> string -> state =
  fun n time0 ord_id       ->
    let ( cord_id, _ ) =
      FA.cancel_order
        ~{ exchange    = exchange         }
        ~{ ord_id      = ord_id           }
        ~{ cfi_code    = cfi_code         }
        ~{ symbol      = ( fst symbol_o ) }
        ~{ security_id = ( snd symbol_o ) }
        ~{ id_src      = id_src           }
        ~{ side        = side             }
        ~{ orig_qty    = qty              }
        rconf
        eo
    in
    let time3    = Unix.gettimeofday ()     in
    do{
      Printf.printf "\nORDER CANCEL SENT @ %f for OrdID=%s\n%!"
                    ( time3 -. time0 ) ord_id;
      CancelSent n time0 ord_id cord_id
    }
  in
  (*--------------------*)
  (* "find_in_quotes":  *)
  (*--------------------*)
  let rec find_in_quotes_rec: FT.market_data -> int -> bool =
  fun md i ->
    if i >= Array.length md.FT.md_entries_W
    then False (* Scanned the whole array, target not found *)
    else
      let mde = md.FT.md_entries_W.( i )  in
      if  mde.FT.md_entry_type_MDE = mde_side &&
          mde.FT.md_entry_px_MDE   = price
      then True
      else find_in_quotes_rec md ( i+1 )
  in
  match state with
  [
    (*--------------------*)
    Init               ->
    (*--------------------*)
      assert False

    (*--------------------*)
  | SubscribedO        ->
    (*--------------------*)
    (* Subscribe to other symbols: *)
    do{
        List.iter
        (
          fun ( symbol, sec_id ) ->
            let ( req_id, _ ) =
              FA.subscr_market_data
                ~{ exchange     = exchange               }
                ~{ symbol       = symbol                 }
                ~{ security_id  = sec_id                 }
                ~{ id_src       = id_src                 }
                ~{ entry_types  = [| FT.Bid; FT.Offer |] }
                ~{ depth        = FT.FullBook            }
                ~{ incr_refresh = True                   }
                rconf
                eq
            in
            Printf.printf "SUBSCRIBED: %s %s %s\n%!" symbol sec_id req_id
        )
        symbols_q;

        SubscribedQs 0
    }
  
    (*--------------------*)
  | SubscribedQs n  ->
    (*--------------------*)
    (* Send the order:    *)
      let time0  = Unix.gettimeofday () in
      let ( ord_id, _ ) =
        FA.send_order
          ~{ exchange    = exchange         }
          ~{ cfi_code    = cfi_code         }
          ~{ symbol      = ( fst symbol_o ) }
          ~{ security_id = ( snd symbol_o ) }
          ~{ id_src      = id_src           }
          ~{ side        = side             }
          ~{ price       = price            }
          ~{ currency    = currency         }
          ~{ qty         = qty              }
          rconf
          eo
      in
      do{
        Printf.printf "\nORDER SENT @ t0: OrdID=%s\n%!" ord_id;
        OrderSent n time0 ord_id
      }
 
    (*--------------------*)
  | ( OrderSent n time0 ord_id ) as curr  ->
    (*--------------------*)
    (* Wait for the "ExecReport": *)
      match msg with
      [
        FT.ExecReport rep when
          rep.FT.cl_order_id_8  = ord_id      &&
          rep.FT.order_status_8 = FT.NewOrder &&
          rep.FT.exec_type_8    = FT.NewOrder ->

          let time1 = Unix.gettimeofday () in
          do{
            Printf.printf "\nORDER ACKNOWLEDGED @ t0+%f: OrdID=%s\n%s\n%!"
                          ( time1 -. time0 )ord_id( FT.string_of_fix_msg msg );
            OrderAcknowledged n time0 ord_id
          }
      | _ -> curr
      ]

    (*--------------------*)
  | ( OrderAcknowledged n time0 ord_id ) as curr  ->
    (*--------------------*)
    (* Wait for this price to appear in quotes: *)
    do{
      let time2 = Unix.gettimeofday ()     in
      match msg with
      [
        FT.MarketData md ->
          if  find_in_quotes_rec md 0
          then
          do{
            (* Order found in the Quotes:        *)
            Printf.printf "\nORDER OBSERVED IN QUOTES @ t0+%f:\n%s\n%!"
                          ( time2 -. time0 )( FT.string_of_fix_msg msg );
            (* Now send a cancellation order:    *)
            send_cancel_req n time0 ord_id
          }
          else
            curr (* Quote not found, stay in the same state: *)

      | _ -> curr
      ]
    }

    (*--------------------*)
  | ( CancelSent n time0 ord_id cord_id ) as curr ->
    (*--------------------*)
      let time4 = Unix.gettimeofday ()  in
      match msg with
      [
        (* Cancellation Request Received: *)
        FT.ExecReport rep when
          rep.FT.cl_order_id_8      = cord_id          &&
          rep.FT.order_status_8     = FT.PendingCancel &&
          rep.FT.exec_type_8        = FT.PendingCancel &&
          rep.FT.orig_cl_order_id_8 = Some ord_id ->
        do{
          Printf.printf
            "\nORDER CANCEL PENDING @ t0+%f: OrdID=%s, CancelID=%s\n%s\n%!"
            ( time4 -. time0 ) ord_id cord_id ( FT.string_of_fix_msg msg );
          (* Stay in the same state:      *)
          curr
        }

        (* Cancellation Request Rejected: *)
      | FT.OrderCancelReject crej when
          crej.FT.orig_cl_order_id_9 = ord_id  &&
          crej.FT.cl_order_id_9      = cord_id ->
        do{
          Printf.printf
            "\nORDER CANCEL REJECTED @ t0+%f: OrdID=%s, CancelID=%s\n%s\n%!"
            ( time4 -. time0 ) ord_id cord_id ( FT.string_of_fix_msg msg );
          (* Try to cancel again:         *)
          send_cancel_req n time0 ord_id
        }

        (* Order Really Cancelled: *)
      | FT.ExecReport rep when
          rep.FT.cl_order_id_8      = cord_id     &&
          rep.FT.order_status_8     = FT.Canceled &&
          rep.FT.exec_type_8        = FT.Canceled &&
          rep.FT.orig_cl_order_id_8 = Some ord_id ->
        do{
          Printf.printf "\nORDER CANCEL CONFIRMED @ t0+%f: OrdID=%s\n%s\n%!"
                        ( time4 -. time0 ) ord_id ( FT.string_of_fix_msg msg );
          CancelAcknowledged n time0 ord_id
        }
      | _ -> curr
      ]
    (*--------------------*)
  | ( CancelAcknowledged n time0 _ ) as curr ->
    (*--------------------*)
      match msg with 
      [
        FT.MarketData md ->
          if md.FT.symbol_W <> fst symbol_o
          then
            (* This quote does not contain the traded symbol at all, so it is
               irrelevant. Stay in the same state:
            *)
            curr
          else
            (* Relevant quote -- check if our trade is gone:                 *)
            let time5 = Unix.gettimeofday () in
            if  find_in_quotes_rec md 0
            then
            do{
              Printf.printf "\nORDER STILL IN QUOTES @ t0+%f:\n%s\n%!"
                            ( time5 -. time0 ) ( FT.string_of_fix_msg msg );
              curr (* Stay in the same state  *)
            }
            else
            if n >= n_iters
            then raise Exit            (* All done, exiting       *)
            else SubscribedQs ( n+1 )  (* Do not subscribe again! *)

      | _ -> curr
      ]
  ];

(* Create the Reactor Config:            *)
value rconf   = FA.new_reactor_conf
                  ~{ engines = [ eq; eo ] }
                  ~{ time_cb = init       }
                  ~{ period  = time_out   }
                  ~{ main_cb = call_back  };

(* Run the reactor in a separate thread: *)

value thr = Thread.create ( FA.reactor rconf ) Init;
Thread.join thr;

exit 0;
