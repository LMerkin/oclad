(* vim:ts=2:syntax=ocaml
*)
Printexc.record_backtrace True;

module FT    = Fix_types;
module FC    = Fix_configs;
module FE    = Fix_engine;
module FS    = Fix_session;
module FA    = Fix_api;
module TC    = Tests_common;

(*---------------------------------------------------------------------------*)
(* Params:                                                                   *)
(*---------------------------------------------------------------------------*)
value config       = FC.RTS_Direct_Orders_Dry;
value debug        = True;
value time_out     = 0.1;
value exchange     = FT.FORTS;
value ccy          = FT.RUB;
(* value our_acc   = "our_acc"; *)
value acc          = "044";

value id_src       = FT.Exchange_Symbol;
value cfi_code_fut = "FXXXXX";
value symbol_fut   = ( "RTS-9.10", "RIU0" );

value price1       = 133000.0;
value qty1         = 5;
value side1        = FT.Buy;
value acc1         = acc; (* "123";   *)

value qty2         = 9;
value acc2         = acc; (* our_acc; *)

value price3       = 143000.0;
value qty3         = 6;
value side3        = FT.Sell;
value acc3         = acc; (* "007";   *)

(*
value symbol_opt   = ( "RTS-9.10M150910CA 1700000", "RI170000BI0" );
value cfi_code_opt = "OCXXXX";
*)
value symbol_opt   = symbol_fut;
value cfi_code_opt = cfi_code_fut;

value qty5         = 1;
value side5        = FT.Buy;
value acc5         = acc; (* our_acc; *)

(*
value price6       = 260.0;
value price7       = 320.0;
*)
value price6       = price1;
value price7       = 1.1 *. price1;
value qty6         = 10;
value side6        = FT.Buy;
value acc6         = acc; (* our_acc; *)

(*===========================================================================*)
(* Test 1:                                                                   *)
(*===========================================================================*)
(* This test does not involve any application-level msgs, so we only need to
   manage the Engine:
*)
print_endline "==============================================================";
print_endline "TEST 1: LogOn / LogOut                                        ";
print_endline "==============================================================";

value e1 = FA.mk_fix_engine ~{ debug=debug } config;

(* Allow some time for the engine to service "TestReq"s and other system-level
  msgs:
*)
Thread.delay 10.0;

(* Explicitly finalise the engine to force LogOut: *)
FS.fix_finalise ~{ graceful=True } ~{ msg="Logging out of Test1" } e1;

Thread.delay 40.0;
flush stdout;
flush stderr;

(*===========================================================================*)
(* Test 2:                                                                   *)
(*===========================================================================*)
print_endline "==============================================================";
print_endline "TEST 2: Order Management                                      ";
print_endline "==============================================================";

value e2     = FA.mk_fix_engine ~{ debug=debug } config;

(*---------------------------------------------------------------------------*)
(* State:                                                                    *)
(*---------------------------------------------------------------------------*)
type state2 =
[
  Init
| LimitOrder          of string
| MarketOrder         of string
| GoodTillCancelOrder of string
| Cancel              of string and string
| OptionOrder1        of string
| OptionOrder2        of string
| OrderMove           of string and string
];

(*---------------------------------------------------------------------------*)
(* The Initial Call-Back:                                                    *)
(*---------------------------------------------------------------------------*)
value init: FA.fix_reactor_conf state2 -> state2 -> state2 =

fun rconf state ->
  match state with
  [
    Init ->
    do{
      flush stdout;
      flush stderr;
      print_endline "----------------------------------------------------";
      print_endline "2.1: Make a Futures Limit Order:                    ";
      print_endline "----------------------------------------------------";
      let ( ord_id, _ ) =
        FA.send_order
          ~{ exchange    = exchange           }
          ~{ account     = acc1               }
          ~{ cfi_code    = cfi_code_fut       }
          ~{ symbol      = ( fst symbol_fut ) }
          ~{ security_id = ( snd symbol_fut ) }
          ~{ id_src      = id_src             }
          ~{ side        = side1              }
          ~{ price       = price1             }
          ~{ currency    = ccy                }
          ~{ qty         = qty1               }
          rconf
          e2
      in
      LimitOrder ord_id
    }
  | _ -> state
  ];

(*---------------------------------------------------------------------------*)
(* The Main Call-Back:                                                       *)
(*---------------------------------------------------------------------------*)
value call_back:
FA.fix_reactor_conf state2 -> FE.fix_engine -> state2 -> FT.fix_msg -> state2 =

fun rconf e state msg  ->
  match state with
  [
    (*--------------------*)
    Init               ->
    (*--------------------*)
      assert False

    (*--------------------*)
  | LimitOrder ord_id  ->
    (*--------------------*)
      (* Wait for ANY single exec report with this "ord_id", then continue:  *)
      match msg with
      [
        FT.ExecReport rep when rep.FT.cl_order_id_8 = ord_id ->
        do{
          flush stdout;
          flush stderr;
          print_endline "----------------------------------------------------";
          print_endline "2.2: Make a Futures Market Order:                   ";
          print_endline "----------------------------------------------------";
          let ( ord_id', _ ) =
            FA.send_order
              ~{ exchange      = exchange           }
              ~{ account       = acc2               }
              ~{ cfi_code      = cfi_code_fut       }
              ~{ symbol        = ( fst symbol_fut ) }
              ~{ security_id   = ( snd symbol_fut ) }
              ~{ id_src        = id_src             }
              ~{ side          = side1              }
              ~{ price         = 0.0                }
              ~{ currency      = ccy                }
              ~{ qty           = qty2               }
              ~{ time_in_force = FT.GoodTillCancel  }
              rconf
              e
          in
          MarketOrder ord_id'
        }
      | _ -> state
      ]

    (*--------------------*)
  | MarketOrder ord_id ->
    (*--------------------*)
      (* Wait for the second ExecReport to come: *)
      match msg with
      [
        FT.ExecReport rep when rep.FT.cl_order_id_8 = ord_id ->
        do{
          flush stdout;
          flush stderr;
          print_endline "----------------------------------------------------";
          print_endline "2.3: Make a GoodTillCancel Order:                   ";
          print_endline "----------------------------------------------------";
          let ( ord_id', _ ) =
            FA.send_order
              ~{ exchange    = exchange           }
              ~{ account     = acc3               }
              ~{ cfi_code    = cfi_code_fut       }
              ~{ symbol      = ( fst symbol_fut ) }
              ~{ security_id = ( snd symbol_fut ) }
              ~{ id_src      = id_src             }
              ~{ side        = side3              }
              ~{ price       = price3             }
              ~{ currency    = ccy                }
              ~{ qty         = qty3               }
              rconf
              e
          in
          GoodTillCancelOrder ord_id'
        }
      | _ -> state
      ]

    (*--------------------*)
  | GoodTillCancelOrder ord_id ->
    (*--------------------*)
      (* Wait for any ExecReport: *)
      match msg with
      [
        FT.ExecReport rep when rep.FT.cl_order_id_8 = ord_id ->
        do{
          flush stdout;
          flush stderr;
          print_endline "----------------------------------------------------";
          print_endline "2.4: Send a Cancel Request:                         ";
          print_endline "----------------------------------------------------";
          let ( cord_id, _ ) =
            FA.cancel_order
              ~{ exchange    = exchange           }
              ~{ account     = acc3               }
              ~{ ord_id      = ord_id             }
              ~{ cfi_code    = cfi_code_fut       }
              ~{ symbol      = ( fst symbol_fut ) }
              ~{ security_id = ( snd symbol_fut ) }
              ~{ id_src      = id_src             }
              ~{ side        = side3              }
              ~{ orig_qty    = qty3               }
              rconf
              e
          in
          Cancel ord_id cord_id
        }
      | _ -> state
      ]

    (*--------------------*)
  | Cancel ord_id cord_id ->
    (*--------------------*)
    (* This is just a buffer state, introduced in order to process a disjunctn
       of 2 guarded conds:
    *)
      let opt_order: unit -> state2 =
      fun () ->
      do{
        flush stdout;
        flush stderr;
        print_endline "----------------------------------------------------";
        print_endline "2.5: Make an Option Market Order:                   ";
        print_endline "----------------------------------------------------";
        let ( ord_id', _ ) =
          FA.send_order
            ~{ exchange    = exchange           }
            ~{ account     = acc5               }
            ~{ cfi_code    = cfi_code_opt       }
            ~{ symbol      = ( fst symbol_opt ) }
            ~{ security_id = ( snd symbol_opt ) }
            ~{ id_src      = id_src             }
            ~{ side        = side5              }
            ~{ price       = 0.0                }
            ~{ currency    = ccy                }
            ~{ qty         = qty5               }
            rconf
            e
        in
        OptionOrder1 ord_id'
      }
      in
      match msg with
      [
        FT.ExecReport rep when
          rep.FT.cl_order_id_8       = cord_id  &&
          rep.FT.orig_cl_order_id_8  = Some ord_id -> opt_order ()

      | FT.OrderCancelReject crej when
          crej.FT.orig_cl_order_id_9 = ord_id   &&
          crej.FT.cl_order_id_9      = cord_id     -> opt_order ()

      | _ -> state
      ]

    (*--------------------*)
  | OptionOrder1 ord_id ->
    (*--------------------*)
      match msg with
      [
        FT.ExecReport rep when rep.FT.cl_order_id_8 = ord_id ->
        do{
          (* Will make another option order, in order to move it later: *)
          flush stdout;
          flush stderr;
          print_endline "----------------------------------------------------";
          print_endline "2.6: Make an Option Limit Order:                    ";
          print_endline "----------------------------------------------------";
          let ( ord_id', _ ) =
            FA.send_order
             ~{ exchange    = exchange           }
             ~{ account     = acc6               }
             ~{ cfi_code    = cfi_code_opt       }
             ~{ symbol      = ( fst symbol_opt ) }
             ~{ security_id = ( snd symbol_opt ) }
             ~{ id_src      = id_src             }
             ~{ side        = side6              }
             ~{ price       = price6             }
             ~{ currency    = ccy                }
             ~{ qty         = qty6               }
             rconf
             e
          in
          OptionOrder2 ord_id'
        }

      | _ -> state
      ]

    (*--------------------*)
  | OptionOrder2 ord_id ->
    (*--------------------*)
      match msg with
      [
        FT.ExecReport rep when rep.FT.cl_order_id_8 = ord_id ->
        do{
          flush stdout;
          flush stderr;
          print_endline "----------------------------------------------------";
          print_endline "...  Move an Option Limit Order:                    ";
          print_endline "----------------------------------------------------";
          let ( cord_id, _ ) =
            FA.modify_order
              ~{ exchange  = exchange           }
              ~{ account   = acc6               }
              ~{ ord_id    = ord_id             }
              ~{ cfi_code  = cfi_code_opt       }
              ~{ symbol    = ( fst symbol_opt ) }
              ~{ side      = side6              }
              ~{ orig_qty  = qty6               }
              ~{ new_price = price7             }
              rconf
              e
          in
          OrderMove ord_id cord_id
        }

      | _ -> state
      ]

    (*--------------------*)
  | OrderMove ord_id cord_id ->
    (*--------------------*)
      match msg with
      [
        FT.ExecReport rep when
          rep.FT.cl_order_id_8       = cord_id  &&
          rep.FT.orig_cl_order_id_8  = Some ord_id -> state (* Infinite lp!  *)

      | FT.OrderCancelReject crej when
          crej.FT.orig_cl_order_id_9 = ord_id   &&
          crej.FT.cl_order_id_9      = cord_id     -> raise Exit

      | _ -> state
      ]
  ];

(*---------------------------------------------------------------------------*)
(* Run Test 2:                                                               *)
(*---------------------------------------------------------------------------*)
(* Create and run the Reactor: *)
value rconf2 = FA.new_reactor_conf
                 ~{ engines = [ e2 ]    }
                 ~{ time_cb = init      }
                 ~{ period  = time_out  }
                 ~{ main_cb = call_back };

FA.reactor rconf2 Init;

(* Explicitly finalise the engine to force LogOut: *)
FS.fix_finalise ~{ graceful=True } ~{ msg="Logging out of Test2" } e2;

Thread.delay 40.0;
exit 0;

