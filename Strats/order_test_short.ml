(* vim:ts=2:syntax=ocaml
*)
Printexc.record_backtrace True;

module FT    = Fix_types;
module FC    = Fix_configs;
module FE    = Fix_engine;
module FA    = Fix_api;
module TC    = Tests_common;

(*---------------------------------------------------------------------------*)
(* Parse the Config File:                                                    *)
(*---------------------------------------------------------------------------*)
value time_out = 0.001;
value
(
  ( _,
    o_config,
    exchange,
    currency,
    side,
    id_src,
    cfi_code,
    symbol_o,
    qty,
    price,
    _
  ),
  n_iters,
  debug,
  _
)
= TC.get_test_config ();

(*---------------------------------------------------------------------------*)
(* Create the Engine:                                                        *)
(*---------------------------------------------------------------------------*)
(* FIX Engine for submitting Orders: *)
value eo       = FA.mk_fix_engine ~{ debug=debug } o_config;

value mde_side = if side = FT.Buy then FT.Bid else FT.Offer;

(*---------------------------------------------------------------------------*)
(* State of the Main CallBack:                                               *)
(*---------------------------------------------------------------------------*)
type state =
[
  Init               of int                       (* The very initial state  *)
| OrderSent          of int and float and string  (* Sent a limit order      *)
| ModifySent1        of int and float and string and string
| ModifySent2        of int and float and string and string
| CancelSent         of int and float and string and string
                                                  (* Sent a Cancellation Req *)
]
with string_of;

(*---------------------------------------------------------------------------*)
(* "send_order":                                                             *)
(*---------------------------------------------------------------------------*)
value send_order:
  FA.fix_reactor_conf state -> FE.fix_engine -> int -> state =

fun rconf _e n ->
do{
  Thread.delay 5.0;

  let time0  = Unix.gettimeofday () in
  let ( ord_id, _ ) =
    FA.send_order
      ~{ exchange    = exchange         }
      ~{ cfi_code    = cfi_code         }
      ~{ symbol      = ( fst symbol_o ) }
      ~{ security_id = ( snd symbol_o ) }
      ~{ id_src      = id_src           }
      ~{ side        = side             }
      ~{ price       = price            }
      ~{ currency    = currency         }
      ~{ qty         = qty              }
      rconf
      eo
  in
  let price' = if side = FT.Buy then 0.98 *. price else 1.02 *. price in
  let ( mod_id, _ )  =
    FA.modify_order
      ~{ exchange  = exchange         }
      ~{ ord_id    = ord_id           }
      ~{ cfi_code  = cfi_code         }
      ~{ symbol    = ( fst symbol_o ) }
      ~{ side      = side             }
      ~{ orig_qty  = qty              }
      ~{ new_price = price'           }
      rconf
      eo
  in
  let price''  = if side = FT.Buy then 1.1 *. price' else 0.9 *. price' in
  let ( mod_id', _ ) =
    FA.modify_order
      ~{ exchange  = exchange         }
      ~{ ord_id    = mod_id           } (* Or ord_id? *)
      ~{ cfi_code  = cfi_code         }
      ~{ symbol    = ( fst symbol_o ) }
      ~{ side      = side             }
      ~{ orig_qty  = qty              }
      ~{ new_price = price''          }
      rconf
      eo
  in
  (* do{
    Printf.printf "\nORDER SENT @ t0: OrdID=%s\n%!" ord_id;
    OrderSent n time0 ord_id
  }
  *)
  ModifySent2 n time0 mod_id mod_id'
};

(*---------------------------------------------------------------------------*)
(* The Time-Driven Call-Back:                                                *)
(*---------------------------------------------------------------------------*)
value init: FA.fix_reactor_conf state -> state -> state =
fun rconf state ->
  match state with
  [ Init 0 -> send_order rconf eo 0
  | _      -> state
  ];

(*---------------------------------------------------------------------------*)
(* The Main Call-Back:                                                       *)
(*---------------------------------------------------------------------------*)
value call_back:
  FA.fix_reactor_conf state -> FE.fix_engine -> state -> FT.fix_msg -> state =

fun rconf _e state msg  ->
  (*--------------------*)
  (* "send_cancel_req": *)
  (*--------------------*)
  let send_cancel_req: float -> string -> int -> state =
  fun time0 ord_id n   ->
    let ( cord_id, _ ) =
      FA.cancel_order
        ~{ exchange    = exchange         }
        ~{ ord_id      = ord_id           }
        ~{ cfi_code    = cfi_code         }
        ~{ symbol      = ( fst symbol_o ) }
        ~{ security_id = ( snd symbol_o ) }
        ~{ id_src      = id_src           }
        ~{ side        = side             }
        ~{ orig_qty    = qty              }
        rconf
        eo
    in
    let time3    = Unix.gettimeofday ()     in
    do{
      Printf.printf "\nORDER CANCEL SENT @ %f for OrdID=%s, CancelID=%s\n%!"
                    ( time3 -. time0 ) ord_id cord_id;
      CancelSent n time0 ord_id cord_id
    }
  in
  match state with
  [
    (*--------------------*)
    Init             _ ->
    (*--------------------*)
      assert False

    (*--------------------*)
  | OrderSent n time0 ord_id ->
    (*--------------------*)
    (* Wait for the "ExecReport": *)
      match msg with
      [
        FT.ExecReport rep when
          rep.FT.cl_order_id_8  = ord_id      &&
          rep.FT.order_status_8 = FT.NewOrder &&
          rep.FT.exec_type_8    = FT.NewOrder ->

          let time1 = Unix.gettimeofday () in
          do{
            Printf.printf "\nORDER ACKNOWLEDGED @ t0+%f: OrdID=%s\n%s\n%!"
                          ( time1 -. time0 )ord_id( FT.string_of_fix_msg msg );

            (* Now modify the order: *)
            let price' = if side = FT.Buy then 0.98 *. price else 1.02 *. price
            in
            let ( mod_id, _ ) =
              FA.modify_order
                ~{ exchange  = exchange         }
                ~{ ord_id    = ord_id           }
                ~{ cfi_code  = cfi_code         }
                ~{ symbol    = ( fst symbol_o ) }
                ~{ side      = side             }
                ~{ orig_qty  = qty              }
                ~{ new_price = price'           }
                rconf
                eo
            in
            ModifySent1 n time0 ord_id mod_id
          }
      | _ -> state
      ]

    (*--------------------*)
  | ModifySent1 n time0 ord_id mod_id ->
    (*--------------------*)
      match msg with
      [
        FT.ExecReport _ ->
          (* Now send a cancellation req:   *)
          (* send_cancel_req time0 ord_id n *)
          (* Modify it again, so that it can be filled: *)
          let price'  = if side = FT.Buy then 1.1 *. price else 0.9 *. price in
          let ( mod_id', _ ) =
            FA.modify_order
              ~{ exchange  = exchange         }
              ~{ ord_id    = mod_id           } (* Or ord_id? *)
              ~{ cfi_code  = cfi_code         }
              ~{ symbol    = ( fst symbol_o ) }
              ~{ side      = side             }
              ~{ orig_qty  = qty              }
              ~{ new_price = price'           }
              rconf
              eo
          in
          ModifySent2 n time0 ord_id mod_id'

      | _ -> state
      ]

    (*--------------------*)
  | ModifySent2 _ _ _ _  -> state

    (*--------------------*)
  | CancelSent n time0 ord_id cord_id ->
    (*--------------------*)
      let time4 = Unix.gettimeofday ()  in
      match msg with
      [
        (* Cancellation Request Received: *)
        FT.ExecReport rep when
          rep.FT.cl_order_id_8      = cord_id          &&
          rep.FT.order_status_8     = FT.PendingCancel &&
          rep.FT.exec_type_8        = FT.PendingCancel &&
          rep.FT.orig_cl_order_id_8 = Some ord_id ->
        do{
          Printf.printf
            "\nORDER CANCEL PENDING @ t0+%f: OrdID=%s, CancelID=%s\n%s\n%!"
            ( time4 -. time0 ) ord_id cord_id ( FT.string_of_fix_msg msg );
          (* Stay in the same state: *)
          state
        }

      | FT.OrderCancelReject crej when
          crej.FT.orig_cl_order_id_9 = ord_id  &&
          crej.FT.cl_order_id_9      = cord_id ->
          match crej.FT.order_status_9 with
          [
            FT.Canceled   ->
            do{
              Printf.printf
                "\nORDER ALREADY CANCELLED:@ t0+%f: OrdID=%s\n%s\n%!"
                ( time4 -. time0 ) ord_id ( FT.string_of_fix_msg msg );
              if n >= n_iters-1
              then raise Exit
              else send_order rconf eo ( n+1 )
            }
          | FT.Filled     ->
            do{
              Printf.printf
                "\nTOO LATE: ORDER FILLED:@ t0+%f: OrdID=%s\n%s\n%!"
                ( time4 -. time0 ) ord_id ( FT.string_of_fix_msg msg );
              if n >= n_iters-1
              then raise Exit
              else send_order rconf eo ( n+1 )
            }
          | FT.PendingNew ->
            do{
              (* Unknown order? Invalid OrderID? *)
              Printf.printf
                "\nINVALID OrderID?@ t0+%f: OrdID=%s\n%s\n%!"
                ( time4 -. time0 ) ord_id ( FT.string_of_fix_msg msg );
              raise Exit (* XXX: This is an error! *)
            }
          | _ ->
            do{
              Printf.printf
                "\nORDER CANCEL REJECTED @ t0+%f: OrdID=%s, CancelID=%s\n%s\n%!"
                ( time4 -. time0 ) ord_id cord_id ( FT.string_of_fix_msg msg );
              (* Try to cancel again:         *)
              send_cancel_req time0 ord_id n
            }
          ]

        (* Order Really Cancelled: *)
      | FT.ExecReport rep when
          rep.FT.cl_order_id_8    = cord_id       &&
          rep.FT.order_status_8   = FT.Canceled   &&
          rep.FT.exec_type_8      = FT.Canceled   &&
          rep.FT.orig_cl_order_id_8 = Some ord_id ->
        do{
          Printf.printf "\nORDER CANCEL CONFIRMED @ t0+%f: OrdID=%s\n%s\n%!"
                        ( time4 -. time0 ) ord_id ( FT.string_of_fix_msg msg );
          if n >= n_iters-1
          then raise Exit
          else send_order rconf eo ( n+1 )
        }

      | _ -> state
      ]
  ];

(* Create the Reactor Config:            *)
value rconf   = FA.new_reactor_conf
                  ~{ engines = [ eo ]    }
                  ~{ time_cb = init      }
                  ~{ period  = time_out  }
                  ~{ main_cb = call_back };

(* Run the reactor in a separate thread: *)

value thr = Thread.create ( FA.reactor rconf ) ( Init 0 );
Thread.join thr;

exit 0;
