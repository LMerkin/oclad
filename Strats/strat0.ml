module FT = Fix_types;
module FC = Fix_configs;
module FA = Fix_api;
module GS = Gen_strat;

(*---------------------------------------------------------------------------*)
(* Instruments Traded:                                                       *)
(*---------------------------------------------------------------------------*)
value rts_instr =
{
  GS.i_exchange    = FT.FORTS;
  GS.i_cfi_code    = "FXXXXX";
  GS.i_symbol      = "RTS-9.10";
  GS.i_sec_id      = "RIU0";
  GS.i_id_src      = FT.Exchange_Symbol;
  GS.i_ccy         = FT.RUB;
  GS.i_conf_q      = FC.OSL_Quotes_All;
  GS.i_conf_o      = FC.RTS_Direct_Orders;
  GS.i_account     = ""
};

value gazp_instr =
{
  GS.i_exchange    = FT.FORTS;
  GS.i_cfi_code    = "FXXXXX";
  GS.i_symbol      = "GAZR-9.10";
  GS.i_sec_id      = "GZU0";
  GS.i_id_src      = FT.Exchange_Symbol;
  GS.i_ccy         = FT.RUB;
  GS.i_conf_q      = FC.OSL_Quotes_All;
  GS.i_conf_o      = FC.RTS_Direct_Orders;
  GS.i_account     = ""
};

value instrs0 = [| rts_instr; gazp_instr |];

value rts_symbol  = rts_instr.GS.i_symbol;
value gazp_symbol = gazp_instr.GS.i_symbol;

value alpha  = 0.9999;
value calpha = 1.0 -. alpha;

(*---------------------------------------------------------------------------*)
(* Utils:                                                                    *)
(*---------------------------------------------------------------------------*)
(* Finding an AOS by Symbol and Side:                                        *)

value find_order:
  GS.gstate float  -> string -> FT.side -> option GS.active_order_status =

fun s0 symbol side ->
  (* The Predicate closure: *)
  let pred =
    fun aos -> aos.GS.aos_symbol = symbol &&
               aos.GS.aos_side   = side
  in
  let   aoss = GS.find_aoss s0 pred in
  match aoss with
  [
    []      -> None (* Not found *)
  | [ aos ] -> Some aos
  | _       ->
      (* In this strategy, multiple orders with the same Symbol and Side cannot
         occur, as we only place an order when there are no current orders:
      *)
      assert False
  ];

(* Extracting the LATEST price from "aos_moves":                             *)
value get_newest_px: GS.active_order_status -> float =
fun aos ->
do{
  assert( aos.GS.aos_moves <> [] );
  GS.get_aosp_price ( List.hd ( List.rev aos.GS.aos_moves ) )
};

(*---------------------------------------------------------------------------*)
(* OnChange:                                                                 *)
(*---------------------------------------------------------------------------*)
value on_change: GS.uact float =
fun rconf _e s0 _msg ->
do{
  let spr_a   = ref ( GS.get_ustate s0 ) in
  let poss    = GS.get_poss   s0 in
  let md      = GS.get_mdata  s0 in

  let rts_md  = Hashtbl.find md  rts_symbol  in
  let gazp_md = Hashtbl.find md  gazp_symbol in

  let () = Printf.printf "===> rts_md=%s\n===> gazp_md=%s\n%!"
              ( GS.string_of_market_data rts_md  )
              ( GS.string_of_market_data gazp_md )
  in
  (* RTS curr Arrival Px:  *)
  let rts_last =
    if rts_md.GS.md_bids <> [| |] && rts_md.GS.md_asks <> [| |]
    then
       0.5 *. ( fst( rts_md.GS.md_bids.( 0 ) ) +.
                fst( rts_md.GS.md_asks.( 0 ) ) )
    else
       0.0
  in
  (* GAZP curr Arrival Px: *)
  let gazp_last =
    if gazp_md.GS.md_bids <> [| |] && gazp_md.GS.md_asks <> [| |]
    then
       0.5 *. ( fst( gazp_md.GS.md_bids.( 0 ) ) +.
                fst( gazp_md.GS.md_asks.( 0 ) ) )
    else
       0.0
  in
  (* Initialise spr_a if it is not initialised yet: *)
  if spr_a.val = infinity
  then
    if rts_last > 0.0 && gazp_last > 0.0
    then
      spr_a.val := rts_last -. 9.0 *. gazp_last
    else ()
  else
  (* GENERIC CASE:
     spr_a has been initialised to a real value:
  *)
  do{
    assert( rts_last > 0.0 && gazp_last > 0.0 );

    (* Compute the spread using Continuous Fraction scaling factors.
       Aently-activelternatively, could do a ratio of Arrival Pxs:
    ently-active*)
    let spr = rts_last -. 9.0 *. gazp_last in
    do{
      (* EMA of spread: *)
      spr_a.val := alpha *. spr_a.val +. calpha *. spr;

      (* RTS and GAZP positions: *)
      let rts_pos    = Hashtbl.find poss rts_symbol  in
      let gazp_pos   = Hashtbl.find poss gazp_symbol in

      (* XXX: Why does spr_target depend on rts_pos? -- This is for risk cont-
         rol, to prevent position build-up in RTS:
      *)
      let spr_target = spr_a.val -. 50.0 *. ( float_of_int rts_pos )       in

      (* We will quote RTS  at spread-adjusted prices;
         will buy/sell GAZP at marlet prices:
      *)
      (* Bid and Min px to sell 9 units of GAZP (corresp to 1 RTS): *)
      let ( gazp_bid, gazp_bid_min ) =
        GS.pxs_from_q ~{ order_q=( gazp_md.GS.md_bids ) } ~{ total_vol=9 } in

      (* Ask and Max px to buy  9 units of GAZP (corresp to 1 RTS): *)
      let ( gazp_ask, gazp_ask_max ) =
        GS.pxs_from_q ~{ order_q=( gazp_md.GS.md_asks ) } ~{ total_vol=9 } in

      (* Enlarged ently-activespread: *)
      let spr_bid = spr_target -. 150.0 in
      let spr_ask = spr_target +. 150.0 in

      (* Quote prices for RTS: Bid/Ask minus/plus the enlarged spread: *)
      let rts_bid = ceil ( ( spr_bid +. 9.0 *. gazp_bid ) /. 10.0 ) *. 10.0 in
      let rts_ask = floor( ( spr_ask +. 9.0 *. gazp_ask ) /. 10.0 ) *. 10.0 in

      (* ORDER MANAGEMENT: *)

      let gazp_qty = 9 * rts_pos + gazp_pos   in
      if  gazp_qty > 0
      then
        (* We got an excessive GASP position,  possibly because rts_pos has
           changed; bring it back to balance by selling gazp_qty at the Mkt
           Price:
        *)
        let   mb_gazp_sell_aos = find_order s0 gazp_symbol FT.Sell in
        match mb_gazp_sell_aos with
        [
          None ->
            (* No active GAZP sell orders -- place a new GAZP sell order: *)
            GS.send_limit_order
              ~{ symbol = gazp_symbol  }
              ~{ side   = FT.Sell      }
              ~{ price  = gazp_bid_min }
              ~{ qty    = gazp_qty     }
              rconf
              s0

        | Some gazp_sell_aos ->
            (* If a GAZP sell order currently exists, modify it if the new px
               is any different.  XXX: But maybe we need to modify the qty as
               well?
            *)
            let gazp_sell_px =  get_newest_px gazp_sell_aos in
            if  gazp_bid_min <> gazp_sell_px
            then
              GS.modify_order
                gazp_sell_aos
                ~{ new_price = gazp_bid_min }
                rconf
                s0
            else ()
        ]
      else
      if gazp_qty < 0
      then
        (* We got an insufficient GAZP position, possibly because rts_pos has
           changed;  bring it back to balance  by buying  gazp_qty at the Mkt
           Price:
        *)
        let   mb_gazp_buy_aos = find_order s0 gazp_symbol FT.Buy in
        match mb_gazp_buy_aos with
        [
          None ->
            (* No active GAZP buy order -- place a new GAZP buy order:  *)
            GS.send_limit_order
              ~{ symbol = gazp_symbol  }
              ~{ side   = FT.Buy       }
              ~{ price  = gazp_ask_max }
              ~{ qty    = (-gazp_qty)  }
              rconf
              s0

        | Some gazp_buy_aos ->
            (* If a GAZP buy order currently exists, modify it if the new px
               is any different. XXX: But maybe we need to modify the qty as
               well?
            *)
            let gazp_buy_px  =  get_newest_px gazp_buy_aos in
            if  gazp_ask_max <> gazp_buy_px
            then
              GS.modify_order
                gazp_buy_aos
                ~{ new_price = gazp_ask_max }
                rconf
                s0
            else ()
        ]
      else
      (* gazp_qty = 0, ie we are in a prefect balance;
         check RTS Buy and Sell:
      *)
      do{
        (* Any active RTS Buy orders? NB: They are quoted at off-market pxs,
           so will likeley be unfilled for a long while.
           Max RTS position allowed is +- 2 (XXX: why this limit?)
        *)
        let   mb_rts_buy_aos = find_order s0 rts_symbol FT.Buy in
        match mb_rts_buy_aos with
        [
          None ->
            if rts_pos < 2
            then
              (* Place a new limit RTS Buy order: *)
              GS.send_limit_order
                ~{ symbol = rts_symbol }
                ~{ side   = FT.Buy     }
                ~{ price  = rts_bid    }
                ~{ qty    = 1          }
                rconf
                s0
            else ()

        | Some rts_buy_aos ->
            (* There is already an active RTS Buy order.   Modify it if the
               "spreaded" price (rts_bid) has changed significatly compared
               to the currenly-active one (by more than 20 pts):
            *)
            let rts_buy_px = get_newest_px rts_buy_aos in
            if  abs_float( rts_bid -. rts_buy_px ) > 20.0
            then
              GS.modify_order
                rts_buy_aos
                ~{ new_price = rts_bid }
                rconf
                s0
            else ()
        ];

        (* Similarly, any active RTS Sell orders? *)
        let   mb_rts_sell_aos = find_order s0 rts_symbol FT.Sell in
        match mb_rts_sell_aos with
        [
          None ->
            if rts_pos > -2
            then
              (* Place a new limit RTS Sell order: *)
              GS.send_limit_order
                ~{ symbol = rts_symbol }
                ~{ side   = FT.Sell    }
                ~{ price  = rts_ask    }
                ~{ qty    = 1          }
                rconf
                s0
            else ()

        | Some rts_sell_aos ->
            (* There is already an active RTS Sell order.   Modify it if the
               "spreaded" price (rts_ask) has changed significantly compared
               to the currently-active one (by more than 20 pts):
            *)
            let rts_sell_px = get_newest_px rts_sell_aos in
            if  abs_float( rts_ask -. rts_sell_px ) > 20.0
            then
              GS.modify_order
                rts_sell_aos
                ~{ new_price = rts_ask }
                rconf
                s0
            else ()
        ]
      }
    }
  };
  (* The user state value to return: *)
  spr_a.val
};

(*---------------------------------------------------------------------------*)
(* Instantiate the Strategy:                                                 *)
(*---------------------------------------------------------------------------*)
value strat_closure =
  GS.mk_strat
    ~{ debug       = True      }
    ~{ engines     = []        }
    ~{ instrs      = instrs0   }
    ~{ user_cb     = on_change }
    ~{ init_ustate = infinity  };

(* Run it: *)
strat_closure ();

