(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                     Common Utils for Strategy Tests                       *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* Parsing the Command Line and the Config File:                             *)
(*---------------------------------------------------------------------------*)
type test_config =
(
  Fix_configs.fix_config_id *           (* FIX Config for Receiving  Quotes *)
  Fix_configs.fix_config_id *           (* FIX Config for Submitting Orders *)
  Fix_types.exchange  *
  Fix_types.currency  *
  Fix_types.side      *
  Fix_types.id_source *                 (* Source for SecurityIDs used      *)
  string       *                        (* CFI Code                         *)
  ( string * string ) *                 (* ( Symbol, SecID ) to Order       *)
  int          *                        (* Qty   ( shares or lots    )      *)
  float        *                        (* Price ( of a share or lot )      *)
  list ( string * string )              (* Extra Symbols / SecIDs to Subscr *)
);

(* "get_test_config":
   Returns ( test_config, n_iters, debug_flag, order_id ):
*)
value get_test_config: unit -> ( test_config * int * bool * string );

