(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                        "Generic Trading Strategy"                         *)
(*===========================================================================*)
(*===========================================================================*)
(* Data Types:                                                               *)
(*===========================================================================*)
module FT = Fix_types;
module FF = Fix_flds;
module FA = Fix_api;
module FC = Fix_configs;
module FE = Fix_engine;
module BL = Bolt.Level;

value mname = "GenStrat";

(* A Traded Instrument:                   *)
type instrument = 
{
  (* Instrument params proper:            *)
  i_exchange: FT.exchange;
  i_cfi_code: string;
  i_symbol  : string;
  i_sec_id  : string;
  i_id_src  : FT.id_source;
  i_ccy     : FT.currency;

  (* Instrument quoting and trading params. Different Engines ( as specified by
     their Configs ) can be provided for receiving Quotes and submitting Orders
     for this Instrument:
  *)
  i_conf_q  : FC.fix_config_id;
  i_conf_o  : FC.fix_config_id;
  i_account : string    (* "" for default *)
}
with string_of;

(* "market_data": for a given instrument, arrays of Bids and Asks as pairs
   ( Price, Qty ); Bids in the descending order, Asks in the ascending order:
*)
type market_data =
{
  md_bids: mutable array ( float * int );          (*  [| ( Price, Qty ) |]  *)
  md_asks: mutable array ( float * int )           (*  [| ( Price, Qty ) |]  *)
}
with string_of;

(* Status of a current ( not yet fulfilled ) order;  "aos_order_phase" is for
   BOTH Orders and Modification Reqs (because the latter are essentially like
   new orders), "aos_cancel_phase" is for Cancel Reqs.
*)
type aos_order_phase  =
[
  OrderSent       of string and FT.date_time and float
| OrderPending    of string and FT.date_time and float
| OrderPartFilled of string and FT.date_time and float
]
with string_of;
(* Args: OrdID, TimeStamp, Price *)

type aos_cancel_phase =
[
  CancelNone
| CancelSent    of string and FT.date_time               (* ReqID, TimeStamp *)
| CancelPending of string and FT.date_time               (* ReqID, TimeStamp *)
]
with string_of;

type active_order_status =
{
  aos_track   : string;           (* Unique ID for internal tracking         *)
  aos_symbol  : string;           (* Symbol being traded                     *)
  aos_side    : FT.side;
  aos_orig_qty: int;              (* Qty as in the original order, > 0       *)
  aos_rem_qty : mutable int;      (* Unfilled qty in [ 0 .. orig_qty ]       *)
  aos_moves   : mutable list aos_order_phase;
  aos_cancel  : mutable aos_cancel_phase
}
with string_of;

(* Semantics of "aos_moves" above: it relates to placing of the original order
   and subsequent price moves. Always non-[]. Head is the last confirmed order
   (unless it was just submitted), newer orders are in the tail of the list.
*)

(* The over-all state, including the user-defined polymorphic state. NB: hash
   tables are mutable by themselves, and will be updated in-place; user-level
   call- back is supposed to do the same, and return the same block ptr:
*)
type position = int;      (* Positive (Long) or Negative (Short) Qty         *)

type uact  'u =
  FA.fix_reactor_conf ( gstate 'u ) -> FE.fix_engine -> gstate 'u ->
  FT.fix_msg -> 'u
and
gstate 'u =
{
  (* Table of instruments used by the current strategy. Key: Symbol:         *)
  st_instrs:      Hashtbl.t string instrument;

  (* Info on the currently subscribed symbols. Key: Symbol; Value: ReqID of its
     subscription ( fully determined by the symbol itself ):
  *)
  st_subscrs:     Hashtbl.t string string;

  (* In order to be able to re-subscribe to all market data following a comm
     failure, we maintain the count of LogOn msgs received per each Engine.
     Key: ConfigID:
  *)
  st_logons:      Hashtbl.t FC.fix_config_id int;

  (* Quotation and Ordering Engines, per Symbol. In the most typical case, there
     will be only 1 Engine shared between all entries here. Key: Symbol:
  *)
  st_engines_o:   Hashtbl.t string FE.fix_engine;
  st_engines_q:   Hashtbl.t string FE.fix_engine;

  (* As above, but just lists of Engines:                                    *)
  st_engs_o_lst:  list FE.fix_engine;
  st_engs_q_lst:  list FE.fix_engine;
  st_engs_all:    list FE.fix_engine;

  (* Table of Market Data for the above Instruments.    Key: Symbol:         *)
  st_mdata :      Hashtbl.t string market_data;

  (* Table of Market Positions for completed orders ( the current securities
     portfolio ). Key: Symbol:
  *)
  st_poss  :      Hashtbl.t string position;

  (* Table of Active ( incomplete ) Orders. Key: OrderID:                    *)
  st_orders:      Hashtbl.t string active_order_status;

  (* Table of past ("dead") OrderIDs, needed only to avoid bogus err msgs:   *)
  st_dead:        Hashtbl.t string unit;

  (* User-defined extra state and the user-level call-back.   Currently, the
     user call-back is invoked only on market data update msgs; order status
     updates are entirely handled by the internals of this module:
  *)
  st_ustate:      mutable 'u;

  (* Extra User-Provided Call-Back:                                          *)
  st_uact  :      uact    'u
};

(*===========================================================================*)
(* FIX API Extensions:                                                       *)
(*===========================================================================*)
(* GS API functions also update the gstate:                                   *)

(*---------------------------------------------------------------------------*)
(* Access to parts of the State from the user call-back:                     *)
(*---------------------------------------------------------------------------*)
value get_mdata:          gstate 'u -> Hashtbl.t string market_data =
fun s0 ->
  s0.st_mdata;

value get_active_orders:  gstate 'u -> Hashtbl.t string active_order_status =
fun s0 ->
  s0.st_orders;

value get_poss:           gstate 'u -> Hashtbl.t string position =
fun s0 ->
  s0.st_poss;

value get_ustate:         gstate 'u -> 'u =
fun s0 ->
  s0.st_ustate;

(* Extracting Engines, e.g. for using them in building another strategy, to pre-
   vent multiple connections to the same Server(s):
*)
value get_engines:        gstate 'u -> list FE.fix_engine =
fun s0 ->
  s0.st_engs_all;

(*---------------------------------------------------------------------------*)
(* "pxs_from_q":                                                             *)
(*---------------------------------------------------------------------------*)
(* Extracting info from Bids/Asks order queues (Arg1).The function goes from
   the top of the queue into the depth and picks up the volume at each price
   level until the whole volume given by Arg2 is accommodated;   returns the
   AvgPrice and the Min/Max Price at which the instrument can be Sold/Purcsd
   ( that Min/Max price corresponds to the deepest queue level used ):
*)
value pxs_from_q:
    ~order_q: array ( float * int ) -> ~total_vol: int -> ( float * float ) =

fun ~{ order_q } ~{ total_vol }     ->
  let len = Array.length order_q  in

  let rec traverse_q:  int -> int -> float -> float -> ( float * float ) =
  fun cpos rem_vol cum_px mm_px ->
    if rem_vol <= 0
    then
      (* All done. NB: rem_vol < 0 is only possible if total_vol < 0 in which
         case the algorith exits immediately at the 1st step:
      *)
      let av_px =
        if   total_vol > 0
        then
        do{
          assert( rem_vol = 0 );
          cum_px /. ( float_of_int total_vol )
        }
        else 0.0
      in
      ( av_px, mm_px )
    else
    if cpos >= len
    then
      (* Insufficient queue capacity: *)
      ( 0.0, 0.0 )
     else
      (* Price and vol at "cpos":     *)
      let ( cpx, cvol ) = order_q.( cpos ) in
      (* Get as much as we can:       *)
      do{
        assert( cvol > 0 && rem_vol > 0 );
        let dvol     = min cvol rem_vol   in
        let rem_vol' = rem_vol - dvol     in

        let cum_px'  = cum_px +. ( float_of_int dvol ) *. cpx in
        traverse_q ( cpos+1 ) rem_vol' cum_px' cpx
     }
  in
  traverse_q 0 total_vol 0.0 0.0;

(*---------------------------------------------------------------------------*)
(* "get_aosp_ord_id":                                                        *)
(* "get_aosp_dt":                                                            *)
(* "get_aosp_price":                                                         *)
(* "get_aosp_3":                                                             *)
(*---------------------------------------------------------------------------*)
value get_aosp_ord_id: aos_order_phase -> string =
fun
[
  OrderSent       ord_id _ _  -> ord_id
| OrderPending    ord_id _ _  -> ord_id
| OrderPartFilled ord_id _ _  -> ord_id
];

value get_aosp_dt:     aos_order_phase -> FT.date_time =
fun
[
  OrderSent       _ dt _      -> dt
| OrderPending    _ dt _      -> dt
| OrderPartFilled _ dt _      -> dt
];

value get_aosp_price:  aos_order_phase -> float =
fun aosp ->
  let price =
    match aosp with
    [
      OrderSent       _ _ px    -> px
    | OrderPending    _ _ px    -> px
    | OrderPartFilled _ _ px    -> px
    ]
  in
  do{
    assert( price > 0.0 );
    price
  };

value get_aosp_3:     aos_order_phase -> ( string * FT.date_time * float ) =
fun
[
  OrderSent           ord_id dt px -> ( ord_id, dt, px )
| OrderPending        ord_id dt px -> ( ord_id, dt, px )
| OrderPartFilled     ord_id dt px -> ( ord_id, dt, px )
];

(*---------------------------------------------------------------------------*)
(* "log_aos_update":                                                         *)
(*---------------------------------------------------------------------------*)
(* TODO: logging into a DB:                                                  *)

value log_aos_update:
  FE.fix_engine -> gstate 'u -> active_order_status -> unit =

fun   e s0 aos ->
do{
  assert( aos.aos_moves <> [] );
  let ord_id = get_aosp_ord_id ( List.hd aos.aos_moves ) in
  do{
    assert( ( Hashtbl.find s0.st_orders ord_id ) == aos );   (* NB: (==) !!! *)
    if e.FE.conf.FC.debug
    then
      FE.log ( Some e ) mname BL.DEBUG
        [ Printf.sprintf "Active Order Status Update:\n%s"
        ( string_of_active_order_status aos ) ]
    else ()
  }
};

(*---------------------------------------------------------------------------*)
(* "get_latest_ord_id":                                                      *)
(*---------------------------------------------------------------------------*)
value get_latest_ord_id: active_order_status -> string =
fun aos ->
  let   rmoves = List.rev aos.aos_moves in
  match rmoves with
  [
    [ m :: _ ] -> get_aosp_ord_id m
  | []         -> assert False                  (* This list cannot be empty *)
  ];

(*---------------------------------------------------------------------------*)
(* "find_aoss":                                                              *)
(*---------------------------------------------------------------------------*)
(* Search for AOS(es) satisfying a given binary predicate:                   *)

value find_aoss:
  gstate 'u -> ( active_order_status -> bool ) -> list active_order_status =

fun s0 pred ->
  Hashtbl.fold
  (
    fun _ aos curr ->
      if pred aos
      then  [ aos :: curr ]
      else  curr
  )
  s0.st_orders [];
                  

(*---------------------------------------------------------------------------*)
(* "send_limit_order":                                                       *)
(*---------------------------------------------------------------------------*)
(* NB: Market orders are currently not supported by the Generic Strategy:    *)

value send_limit_order:
  ~symbol:        string            ->
  ~side:          FT.side           ->
  ~price:         float             ->
  ~qty:           int               ->
  ?time_in_force: FT.time_in_force  ->
  FA.fix_reactor_conf ( gstate 'u ) ->
  ( gstate 'u )                     ->
  unit                              =

fun ~{ symbol } ~{ side } ~{ price } ~{ qty } ?{ time_in_force=FT.Day }
    rconf s0   ->
try
  let e    =  Hashtbl.find s0.st_engines_o symbol in
  if price <= 0.0
  then
    (* price <= 0.0 would mean a market order, but this is not allowed here: *)
    FE.log ( Some e ) mname BL.ERROR
      [ Printf.sprintf "send_limit_order: Invalid Price=%f" price ]
  else
    let t      = Hashtbl.find s0.st_instrs symbol in
    let ( ord_id, dt ) =
      FA.send_order
        ~{ exchange      = t.i_exchange  }
        ~{ account       = t.i_account   }
        ~{ cfi_code      = t.i_cfi_code  }
        ~{ symbol        = symbol        }
        ~{ security_id   = t.i_sec_id    }
        ~{ id_src        = t.i_id_src    }
        ~{ side          = side          }
        ~{ price         = price         }
        ~{ currency      = t.i_ccy       }
        ~{ qty           = qty           }
        ~{ time_in_force = time_in_force }
        rconf
        e
    in
    (* Create and attach a New Active Order Status:  *)
    let new_aos =
    {
      aos_track     = FE.new_req_id ();
      aos_symbol    = symbol;
      aos_side      = side;
      aos_orig_qty  = qty;
      aos_rem_qty   = qty;
      aos_moves     = [ OrderSent ord_id dt price ];
      aos_cancel    = CancelNone
    }
    in
    do{
      assert( t.i_symbol = symbol );
      assert( not( Hashtbl.mem s0.st_orders ord_id ) );

      Hashtbl.add s0.st_orders ord_id new_aos;
      log_aos_update e s0 new_aos
    }
with
[ Not_found ->
    FE.log None mname BL.ERROR
      [ "send_limit_order: Unconfigured Symbol="; symbol ]
];

(*---------------------------------------------------------------------------*)
(* "cancel_order":                                                           *)
(*---------------------------------------------------------------------------*)
value cancel_order:
  active_order_status                ->
  FA.fix_reactor_conf ( gstate 'u )  ->
  ( gstate 'u )                      ->
  unit                               =

fun aos rconf s0                     ->
  let symbol  = aos.aos_symbol                      in
  let e       = Hashtbl.find s0.st_engines_o symbol in
  let t       = Hashtbl.find s0.st_instrs    symbol in
  let ord_id  = get_latest_ord_id  aos              in
  let ( cord_id, dt ) =
    FA.cancel_order
      ~{ exchange    = t.i_exchange     }
      ~{ account     = t.i_account      }
      ~{ ord_id      = ord_id           }
      ~{ cfi_code    = t.i_cfi_code     }
      ~{ symbol      = symbol           }
      ~{ security_id = t.i_sec_id       }
      ~{ id_src      = t.i_id_src       }
      ~{ side        = aos.aos_side     }
      ~{ orig_qty    = aos.aos_orig_qty }
      rconf
      e
  in
  do{
    assert ( t.i_symbol = symbol );
    (* Update the Active Order Status: *)
    aos.aos_cancel := CancelSent cord_id dt;
    log_aos_update e s0 aos
  };

(*---------------------------------------------------------------------------*)
(* "modify_order":                                                           *)
(*---------------------------------------------------------------------------*)
(* Currently, only the Price can be modified, not the Qty or other fields:   *)

value modify_order:
  active_order_status                ->
  ~new_price:  float                 ->
  FA.fix_reactor_conf ( gstate 'u )  ->
  ( gstate 'u )                      ->
  unit                               =

fun aos ~{ new_price } rconf s0      ->
  let symbol  = aos.aos_symbol                      in
  let e       = Hashtbl.find s0.st_engines_o symbol in
  let t       = Hashtbl.find s0.st_instrs symbol    in
  let ord_id  = get_latest_ord_id  aos              in
  let ( mord_id, dt ) =
    FA.modify_order
      ~{ exchange  = t.i_exchange     }
      ~{ account   = t.i_account      }
      ~{ ord_id    = ord_id           }
      ~{ cfi_code  = t.i_cfi_code     }
      ~{ symbol    = symbol           }
      ~{ side      = aos.aos_side     }
      ~{ orig_qty  = aos.aos_orig_qty }
      ~{ new_price = new_price        }
      rconf
      e
  in
  do{
    assert ( t.i_symbol = symbol );
    (* Update the Active Order Status: *)
    aos.aos_moves := aos.aos_moves @ [ OrderSent mord_id dt new_price ];
    log_aos_update e s0 aos
  };

(*===========================================================================*)
(* Updating Market Data:                                                     *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "check_md_entries":                                                       *)
(*---------------------------------------------------------------------------*)
(* Returns ( SortedFlag, GapPos ), where "GapPos" is the length of the "OK"
   array segment (before the first gap in the position numbers); so 0 means
   that the array is no good at all,  while if "GapPos=ArrayLen", the whole
   array is OK:
*)
value check_md_entries:
  array ( float * int * int ) -> ~ascend:bool -> ( bool * int ) =
  (*      Price,  Size, Idx                      Sorted,  OK_Len             *)

fun en3s ~{ ascend } ->
  let sorted   = ref True                                 in
  let ok_len   = ref 0                                    in
  let pr_price = ref ( if ascend then 0.0 else infinity ) in
  do{
    Array.iteri
    (
      fun i ( price, _, idx ) ->
      do{
        if idx = i+1
        then ok_len.val := idx
        else ();

        if (       ascend   && ( price <= pr_price.val ) ) ||
           ( ( not ascend ) && ( price >= pr_price.val ) )
        then sorted.val := False
        else ();

        pr_price.val := price
      }
    )
    en3s;
    (* The resulting flags:  *)
    ( sorted.val, ok_len.val )
  };

(*---------------------------------------------------------------------------*)
(* "sort_md_entries":                                                        *)
(*---------------------------------------------------------------------------*)
(* Sorts the entries array if necessary (and possible), and returns the correct
   (possibly shortened) array of pairs  ( Price, Size ):
*)
value sort_md_entries:
  array ( float * int * int ) -> ~ascend:bool -> array ( float * int ) =
  (*      Price,  Size, Idx                              Price,  Size        *)
                       
fun en3s ~{ ascend } ->
  let   len = Array.length en3s in
  match check_md_entries   en3s ~{ ascend=ascend } with
  [
    ( True,  ok_len ) ->
      (* Sorted, with or without gaps: return the projection of the "OK" part
         of the array:
      *)
      Array.init ok_len
        ( fun i -> let ( price, size, _ ) = en3s.( i ) in ( price, size ) )

  | ( False, ok_len ) when ok_len = len  ->
    (* Not actually sorted by price, but no gaps in position numbers  (XXX: is
       it actually possible -- this means the pos numbers do not correspond to
       ascneding or descending prices?).  So all data are present, just do the
       projection and sort the resulting array:
    *)
    let en2s = Array.map ( fun ( price, size, _ ) -> ( price, size ) ) en3s in
    let ord2: ( float * int ) -> ( float * int  ) -> int =
      if ascend
      then fun ( p1, _ ) ( p2, _ ) -> compare p1 p2
      else fun ( p1, _ ) ( p2, _ ) -> compare p2 p1
    in
    do{
      Array.sort ord2 en2s;
      en2s
    }

  | ( False, ok_len ) when ok_len <> len ->
    (* Not sorted, and with gaps -- but gaps are likely to be due to the array
       being unsorted, so sort it first:
    *)
    let ord3: ( float * int * int ) -> ( float * int * int ) -> int =
      if ascend
      then fun ( p1, _, _ ) ( p2, _, _ ) -> compare p1 p2
      else fun ( p1, _, _ ) ( p2, _, _ ) -> compare p2 p1
    in
    do{
      (* XXX: Side-effect: the following sorts the arg array in-place!       *)
      Array.sort ord3 en3s;

      (* Now try again:  *)
      match check_md_entries en3s ~{ ascend=ascend } with
      [
        ( True, ok_len ) ->
          (* Do the projection, exactly as above: *)
          Array.init ok_len
            ( fun i -> let ( price, size, _ ) = en3s.( i ) in ( price, size ) )

      | _ ->
          (* It MUST be sorted at this point!     *)
          assert False
      ]
    }

  | _ ->  assert False (* Not reached *)
  ];

(*---------------------------------------------------------------------------*)
(* "from_option":                                                            *)
(*---------------------------------------------------------------------------*)
value from_option: option 'a -> 'a =
fun
[
  Some a -> a
| None   -> raise Not_found
];

(*===========================================================================*)
(* "update_market_data":                                                     *)
(*===========================================================================*)
(* NB: The gstate is mutable, and is updated in-place -- for efficiency:     *)

value update_market_data:
  FE.fix_engine -> FT.market_data -> gstate 'u -> unit =

fun e md s0 ->
try
  (* Update the market data in the gstate. If, somehow, the data belongs to an
     instrument which is not in the subscribed list -- ignore it:
  *)
  let symbol = md.FT.symbol_W in
  let eq = Hashtbl.find s0.st_engines_q symbol in
  let t  = Hashtbl.find s0.st_instrs    symbol in   (* May raise Not_found  *)
  if  e                     != eq                        ||
      ( Some t.i_exchange ) <> md.FT.security_exchange_W ||
             t.i_symbol     <> symbol                    ||
      ( Some t.i_sec_id   ) <> md.FT.security_id_W       ||
      ( Some t.i_id_src   ) <> md.FT.id_source_W
  then
    (* In our list, we have an instrument with that key ( Symbol ), but it is
       actually different from the one received  -- so ignore the latter  and
       produce a warning:
    *)
    FE.log ( Some e ) mname BL.WARN
      [ "Improper Market Data received:\n";
        Printf.sprintf
          "Engine=%s, Symbol=%s, Exchange=%s, SecID=%s, IDSrc=%s:\n%s"
          ( FC.string_of_fix_config_id eq.FE.conf.FC.config_id )
          symbol     ( FT.string_of_exchange  t.i_exchange     )
          t.i_sec_id ( FT.string_of_id_source t.i_id_src       )
          ( FT.string_of_market_data md )
      ]
  else
    (* Verify that Bids and Asks are sorted properly in  the MarketData msg  we
       got, and if not, sort them. The Bids and Asks are constructed as append-
       able lists initially,  as we do not know how many useful entries we will
       get:
    *)
    let ( bids, asks ) =
      Array.fold_left
      (
        fun ( curr_bids, curr_asks ) mde ->
          (* Only accept entries with a known Price, Size and PosNum:        *)
          if  mde.FT.md_entry_px_MDE      = FT.n_a_price ||
              mde.FT.md_entry_size_MDE    = None         ||
              mde.FT.md_entry_pos_num_MDE = None
          then
            ( curr_bids, curr_asks )    (* Ignore this entry!                *)
          else
            let price = mde.FT.md_entry_px_MDE                  in
            let size  = from_option mde.FT.md_entry_size_MDE    in
            let idx   = from_option mde.FT.md_entry_pos_num_MDE in
            let en3   = ( price, size, idx )                    in
            match mde.FT.md_entry_type_MDE with
            [
              FT.Bid   -> ( [ en3 :: curr_bids ], curr_asks )
            | FT.Offer -> ( curr_bids, [ en3 :: curr_asks ] )
            | _        -> ( curr_bids, curr_asks )
            ]             (* Unlikely: we subscribe to Bids and Asks only    *)
      )
      ( [], [] ) md.FT.md_entries_W
    in
    (* Create arrays out of "bid" and "ask" lists:    *)
    let bids2 = Array.of_list ( List.rev bids )         in
    let asks2 = Array.of_list ( List.rev asks )         in

    (* Verify the arrays and sort them if necessary:  *)
    let bids3 = sort_md_entries bids2 ~{ ascend=False } in
    let asks3 = sort_md_entries asks2 ~{ ascend=True  } in

    (* Actually update the market data in the gstate. NB: "Hashtbl.find" retrns
       ( actually a ptr to ) a mutable record, so we can just update that rec
       without explicitly storing it back in "s0":
    *)
    do{
        assert( Hashtbl.mem s0.st_mdata symbol );

        let curr_md = Hashtbl.find s0.st_mdata symbol in
        do{
          if Array.length bids3 > 0
          then curr_md.md_bids := bids3
          else ();

          if Array.length asks3 > 0
          then curr_md.md_asks := asks3
          else ()
        };

      (* Print out the updated market data, for debugging purposes:          *)
      if e.FE.conf.FC.debug
      then
        FE.log ( Some e ) mname BL.DEBUG
          [ "Market Data Updated:\n";
            string_of_market_data ( Hashtbl.find s0.st_mdata symbol ) ]
      else ()
    }
with
[ Not_found ->
  (* Non-subscribed datum received:                                          *) 
  FE.log ( Some e ) mname BL.WARN
    [ "Received Market Data for a non-subscribed Symbol="; md.FT.symbol_W;
      "\n"; FT.string_of_market_data md ]
];

(*===========================================================================*)
(* Some Utils:                                                               *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "log_unexpected_rep":                                                     *)
(*---------------------------------------------------------------------------*)
value log_unexpected_rep:
  FE.fix_engine -> active_order_status -> FT.exec_report -> unit =

fun e aos rep   ->
  FE.log ( Some e ) mname BL.WARN
         [ "Unexpected ExecReport:\n"; FT.string_of_exec_report rep;
           "ActiveOrderStatus    :\n"; string_of_active_order_status aos
         ];

(*---------------------------------------------------------------------------*)
(* "check_engine":                                                           *)
(*---------------------------------------------------------------------------*)
value check_engine: FE.fix_engine -> gstate 'u -> string -> bool =
fun e s0 symbol ->
  let ok =
    try
      let  eo = Hashtbl.find s0.st_engines_o symbol in
      e == eo
    with
      [ Not_found -> False ]
  in
  do{
    if not ok
    then
      FE.log ( Some e ) mname BL.ERROR
        [ Printf.sprintf "Improper Engine=%s for Symbol=%s"
            ( FC.string_of_fix_config_id e.FE.conf.FC.config_id ) symbol
        ]
    else ();
    ok
  };

(*---------------------------------------------------------------------------*)
(* "get_rep_ords":                                                           *)
(*---------------------------------------------------------------------------*)
(* NB: FIX.4.2 uses "PartFilled" and "Filled", FIX.4.4 uses "Traded", but it'u
   OK to consider them here in a single match. Returns ( OrdID, COrdID ):
*)
value get_rep_ords:
  FE.fix_engine -> FT.exec_report -> option ( string * string ) =

fun e rep ->
  match rep.FT.exec_type_8 with
  [
    FT.NewOrder
  | FT.PartFilled
  | FT.Filled
  | FT.Traded
  | FT.DoneForDay
  | FT.Expired
  | FT.Rejected ->
      Some ( rep.FT.cl_order_id_8, "" )

  | FT.PendingCancel
  | FT.PendingReplace
  | FT.Canceled
  | FT.Replaced ->
      Some ( from_option rep.FT.orig_cl_order_id_8, rep.FT.cl_order_id_8 )

  | _ ->
    do{
      FE.log ( Some e ) mname BL.ERROR
        [ "Cannot extract OrdID/COrdID from ExecReport:\n";
          ( FT.string_of_exec_report rep )
        ];
      None
    }
  ];

(*---------------------------------------------------------------------------*)
(* "get_aos":                                                                *)
(*---------------------------------------------------------------------------*)
(* Extracts the Active Order Status by the OrdID:                            *)

value get_aos:
  option FE.fix_engine -> gstate 'u -> string -> option active_order_status =

fun mbe s0 ord_id ->
try
  let aos = Hashtbl.find s0.st_orders ord_id in
  (* Do further checks: "ord_id" must always correspond to the head of the
     "aos_moves" list:
  *)
  if aos.aos_moves = [] ||
     ( get_aosp_ord_id ( List.hd aos.aos_moves ) ) <> ord_id
  then
  do{
    FE.log mbe mname BL.ERROR
      [ Printf.sprintf "Inconsistent ActiveOrderStatus for OrdID=%s:\n%s"
          ord_id ( string_of_active_order_status aos )
      ];
    None
  }
  else
    Some aos  (* Success! *)
with
[ Not_found ->
  do{
    (* If this "ord_id" is listed as "dead", it'u OK:                    *)
    if Hashtbl.mem s0.st_dead ord_id
    then
      FE.log mbe mname BL.DEBUG [ "Skipping \"dead\" OrdID="; ord_id ]
    else
      FE.log mbe mname BL.ERROR
        [ "ActiveOrderStatus not found for OrdID="; ord_id ];
    None
  }
];

(*---------------------------------------------------------------------------*)
(* "get_aos_from_rep":                                                       *)
(*---------------------------------------------------------------------------*)
(* Returns option ( ActiveOrderStatus, OrdID, COrdID ):                      *)

value get_aos_from_rep:
  FE.fix_engine -> gstate 'u -> FT.exec_report     ->
  option ( active_order_status * string * string ) =

fun e s0 rep ->
  match get_rep_ords e rep with
  [
    Some ( ord_id, cord_id ) ->
      match get_aos ( Some e ) s0 ord_id with
      [
        Some aos ->
          (* Extra checks to verify that the ExecReport received really corresp
             to a given Engine and ActiveOrderStatus:
          *)
          if ( check_engine e s0 rep.FT.symbol_8 )   &&
             aos.aos_side   = rep.FT.side_8          &&
             aos.aos_symbol = rep.FT.symbol_8        &&
             ( rep.FT.order_qty_8        = None      ||
               rep.FT.order_qty_8        = Some aos.aos_orig_qty
             )                                       &&
             ( rep.FT.orig_cl_order_id_8 = None      ||
               rep.FT.orig_cl_order_id_8 = Some ord_id
             )                                       &&
             ( cord_id = ""                          ||
               rep.FT.cl_order_id_8      = cord_id
             )
          then
            Some ( aos, ord_id, cord_id )
          else
          do{
            log_unexpected_rep e aos rep;
            None
          }
      | None -> None
      ]

  | None -> None
  ];

(*---------------------------------------------------------------------------*)
(* "ver42":                                                                  *)
(*---------------------------------------------------------------------------*)
(* Currently, only versions 4.2 and 4.4 are supported:                       *)

value ver42: FE.fix_engine -> bool =
fun  e ->
  if e.FE.conf.FC.fix_version = "FIX.4.2"
  then
    True
  else
  do{
    assert( e.FE.conf.FC.fix_version = "FIX.4.4" );
    False
  };

(*---------------------------------------------------------------------------*)
(* "book_position":                                                          *)
(*---------------------------------------------------------------------------*)
value book_position:
  FE.fix_engine -> gstate 'u -> active_order_status -> int -> float -> unit =

fun e s0 aos filled_qty px ->
do{
  assert( filled_qty > 0 );
  let dpos =
    match aos.aos_side with
    [ FT.Buy  ->   filled_qty
    | FT.Sell -> - filled_qty ]
  in
  let symbol = aos.aos_symbol   in
  do{
    assert( Hashtbl.mem s0.st_poss symbol );

    (* NB: "prev" may be 0: *)
    let prev = Hashtbl.find s0.st_poss symbol  in
    let curr = prev + dpos                     in
    do{
      Hashtbl.replace s0.st_poss symbol curr;
      FE.log ( Some e ) mname BL.INFO
        [ Printf.sprintf "BOOKED: %s -> %d @ %f; pos=%d" symbol dpos px curr ]
    }
  }
  (* TODO: PERSISTENCE and the PRICE at which it was booked!                 *)
};

(*---------------------------------------------------------------------------*)
(* "ackn_move":                                                              *)
(*---------------------------------------------------------------------------*)
value ackn_move:
  active_order_status -> string -> string -> FT.date_time -> bool =

fun aos ord_id cord_id dt ->
  let rec scan_moves_rec  =
  fun before tail ->
    match tail with
    [ [] -> False (* Done traversing, nothing more to update  *)

    | [ m :: ms ] ->
      let mord_id = get_aosp_ord_id m in
      if  mord_id = cord_id
      then
        (* This is a candidate entry to update:               *)
        match m with
        [
          OrderSent _ mdt mpx ->
            do{
              (* Yes, we can really update it:                *)
              assert( ( FF.compare_dts dt mdt ) >= 0 );
              let m'     = OrderPending cord_id dt mpx  in
              let moves' = before @ [ m' :: ms ]        in
              do{
                aos.aos_moves := moves';
                True
              }
          }
        | _ ->
            False (* Inconsistency -- cannot update           *)
        ]
      else
        (* Continue traversing:                               *)
        let before' = before @ [ m ] in   (* XXX: not very efficient!    *)
        scan_moves_rec before' ms
    ]
  in
  do{
    assert( ord_id <> cord_id );
    match aos.aos_moves with
    [
      [ m :: ms ] ->
        (* The original "ord_id" must be at the head of the list. Scan "ms" for
           "cord_id" and change its status from "OrderSent" to "Order Pending":
        *)
        do{
          assert( ( get_aosp_ord_id m ) = ord_id );
          scan_moves_rec [ m ] ms
        }
    | _ ->
        assert False  (* Impossible *)
    ]
  };

(*---------------------------------------------------------------------------*)
(* "shift_moves":                                                            *)
(*---------------------------------------------------------------------------*)
value shift_moves:
  FE.fix_engine -> gstate 'u -> active_order_status -> string -> string ->
  FT.date_time  -> bool =

fun e s0 aos ord_id cord_id dt ->
do{
  (* Memoise the original "root" from which we got the "ord_id":             *)
  assert( aos.aos_moves <> [] && ord_id <> cord_id &&
          get_aosp_ord_id ( List.hd aos.aos_moves ) = ord_id );

  (* Drop all "aos_moves" entries until "cord_id" is encountered:            *)
  let rec drop_moves_rec: list aos_order_phase -> list aos_order_phase =
    fun
    [ [] -> []

    | [ m :: ms ]  as moves ->
        let mord_id = get_aosp_ord_id m in
        if  mord_id = cord_id
        then
          (* Found the 1st entry which is NOT to be dropped:                 *)
          moves
        else
        do{
          (* "m" is to be dropped, so remove its ID from the fwding map and put
             it also into the local "dead" table, in case if any msgs with this
             ID have already been fwded:
          *)
          FA.remove_from_fwding_map e mord_id;
          Hashtbl.replace  s0.st_dead mord_id ();

          drop_moves_rec ms
        }
    ]
  in
  let   root   = List.hd aos.aos_moves          in
  let   moves' = drop_moves_rec aos.aos_moves   in
  match moves' with
  [
    [] -> False  (* "cord_id" not found! *)

  | [ m :: ms ] ->
      let ( mord_id, mdt, mpx ) = get_aosp_3 m  in

      (* The new root "m" must have the same ctor as the original one, AND
         the new time stamp ( "dt" ):
      *)
      let new_root =
        match m with
        [
          OrderSent       _ _ _ ->
            (* Overwrite "m" phase by "root" phase if the latter is higher:  *)
            match root with
            [
              OrderSent       _ _ _ -> OrderSent       mord_id dt mpx
            | OrderPending    _ _ _ -> OrderPending    mord_id dt mpx
            | OrderPartFilled _ _ _ -> OrderPartFilled mord_id dt mpx
            ]
        | OrderPending    _ _ _ ->
            match root with
            [
              OrderSent       _ _ _ -> OrderPending    mord_id dt mpx
            | OrderPending    _ _ _ -> OrderPending    mord_id dt mpx
            | OrderPartFilled _ _ _ -> OrderPartFilled mord_id dt mpx
            ]
        | OrderPartFilled _ _ _ ->
            (* This can never happen -- "OrderPartFilled" is only put at the
               "root", so it cannot occur at "m":
            *)
            assert False
        ]
      in
      do{
        assert( mord_id = cord_id && FF.compare_dts dt mdt >= 0 );

        (* Install the truncated "moves'" with "new_root":                   *)
        aos.aos_moves := [ new_root :: ms ];

        (* Re-attach this "aos" to "aos_orders":                             *)
        Hashtbl.remove s0.st_orders ord_id;
        assert( not( Hashtbl.mem s0.st_orders cord_id ) );
        Hashtbl.add    s0.st_orders cord_id aos;
        True
      }
  ]
};

(*---------------------------------------------------------------------------*)
(* "remove_from_active":                                                     *)
(*---------------------------------------------------------------------------*)
value remove_from_active:
  FE.fix_engine -> gstate 'u -> active_order_status -> string -> string ->
  unit =

fun e s0 aos ord_id reason   ->
do{
  Hashtbl.remove s0.st_orders ord_id;

  (* Also remove all associated ord_ids from the fwding map, and put them into
     the local "dead" table as well:
  *)
  List.iter
  (
    fun m ->
      let mord_id = get_aosp_ord_id m in
      do{
        FA.remove_from_fwding_map e mord_id;
        Hashtbl.replace  s0.st_dead mord_id ()
      }
  )
  aos.aos_moves;

  FE.log ( Some e ) mname BL.INFO
    [ Printf.sprintf "Order %s, removed from active: OrdID=%s" reason ord_id ]
};

(*---------------------------------------------------------------------------*)
(* "check_filled":                                                           *)
(*---------------------------------------------------------------------------*)
value check_filled:   bool -> FT.order_status -> int -> bool =
fun is42 exec_type rem_qty ->
do{
  assert ( rem_qty >= 0 );
  if is42
  then
    match exec_type with
    [
      FT.Filled     -> rem_qty = 0
    | FT.PartFilled -> rem_qty > 0
    | _             -> assert False
    ]
  else
    True  (* No invariant to verify for FIX.4.4 *)
};

(*===========================================================================*)
(* "update_order_status":                                                    *)
(*===========================================================================*)
(* Updates the status of active and/or booked orders on the information from
   "ExecReport":
*)
value update_order_status:
  FE.fix_engine -> FT.exec_report -> gstate 'u -> unit =

fun e rep s0 ->
  let dt   = FE.get_date_time ( Some e )                   in
  let is42 = ver42 e                                       in
  (* Precedences for "OrderStatus" values:                 *)
  let prec =
    if is42
    then
      (* FIX.4.2: *)
      fun
      [
        FT.PendingCancel      -> 12
      | FT.PendingReplace     -> 11
      | FT.DoneForDay         -> 10
      | FT.Calculated         ->  9
      | FT.Filled             ->  8
      | FT.Stopped            ->  7
      | FT.Suspended          ->  6
      | FT.Canceled
      | FT.Expired            ->  5
      | FT.PartFilled         ->  4
      | FT.Replaced           ->  3
      | FT.NewOrder
      | FT.Rejected
      | FT.PendingNew         ->  2
      | FT.AcceptedForBidding ->  1
      | _                     ->  assert False (* Do not occur here *)
      ]
    else
      (* FIX.4.4: *)
      fun
      [
        FT.PendingCancel      -> 11
      | FT.PendingReplace     -> 10
      | FT.DoneForDay         ->  9
      | FT.Calculated         ->  8
      | FT.Filled             ->  7
      | FT.Stopped            ->  6
      | FT.Suspended          ->  5
      | FT.Canceled
      | FT.Expired            ->  4
      | FT.PartFilled         ->  3
      | FT.NewOrder
      | FT.Rejected
      | FT.PendingNew         ->  2
      | FT.AcceptedForBidding ->  1
      | _                     ->  assert False (* Do not occur here *)
      ]
  in
  let prec_os = prec rep.FT.order_status_8 in

  match get_aos_from_rep e s0 rep with
  [
    Some ( aos, ord_id, cord_id ) ->
      (*---------------------------------------------*)
      if prec_os           >= prec FT.NewOrder  &&
         rep.FT.exec_type_8 =      FT.NewOrder
      (*---------------------------------------------*)
      then
        (* This is a New Order Ack; the curr status must be "OrderSent":     *)
        match aos.aos_moves with
        [
          [ ( OrderSent mord_id mdt mpx ) :: ms ] ->
            do{
              (* The order has been confirmed: *)
              assert( mord_id = ord_id && ( FF.compare_dts dt mdt ) >= 0 );
              aos.aos_moves := [ ( OrderPending ord_id dt mpx ) :: ms ];
              log_aos_update e s0 aos
            }
        | _ ->
            log_unexpected_rep e aos rep
        ]
      else
      (*---------------------------------------------*)
      if prec_os           >= prec FT.PendingCancel &&
         rep.FT.exec_type_8 =      FT.PendingCancel
      (*---------------------------------------------*)
      then
        (* This is a Cancel Req Acknowledgement:     *)
        match aos.aos_cancel with
        [
          CancelSent cord_id' cdt' when cord_id' = cord_id ->
          do{
            assert( ( FF.compare_dts dt cdt' ) >= 0 );
            aos.aos_cancel := CancelPending cord_id dt;
            log_aos_update e s0 aos
          }
        | _ ->
          log_unexpected_rep e aos rep
        ]
      else
      (*---------------------------------------------*)
      if prec_os           >= prec FT.PendingReplace &&
         rep.FT.exec_type_8 =      FT.PendingReplace
      (*---------------------------------------------*)
      then
        (* This is a Modify Req Acknowlegement:      *)
          if   ackn_move aos ord_id cord_id dt
          then log_aos_update     e s0  aos
          else log_unexpected_rep e aos rep
      else
      (*---------------------------------------------*)
      if prec_os             >= prec           FT.PartFilled &&
         ( rep.FT.exec_type_8 = ( if is42 then FT.PartFilled else FT.Traded ) ||
           rep.FT.exec_type_8 = ( if is42 then FT.Filled     else FT.Traded )
         )
      (*---------------------------------------------*)
      then
        (* The order has been partially or completely filled. This Rep should
           apply only to the head OrdID which must by now be pending at least:
        *)
        match aos.aos_moves with
        [
          [ ( OrderPending    mord_id mdt mpx ) :: ms ]
        | [ ( OrderPartFilled mord_id mdt mpx ) :: ms ] ->
            (* Also verify the Qtys:                 *)
            let rem_qty1   = aos.aos_rem_qty                                in
            let rem_qty2   = rep.FT.leaves_qty_8                            in
            let filled_qty = rem_qty1 - rem_qty2                            in
        
            if  filled_qty > 0                                              &&
                rep.FT.cum_qty_8 + rem_qty2 = aos.aos_orig_qty              &&
                rep.FT.order_qty_8          = Some aos.aos_orig_qty         &&
                rep.FT.last_qty_8           = Some filled_qty               &&
                rep.FT.price_8              = Some mpx                      &&
                rep.FT.last_px_8           <> None                          &&
                ( check_filled is42 rep.FT.exec_type_8 rem_qty2 )
            then
            do{
              assert( mord_id = ord_id && rem_qty2 < rem_qty1 &&
                      FF.compare_dts dt mdt >= 0 );

              (* In any case, book the quantity filled: *)
              book_position
                e s0 aos filled_qty ( from_option rep.FT.last_px_8 );

              (* Fill or PartialFill? In FIX.4.4, "exec_type" is just "Traded"
                 and the "order_status" may be overrun by one of a higher prec,
                 so need the following tests:
              *)
              if rem_qty2 = 0
              then
                (* Yes, this is a complete Fill: *)
                remove_from_active e s0 aos ord_id "Filled"
              else
              do{
                (* Update "aos" in place:        *)
                aos.aos_moves   := [ ( OrderPartFilled ord_id dt mpx ) :: ms ];
                aos.aos_rem_qty := rem_qty2;

                log_aos_update e s0 aos;
              }
            }
            else
              FE.log ( Some e ) mname BL.ERROR
                [ Printf.sprintf "SPURIOUS Fill:\n%s\n%s"
                  ( FT.string_of_exec_report rep      )
                  ( string_of_active_order_status aos )
                ]
        | _ ->
            log_unexpected_rep e aos rep
        ]
      else
      (*---------------------------------------------*)
      if  ( prec_os           >= prec FT.DoneForDay &&
            rep.FT.exec_type_8 =      FT.DoneForDay )

       || ( prec_os           >= prec FT.Expired    &&
            rep.FT.exec_type_8 =      FT.Expired    )

       || ( prec_os           >= prec FT.Rejected   &&
            rep.FT.exec_type_8 =      FT.Rejected   )

       || ( prec_os           >= prec FT.Canceled   &&
            rep.FT.exec_type_8 =      FT.Canceled   )
      (*---------------------------------------------*)
      then
      do{
        (* The order is no longer active. So remove the order from the active
           list and from the fwding map -- all of its pending IDs, never mind
           if some if them are unconfirmed yet, and we will later receive un-
           forwardable msgs:
        *)
        remove_from_active e s0 aos ord_id
          ( FT.string_of_order_status rep.FT.exec_type_8 );
          (* NB: "exec_type" here! *)

        (* Special Cases: *)

        match rep.FT.exec_type_8 with
        [
          FT.Rejected ->
            (* XXX: what happens with "cum_qty" ( 0 or not? ) if it was first
               "PartFilled", then "Rejected" following an improper move?
            *)
            let reason =
              match rep.FT.text_8 with
              [ Some r -> r
              | None   -> "Unknown"  ]
            in
            FE.log ( Some e ) mname BL.ERROR
              [ Printf.sprintf "Order Rejected: OrdID=%s: %s" ord_id reason ]

        | FT.Canceled ->
            (* Verify its cancellation phase, although the order has already
               been removed:
            *)
            match aos.aos_cancel with
            [
              CancelPending cord_id' _ when cord_id' = cord_id ->
                ()
            | _ ->
                FE.log ( Some e ) mname BL.ERROR
                  [ Printf.sprintf
                      "Order cancelled from improper phase? COrdID=%s\n%s\n%s"
                      cord_id ( string_of_active_order_status aos )
                      ( FT.string_of_exec_report rep )
                  ]
            ]

        | _ -> () (* No more special cases *)
        ]
      }
      else
      (*---------------------------------------------*)
      if ( is42  && ( prec_os >= prec FT.Replaced ) ||
         ( not is42 ) )
         && rep.FT.exec_type_8 =      FT.Replaced
      (*---------------------------------------------*)
      (* Successful Order Modification. The COrdID of the move req will become
         the OrdID of the modified order:
      *)
      then
        if   shift_moves        e s0 aos ord_id cord_id dt
        then log_aos_update     e s0 aos
        else log_unexpected_rep e aos rep
      else
      (*---------------------------------------------*)
      (* Anything else -- Don't know what to do:     *)
      (*---------------------------------------------*)
        FE.log ( Some e ) mname BL.WARN
          [ Printf.sprintf "Unprocessed ExecReport:\n%s"
              ( FT.string_of_exec_report rep )
          ]

  | None -> ()  (* Nothing to do, an error msg has already been produced     *)
  ];

(*===========================================================================*)
(* Processing Other Msgs:                                                    *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "exclude_failed_move":                                                    *)
(*---------------------------------------------------------------------------*)
(* Returns Some ( list of moves without the given one ) if "cord_id" (presumed
   to be failed) was indeed found among the "moves", and None otherwise:
*)
value exclude_failed_move:
  list aos_order_phase  -> string -> FT.date_time  ->
  option ( list aos_order_phase ) =

fun moves cord_id dt    ->
  let rec exclude_rec =
  fun curr prev ->
    match curr with
    [ []        ->
        None (* Otherwise we would return earlier *)

    | [ m :: ms ] ->
        do{
          assert( FF.compare_dts dt ( get_aosp_dt m ) >= 0 );

          if   ( get_aosp_ord_id m ) = cord_id
          then Some ( ( List.rev prev ) @ ms )
          else exclude_rec ms [ m :: prev ]
        }
    ]
  in
  exclude_rec moves [];

(*---------------------------------------------------------------------------*)
(* "process_cancel_reject":                                                  *)
(*---------------------------------------------------------------------------*)
(* This msg may be sent in response to both Cancel and Modify reqs. NB: this
   func currently does not require an "rconf" arg:
*)
value process_cancel_reject:
  FE.fix_engine -> FT.order_cancel_reject -> gstate 'u -> unit =

fun e crej s0   ->
  let dt         = FE.get_date_time ( Some e )      in
  let ord_id     = crej.FT.orig_cl_order_id_9       in
  let cord_id    = crej.FT.cl_order_id_9            in
  let ord_status = crej.FT.order_status_9           in
  let req_type   = crej.FT.cxl_reject_resp_to_9     in
  let reason1    =
    match  crej.FT.cxl_reject_reason_9 with
    [ Some r -> FT.string_of_cxl_rej_reason r
    | None   -> "" ]
  in
  let reason2    =
    match crej.FT.text_9 with
    [ Some r -> if reason1 <> "" then reason1 ^": "^ r else r
    | None   -> if reason1 <> "" then reason1 else "Unknown Reason" ]
  in
  match get_aos ( Some e ) s0 ord_id with
  [
    Some aos ->
      if not( check_engine e s0 aos.aos_symbol )
      then
        ()  (* The error has already been logged                             *)
      else
      match ord_status with
      [
        FT.Canceled
      | FT.Filled
      | FT.Traded ->
          (* Then is why this order still listed in Active Orders???         *)
          FE.log ( Some e ) mname BL.ERROR
            [ "OrdID="; ord_id;
              " should already have been removed from Active Orders:\n";
              ( FT.string_of_order_cancel_reject crej ); "\n";
              ( string_of_active_order_status aos )
            ]
      | _ ->
          (* Any other reason of cancel / moving failure: error cond:        *)
          let explained =
            match req_type with
            [
              FT.CancelReq ->
                match aos.aos_cancel with
                [
                  CancelSent    cord_id' _ 
                | CancelPending cord_id' _ when cord_id' = cord_id ->
                    do{
                      (* Really, an unsuccessful cancellation attempt:       *)
                      FE.log ( Some e ) mname BL.ERROR
                        [ Printf.sprintf
                            "Order cancel failed: OrdID=%s, COrdID=%s: %s"
                            ord_id cord_id reason2
                        ];
                      (* Remove this COrdID from the fwding map and re-set the
                         phase but do NOT try to re-cancel; NB: "cord_id" was
                         NOT an OrderID from the AOS point of view, so do not
                         put it into the AOS "dead" tbl:
                      *)
                      FA.remove_from_fwding_map e cord_id;
                      aos.aos_cancel := CancelNone;
                      True
                    }
              | _ -> False
              ]

            | FT.ModifyReq ->
                (* Did we really try to modify this order? If so, it must
                   now be removed from the "aos_moves" and the fwd map:
                *)
                match exclude_failed_move aos.aos_moves cord_id dt with
                [
                  Some ms' ->
                  do{
                    (* Log the failed move but do NOT re-issue it: by now,
                       it may now be completely out-of-date:
                    *)
                    FE.log ( Some e ) mname BL.ERROR
                      [ Printf.sprintf
                          "Order modification failed: OrdID=%s, COrdID=%s: %s"
                          ord_id cord_id reason2
                      ];

                    FA.remove_from_fwding_map e cord_id;
                    Hashtbl.replace  s0.st_dead cord_id ();
                    aos.aos_moves := ms';
                    True
                  }
              | None  -> False
              ]
            ]
          in
          if not explained
          then
            (* Don't know what to do: *)
            FE.log ( Some e ) mname BL.WARN
              [ Printf.sprintf "Unprocessed Cancel/Modify Reject:\n%s\n%s"
                ( FT.string_of_order_cancel_reject crej )
                ( string_of_active_order_status aos     )
              ]
          else ()
      ]

  | None ->
      (* This might be quite normal if the order has already been filled or can-
         celled. In this case, the OrderID should already be listed as "dead":
      *)
      let is_dead = Hashtbl.mem s0.st_dead ord_id in
      match ord_status with
      [
        FT.Canceled when is_dead ->
          FE.log ( Some e ) mname BL.INFO
            [ Printf.sprintf "Order already cancelled: OrdID=%s, COrdID=%s: %s"
              ord_id cord_id reason2
            ]
      | FT.Filled
      | FT.Traded   when is_dead ->
          FE.log ( Some e ) mname BL.WARN
            [ Printf.sprintf
                "Cannot cancel: Order already traded: OrdID=%s, COrdID=%s: %s"
                ord_id cord_id reason2
            ]
      | _ ->
          let what =
            if is_dead
            then "(already removed)"
            else "non-existing but not removed"
          in
          FE.log ( Some e ) mname BL.ERROR
            [ Printf.sprintf
                "Order Cancel/Modify Reject for %s OrdID=%s, COrdID=%s: %s\n%s"
                what ord_id cord_id reason2
                ( FT.string_of_order_cancel_reject crej )
            ]
      ]
  ];

(*---------------------------------------------------------------------------*)
(* "maybe_subscribe_symbols":                                                *)
(*---------------------------------------------------------------------------*)
value maybe_subscribe_symbols:
  FA.fix_reactor_conf ( gstate 'u ) -> FE.fix_engine -> gstate 'u -> unit =

fun rconf e s0 ->
  Hashtbl.iter
  (
    fun symbol t ->
    do{
      assert( symbol = t.i_symbol );
      (* If this symbol is not currently subscribed, subscribe it:           *)
      if not ( Hashtbl.mem s0.st_subscrs symbol )
      then
        let ( req_id, _ ) =
          FA.subscr_market_data
            ~{ exchange     = t.i_exchange           }
            ~{ account      = t.i_account            }
            ~{ symbol       = symbol                 }
            ~{ security_id  = t.i_sec_id             }
            ~{ id_src       = t.i_id_src             }
            ~{ entry_types  = [| FT.Bid; FT.Offer |] }
            ~{ depth        = FT.FullBook            }
            ~{ incr_refresh = False                  }
            (* TODO: Incremental updates as well    *)
            rconf
            e
        in
        Hashtbl.add s0.st_subscrs symbol req_id
      else ()
    }
  )
  s0.st_instrs;

(*---------------------------------------------------------------------------*)
(* "process_logon":                                                          *)
(*---------------------------------------------------------------------------*)
value process_logon: FE.fix_engine -> gstate 'u -> unit =
fun e s0 ->
  let conf = e.FE.conf.FC.config_id in
  try
    let lcount = Hashtbl.find s0.st_logons conf in
    do{
      (* Increment the LogOns count: *)
      Hashtbl.replace s0.st_logons conf ( lcount + 1 );

      (* If this is not the 1st LogOn, a disconnect happened; invalidate
         subscrs for all symbols which use this Engine,   so they can be
         re-subscribed:
      *)
      if lcount <> 0
      then
        (* Go through all Symbols: *)
        Hashtbl.iter
        (
          fun symbol t ->
          do{
            assert( t.i_symbol = symbol );
            if t.i_conf_q = conf
            then
            do{
              (* Invalidate this subscription. Do not remove it from the fwding
                 map as it will be re-issued with the same ReqID:
              *)
              Hashtbl.remove s0.st_subscrs symbol;
              assert( not( Hashtbl.mem s0.st_subscrs symbol ) )
            }
            else ()
          }
        )
        s0.st_instrs
      else
        (* First LogOn -- nothing to invalidate: *)
        ()
    }
  with
  [ Not_found ->
      FE.log ( Some e ) mname BL.ERROR
        [ "LogOn from unconfigured Engine: ";
        ( FC.string_of_fix_config_id conf ) ]
  ];

(*===========================================================================*)
(* Reactor Call-Backs:                                                       *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* The Time-Driven ( Periodic ) Call-Back:                                   *)
(*---------------------------------------------------------------------------*)
value periodic_cb: FA.call_back0 ( gstate 'u ) =
fun rconf s0 ->
do{
  (* We check if there are any currently unsubscribed symbols for all Engines,
     and subscribe or re-subscribe such symbols, but not before we get the 1st
     LogOn confirmation.  TODO: More integrity checks, e.g. eliminate hanging
     orders...
  *)
  List.iter
  (
    fun eq ->
      let conf = eq.FE.conf.FC.config_id in
      try
        let lcount = Hashtbl.find s0.st_logons conf in
        if  lcount = 0
        then
          () (* No LogOn yet -- nothing to do *)
        else
          maybe_subscribe_symbols rconf eq s0
    with
    [ Not_found ->
        FE.log ( Some eq ) mname BL.ERROR
          [ "LogOn from unconfigured Engine: ";
          ( FC.string_of_fix_config_id conf ) ]
    ]
  )
  s0.st_engs_q_lst;
  s0
};

(*---------------------------------------------------------------------------*)
(* The Main Call-Back:                                                       *)
(*---------------------------------------------------------------------------*)
(* NB: "system-level" processing updates the gstate in place but the user-level
   extention notionally returns a new state ( 'u ), although it could actually
   be the same one as the input ( updated or otherwise ):
*)
value main_cb: FA.call_back1 ( gstate 'u ) =
fun   rconf e  s0 msg ->
do{
  match msg with
  [
    FT.MarketData md ->
    do{
      update_market_data e md s0;
      (* Apply the user-level call-back stored in the gstate itself:         *)
      s0.st_ustate := s0.st_uact rconf e s0 msg
    }
    (* TODO: "MarketDataIncr"   *)
    (* TODO: "MarketDataReqRej" *)

  | FT.ExecReport rep ->
    do{
      update_order_status e rep s0;
      (* Apply the user-level call-back stored in the gstate itself:         *)
      s0.st_ustate := s0.st_uact rconf e s0 msg
    }

  | FT.OrderCancelReject crej ->
      process_cancel_reject e crej s0

  | FT.LogOn _ ->
      process_logon e s0

  | _ ->
      FE.log ( Some e ) mname BL.WARN
      [ Printf.sprintf "Unprocessed Msg:\n%s" ( FT.string_of_fix_msg msg ) ]
  ];

  (* Return the ( updated ) gstate: *)
  s0
};

(*===========================================================================*)
(* Creating a concrete Strategy:                                             *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "maybe_create_engine":                                                    *)
(*---------------------------------------------------------------------------*)
(* Creates a new Engine and attaches it to the "all_engines" table if an engine
   with that config does not exist there yet:
*)
value maybe_create_engine:
  bool                                     ->
  Hashtbl.t FC.fix_config_id FE.fix_engine ->
  Hashtbl.t string           FE.fix_engine ->
  ref ( list FE.fix_engine )               ->
  ref ( list FE.fix_engine )               ->
  Hashtbl.t FC.fix_config_id int           ->
  FC.fix_config_id                         ->
  string                                   ->
  unit =
fun debug all_engines sym_engines all_engines_lst engs_oq_lst logons conf symbol
->
do{
  (* Create the Engine if it does not exist yet: *)
  if not( Hashtbl.mem all_engines conf )
  then
    let e = FA.mk_fix_engine ~{ debug=debug } conf in
    do{
      Hashtbl.add all_engines conf e;
      all_engines_lst.val := [ e :: all_engines_lst.val ];
      engs_oq_lst.val     := [ e :: engs_oq_lst.val     ]
    }
  else ();

  (* Initially, there have been no logons:       *)
  if not( Hashtbl.mem logons conf )
  then
    Hashtbl.add logons conf 0
  else ();

  (* NB: "e" will always be found; no exn. The symbol is new, so there was no
     Engine for this symbol:
  *)
  let e = Hashtbl.find all_engines conf in
  do{
    assert( not( Hashtbl.mem  sym_engines symbol ) );
    Hashtbl.add sym_engines symbol e
  }
};

(*---------------------------------------------------------------------------*)
(* "mk_strat":                                                               *)
(*---------------------------------------------------------------------------*)
(* Instantiates the Generic Strategy and returns a closure which will run the
   created concrete strategy on applying that closure to ():
*)
value mk_strat:
  ?debug:       bool                         ->
  ?engines:     list Fix_engine.fix_engine   ->
  ~instrs:      array instrument             ->
  ~user_cb:     uact 'u                      ->
  ~init_ustate: 'u                           ->
  ( unit -> unit )                           =

fun ?{ debug=False } ?{ engines=[] } ~{ instrs } ~{ user_cb }
    ~{ init_ustate } ->

  let n            = Array.length instrs  in
  if  n = 0
  then
    failwith "mk_strat: No instruments provided"
  else
  let st_instrs       = Hashtbl.create n     in
  let st_subscrs      = Hashtbl.create n     in
  let st_logons       = Hashtbl.create n     in
  let st_engines_o    = Hashtbl.create n     in
  let st_engines_q    = Hashtbl.create n     in
  let st_poss         = Hashtbl.create n     in
  let st_mdata        = Hashtbl.create n     in
  let all_engines     = Hashtbl.create n     in (* Conf => Engine            *)
  let all_engines_lst = ref []               in
  let st_engs_o_lst   = ref []               in
  let st_engs_q_lst   = ref []               in
  do{
    (* If we got any existing Engines, put them into "all_engines":          *)
    List.iter
    (
      fun e ->
        if not( Hashtbl.mem all_engines e.FE.conf.FC.config_id )
        then
        do{
            Hashtbl.add  all_engines e.FE.conf.FC.config_id e;
            all_engines_lst.val := [ e :: all_engines_lst.val ]
        }
        else ()
    )
    engines;

    (* Now go through all instruments provided:                              *)
    Array.iter
    (
      fun t ->
        let symbol = t.i_symbol in
        do{
          (* Attach this instr to "st_instrs". Duplicate symbols not allowed *)
          if Hashtbl.mem st_instrs symbol
          then
            failwith( "mk_strat: Duplicate symbol: "^symbol )
          else
            Hashtbl.add st_instrs symbol t;

          (* Possible create new Engines and attach them to "st_logons" and
             "st_engines_{o|q}".
             NB: since "all_engines" may have already contain some Engines from
             the explicit param of this func, it may happen that there will be
             Engines connected to this Strategy which did not come from Instrs
             and are not initially with any symbols. However, they would still
             fwd msgs such as LogOns to the Reactor / Strategy.
          *)
          maybe_create_engine debug all_engines st_engines_o all_engines_lst
              st_engs_o_lst
              st_logons t.i_conf_o symbol;

          maybe_create_engine debug all_engines st_engines_q all_engines_lst
              st_engs_q_lst st_logons t.i_conf_q symbol;

          (* Initialise the positions to 0:                                  *)
          assert( not( Hashtbl.mem st_poss symbol ) );
          Hashtbl.add st_poss symbol 0;

          (* Initialise the Market Data so that the head is always there:    *)
          assert( not( Hashtbl.mem st_mdata symbol ) );
          let new_md =
            {
              md_bids = [| |];
              md_asks = [| |]
            }
          in
          Hashtbl.add st_mdata symbol new_md

          (* "st_subscrs", "st_orders" are initially empty *)
      }
    )
    instrs;

    (* Create the initial gstate:    *)
    let s0    =
    {
      st_instrs     = st_instrs;
      st_subscrs    = st_subscrs;
      st_logons     = st_logons;
      st_engines_o  = st_engines_o;
      st_engines_q  = st_engines_q;
      st_engs_o_lst = st_engs_o_lst.val;
      st_engs_q_lst = st_engs_q_lst.val;
      st_engs_all   = all_engines_lst.val;
      st_mdata      = st_mdata;
      st_poss       = st_poss;
      st_orders     = Hashtbl.create n;
      st_dead       = Hashtbl.create 1048576;
      st_ustate     = init_ustate;
      st_uact       = user_cb
    }
    in
    (* Create the Reactor conf. The minimum period between the call_back0 invo-
       cations is set to 100ms; it is not user-controllable because  that call-
       back itself is not provided by the user:
    *)
    let rconf = FA.new_reactor_conf
                  ~{ engines = all_engines_lst.val }
                  ~{ time_cb = periodic_cb         }
                  ~{ period  = 0.1                 }
                  ~{ main_cb = main_cb             }
    in
    do{
      assert( ( List.length ( FA.get_engines rconf ) ) =
              ( List.length all_engines_lst.val    ) );

      (* Return the Closure:         *)
      fun () -> ( FA.reactor rconf ) s0
    }
  };
