(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*    FIX Client Engine: Config, State and Sending / Receiving FIX Msgs      *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* FIX Engine ( Client ) Object:                                             *)
(*---------------------------------------------------------------------------*)
(* XXX: Exposing the "fix_engine" type is highly undesirable,  but it must be
   visible to the FIX Session Layer.
   XXX: The semantics of "fix_engine_mode" is defined in a client module (Fix_
   session):
*)
type fix_engine_mode =
[
  Init
| Normal
| ReSet
| LogOut_Graceful
| LogOut_Quick
];

type fix_engine =
{
  (*------------------------------------------*)
  (* Config is memoised inside the Engine:    *)
  (*------------------------------------------*)
  conf              : Fix_configs.fix_config;

  (*------------------------------------------*)
  (* Low-Level I/O Structs:                   *)
  (*------------------------------------------*)
  (* Server Communication Socket:             *)
  socket            : mutable option Unix.file_descr;

  (* Sizes of kernel and user-level buffs:    *)
  buff_size         : int;

  (* Buffer for assembling msgs to send:      *)
  send_buff         : Buffer.t;

  (* Max send time-out: cannot block oo-ly:   *)
  send_timeout      : float;

  (* Buffer for receiving msgs, and the curr offset within it: *)
  recv_buff         : string;
  recv_from         : mutable int;
  parse_from        : mutable int;

  (* Counter for sequential time-outs on reading:              *)
  to_count          : mutable int;

  (*------------------------------------------*)
  (* Threads and Multi-Thread Communication:  *)
  (*------------------------------------------*)
  sender_thread     : mutable option Thread.t;
  recver_thread     : mutable option Thread.t;
  monitor_thread    : mutable option Thread.t;

  (* System ( high-priority ) and application-level channels for submitting
     msgs for sending:
  *)
  last_unsent       : mutable option ( ( option int ) * Fix_types.fix_msg );
  sent_memo         : Queue.t        ( int            * Fix_types.fix_msg );
  send_sys_q        : Event.channel  ( ( option int ) * Fix_types.fix_msg );
  send_app_q        : Event.channel  ( ( option int ) * Fix_types.fix_msg );

  (*------------------------------------------*)
  (* Session-Level Structs:                   *)
  (*------------------------------------------*)
  (* Currenly in process of LogOut?           *)
  curr_mode         : mutable fix_engine_mode;

  (* Counters of sent and recvd msgs, from 1: *)
  next_send_seq_num : mutable int;
  next_recv_seq_num : mutable int;

  (* Window for receiving re-sent msgs: used only if gaps were encountered:  *)
  gap_window        : array ( option Fix_types.fix_msg );
  gap_from          : mutable int;
  gap_to            : mutable int;

  (*------------------------------------------*)
  (* Application-Level Structs:               *)
  (*------------------------------------------*)
  (* XXX: These structs do not really belong to the Engine, but still have to be
     placed here because an Engine can be connected to multiple appl-level  objs
     ( Reactors ):
  *)
  (* Map: MsgID => [ ReactorID ], for Events forwarding:                     *)
  reactor_map       : Hashtbl.t string ( list string );

  (* Hash table of MsgIDs previously excluded from the "reactor_map":        *)
  dead_msg_ids      : Hashtbl.t string unit;

  (* Map: ReactorID => Channel, for the same purpose. The elements of thsi map
     really contain Channels used for forwarding msgs along with EngineConfIDs:
  *)
  channel_map       : Hashtbl.t string
                      ( Event.channel ( Fix_types.fix_msg *
                                        Fix_configs.fix_config_id ) );

  (* The lock protecting updates to the above maps:      *)
  maps_rwl          : RWLock.t
};

(*---------------------------------------------------------------------------*)
(* Error Handling:                                                           *)
(*---------------------------------------------------------------------------*)
exception Socket_Error  of string;

(* "string_of_fix_msg_error":
   Creates a formatted FIX error string:
*)
value string_of_fix_msg_error: Fix_types.fix_msg_error -> string;

(* "log":
   The logging facility:
*)
value log: option fix_engine -> string -> Bolt.Level.t -> list string -> unit;
(*         engine_where         module_where              msg_lines          *)

(*---------------------------------------------------------------------------*)
(* Socket I/O:                                                               *)
(*---------------------------------------------------------------------------*)
(* "fix_connect":
   Creates the communication socket, makes TCP connection and exchanges initial
   msgs with the server:
*)
value fix_connect  : fix_engine -> unit;

(* "fix_close":
   Close and invalidate the socket ignoring any errors:
*)
value fix_close    : fix_engine -> unit;

(* Send a FIX msg, possibly with an explicit "SeqNum" ( for duplicates ):    *)
value send_fix_msg :
  fix_engine -> ?mb_seq_num:( option int ) -> Fix_types.fix_msg -> unit;

(* Receive next msg(s) and return them as
   [ ( msg, seq_num, last_ok, dup_flag ) ]:
*)
value recv_fix_msgs:
  fix_engine -> list ( Fix_types.fix_msg * int * int * bool );

(*---------------------------------------------------------------------------*)
(* Utils:                                                                    *)
(*---------------------------------------------------------------------------*)
(* Determine which thread the caller is running in: *)
value in_sender_thread: fix_engine -> bool;
value in_recver_thread: fix_engine -> bool;

(* Submit a system ( admin ) msg for sending via the Sender Thread. The caller
   must NOT run within the Sender Thread itself:
*)
value submit_sys_msg    :
  fix_engine -> ?mb_seq_num:( option int ) -> Fix_types.fix_msg -> unit;

(* Generate a new random ID string for msgs which require it. The Engine arg is
   only for fine-grained locking ( if not provided, the global lock is used ):
*)
value new_req_id        : unit -> string;

(* Get the Server (in Engine is specified) or Client time: time correction is
   taken from the corresp Engine if available, so the values returned by this
   func may be incompatible across multiple Engines:
*)
value get_date_time     : option fix_engine -> Fix_types.date_time;

