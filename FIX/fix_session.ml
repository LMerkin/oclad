(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                        FIX Session-Level Controls:                        *)
(*===========================================================================*)
module FT = Fix_types;
module FM = Fix_msgs;
module FC = Fix_configs;
module FE = Fix_engine;
module BL = Bolt.Level;

(*===========================================================================*)
(* LogOut / ShutDown Management:                                             *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "fix_shutdown":                                                           *)
(*---------------------------------------------------------------------------*)
(* Shutting down the FIX engine ( gracefully or otherwise ):                 *)
value mname = "Session";

value rec fix_shutdown: FE.fix_engine -> FE.fix_engine_mode -> string -> unit =
fun e mode reason ->
do{
  (* Log the shutdown request: *)
  let mode_str =
    match mode with
    [
      FE.Init            -> "Initialising"
    | FE.Normal          -> "Re-Connecting"
    | FE.ReSet           -> "Re-Setting"
    | FE.LogOut_Graceful -> "Gracefully Logging Out"
    | FE.LogOut_Quick    -> "Immediately Logging Out"
    ]
  in
  FE.log ( Some e ) mname BL.INFO
         [ Printf.sprintf "%s the FIX Engine: %s" mode_str reason ];

  (* Switch to the requested mode: *)
  e.FE.curr_mode := mode;

  if mode = FE.LogOut_Graceful
  then
  (*--------------------------------------------*)
  (* Graceful Shutdown:                         *)
  (*--------------------------------------------*)
  do{
    (* Initiate the logout process. Send "TestReq" and "LogOut" msgs. This is
       done differently depending on which thread is calling this func.  NB:
       we do not send "Reason" to the FIX server as part of the "LogOut" msg:
    *)
    let test_req_msg = FT.TestReq { FT.test_req_id_1 = FE.new_req_id () } in
    let logout_msg   = FT.LogOut  { FT.text_5        = None             } in

    (* It is probably OK to send "logOut" immediately after "TestReq", since
       the server will respond to both anyway:
    *)
    if FE.in_sender_thread e
    then
      try
      do{  (* Send the msgs directly: *)
        FE.send_fix_msg e test_req_msg;
        FE.send_fix_msg e logout_msg
      }
      with (* In case of any errors, perform non-graceful "LogOut": *)
      [
        err -> fix_shutdown e FE.LogOut_Quick
               ( "ERROR during graceful shutdown:\n"^( FM.string_of_exn err ) )
      ]
    else
      do{ (* Queue msgs for sending: *)
        FE.submit_sys_msg e test_req_msg;
        FE.submit_sys_msg e logout_msg
      }
    (* The initial phase of graceful shutdown is complete. The rest will be
       done when all threads terminate...
    *)
  }
  else
    (*--------------------------------------------*)
    (* Non-Graceful Shutdown:                     *)
    (*--------------------------------------------*)
    (* Close the socket, ignoring any errors. This will cause the Sender and
       the Receiver threads to fail.  In the "LogOut" mode, they will not be
       re-started by the Monitor:
    *)
    FE.fix_close e
};

(*---------------------------------------------------------------------------*)
(* "fix_finalise":                                                           *)
(*---------------------------------------------------------------------------*)
(* The shutdown procedure to be run when a FIX Engine is finalised, or upon an
   explicit logout:
*)
value fix_finalise: ~graceful:bool -> ~msg:string -> FE.fix_engine -> unit =
fun ~{ graceful } ~{ msg } e ->
  let mode =
    if graceful
    then FE.LogOut_Graceful
    else FE.LogOut_Quick
  in
  fix_shutdown e mode msg;


(*===========================================================================*)
(* FIX Engine Sender Thread:                                                 *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "fix_sender_loop":                                                        *)
(*---------------------------------------------------------------------------*)
value rec fix_sender_loop: FE.fix_engine -> unit =
fun e ->
  (* Get a msg to send in the following priority order:
     ( 1 ) last unsuccessfully sent msg;
     ( 2 ) head of the system msg queue;
     ( 3 ) the application or system msg queue ( waiting for an available msg,
           with a time-out );
     ( 4 ) if time-out expires: HeartBeat:
  *)
  let ( mb_seq_num, to_send ) =
    (* ( 1 ) Previously unsent msg, if any, with a poss explicit "SeqNum":   *)
    match e.FE.last_unsent with
    [
      Some p0  -> p0
    | None     ->
        (* ( 2 ) Any system ( admin ) msgs, if readily available, including
                 re-sends; the latter come with explicit "SeqNum"s:          *)
        let from_sys =
          Event.timed_sync ( Event.receive e.FE.send_sys_q ) 0.0
        in
        match from_sys with
        [
          Some p1 -> p1
        | None     ->
            (* ( 3 ) Wait for an application- or system-level msg, whatever
                     comes first, with a time-out:
            *)
            let from_app =
              Event.timed_sync
                ( Event.choose [ Event.receive e.FE.send_app_q;
                                 Event.receive e.FE.send_sys_q ] )
                ( float_of_int e.FE.conf.FC.heart_bt_int )
            in
            match from_app with
            [
              Some p2 -> p2
            | None    ->
                (* ( 4 ) Time-out expired: Check again the sys queue:  *)
                let   from_sys' =
                  Event.timed_sync ( Event.receive e.FE.send_sys_q ) 0.0
                in
                match from_sys' with
                [
                  Some p3  -> p3
                | None     ->
                    (* ( 5 ) The HeartBeat is generated: *)
                    ( None, FT.HeartBeat { FT.test_req_id_0 = None } )
                ]
            ]
        ]
    ]
  in
  let send_ok =
  do{
    (* Attempt sending. In any case, the previous unsuccessfully-send msg can
       now be re-set.  NB: we do not mark that msg as a possible duplicate --
       it will be sent with a new "SeqNum":
    *)
    e.FE.last_unsent := None;
    try do{
      FE.send_fix_msg e ~{ mb_seq_num=mb_seq_num } to_send;
      True
    }
    with
    [ err ->
      do{
        (* Most likely a "Socket_Error"; log it. Recognise if we are already in
           a LogOut mode:
        *)
        if e.FE.curr_mode <> FE.LogOut_Graceful &&
           e.FE.curr_mode <> FE.LogOut_Quick
        then
        do{
          FE.log ( Some e ) mname BL.ERROR
                 [ "Sender:\n";   FM.string_of_exn err   ];

          (* The msg is assumed to be unsent, so memoise it:  *)
          e.FE.last_unsent := Some ( mb_seq_num, to_send )
        }
        else
          FE.log ( Some e ) mname BL.INFO [ "Sender: Shutting Down" ];

        (* Close the socket -- this will cause the Receiver Thread to fail and
           terminate as well:
        *)
        FE.fix_close e;
        False
    } ]
  }
  in
  if send_ok
  then fix_sender_loop e    (* Next iteration               *)
  else ();                  (* Stop, to be re-started       *)

(*---------------------------------------------------------------------------*)
(* "fix_sender_main":                                                        *)
(*---------------------------------------------------------------------------*)
value fix_sender_main: FE.fix_engine -> unit =
fun e ->
do{
  (* Save the Sender Thread handle in the Engine:           *)
  e.FE.sender_thread := Some( Thread.self () );

  (* Enter the Sender Loop:                                 *)
  fix_sender_loop e;

  if e.FE.curr_mode = FE.Normal
  then
    FE.log ( Some e ) mname BL.WARN
           [ "Unexpected exit from the FIX Engine Sender" ]
  else ()
};

(*===========================================================================*)
(* Application-Level Msg Processing:                                         *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "get_fix_msg_ids":                                                        *)
(*---------------------------------------------------------------------------*)
(* Extract ID(s) from various types of FIX msgs. For msg types without an ID,
   return []. For "ExecReport"s etc, multiple IDs may be present:
*)
value get_fix_msg_ids: FT.fix_msg -> list string =
fun
[ FT.SecurityDefReq     x -> [ x.FT.security_req_id_c  ]
| FT.SecurityDef        x -> [ x.FT.security_req_id_d  ]
| FT.MarketDataReq      x -> [ x.FT.md_req_id_V        ]
| FT.MarketData         x -> [ x.FT.md_req_id_W        ]

| FT.MarketDataIncr     x ->
  match x.FT.md_req_id_X with
  [ Some id -> [ id ]
  | None    -> []   ]

| FT.MarketDataReqRej   x -> [ x.FT.md_req_id_Y        ]
| FT.NewSingleOrder     x -> [ x.FT.cl_order_id_D      ]
| FT.OrderCancelReq     x -> [ x.FT.orig_cl_order_id_F; x.FT.cl_order_id_F ]
| FT.OrderModifyReq     x -> [ x.FT.orig_cl_order_id_G; x.FT.cl_order_id_G ]
| FT.OrderCancelReject  x -> [ x.FT.orig_cl_order_id_9; x.FT.cl_order_id_9 ]

| FT.ExecReport         x ->
  (* This may contain a report on the orig msg itself, but also on cancel/modify
     req regarding that orig msg:
  *)
  let id = x.FT.cl_order_id_8         in
  match    x.FT.orig_cl_order_id_8    with
  [ Some id' -> [ id; id'  ]
  | None     -> [ id ]     ]

| FT.OrderStatusReq     x ->
  (* Again, we may have 1 or 2 IDs: *)
  let id = x.FT.cl_order_id_H         in
  match    x.FT.order_status_req_id_H with
  [ Some id' ->  [ id; id' ]
  | None     ->  [ id ]    ]

  (* Other msgs do not have an ID: *)
  | _                     -> []
];

(*---------------------------------------------------------------------------*)
(* "get_all_channels":                                                       *)
(*---------------------------------------------------------------------------*)
(* Get all ReactorIDs and then all Channels to which the given "msg" should be
   fwded:
*)
value get_all_channels:
  FE.fix_engine -> FT.fix_msg ->
  list ( Event.channel ( FT.fix_msg * FC.fix_config_id ) ) =

fun e msg ->
  (* Get the list of ReactorIDs first:                                       *)
  let reactors =
    match msg with
    [
      (* Some msgs correspond to critical events within the Engine, and should
         be fwded to ALL attached reactors. Currently, this is only for "LogOn":
      *)
      FT.LogOn _ ->
        (* Traverse the "channel_map" to get absolutely all ReactorIDs.  NB: do
           NOT use "reactor_map", as the latter depends on MsgIDs registered so
           far, so that info may be incomplete!
        *)
        Hashtbl.fold ( fun r _ currs -> [ r :: currs ] ) e.FE.channel_map []

    | _ ->
      (* Generic case: Need to extract the MsgIDs first:                     *)
        let msg_ids = get_fix_msg_ids msg in
        List.fold_left
        (
          fun currs1 msg_id ->
            let  rs =   (* All ReactorIDs for this "msg_id"                  *)
              try
                Hashtbl.find e.FE.reactor_map msg_id
              with
              [ Not_found ->
                do{
                  (* Probably an outdated MsgID, already excluded from map.
                     Check the "dead pool" first:
                  *)
                  if Hashtbl.mem e.FE.dead_msg_ids msg_id
                  then
                    (* Yes, this "msg_id" was explicitly removed            *)
                    if e.FE.conf.FC.debug
                    then
                      FE.log ( Some e ) mname BL.DEBUG
                        [ Printf.sprintf
                            "Msg Dropped from Fwding: MsgID=%s was removed:\n%s"
                            msg_id ( FT.string_of_fix_msg msg )
                        ]
                    else ()
                  else
                    FE.log ( Some e ) mname BL.WARN
                      [ Printf.sprintf
                          "Could not Fwd the Msg: MsgID=%s:\n%s" msg_id
                          ( FT.string_of_fix_msg msg )
                      ];
                  []  (* So no corresponding ReactorIDs                      *)
                }
              ]
            in
            (* Make a list of unique ReactorIDs:                             *)
            List.fold_left
            (
              fun currs2 r ->
                if not( List.mem r currs2 ) then [ r :: currs2 ] else currs2
            )
            currs1 rs
        )
        [] msg_ids
    ]
  in
  (* Now get Channels for those ReactorIDs:                                  *)
  List.fold_left
  (
    fun curr reactor ->
      try
        let ch = Hashtbl.find e.FE.channel_map reactor in
        [ ch :: curr ]
      with
      [ Not_found ->
        do{
          (* This is a serios error condition -- must not happen             *)
          FE.log ( Some e ) mname BL.ERROR
            [ "ReactorID="; reactor; " is missing in the ChannelMap" ];
          curr
        }
      ]
  )
  [] reactors;

(*---------------------------------------------------------------------------*)
(* "process_incoming_msg_2":                                                 *)
(*---------------------------------------------------------------------------*)
(* "Towards the application-level msg processing":                           *)

value process_incoming_msg_2: FE.fix_engine -> FT.fix_msg -> unit =
fun e msg ->
  match msg with
  [
  (*-----------------*)
    FT.Reject  rej ->
  (*-----------------*)
    (* This is currently viewed as a fatal error, since this means a protocol
       incompatibility between our Client and the FIX Server:
    *)
    let fatal_error = Printf.sprintf "ERROR: Got a Reject:\n%s\nSHUTTING DOWN"
                                     ( FT.string_of_reject rej )           in
    fix_shutdown e FE.LogOut_Quick fatal_error

  (*-----------------*)
  | FT.LogOut  lo  ->
  (*-----------------*)
    if e.FE.curr_mode = FE.LogOut_Quick ||
       e.FE.curr_mode = FE.LogOut_Graceful
    then
      () (* Logout already in progress, nothing to do *)
    else
      (* Received a logout request from the server:   *)
      let reason =
        "Server sent LogOut" ^
          ( match lo.FT.text_5 with
            [ Some r -> ": "^r
            | None   -> "" ]
          )
      in
      (* Do immediate shutdown:                       *)
      fix_shutdown e FE.LogOut_Quick reason

  (*-----------------*)
  | FT.TestReq tr  ->
  (*-----------------*)
    (* Respond with a "HeartBeat":  *)
    let hb = FT.HeartBeat { FT.test_req_id_0 = Some tr.FT.test_req_id_1 } in
    FE.submit_sys_msg e hb

  (*-----------------*)
  | _ ->
  (*-----------------*)
    (* GENERIC CASE:
       Get the  MsgID ( "" if not available for this "msg" ), and search the
       maps for this MsgID:
    *)
    let msg_pair = ( msg, e.FE.conf.FC.config_id )           in
    let channels = ref [] in
    do{
      (* Acquire the lock in the RO mode so the maps are not modified from a
         Reactor side while in use for msgs dispatching:
      *)
      RWLock.lock_rd e.FE.maps_rwl;

      (* Get the Reactors subscribed to this msg.  In general, if there are
         multiple MsgIDs associated with this msg,  one can expect that the
         list of Reactors will be the same for each MsgID;  but  to be on a
         safe side, we produce a union. NB: Some specific msgs will need to
         be forwarded to ALL reactors attached to this Engine, irrespective
         of the MsgID:
      *)
      channels.val := get_all_channels e msg;

      (* Release the lock: *)
      RWLock.unlock e.FE.maps_rwl;

      (* Dispatch the msg to the "channels": *)
      List.iter ( fun ch -> Event.sync ( Event.send ch msg_pair ) ) channels.val
    }
  ];

(*===========================================================================*)
(* System-Level Msg Processing:                                              *)
(*===========================================================================*)
exception ReSet_Session of string;

(*---------------------------------------------------------------------------*)
(* "send_gap_fill":                                                          *)
(*---------------------------------------------------------------------------*)
value send_gap_fill: FE.fix_engine -> int -> int -> unit =
fun e gap_from gap_after ->
do{
  assert ( gap_from < gap_after );
  let gf_msg =
    FT.SeqReset
    {
      (* Fill the gap up to "gap_after-1":              *)
      FT.new_seq_num_4 = gap_after;
      FT.gap_fill_flag_4 = Some True
    }
  in
  (* "gf_seq_num" must be <= "new_seq_num" above:       *)
  FE.submit_sys_msg e ~{ mb_seq_num=( Some gap_from ) } gf_msg
};

(*---------------------------------------------------------------------------*)
(* "resend_msgs":                                                            *)
(*---------------------------------------------------------------------------*)
value resend_msgs: FE.fix_engine -> FT.resend_req -> unit =
fun e rr ->
  let resend_from = ref rr.FT.begin_seq_num_2 in
  do{
    (* Go through all memoised sent msgs, re-send them, fill in the gaps:    *)
    Queue.iter
    (
      fun ( seq_num', msg' ) ->
        if  seq_num' >= resend_from.val
        then
          let resend_to = min rr.FT.end_seq_num_2 seq_num' in
          do{
            if  resend_to > resend_from.val
            then
            do{
              (* Send a "GapFill"; "resend_to" is AFTER the gap:             *)
              send_gap_fill e resend_from.val resend_to;
              resend_from.val := resend_to
            }
            else ();

            (* Now possibly re-send the memoised "msg'" at "seq_num'":       *)
            assert ( resend_from.val >= resend_to && seq_num' >= resend_to );

            if resend_from.val = resend_to && seq_num' = resend_to
            then
            do{
              FE.submit_sys_msg e ~{ mb_seq_num=( Some seq_num' ) } msg';
              incr resend_from
            }
            else ();

            (* We advanced "resend_from" past the curr "resend_to":          *)
            assert ( resend_from.val > resend_to )
          }
        else
          (* seq_num' < resend_from: Nothing to do here, go to the next
             seq_num' if available :
          *)
          ()
    )
    e.FE.sent_memo;

    (* If the re-send interval was not covered after all -- eg empty memo:
       do a GapFill; again, the last ar is the pos AFTER the gap:
    *)
    if resend_from.val <= rr.FT.end_seq_num_2
    then
      send_gap_fill e resend_from.val ( rr.FT.end_seq_num_2 + 1 )
    else ()
};

(*---------------------------------------------------------------------------*)
(* "upper_seq_num":                                                          *)
(*---------------------------------------------------------------------------*)
value upper_seq_num: FT.fix_msg -> int -> int =
fun msg seq_num ->
  match msg with
  [
    FT.SeqReset sr ->
      if sr.FT.gap_fill_flag_4 = Some True
      then
        (* This is a "SeqReset-GapFill" msg which is allowed to increase the
           "seq_num":
        *)
        if   sr.FT.new_seq_num_4 >= seq_num + 1
        then sr.FT.new_seq_num_4 - 1 (* NB: upper for THIS msg, not the next! *)
        else raise( ReSet_Session
                  ( Printf.sprintf "ERROR: SeqReset: SeqNum=%d, NewSeqNum=%d"
                    seq_num sr.FT.new_seq_num_4 ) )
      else
        (* "SeqReset-Reset": must not occur here:     *)
        assert False
  | _ ->
    (* Any other msgs: no SeqNum change for THIS msg: *)
    seq_num
  ];

(*---------------------------------------------------------------------------*)
(* "save_msg_in_gap_window":                                                 *)
(*---------------------------------------------------------------------------*)
value save_msg_in_gap_window:
  FE.fix_engine -> FT.fix_msg -> int -> int -> unit =

fun e msg seq_num seq_num'    ->
do{
  (* The window must be active: *)
  assert( 1             <= e.FE.gap_from          &&
          e.FE.gap_from <= e.FE.gap_to            &&
          e.FE.gap_to   <  e.FE.next_recv_seq_num &&
          e.FE.gap_from <= seq_num                &&
          seq_num       <= seq_num'               );

  (* Also, the range of "seq_no"s must be entirely within the gap part of the
     window, or the post-gap part:
  *)
  assert( seq_num'      <= e.FE.gap_to            ||
          seq_num       >  e.FE.gap_to            );

  let j0   = seq_num    -  e.FE.gap_from          in
  let j1   = seq_num'   -  e.FE.gap_from          in
  let wlen = Array.length  e.FE.gap_window        in

  (* It is possible that "seq_no"(s) are too large -- the window was not closed
     for a long time due to non-receipt of duplicates,   and was growing to the
     right in the meantime:
  *)
  if  j1  >= wlen
  then
    raise( ReSet_Session
         ( Printf.sprintf "ERROR: SeqNum=%d > GapWindow=[ %d..%d ]"
           seq_num' e.FE.gap_from ( e.FE.gap_from + wlen - 1 ) ) )
  else
  (* The range of indices is OK, store the same msg for all indices ( in most
     cases, we would just have j0 = j1 ):
  *)
  for j = j0 to j1
  do{
    (* However, if it happens that the corresp position in the gap window is
       already non-"None", this means a double-duplicate,  or an unsolicited
       duplicate for a post-gap msg --  but the latter should already be de-
       tected by the caller:
    *)
    if e.FE.gap_window.( j ) <> None
    then
    do{
      assert( j <= e.FE.gap_to - e.FE.gap_from );
      raise( ReSet_Session
           ( Printf.sprintf "ERROR: Double Dup for SeqNum=%d:\n%s\nRESET"
             seq_num ( FT.string_of_fix_msg msg ) ) )
    }
    else
      e.FE.gap_window.( j ) := Some msg
  }
};

(*---------------------------------------------------------------------------*)
(* "process_incoming_msg_1":                                                 *)
(*---------------------------------------------------------------------------*)
(* Performs admin-level processing, mostly about the SeqNums:                *)

value process_incoming_msg_1:
  FE.fix_engine -> ( FT.fix_msg * int * int * bool ) -> unit =

fun e ( msg, seq_num, _, dup_flag ) ->
try
do{
  (* In the debug mode, log the msg as an OCaml term:     *)
  if e.FE.conf.FC.debug
  then
    let dup_str  = if dup_flag then ", DUP" else ""       in
    let exp_warn =
      if seq_num = e.FE.next_recv_seq_num
      then ""
      else ", EXPECTED="^( string_of_int e.FE.next_recv_seq_num )
    in
    FE.log ( Some e ) mname BL.DEBUG
           [ Printf.sprintf "[ %s #%d%s%s ]:\n"
              ( FC.string_of_fix_config_id e.FE.conf.FC.config_id  ) seq_num
              dup_str exp_warn;
             FT.string_of_fix_msg msg
           ]
  else ();

  (* Do the following even before checking the "SeqNum" validity:            *)
  match msg with
  [
  (*-------------------------------------------------------------------------*)
    FT.SeqReset sr when sr.FT.gap_fill_flag_4 <> Some True ->
  (*-------------------------------------------------------------------------*)
      let nsn = sr.FT.new_seq_num_4 in
      do{
        (* This is a "SeqReset-Reset" msg. We reset BOTH the recv and the send
           counters to the value requested  without verifying the "seq_num" of
           this msg itself; the new counter can also be of any value, e.g. de-
           creasing:
        *)
        FE.log ( Some e ) mname BL.WARN
          [ Printf.sprintf
            "SeqReset-Reset received: next_recv_seq_num: %d -> %d"
            e.FE.next_recv_seq_num nsn
          ];
        (* Force re-set: *)
        e.FE.next_recv_seq_num := nsn;
        e.FE.next_send_seq_num := nsn;

        (* Also, in this case we remove the memo of sent app msgs:           *)
        Queue.clear e.FE.sent_memo;
        raise Exit
      }
  (*-------------------------------------------------------------------------*)
  | FT.ResendReq rr ->
  (*-------------------------------------------------------------------------*)
      (* If we memoised any of app msgs sent by us, and they fall into the "rr"
         range, re-send them. Otherwise, just send GapFills:
      *)
      resend_msgs e rr

  | _ -> ()   (* Anything else -- nothing to do at this stage                *)
  ];

  (* NOW: For all remaining msg types, we must check the "seq_num";
     "e.FE.next_recv_seq_num" is the expected value.
     The following logic depends on whether we have an active gap window.  The
     includes the space for a gap itself ( msgs to be re-sent by our request )
     and the post-gap msgs which cannot be processed before all hap msgs are
     received, hence a queue;
     "wlen"    : the total physical size of the gap window ( space allocated for
                 gap and post-gap msgs, possibly unused );
     "seq_num'": up-adjusted "seq_num" for THIS msg, if "SeqReset-GapFill":
    *)
  let wlen        = Array.length  e.FE.gap_window in
  let window_open = e.FE.gap_from >= 1            in
  let seq_num'    = upper_seq_num msg seq_num     in

  (*-------------------------------------------------------------------------*)
  if ( seq_num > e.FE.next_recv_seq_num ) && ( not dup_flag )
  (*-------------------------------------------------------------------------*)
  (* Gap detected.  NB: we require the "dup_flag" NOT to be be set in this
     case, although it may be stricter than the FIX standard requirements:
  *)
  then
  do{
    FE.log ( Some e ) mname BL.WARN
           [ Printf.sprintf "Gap in Received Msgs: Missing: %d..%d"
                            e.FE.next_recv_seq_num ( seq_num-1 )
           ];
    (* The minimal size of the window which will be required for this gap and
       the post-gap msgs ( including the one just received ):
    *)
    let gsize    = seq_num' - e.FE.next_recv_seq_num + 1 in
    if  gsize    > wlen
    then
      (* The gap is too large -- no room to store the msgs to be re-sent:    *)
      raise( ReSet_Session
           ( Printf.sprintf "ERROR: Too large SeqNum gap: %d -> %d:\n%s\nRESET"
             e.FE.next_recv_seq_num seq_num' ( FT.string_of_fix_msg msg ) ) )
    else
    do{
      (* Open or extend the gap window.     In this process we also update the
         "next_recv_seq_num". If the window exists, its beginning ( "gap_from" )
         stays the same but "gap_to" moves to the right:
      *)
      if window_open
      then
        assert( e.FE.gap_from          <= e.FE.gap_to            &&
                e.FE.gap_to            <  e.FE.next_recv_seq_num &&
                e.FE.next_recv_seq_num <  seq_num                && (* By If *)
                seq_num                <= seq_num'               )
      else
        e.FE.gap_from        := e.FE.next_recv_seq_num;

      e.FE.gap_to            := seq_num  - 1;
      e.FE.next_recv_seq_num := seq_num' + 1;

      assert(   1                     <= e.FE.gap_from           &&
                e.FE.gap_from         <= e.FE.gap_to             &&
                e.FE.gap_to           <  e.FE.next_recv_seq_num );

      (* In both cases, completely re-initialise the physical window because we
         will request a new re-send series:
      *)
      Array.fill e.FE.gap_window 0 wlen None;

      (* Store the msg received in the post-gap part of the window. If it was a
         "GapFill", then more than 1 pos is filled with this msg:
      *)
      save_msg_in_gap_window e msg seq_num seq_num';

      (* Request re-sending of all msgs from the "gap_from" on. NB: we do NOT
         set "end_seq_num" to 0 ( indefinite ) -- doing so would require diff-
         erent logic of separating re-sends from new msgs:
      *)
      let resend_req =
        FT.ResendReq
        {
          FT.begin_seq_num_2 = e.FE.gap_from;
          FT.end_seq_num_2   = e.FE.gap_to
        }
      in
      FE.submit_sys_msg e resend_req
    }
  }
  else
  (*-------------------------------------------------------------------------*)
  if ( seq_num < e.FE.next_recv_seq_num ) && dup_flag
  (*-------------------------------------------------------------------------*)
  (* OK, this is a possible duplicate. If no gap window is currently open,  or
     the duplicate is outside the window, we treat it as a serious error cond:
  *)
  then
    if  seq_num  < e.FE.gap_from  || seq_num' > e.FE.gap_to
    then
      (* The duplicate is beyond the window -- below or above.   In the latter
         case, it actually duplicates some msg received after the gap, but not
         processed yet ( because the window was not closed yet ).    This also
         includes the case when there is no gap window at all ( ie unsolicited
         duplicate ):
      *)
      raise( ReSet_Session
           ( Printf.sprintf
              "ERROR: DupSeqNum=[ %d..%d ]; Gap=[ %d..%d ]:\n%s\nRESET"
              seq_num seq_num'  ( e.FE.gap_from ) ( e.FE.gap_to )
              ( FT.string_of_fix_msg msg )
           ) )
    else
    do{
      (*---------------------------------------------------------------------*)
      (* A Valid Duplicate:                                                  *)
      (*---------------------------------------------------------------------*)
      assert( window_open                             &&
              e.FE.gap_from <= seq_num                &&
              seq_num       <= seq_num'               &&
              seq_num'      <= e.FE.gap_to            &&
              e.FE.gap_to   <  e.FE.next_recv_seq_num );

      (* Save the re-sent msg ( incl "GapFill"s ) in the hap window:         *)
      save_msg_in_gap_window e msg seq_num seq_num';

      (* Check if the window is filled now. For that, we only need to scan the
         gap part itself; the post-gap part grows sequentially:
      *)
      let gmax = e.FE.gap_to - e.FE.gap_from in

      let rec is_gap_filled:   int -> bool   =
      fun  j ->
        if j > gmax
        then True
        else
        if e.FE.gap_window.( j ) = None
        then False
        else is_gap_filled ( j+1 )
      in
      if is_gap_filled 0
      then  
      do{
        (* The window is now filled in -- process all accumulated msgs:      *)
        for j = 0 to ( e.FE.next_recv_seq_num - 1 - e.FE.gap_from )
        do{
          assert( j < wlen );
          match e.FE.gap_window.( j ) with
          [
            Some msg' -> process_incoming_msg_2 e msg'
          | _         -> assert False
          ]
        };
        (* Close the gap window:                                             *)
        e.FE.gap_from := 0;
        e.FE.gap_to   := 0;
        Array.fill e.FE.gap_window 0 wlen None
      }
      else ()
    }
  else
  (*-------------------------------------------------------------------------*)
  if ( seq_num = e.FE.next_recv_seq_num ) && ( not dup_flag )
  (*-------------------------------------------------------------------------*)
  then
  do{
    e.FE.next_recv_seq_num := seq_num' + 1;
    if window_open
    then
      (* Window open -- just save the msg in its post-gap part:              *)
      save_msg_in_gap_window e msg seq_num seq_num'
    else
      (* No gap window -- MOST COMMON CASE -- process the msg now:           *)
      process_incoming_msg_2 e msg
  }
  else
  (*--------------------------------------------------------------------------*)
  (* All other cases are treated as unrecoverable SeqNum errors:              *)
  (*--------------------------------------------------------------------------*)
   raise( ReSet_Session
        ( Printf.sprintf
          "ERROR: SeqNum=[ %d..%d ], PossDup=%b, NextExpected=%d:\n%s\n"
          seq_num seq_num' dup_flag e.FE.next_recv_seq_num
          ( FT.string_of_fix_msg msg ) ) )
}
with
[ Exit            -> ()
| ReSet_Session s -> fix_shutdown e FE.ReSet s
];

(*===========================================================================*)
(* FIX Engine Receiver Thread:                                               *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "fix_recver_loop":                                                        *)
(*---------------------------------------------------------------------------*)
value rec fix_recver_loop: FE.fix_engine -> unit =
fun e ->
  (* Wait for the msgs to arrive, and get them. NB: It is possible ( e.g. on
     buffer end ) that "FE.recv_fix_msgs" yields [],   yet there is no error
     ( continue = True ):
  *)
  let ( msgs, continue ) =
    try
      ( FE.recv_fix_msgs e, True )
    with
    [ err ->
      do{
        (* Most likely a "Socket_Error"; log it. Recognise if we are already in
           a ShutDown mode:
        *)
        if e.FE.curr_mode <> FE.LogOut_Graceful &&
           e.FE.curr_mode <> FE.LogOut_Quick
        then
          FE.log ( Some e ) mname BL.ERROR
                 [ "Receiver:\n"; FM.string_of_exn err ]
        else
          FE.log ( Some e ) mname BL.INFO [ "Receiver: Shutting Down" ];

        (* Close the socket -- this will cause the Sender Thread to fail and
           terminate as well:
        *)
        FE.fix_close e;
        ( [], False )
    } ]
  in
  if continue
  then
  do{
    (* Process the msgs received:  *)
    List.iter ( process_incoming_msg_1 e ) msgs;

    (* Next Iteration:             *)
    fix_recver_loop e
  }
  else
    (); (* Exit the loop,to be re-started *)

(*---------------------------------------------------------------------------*)
(* "fix_recver_main":                                                        *)
(*---------------------------------------------------------------------------*)
value fix_recver_main: FE.fix_engine -> unit =
fun e ->
do{
  (* Save the Receiver Thread handle in the Engine:         *)
  e.FE.recver_thread := Some( Thread.self () );

  (* Enter the Receiver Loop:                               *)
  fix_recver_loop e;

  if e.FE.curr_mode = FE.Normal
  then
    FE.log ( Some e ) mname BL.WARN
           [ "Unexpected exit from the FIX Engine Receiver" ]
  else ()
};

(*===========================================================================*)
(* FIX Engine Monitor Thread:                                                *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "fix_monitor_loop":                                                       *)
(*---------------------------------------------------------------------------*)
value rec fix_monitor_loop: FE.fix_engine -> unit =
fun e ->
do{
  (* Register ourselves on the 1st iteration:  *)
  if    e.FE.monitor_thread  = None
  then  e.FE.monitor_thread := Some ( Thread.self () )
  else  ();

  (* The initial / restart condition:          *)
  assert( e.FE.sender_thread = None &&  e.FE.recver_thread = None     &&
          e.FE.socket        = None &&
        ( e.FE.curr_mode     = FE.Normal || e.FE.curr_mode = FE.ReSet ||
          e.FE.curr_mode     = FE.Init  ) );

  (* Re-set the counters and the gap window if requested -- and will ask the
     server to do the same. Also re-set them in the "Normal" mode if we know
     that they are to be re-set on disconnect:
  *)
  if ( e.FE.curr_mode = FE.ReSet ) ||
     ( e.FE.curr_mode = FE.Normal  && e.FE.conf.FC.reset_on_disconn )
  then
  do{
    e.FE.next_send_seq_num := 1;
    e.FE.next_recv_seq_num := 1;
    Array.fill e.FE.gap_window 0 ( Array.length e.FE.gap_window ) None
  }
  else ();

  (* In the "ReSet" ( and also "Init", to be sure ) mode, we remove the queue
     of previously sent app msgs:
  *)
  let  reset_seqs = ( e.FE.curr_mode = FE.Init || e.FE.curr_mode = FE.ReSet ) in
  do{
    if reset_seqs
    then Queue.clear e.FE.sent_memo
    else ();

    (* Create the socket and make a TCP connection to the server, then send a
       "LogOn" msg:
    *)
    let logon_ok =
      try
      do{
        (* NB: because the socket is non-blocking, the "connect" function might
           not raise an exception even if connection is refused:
        *)
        FE.fix_connect e;

        let logon_msg   =
          FT.LogOn
          {
            FT.encrypt_method_A     = FT.UnEncrypted;
            FT.heart_bt_int_A       = e.FE.conf.FC.heart_bt_int;
            FT.reset_seq_num_flag_A = if reset_seqs then Some True else None
          }
        in
        (* Send it:  *)
        FE.send_fix_msg e logon_msg;

        (* The FIX standard recommends to wait for some time after "LogOn" or
           to send a "TestReq" and wait for the response in order to allow the
           counter-party to synchronise the seq_nums. We opt to wait, but that
           is probably not necessary in the "Init" mode (no prev msgs):
        *)
        if e.FE.curr_mode <> FE.Init
        then Thread.delay  ( float_of_int e.FE.conf.FC.reconnect_int )
        else ();

        if e.FE.conf.FC.debug
        then FE.log ( Some e ) mname BL.INFO [ "LOGON INITIATED" ]
        else ();
        True
      }
      with
      [ err ->
        do{  (* Most likely a "Socket_Error"; log it and close the socket:   *)
          FE.log ( Some e ) mname BL.ERROR
                 [ "Cannot Connect/LogOn to FIX server, WAITING:\n";
                     FM.string_of_exn err
                 ];
          FE.fix_close e;
          False
      } ]
    in
    let  continue =
      if logon_ok
      then
      do{
         e.FE.curr_mode := FE.Normal;

        (* Create the Sender and Receiver Threads. NB: to avoid any race conds,
           the threads will have to register THEMSELVES with the Engine:
        *)
        let sender = Thread.create fix_sender_main e in
        let recver = Thread.create fix_recver_main e in
        do{
          (* Wait for termination of BOTH of the above threads. NB: if one thrd
             termiantes ( e.g. due to an error condition ),  it would close the
             socket and thus also terminate the other,  so it is correct indeed
             to wait for termination of both -- we currently  have no mechanism
             to wait for a disjunction of thread events:
          *)
          Thread.join sender;
          e.FE.sender_thread := None;

          Thread.join recver;
          e.FE.recver_thread := None;

          (* The socket must have been closed at this point: *)
          assert( e.FE.socket = None );

          (* Continue ( and re-start the thrds ) unless a LogOut was called: *)
          match e.FE.curr_mode with
          [
            FE.Init            -> assert False         (* Cannot happen here *)
          | FE.Normal                   (* Lost connection,  re-connect      *)
          | FE.ReSet           -> True  (* Invalid counters, re-set, re-conn *)
          | FE.LogOut_Graceful          (* Exit *)
          | FE.LogOut_Quick    -> False (* Exit *)
          ]
        }
      }
      else
        (* Connection failed -- try to re-connect:       *)
        True
    in
    if continue
    then do{
      (* Wait before attempting re-connection: *)
      Thread.delay ( float_of_int e.FE.conf.FC.reconnect_int );

      FE.log ( Some e ) mname BL.INFO
             [ "RE-CONNECTING to the FIX Server..."  ];
      (* Start over:                           *)
      fix_monitor_loop e
    }
    else
      (* All done, exit:                       *)
      FE.log ( Some e ) mname BL.INFO
             [ "LOGGED OUT, Exiting the FIX Engine Monitor..." ]
  }
};

