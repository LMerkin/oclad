(* vim:ts=2:syntax=ocaml
*)
value ch: Event.channel string = Event.new_channel ();


value rec thread1: int -> unit =
fun i ->
do{
  let recvd = Event.timed_sync ( Event.receive ch ) 0.1 in
  match recvd with
  [
    Some s ->
    do{
      Printf.printf "%d: RECEIVED: %s\n%!" i s;
      if s = ""
      then ()
      else thread1 ( i+1 )
    }

  | None   ->
    do{
      Printf.printf "%d: RECEIVER TIME-OUT\n%!" i;
      thread1 ( i+1 )
    }
  ]
};

value rec thread2: int -> unit =
fun i ->
do{
  Thread.delay 0.05;
  if i = 100
  then
    Event.sync ( Event.send ch "" )
  else
  let msg = string_of_int i in
  do{
    Event.sync ( Event.send ch msg );
    thread2 ( i+1 )
  }
};
  
value thr1 = Thread.create thread1 0;
value thr2 = Thread.create thread2 0;

Thread.join thr1;
Thread.join thr2;

exit 0;

