(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                          "RWLock" Implementation:                         *)
(*===========================================================================*)
(* The type of RWLocks is a custom one: *)
type     t = 'a;

external create     : unit -> t = "rwlock_create";
external lock_rd    : t -> unit = "rwlock_lock_rd";
external lock_wr    : t -> unit = "rwlock_lock_wr";
external try_lock_rd: t -> bool = "rwlock_try_lock_rd";
external try_lock_wr: t -> bool = "rwlock_try_lock_wr";
external unlock     : t -> unit = "rwlock_unlock";

