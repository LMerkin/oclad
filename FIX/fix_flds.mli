(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(* Fields of FIX Msgs:                                                       *)
(*===========================================================================*)
(* Error Handling:  *)
exception Partial_FIX_Msg;

(* Extracting Types of FIX Flds: *)
value get_fix_fld_type : Fix_types.fix_fld -> Fix_types.fix_fld_type;

(* Encoding / Decoding of FIX Flds.
   NB: "fix_fld_to_buffer" outputs a full SOH-terminated fld:
*)
value fix_fld_to_buffer: Buffer.t               -> Fix_types.fix_fld  -> unit;
value fix_fld_of_string: Fix_types.fix_fld_type -> string -> Fix_types.fix_fld;

(* "parse_fix_fld": Low-level parsing of FIX Flds.

   Args:      buff,     from_pos, max_len
   Returns: ( fld_type, fld_val,  next_pos ).

   NB: Std hdr fld types and vals are also returned by this function,  but they
   CANNOT be converted into "fix_fld" by "fix_fld_of_string" since there are no
   corresponding "fix_fld" ctors.

   Raises "Partial_FIX_Msg" if "max_len" chars have been read and the fld termi-
   nator SOH was not found.
   Raises "Invalid_FIX_Msg" on other errors:
*)
value     parse_fix_fld:   string ->   int -> int ->
        ( Fix_types.fix_fld_type  * string  * int );

(* Encoding / Decoding / Comparing Dates / Times:   *)

value compare_dts:         Fix_types.date_time -> Fix_types.date_time -> int;
value string_of_date_time: Fix_types.date_time -> string;
value date_time_of_string: string -> Fix_types.date_time;

(* Encoding / Decoding of FIX Msg Types:            *)

value fix_msg_type_of_string: string       -> Fix_types.fix_msg_type;
value nat_of_string         : string       -> int;

(* The following functions split/assemble FIX Msgs into/from lists of FIX Flds.
   They would more naturally belong to the "Fix_msgs" module, but installed
   here in order to avoid exporting lots of other ( low-level ) funcs:
*)
value fix_msg_to_flds: Fix_types.fix_msg      -> list Fix_types.fix_fld;

value fix_msg_of_flds: Fix_types.fix_msg_type -> list Fix_types.fix_fld ->
                       Fix_types.fix_msg;
