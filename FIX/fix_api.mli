(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*               FIX Application-Level ( API ) Functions                     *)
(*===========================================================================*)
(*===========================================================================*)
(* Engine and Reactor Management:                                            *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* Create and start a new FIX Engine:                                        *)
(*---------------------------------------------------------------------------*)
value mk_fix_engine:
  ?debug:bool -> Fix_configs.fix_config_id -> Fix_engine.fix_engine;

(*---------------------------------------------------------------------------*)
(* Explicit Graceful Shut-Down of a FIX Engine:                              *)
(*---------------------------------------------------------------------------*)
value shutdown: Fix_engine.fix_engine -> unit;

(*---------------------------------------------------------------------------*)
(* Reactor & Call-Backs:                                                     *)
(*---------------------------------------------------------------------------*)
type  fix_reactor_conf 's = 'a;

(* User Call-Back on FIX Events: The data are FIX msgs received, but the FIX
   Engine and the ReactorConf are also passed in as args,   since it may be re-
   quired e.g. for call-back actions. The FIX Engine passed in is the one which
   received the current event. This type is parameterised by the type of the
   CallBack state ( 's ):
*)
type call_back1     's =
   fix_reactor_conf 's -> Fix_engine.fix_engine -> 's -> Fix_types.fix_msg  ->
   's;

(* The default call-back which is invoked when the Reactor is not subscribed to
   any Events, so there is no msg to pass to it, or waiting for incoming msgs
   has timed-out (time-driven invocation):
*)
type call_back0     's = fix_reactor_conf 's -> 's -> 's;

(*---------------------------------------------------------------------------*)
(* "new_reactor_conf":                                                       *)
(*---------------------------------------------------------------------------*)
(* Creating an initial Reactor Conf, connected to [engines] if specified:    *)
value new_reactor_conf:
  ?engines: list Fix_engine.fix_engine ->
  ~time_cb: call_back0 's              ->
  ~period:  float                      ->
  ~main_cb: call_back1 's              ->
  fix_reactor_conf 's;

(*---------------------------------------------------------------------------*)
(* "reactor":                                                                *)
(*---------------------------------------------------------------------------*)
(* Runs in an infinite loop and invokes the provided call-backs on receiving the
   corresp events.  Reactor exits on the "Exit" exn raised by any call-back. The
   initial state is passed as Arg2:
*)
value reactor:  fix_reactor_conf 's -> 's -> unit;

(*===========================================================================*)
(* Composing and Sending FIX Msgs:                                           *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "req_securities_list":                                                    *)
(*---------------------------------------------------------------------------*)
value req_securities_list:
  ~req_type:      Fix_types.security_req_type ->
  fix_reactor_conf 's                         ->
  Fix_engine.fix_engine                       ->
  ( string * Fix_types.date_time );                 (* ReqID, TimeStamp      *)

(*---------------------------------------------------------------------------*)
(* "subscr_market_data":                                                     *)
(*---------------------------------------------------------------------------*)
(* XXX: We currently permit only 1 symbol per subscription, though the FIX pro-
   tocol generally allows multiple subscriptions at once. The reasons for our
   policy are the following:
   -- Some FIX implementations ( e.g. TFast ) cannot handle more than 1 symbol
      per subscription;
   -- To preserve the network bandwidth, we want to share subscriptions to com-
      mon symbols between different clients ( strategies ), so finer granulari-
      ty would be better:
*)
value subscr_market_data:
  ~exchange:      Fix_types.exchange            ->
  ?account:       string                        ->
  ~symbol:        string                        ->  (* "Long"  Symbol        *)
  ~security_id:   string                        ->  (* "Short" SecurityID    *)
  ~id_src:        Fix_types.id_source           ->
  ~entry_types:   array Fix_types.md_entry_type ->
  ~depth:         Fix_types.market_depth        ->
  ~incr_refresh:  bool                          ->
  fix_reactor_conf 's                           ->
  Fix_engine.fix_engine                         ->
  ( string * Fix_types.date_time );                 (* ReqID, TimeStamp      *)

(*---------------------------------------------------------------------------*)
(* "unsubscr_market_data":                                                   *)
(*---------------------------------------------------------------------------*)
(* TODO *)

(*---------------------------------------------------------------------------*)
(* "send_order":                                                             *)
(*---------------------------------------------------------------------------*)
(* If "price" is given,  it is a "LimitOrder", otherwise "MarketOrder";
   "time_in_force" defaults to "Day":
*)
value send_order:
  ~exchange:      Fix_types.exchange            ->
  ?account:       string                        ->  (* From exchange by def. *)
  ~cfi_code:      string                        ->
  ~symbol:        string                        ->  (* "Long"  Symbol        *)
  ~security_id:   string                        ->  (* "Short" SecurityID    *)
  ~id_src:        Fix_types.id_source           ->
  ~side:          Fix_types.side                ->
  ?price:         float                         ->  (* Market ord. if no prx *)
  ~currency:      Fix_types.currency            ->
  ~qty:           int                           ->
  ?time_in_force: Fix_types.time_in_force       ->
  fix_reactor_conf 's                           ->
  Fix_engine.fix_engine                         ->
  ( string * Fix_types.date_time );                 (* OrderID, TimeStamp    *)

(*---------------------------------------------------------------------------*)
(* "req_order_status":                                                       *)
(*---------------------------------------------------------------------------*)
(* NB: this function currently does not return an ReqID -- such functionality
   is only available in FIX.4.4 ( not FIX.4.2 ),  and that ReqID would not be
   of much use anyway -- there are no responses which would carry it:
*)
value req_order_status:
  ~exchange:      Fix_types.exchange            ->
  ?account:       string                        ->  (* From exchange by def. *)
  ~ord_id:        string                        ->
  ~cfi_code:      string                        ->
  ~symbol:        string                        ->
  ~side:          Fix_types.side                ->
  fix_reactor_conf 's                           ->
  Fix_engine.fix_engine                         ->
  Fix_types.date_time;                              (* TimeStamp             *)

(*---------------------------------------------------------------------------*)
(* "cancel_order":                                                           *)
(*---------------------------------------------------------------------------*)
value cancel_order:
  ~exchange:      Fix_types.exchange            ->
  ?account:       string                        ->  (* From exchange by def. *)
  ~ord_id:        string                        ->
  ~cfi_code:      string                        ->
  ~symbol:        string                        ->
  ~security_id:   string                        ->
  ~id_src:        Fix_types.id_source           ->
  ~side:          Fix_types.side                ->
  ~orig_qty:      int                           ->
  fix_reactor_conf 's                           ->
  Fix_engine.fix_engine                         ->
  ( string * Fix_types.date_time );                 (* COrderID, TimeStamp   *)

(*---------------------------------------------------------------------------*)
(* "cancel_all":                                                             *)
(*---------------------------------------------------------------------------*)
(* TODO *)

(*---------------------------------------------------------------------------*)
(* "modify_order":                                                           *)
(*---------------------------------------------------------------------------*)
value modify_order:
  ~exchange:      Fix_types.exchange            ->
  ?account:       string                        ->  (* From exchange by def. *)
  ~ord_id:        string                        ->
  ~cfi_code:      string                        ->
  ~symbol:        string                        ->
  ~side:          Fix_types.side                ->
  ~orig_qty:      int                           ->
  ~new_price:     float                         ->
  fix_reactor_conf 's                           ->
  Fix_engine.fix_engine                         ->
  ( string * Fix_types.date_time );                 (* MOrderID, TimeStamp   *)

(*===========================================================================*)
(* Misc Utils:                                                               *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "remove_from_fwding_map":                                                 *)
(* "clean_dead_msg_ids" :                                                    *)
(*---------------------------------------------------------------------------*)
(* When we know that we are done with a particular MsgID ( e.g., and order has
   been filled or cancelled ), we can explicitly remove it from the forwarding
   map of the corresp engine so that the latter would not grow indefinitely:
*)
value remove_from_fwding_map: Fix_engine.fix_engine -> string -> unit;

(* The above function transfers the removed MsgID into a "dead pool" within the
   Engine ( this is to avoid spurious errors when unable to dispatch incoming
   msgs ). This map can be cleaned periodically, e.g. after the end of the oper
   day:
*)
value clean_dead_msg_ids    : Fix_engine.fix_engine -> unit;

(*---------------------------------------------------------------------------*)
(* "get_engines":                                                            *)
(*---------------------------------------------------------------------------*)
(* Returns the list of all Engines currently attched to the given Reactor:   *)

value get_engines: fix_reactor_conf 's -> list Fix_engine.fix_engine;

