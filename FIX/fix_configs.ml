(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                         Configs for FIX Connections:                      *)
(*===========================================================================*)
module FT = Fix_types;
module FM = Fix_msgs;

(*===========================================================================*)
(* Types:                                                                    *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* Enumerator of all supported Configs:                                      *)
(*---------------------------------------------------------------------------*)
type fix_config_id =
[
  RTS_Direct_Orders_Dry
| RTS_Direct_Orders
| OSL_Quotes_All
| OSL_Orders_All
| OSL_MICEX_Orders_Dry
| TFast_FORTS_Dry
]
with string_of, parse;

(*---------------------------------------------------------------------------*)
(* The FIX Config Type:                                                      *)
(*---------------------------------------------------------------------------*)
type multi_acc  = ( FT.exchange * string * string );
                  (* Exchange,    Acc:1,   ClientID:109       *)

type fix_config =
{
  config_id         : fix_config_id;

  (* Flds of the standard FIX header etc:                     *)
  fix_version       : string;             (* E.g. "FIX.4.2"   *)
  our_comp_id       : string;
  server_comp_id    : string;
  accounts          : list multi_acc;
  heart_bt_int      : int;                (* In sec           *)

  (* Connection params:                                       *)
  server_ip         : string;
  server_port       : int;
  reconnect_int     : int;                (* In sec                          *)

  (* FIX session management:                                  *)
  time_corr         : float;              (* ServerTime - LocalTime, sec     *)
  reset_on_disconn  : bool;               (* Disconnect resets the counters? *)
  max_sent_memo_len : int;

  (* Logging and Debugging. The Bolt Logger config is separate:              *)
  debug             : bool
};


(*===========================================================================*)
(* Direct FORTS Configs:                                                     *)
(*===========================================================================*)
(*-------------------------*)
(* "rts_direct_dry_conf":  *)
(*-------------------------*)
(* Config for submitting orders directly via the RTS FIX interface -- currently
   dry-run mode only. Not suitable for receiving market quotes either:
*)
value rts_direct_dry_conf: fix_config =
{
  config_id          = RTS_Direct_Orders_Dry;
  fix_version        = "FIX.4.4";
  our_comp_id        = "FEDOSEEV";
  server_comp_id     = "FORTS_GAME_01";
  accounts           = [ ( FT.FORTS, "044", "FZ00044" ) ];
  heart_bt_int       = 30;     (* Not specified by the RTS FIX docs       *)
  server_ip          = "194.247.133.77";
  server_port        = 5001;
  reconnect_int      = 10;     (* Not specified by the RTS FIX docs       *)
  time_corr          = 0.0;
  reset_on_disconn   = False;
  max_sent_memo_len  = 10;     (* Keep it short!                          *)
  debug              = False
};

(*-------------------------*)
(* "rts_direct_conf":      *)
(*-------------------------*)
(* Config for submitting orders directly via the RTS FIX interface -- currently
   dry-run mode only. Not suitable for receiving market quotes either:
*)
value rts_direct_conf: fix_config =
{
  config_id          = RTS_Direct_Orders;
  fix_version        = "FIX.4.4";
  our_comp_id        = "OPENM@GATE_CTX5DOFIX1";
  server_comp_id     = "RTS_FIX_01";
  accounts           = [ ( FT.FORTS, "5DO", "FZ00044" ) ];
  heart_bt_int       = 30;     (* Not specified by the RTS FIX docs       *)
  server_ip          = "194.247.133.24";
  server_port        = 5001;
  reconnect_int      = 10;     (* Not specified by the RTS FIX docs       *)
  time_corr          = 0.0;
  reset_on_disconn   = False;
  max_sent_memo_len  = 10;     (* Keep it short!                          *)
  debug              = False
};

(*===========================================================================*)
(* OSL Configs:                                                              *)
(*===========================================================================*)
(*-------------------------*)
(* "osl_orders_all_conf":  *)
(*-------------------------*)
(* Config for submitting ANY Market Orders over OSL FIX interface:           *)

value osl_orders_all_conf: fix_config =
{
  config_id          = OSL_Orders_All;
  fix_version        = "FIX.4.2";
  our_comp_id        = "ALTA";
  server_comp_id     = "OPEN";
  accounts           = [ ( FT.SETS,  "L0-GLSpot",   "AL0346" );      (* LSE! *)
                         ( FT.NYSE,  "L0-GLSpot",   "AL0346" );
                         ( FT.MICEX, "7265/00",     "AL0346" );
                         ( FT.FORTS, "SPBFUT265DO", "AL0346" )
                       ];
  heart_bt_int       = 30;
  server_ip          = "193.219.127.34";
  server_port        = 9860;
  reconnect_int      = 10;
  time_corr          = 0.0;
  reset_on_disconn   = False;
  max_sent_memo_len  = 10;
  debug              = False
};

(*-------------------------*)
(* "osl_micex_dry_conf":   *)
(*-------------------------*)
(* Config for submitting MICEX Dry-Run Orders over OSL FIX interface. XXX: There
   is no similar capability with OSL for RTS or other exchanges yet:
*)
value osl_micex_dry_conf: fix_config =
{
  ( osl_orders_all_conf ) with
  config_id          = OSL_MICEX_Orders_Dry;
  accounts           = [ ( FT.MICEX, "L01-00000F00", "427" ) ]
};

(*-------------------------*)
(* "osl_quotes_all_conf":  *)
(*-------------------------*)
(* Config for receiving Russian Market Quotes over OSL FIX interface:        *)

value osl_quotes_all_conf: fix_config =
{
  config_id          = OSL_Quotes_All;
  fix_version        = "FIX.4.2";
  our_comp_id        = "ALTAMD1";
  server_comp_id     = "OPENMD1";
  heart_bt_int       = 30;
  accounts           = [ ( FT.SETS,  "L0-GLSpot",   "AL0346" );      (* LSE! *)
                         ( FT.NYSE,  "L0-GLSpot",   "AL0346" );
                         ( FT.MICEX, "7265/00",     "AL0346" );
                         ( FT.FORTS, "SPBFUT265DO", "SPBFUT265DO" )
                       ];
  server_ip          = "193.219.127.34";
  server_port        = 9862;
  reconnect_int      = 10;
  time_corr          = 0.0;
  reset_on_disconn   = False;
  max_sent_memo_len  = 10;
  debug              = False
};

(*===========================================================================*)
(* TFast Configs:                                                            *)
(*===========================================================================*)
(* TFast configs are obtained dynamically from a designated Config Server:   *)

(*-------------------------*)
(* "send_str":             *)
(*-------------------------*)
(* Send a single string over a socket:      *)

value send_str: Unix.file_descr -> string -> unit =
fun sock str ->
  let len  = String.length  str  in
  let sent = Unix.send sock str 0 len []    in
  if  sent <> len
  then failwith "Incomplete send"
  else ();

(*-------------------------*)
(* "recv_str":             *)
(*-------------------------*)
(* Receive a single string over a socket:   *)

value recv_str: Unix.file_descr -> string -> string =
fun sock buff ->
  let len  = String.length  buff in
  let got  = Unix.recv sock buff 0 len []    in
  String.sub buff 0 got;

(*-------------------------*)
(* RegExps:                *)
(*-------------------------*)
(* For parsing the Confif Server responses: *)

value ts_re      = Pcre.regexp ~{ study=True }
  ( "Syncronized data: \\t(\\d{4}) (\\d{1,2}) (\\d) (\\d{1,2}) "^
  (*                          YYYY     MM       DoW     DD   *)
    "(\\d{1,2}) (\\d{1,2}) (\\d{1,2}) (\\d{1,3})" );
  (*    hh         mm         ss        .sss                 *)

value fix_ver_re = Pcre.regexp ~{ study=True } "BeginString=([A-Z0-9.]+)";
value hb_int_re  = Pcre.regexp ~{ study=True } "HeartBtInt=([0-9]+)";
value rc_int_re  = Pcre.regexp ~{ study=True } "ReconnectInterval=([0-9]+)";
value tcid_re    = Pcre.regexp ~{ study=True } "TargetCompID=([A-Za-z0-9_-]+)";
value scid_re    = Pcre.regexp ~{ study=True } "SenderCompID=([A-Za-z0-9_-]+)";
value host_re    = Pcre.regexp ~{ study=True } "SocketConnectHost=([0-9.]+)";
value port_re    = Pcre.regexp ~{ study=True } "SocketConnectPort=([0-9]+)";
value resetd_re  = Pcre.regexp ~{ study=True } "ResetOnDisconnect=([YN])";

(*-------------------------*)
(* "tfast_forts_dry_conf": *)
(*-------------------------*)
(* Config for submitting FORTS Dry-Run Orders over TFast FIX interface:      *)

value tfast_forts_dry_conf: unit -> fix_config =
fun () ->
  let conf_server_ip   = "217.15.196.112"   in
  let conf_server_port = 10002              in
  let buff             = String.create 8192 in
  try
    let sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
    do{
      Unix.connect
        sock ( Unix.ADDR_INET ( Unix.inet_addr_of_string conf_server_ip )
                                conf_server_port  );
      (*----------------------------------------------------------*)
      (* Get Date/Time from the server to synchronise the clocks: *)
      (*----------------------------------------------------------*)
      send_str sock "Get\ttime\r\n";
      let dt0      = Unix.gettimeofday ()           in

      let tstr     = recv_str sock buff             in
      let dt_parts = Pcre.extract ~{ full_match=False } ~{ rex=ts_re } tstr in
      let year     = int_of_string   dt_parts.( 0 ) in
      let month    = int_of_string   dt_parts.( 1 ) in
      let wday     = int_of_string   dt_parts.( 2 ) in
      let day      = int_of_string   dt_parts.( 3 ) in
      let hour     = int_of_string   dt_parts.( 4 ) in
      let min      = int_of_string   dt_parts.( 5 ) in
      let sec      = int_of_string   dt_parts.( 6 ) in
      let msec     = float_of_string dt_parts.( 7 ) /. 1000.0 in
      let tm       =
      {
        Unix.tm_sec   = sec;
        Unix.tm_min   = min;
        Unix.tm_hour  = hour;
        Unix.tm_mday  = day;
        Unix.tm_mon   = month - 1;    (* 0 .. 11    *)
        Unix.tm_year  = year  - 1900; (* Since 1900 *)
        Unix.tm_wday  = wday;         (* Not used   *)
        Unix.tm_yday  = 0;            (* Not used   *)
        Unix.tm_isdst = False         (* Not used   *)
      }
      in
      (* Convert "tmp" into Seconds for the Epoch, but this is done is the
         local time zone while the input was actually in GMT. So set TZ to
         GMT and restore it afterwards:
      *)
      let tz  =
        try   Unix.getenv   "TZ"
        with[ _ -> failwith "ERROR: TZ env var not set" ]
      in
      let dt1 =
      do{
        Unix.putenv "TZ" "GMT";
        let ( dt1', _ ) = Unix.mktime tm in
        do{
          Unix.putenv "TZ" tz ;
          (* Add milliseconds as well: *)
          dt1' +. msec
        }
      }
      in
      (* Compute the clock offset: "dt1" is the TFast time, "dt0" is our local
         time:
      *)
      let time_corr = dt1 -. dt0         in
      Printf.printf "TFast TimeCorr=%f sec\n%!" time_corr;

      (*----------------------------------------------------------*)
      (* Extract the rest of the TFast Config:                    *)
      (*----------------------------------------------------------*)
      send_str sock "Get FIX Client config|DEFAULT\r\n";
      let tstr          = recv_str sock buff             in
      let ()            = prerr_endline tstr             in
      let fix_ver       =
        ( Pcre.extract ~{ full_match=False } ~{ rex=fix_ver_re } tstr ).( 0 )
      in
      let heart_bt_int  =
        int_of_string
        ( Pcre.extract ~{ full_match=False } ~{ rex=hb_int_re }  tstr ).( 0 )
      in
      let reconn_int    =
        int_of_string
        ( Pcre.extract ~{ full_match=False } ~{ rex=rc_int_re }  tstr ).( 0 )
      in
      let targ_comp_id  =
        ( Pcre.extract ~{ full_match=False } ~{ rex=tcid_re   }  tstr ).( 0 )
      in
      let sndr_comp_id  =
        ( Pcre.extract ~{ full_match=False } ~{ rex=scid_re   }  tstr ).( 0 )
      in
      let host          =
        ( Pcre.extract ~{ full_match=False } ~{ rex=host_re   }  tstr ).( 0 )
      in
      let port          =
        int_of_string
        ( Pcre.extract ~{ full_match=False } ~{ rex=port_re   }  tstr ).( 0 )
      in
      let reset_disconn =
        ( Pcre.extract ~{ full_match=False } ~{ rex=resetd_re }  tstr ).( 0 )
        = "Y"
      in
      do{
        (* Close the config socket: *)
        Unix.close sock;

        (* The resulting config:    *)
        {
          config_id          = TFast_FORTS_Dry;
          fix_version        = fix_ver;
          our_comp_id        = sndr_comp_id;
          server_comp_id     = targ_comp_id;
          accounts           = [ ( FT.FORTS, "DEMO.001", "" ) ];
          heart_bt_int       = heart_bt_int;
          server_ip          = host;
          server_port        = port;
          reconnect_int      = reconn_int;
          time_corr          = time_corr;
          reset_on_disconn   = reset_disconn;
          max_sent_memo_len  = 8;
          debug              = False
        }
      }
    }
  with
  [ any -> failwith( "ERROR: Unable to get TFast FIX Server Config:\n"^
                   ( FM.string_of_exn any ) )
  ];

(*---------------------------------------------------------------------------*)
(* "mk_fix_config":                                                          *)
(*---------------------------------------------------------------------------*)
value mk_fix_config: ?debug:bool -> fix_config_id -> fix_config =

fun ?{ debug=False } config_id   ->
  let conf0 =
    match config_id with
    [
      RTS_Direct_Orders_Dry -> rts_direct_dry_conf
    | RTS_Direct_Orders     -> rts_direct_conf
    | OSL_Quotes_All        -> osl_quotes_all_conf
    | OSL_Orders_All        -> osl_orders_all_conf
    | OSL_MICEX_Orders_Dry  -> osl_micex_dry_conf
    | TFast_FORTS_Dry       -> tfast_forts_dry_conf ()
    ]
  in
  { ( conf0 ) with
    debug            = debug
  };

