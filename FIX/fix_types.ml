(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                            Types of FIX Messages:                         *)
(*===========================================================================*)
(* XXX: The Enum and Msg types here are generated separately ( rather than "en
   mass" ) only because of "with string_of" additions. This should be fixed in
   the future, so "fix_defs.ml" would be the only src of all FIX types:
*)
(*===========================================================================*)
(* Common Types and Values:                                                  *)
(*===========================================================================*)
type price              = float                              with string_of;

(* Date: ( YYYY, MM, DD       )
   Time: ( hh,   mm, ss[.sss] )
*)
type date               = ( int * int * int )                with string_of;
type time               = ( int * int * float )              with string_of;
type date_time          = ( date   *   time )                with string_of;

(*===========================================================================*)
(* Admin Msgs:                                                               *)
(*===========================================================================*)
type fix_msg_type       = FIX_ENUM_TYPE "fix_msg_type"       with string_of;
type fix_fld_type       = FIX_ENUM_TYPE "fix_fld_type"       with string_of;

type heart_beat         = FIX_MSG       "HeartBeat"          with string_of;
type test_req           = FIX_MSG       "TestReq"            with string_of;
type resend_req         = FIX_MSG       "ResendReq"          with string_of;

type session_rej_reason = FIX_ENUM_TYPE "session_rej_reason" with string_of;
type reject             = FIX_MSG       "Reject"             with string_of;
type seq_reset          = FIX_MSG       "SeqReset"           with string_of;
type log_out            = FIX_MSG       "LogOut"             with string_of;

type encrypt_method     = FIX_ENUM_TYPE "encrypt_method"     with string_of;
type log_on             = FIX_MSG       "LogOn"              with string_of;

(*===========================================================================*)
(* Application Msgs:                                                         *)
(*===========================================================================*)
(*-----------------------------------------*)
(* "SecurityDefReq":                       *)
(*-----------------------------------------*)
type security_req_type  = FIX_ENUM_TYPE "security_req_type"  with string_of;
type security_def_req   = FIX_MSG       "SecurityDefReq"     with string_of;

(*-----------------------------------------*)
(* Parsed Types:                           *)
(*-----------------------------------------*)
type exchange           = FIX_ENUM_TYPE "exchange"
  with string_of, parse;

type id_source          = FIX_ENUM_TYPE "id_source"
  with string_of, parse;

type currency           = FIX_ENUM_TYPE "currency"
  with string_of, parse;

type side               = FIX_ENUM_TYPE "side"
  with string_of, parse;

(*-----------------------------------------*)
(* "SecurityDef":                          *)
(*-----------------------------------------*)
type security_resp_type = FIX_ENUM_TYPE "security_resp_type" with string_of;
type security_type      = FIX_ENUM_TYPE "security_type"      with string_of;
type security_def       = FIX_MSG       "SecurityDef"        with string_of;

(*-----------------------------------------*)
(* "MarketDataReq":                        *)
(*-----------------------------------------*)
type md_update_type     = FIX_ENUM_TYPE "md_update_type"     with string_of;
type md_entry_type      = FIX_ENUM_TYPE "md_entry_type"      with string_of;
type subscr_req_type    = FIX_ENUM_TYPE "subscr_req_type"    with string_of;
type symbol_entry       = FIX_FLD_GROUP "SymbolEntry"        with string_of;

type market_depth       = (* Not an enum of 0-ctors: cannot auto-generate: *)
[
  FullBook
| BookTop
| BestPriceTiers of int
]
with string_of;

type market_data_req    = FIX_MSG       "MarketDataReq"      with string_of;

(*-----------------------------------------*)
(* "MarketData":                           *)
(* "MarketDataIncrRefresh":                *)
(*-----------------------------------------*)
type md_entry           = FIX_FLD_GROUP "MDEntry"            with string_of;
type md_update_action   = FIX_ENUM_TYPE "md_update_action"   with string_of;
type md_entry_incr      = FIX_FLD_GROUP "MDEntryIncr"        with string_of;
type market_data        = FIX_MSG       "MarketData"         with string_of;
type market_data_incr   = FIX_MSG       "MarketDataIncr"     with string_of;

(*-----------------------------------------*)
(* "MarketDataReqReject":                  *)
(*-----------------------------------------*)
type md_req_rej_reason  = FIX_ENUM_TYPE "md_req_rej_reason"  with string_of;
type market_data_req_rej= FIX_MSG       "MarketDataReqRej"   with string_of;

(*-----------------------------------------*)
(* "NewSingleOrder":                       *)
(*-----------------------------------------*)
type handl_inst         = FIX_ENUM_TYPE "handl_inst"         with string_of;
type order_type         = FIX_ENUM_TYPE "order_type"         with string_of;
type time_in_force      = FIX_ENUM_TYPE "time_in_force"      with string_of;
type settlmnt_type      = FIX_ENUM_TYPE "settlmnt_type"      with string_of;
type party_id_source    = FIX_ENUM_TYPE "party_id_source"    with string_of;
type party_role         = FIX_ENUM_TYPE "party_role"         with string_of;
type party              = FIX_FLD_GROUP "Party"              with string_of;
type new_single_order   = FIX_MSG       "NewSingleOrder"     with string_of;

(*-----------------------------------------*)
(* "ExecReport":                           *)
(*-----------------------------------------*)
type order_status       = FIX_ENUM_TYPE "order_status"       with string_of;
type exec_trans_type    = FIX_ENUM_TYPE "exec_trans_type"    with string_of;
type trade_type         = FIX_ENUM_TYPE "trade_type"         with string_of;
type order_capacity     = FIX_ENUM_TYPE "order_capacity"     with string_of;
type comm_type          = FIX_ENUM_TYPE "comm_type"          with string_of;
type order_rej_reason   = FIX_ENUM_TYPE "order_rej_reason"   with string_of;
type exec_restatement_reason
                        = FIX_ENUM_TYPE "exec_restatement_reason"
                                                             with string_of;
type exec_report        = FIX_MSG       "ExecReport"         with string_of;

(*-----------------------------------------*)
(* "OrderCancelReq":                       *)
(* "OrderModifyReq":                       *)
(* "OrderStatusReq":                       *)
(*-----------------------------------------*)
type order_cancel_req   = FIX_MSG       "OrderCancelReq"     with string_of;
type order_modify_req   = FIX_MSG       "OrderModifyReq"     with string_of;
type order_status_req   = FIX_MSG       "OrderStatusReq"     with string_of;

(*-----------------------------------------*)
(* "OrderCancelReject":                    *)
(*-----------------------------------------*)
type cxl_rej_resp_to    = FIX_ENUM_TYPE "cxl_rej_resp_to"    with string_of;
type cxl_rej_reason     = FIX_ENUM_TYPE "cxl_rej_reason"     with string_of;
type order_cancel_reject= FIX_MSG       "OrderCancelReject"  with string_of;

(*===========================================================================*)
(* The Top_level FIX Fld and FIX Msg Types:                                  *)
(*===========================================================================*)
type fix_fld       = FIX_TOP "fix_fld" with string_of;
type fix_msg       = FIX_TOP "fix_msg" with string_of;

(*===========================================================================*)
(* Error Handling:                                                           *)
(*===========================================================================*)
(* The following exception provides exception info in a format similar ( but
   not identical ) to "session_rej_reason":
*)
type fix_msg_error =
{
  err_seq_num     : int;                            (* <= 0 if not available *)
  err_msg_type    : option fix_msg_type;
  err_fld_type    : option fix_fld_type;
  err_fld_val     : string;                         (* ""   if not available *)
  err_text        : string;                         (* ""   if not available *)
  err_reason      : option session_rej_reason;
  err_off         : int;      (* Offset from msg begin, < 0 if not available *)
  err_next_msg_pos: int       (* Next parsing positon,  < 0 if not available *)
}
with string_of;

exception Invalid_FIX_Msg of fix_msg_error;

(*===========================================================================*)
(* Special Arrangements:                                                     *)
(*===========================================================================*)
(* "N/A" values -- used to represent uninitialised ( or totally missing ) vals
   of the corresp types. Translated into "None" for optional flds,  or parsing
   errors for mandatory ones. XXX: this is an OSL-specific hack!
*)
value n_a_price     = -1.0;
value n_a_nat       = -1;
value n_a_date      = ( 0, 0, 0   );
value n_a_time      = ( 0, 0, 0.0 );
value n_a_date_time = ( n_a_date, n_a_time );
value n_a_currency  = CCY_N_A;

