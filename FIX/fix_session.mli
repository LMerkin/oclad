(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                              FIX Session API:                             *)
(*===========================================================================*)
(* NB: The "fix_session" mosule is mostly concerns with internal working of the
   FIX Engine. It exports very few public values:
*)
(* "get_fix_msg_ids":
   Extracting Message ID(s) from various FIX msgs. Typically, session-level
   msgs have no IDs; application-level msgs have 1, and sometimes 2, IDs:
*)
value get_fix_msg_ids : Fix_types.fix_msg -> list string;

(* "fix_finalise":
   Close the FIX Engine properly before finalisation:
*)
value fix_finalise    :
  ~graceful:bool -> ~msg:string -> Fix_engine.fix_engine -> unit;

(* "fix_monitor_loop":
   The body of the main ( monitor ) thread created inside the FIX Engine:
*)
value fix_monitor_loop: Fix_engine.fix_engine -> unit;

