(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                "RWLock": Read-Only and Read-Write Locks:                  *)
(*===========================================================================*)

(* The type of RWLocks: *)
type t = 'a;

(* Create a new RWLock: *)
value create : unit -> t;

(* Acquite the lock in the read-only mode. Multiple reader threads can acquire
   the lock simultaneously, provided that there are no writers:
*)
value lock_rd: t -> unit;

(* Acquire the lock in the read-write mode: only 1 thread can do it at a time,
   provided that there are no readers either:
*)
value lock_wr: t -> unit;

(* Same as above, but non-blocking: the following functions return "true" imme-
   diately if the lock was acquired, "False" otherwise:
*)
value try_lock_rd: t -> bool;
value try_lock_wr: t -> bool;

(* Release the lock. Unspecified behaviour ( possibly an exception ) if the lock
   to be released was not previously acquired:
*)
value unlock: t -> unit;

