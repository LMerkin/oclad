(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*         CamlP5 Definitions for Generating FIX Types and Functions         *)
(*===========================================================================*)
(* Manfatory ( Single ), Optional and Group Fields. For the latter, we must
   also specifu the name of the fld containing the count of groups, hence the
   arg:
*)
type  sog =
[
  Single
| Option
| Group    of string  (* Count Fld Name *)
| OptGroup of string  (* Ditto          *)
];

type codec =
[
  Char
| Int
| String
];

(*===========================================================================*)
(* Non-Elementary Sub-Types of FIX Field Values:                             *)
(*===========================================================================*)
value fix_fld_subtypes_def =
[
  (* XXX: the following types are listed here only to specify their codecs --
     the value sets are constructed differently:
  *)
  ( "fix_fld_type",                     Int,              [] );
  ( "fix_msg_type",                     String,           [] );
  ( "market_depth",                     Int,              [] );

  (* The rest is the generic case:          *)

  ( "comm_type",                        Char,
    [
      ( "PerShare",                     "1" );
      ( "Percentage",                   "2" );
      ( "Absolute",                     "3" )
    ]
  );
  ( "exec_trans_type",                  Char,
    [
      ( "NewExec",                      "0" );
      ( "CancelExec",                   "1" );
      ( "CorrectExec",                  "2" );
      ( "StatusExec",                   "3" )
    ]
  );
  ( "handl_inst",                       Char,
    [
      ( "AutoExecPrivate",              "1" )
    ]
  );
  ( "id_source",                        Char,
    [
      ( "CUSIP",                        "1" );
      ( "SEDOL",                        "2" );
      ( "QUIK",                         "3" );
      ( "ISIN",                         "4" );
      ( "RIC",                          "5" );
      ( "ISO_CcyCode",                  "6" );
      ( "ISO_CountryCode",              "7" );
      ( "Exchange_Symbol",              "8" );
      ( "CTA_Symbol",                   "9" )
    ]
  );
  ( "order_status",                     Char,
    [
      ( "NewOrder",                     "0" );
      ( "PartFilled",                   "1" );
      ( "Filled",                       "2" );
      ( "DoneForDay",                   "3" );
      ( "Canceled",                     "4" );
      ( "Replaced",                     "5" );
      ( "PendingCancel",                "6" );
      ( "Stopped",                      "7" );
      ( "Rejected",                     "8" );
      ( "Suspended",                    "9" );
      ( "PendingNew",                   "A" );
      ( "Calculated",                   "B" );
      ( "Expired",                      "C" );
      ( "AcceptedForBidding",           "D" );
      ( "PendingReplace",               "E" );
      (* The rest are for FIX-4.4 only:    *)
      ( "Traded",                       "F" ); (* "PartFilled" or "Filled"   *)
      ( "TradeCorrect",                 "G" );
      ( "TradeCancel",                  "H" ); (* "Cancelled" ???            *)
      ( "OrderStatusHmm",               "I" ); (* What is it  ???            *)
      ( "TradeInClearingHold",          "J" );
      ( "TradeReleasedToClearing",      "K" );
      ( "TriggeredBySystem",            "L" )
    ]
  );
  ( "order_type",                       Char,
    [
      ( "Market",                       "1" );
      ( "Limit",                        "2" );
      ( "Stop",                         "3" );
      ( "StopLimit",                    "4" )
    ]
  );
  (* NB "order_capacity" is known as "Rule80A" in the US. We maintain a small
     subset of the US type:
  *)
  ( "order_capacity",                   Char,
    [
      ( "Principal",                    "P" )
    ]
  );
  ( "side",                             Char,
    [
      ( "Buy",                          "1" );
      ( "Sell",                         "2" )
    ]
  );
  ( "time_in_force",                    Char,
    [
      ( "Day",                          "0" );
      ( "GoodTillCancel",               "1" );
      ( "FillAndKill",                  "3" );  (* Also "ImmediateOrCancel"  *)
      ( "FillOrKill",                   "4" );
      ( "GoodTillDate",                 "5" )   (* Then "ExpiryDate" needed  *)
    ]
  );
  ( "settlmnt_type",                    Char,
    [
      ( "Regular",                      "0" );
      ( "Cash",                         "1" );
      ( "NextDay",                      "2" );  (* T + 1d *)
      ( "Tp2",                          "3" );  (* T + 2d *)
      ( "Tp3",                          "4" );  (* T + 3d *)
      ( "Tp4",                          "5" );  (* T + 4d *)
      ( "Future",                       "6" );
      ( "WhenIssued",                   "7" );
      ( "SellersOption",                "8" );
      ( "Tp5",                          "9" )   (* T + 5d *)
    ]
  );
  ( "encrypt_method",                   Char,
    [
      ( "UnEncrypted",                  "0" );
      ( "PKCS",                         "1" );
      ( "DES_ECB",                      "2" );
      ( "PKCS_DES",                     "3" );
      ( "PGP_DES",                      "4" );
      ( "PGP_DES_MD5",                  "5" );
      ( "PEM_DES_MD5",                  "6" )
    ]
  );
  ( "subscr_req_type",                  Char,
    [
      ( "Snapshot",                     "0" );
      ( "Subscribe",                    "1" );
      ( "UnSubscribe",                  "2" )
    ]
  );
  (* Types of subscriptions to market data:            *)
  ( "md_update_type",                   Char,
    [
      ( "FullRefresh",                  "0" );
      ( "IncrRefresh",                  "1" )
    ]
  );
  (* Types of market data updates:                     *)
  ( "md_update_action",                 Char,
    [
      ( "New",                          "0" );
      ( "Change",                       "1" );
      ( "Delete",                       "2" )
    ]
  );
  (* Different types of prices which can be requested: *)
  ( "md_entry_type",                    Char,
    [
      ( "Bid",                          "0" );
      ( "Offer",                        "1" );
      ( "Trade",                        "2" );
      ( "IndexValue",                   "3" );
      ( "OpeningPrice",                 "4" );
      ( "ClosingPrice",                 "5" );
      ( "SettlmntPrice",                "6" );
      ( "SessionHigh",                  "7" );
      ( "SessionLow",                   "8" );
      ( "SessionVWAP",                  "9" )
    ]
  );
  ( "security_req_type",                Char,
    [
      ( "ReqSecurityIDAndSpecs",        "0" );
      ( "ReqSecurityIDForSpecs",        "1" );
      ( "ReqListSecurityTypes",         "2" );
      ( "ReqListSecurities",            "3" )
    ]
  );
  ( "security_resp_type",               Char,
    [
      ( "AcceptSecurityAsIs",           "1" );
      ( "AcceptSecurityWithRevisions",  "2" );
      ( "ListSecurityTypes",            "3" );
      ( "ListSecurities",               "4" );
      ( "RejectSecurity",               "5" );
      ( "CanNotMatchSelection",         "6" )
    ]
  );
  (* XXX: The following type is a non-standard extension:            *)
  ( "trade_type",                       Char,
    [
      ( "Repo",                         "R" );
      ( "OTC",                          "N" );  (* "Negotiated"      *)
      ( "PrimaryDistr",                 "P" );
      ( "CommonTrade",                  "T" )
    ]
  );
  ( "md_req_rej_reason",                Char,
    [
      ( "UnknownMDSymbol",              "0" );
      ( "DuplicateMDReqID",             "1" );
      ( "InsufficientBandwidth",        "2" );
      ( "InsufficientPermission",       "3" );
      ( "UnsupportedSubscrReqType",     "4" );
      ( "UnsupportedMarketDepth",       "5" );
      ( "UnsupportedMDUpdateType",      "6" );
      ( "UnsupportedAggregatedBook",    "7" );
      ( "UnsupportedMDEntryType",       "8" )
    ]
  );
  ( "cxl_rej_resp_to",                  Char,
    [
      ( "CancelReq",                    "1" );
      ( "ModifyReq",                    "2" )
    ]
  );
  ( "cxl_rej_reason",                   Char,
    [
      ( "TooLateToCancel",              "0" );
      ( "UnknownOrderToCancel",         "1" );
      ( "BrokerOptionToCancel",         "2" );
      ( "CancelAlreadyPending",         "3" )
    ]
  );
  ( "party_id_source",                  Char,
    [
      ( "BIC",                          "B" );
      ( "General",                      "C" );
      ( "PropCustom",                   "D" );
      ( "ISO_CountryCode_PIDS",         "E" );
      ( "SettlmntEntityLoc",            "F" );
      ( "MIC",                          "G" );
      ( "CSD",                          "H" )
    ]
  );
  ( "order_rej_reason",                 Int,
    [
      ( "BrokerOption",                 "0" );
      ( "UnknownOrdSymbol",             "1" );
      ( "ExchangeClosed",               "2" );
      ( "OrderExceedsLimit",            "3" );
      ( "TooLateToEnter",               "4" );
      ( "UnknownOrder",                 "5" );
      ( "DuplicateOrder",               "6" );
      ( "DuplicateOfVerbalOrder",       "7" );
      ( "StaleOrder",                   "8" );
      (* The rest is for FIX 4.4.+ only:   *)
      ( "TradeAlongReq",                "9" );
      ( "InvInvID",                    "10" );
      ( "UnsuppOrderChar",             "11" );
      ( "Surveillence",                "12" );
      ( "IncorrectQty",                "13" );
      ( "IncorrectAllocatedQty",       "14" );
      ( "UnknownAccounts",             "15" );
      ( "OtherOrderRejReason",         "99" )
    ]
  );
  ( "session_rej_reason",               Int,
    [
      ( "InvalidTagNum",                "0" );  (* No such Tag       *)
      ( "RequiredTagMissing",           "1" );
      ( "ImproperTag",                  "2" );
      ( "UndefinedTag",                 "3" );  (* Value without Tag *)
      ( "TagWithoutValue",              "4" );
      ( "ValueRangeError",              "5" );
      ( "ValueFormatError",             "6" );
      ( "DecryptionError",              "7" );
      ( "SignatureError",               "8" );
      ( "CompIDError",                  "9" );
      ( "SendingTimeError",            "10" );
      ( "InvalidMsgType",              "11" )
    ]
  );
  ( "party_role",                       Int,
    [
      ( "ExecFirm",                      "1" );
      ( "BrokerOfCredit",                "2" );
      ( "ClientID_PR",                   "3" );
      ( "ClearingFirm",                  "4" );
      ( "InvestorID",                    "5" );
      ( "IntroducingFirm",               "6" );
      ( "EnteringFirm",                  "7" );
      ( "LocateLendingFirm",             "8" );
      ( "FundManagerClientID",           "9" );
      ( "SettlmntLocation",             "10" );
      ( "OrderOrigTrader",              "11" );
      ( "ExecTrader",                   "12" );
      ( "OrderOrigFirm",                "13" );
      ( "GiveUpClearingFirm",           "14" );
      ( "CorrespClearingFirm",          "15" );
      ( "ExecSystem",                   "16" );
      ( "ContraFirm",                   "17" );
      ( "ContraClearingFirm",           "18" );
      ( "SponsoringFirm",               "19" );
      ( "UnderlyingContraFirm",         "20" );
      ( "ClearingOrganisation",         "21" );
      ( "Exchange",                     "22" );
      (* Unassigned:                    "23" *)
      ( "CustomerAccount",              "24" );
      ( "CorrespClearingOrganisation",  "25" );
      ( "CorrespBroker",                "26" );
      ( "BuyerSeller",                  "27" );
      ( "Custodian",                    "28" );
      ( "Intermediary",                 "29" );
      ( "Agent",                        "30" );
      ( "SubCustodian",                 "31" );
      ( "Beneficiary",                  "32" );
      ( "InterestedParty",              "33" );
      ( "RegulatoryBody",               "34" );
      ( "LiquidityProvider",            "35" );
      ( "EnteringTrader",               "36" );
      ( "ContraTrader",                 "37" );
      ( "PosAccount",                   "38" )
    ]
  );
  ( "exec_restatement_reason",          Int,
    [
      ( "GTCorpAct",                    "0" );
      ( "GTRenew",                      "1" );
      ( "Verbal",                       "2" );
      ( "RePx",                         "3" );
      ( "BrkrOpt",                      "4" );
      ( "PartDec",                      "5" );
      ( "CxlTradingHalt",               "6" );
      ( "CxlSystemFailure",             "7" );
      ( "MrktOption",                   "8" );
      ( "CanceledNotBest",              "9" );
      ( "WarehouseRecap",              "10" );
      ( "OtherRestatementReason",      "99" )
    ]
  );
  (* Limited set of currencies is supported:                                 *)
  ( "currency",                         String,
    [
      ( "RUB",                          "RUB" );
      ( "SUR",                          "SUR" );  (* Same as RUB, deprecated *)
      ( "UAH",                          "UAH" );
      ( "BYR",                          "BYR" );  (* Byelorussian Ruble?     *)
      ( "KZT",                          "KZT" );
      ( "USD",                          "USD" );
      ( "EUR",                          "EUR" );
      ( "CCY_N_A",                      "N/A" )   (* XXX: OSL hack!..        *)
    ]
  );
  (* XXX: Here is a very limited list of exchanges currently supported:      *)
  ( "exchange",                         String,
    [
      (* Russia / CIS:                                                       *)
      ( "FORTS",                        "FORTS" );
      ( "MICEX",                        "MICEX" );
      ( "RTS_Std",                      "RTSST" );  (* RTS Std               *)
      ( "RTS_SGK",                      "GTS"   );  (* RTS SGK               *)
      ( "RTS_ClassicA",                 "RTS"   );  (* RTS Anonym Classic    *)
      (* International:                                                      *)
      ( "NYSE",                         "NYSE"   );
      ( "NASDAQ",                       "NASDAQ" );
      ( "AMEX",                         "AMEX"   );
      ( "SETS",                         "SETS"   ); (* LSE                   *)
      ( "CETS",                         "CETS"   ); (* XXX: What is it?      *)
      ( "Paris",                        "Paris"  )
    ]
  );
  (* XXX: Very limited sub-set of ISITC Security Types currently supported:  *)
  ( "security_type",                    String,
    [
      ( "FOR",                          "FOR" );  (* FX Contract             *)
      ( "CS",                           "CS"  );  (* Common    Stock         *)
      ( "PS",                           "PS"  );  (* Preferred Stock         *)
      ( "FUT",                          "FUT" );  (* Futures                 *)
      ( "OPT",                          "OPT" );  (* Option                  *)
      ( "WAR",                          "WAR" )   (* Warrant                 *)
    ]
  )
];
(*---------------------------------------------------------------------------*)
(* FIX Fields:                                                               *)
(*---------------------------------------------------------------------------*)
value fix_fld_def =
[
  ( "Account",                        1,    "string"                        );
  ( "AvgPx",                          6,    "price"                         );
  ( "BeginSeqNum",                    7,    "int"                           );
  ( "BeginString",                    8,    "string"                        );
  ( "BodyLength",                     9,    "int"                           );
  ( "CheckSum",                      10,    "int"                           );
  ( "ClOrderID",                     11,    "string"                        );
  ( "Commission",                    12,    "price"                         );
  ( "CommType",                      13,    "comm_type"                     );
  ( "CumQty",                        14,    "int"                           );
  ( "Currency",                      15,    "currency"                      );
  ( "EndSeqNum",                     16,    "int"                           );
  ( "ExecID",                        17,    "string"                        );
  ( "ExecTransType",                 20,    "exec_trans_type"               );
  ( "HandlInst",                     21,    "handl_inst"                    );
  ( "IDSource",                      22,    "id_source"                     );
  ( "LastPx",                        31,    "price"                         );
  ( "LastQty",                       32,    "int"                           );
  ( "SeqNum",                        34,    "int"                           );
  ( "MsgType",                       35,    "fix_msg_type"                  );
  ( "NewSeqNum",                     36,    "int"                           );
  ( "OrderID",                       37,    "string"                        );
  ( "OrderQty",                      38,    "int"                           );
  ( "OrderStatus",                   39,    "order_status"                  );
  ( "OrderType",                     40,    "order_type"                    );
  ( "OrigClOrderID",                 41,    "string"                        );
  ( "PossDupFlag",                   43,    "bool"                          );
  ( "Price",                         44,    "price"                         );
  ( "RefSeqNum",                     45,    "int"                           );
  ( "OrderCapacity",                 47,    "order_capacity"                );
  ( "SecurityID",                    48,    "string"                        );
  ( "SenderCompID",                  49,    "string"                        );
  ( "SendingTime",                   52,    "date_time"                     );
  ( "Side",                          54,    "side"                          );
  ( "Symbol",                        55,    "string"                        );
  ( "TargetCompID",                  56,    "string"                        );
  ( "Text",                          58,    "string"                        );
  ( "TimeInForce",                   59,    "time_in_force"                 );
  ( "TransactTime",                  60,    "date_time"                     );
  ( "SettlmntType",                  63,    "settlmnt_type"                 );
  ( "SettlmntDate",                  64,    "date"                          );
  ( "TradeDate",                     75,    "date"                          );
  ( "EncryptMethod",                 98,    "encrypt_method"                );
  ( "ExDest",                       100,    "exchange"                      );
  ( "CxlRejectReason",              102,    "cxl_rej_reason"                );
  ( "OrderRejectReason",            103,    "order_rej_reason"              );
  ( "HeartBtInt",                   108,    "int"                           );
  ( "ClientID",                     109,    "string"                        );
  ( "MinQty",                       110,    "int"                           );
  ( "TestReqID",                    112,    "string"                        );
  ( "SettlmntCurrency",             120,    "currency"                      );
  ( "OrigSendingTime",              122,    "date_time"                     );
  ( "GapFillFlag",                  123,    "bool"                          );
  ( "ResetSeqNumFlag",              141,    "bool"                          );
  ( "RelatedSymCount",              146,    "int"                           );
  ( "ExecType",                     150,    "order_status"                  );
  ( "LeavesQty",                    151,    "int"                           );
  ( "SecurityType",                 167,    "security_type"                 );
  ( "SecondaryOrderID",             198,    "string"                        );
  ( "SecurityExchange",             207,    "exchange"                      );
  ( "MDReqID",                      262,    "string"                        );
  ( "SubscrReqType",                263,    "subscr_req_type"               );
  ( "MarketDepth",                  264,    "market_depth"                  );
  ( "MDUpdateType",                 265,    "md_update_type"                );
  ( "MDEntryTypesCount",            267,    "int"                           );
  ( "MDEntriesCount",               268,    "int"                           );
  ( "MDEntryType",                  269,    "md_entry_type"                 );
  ( "MDEntryPx",                    270,    "price"                         );
  ( "MDEntrySize",                  271,    "int"                           );
  ( "MDEntryTime",                  273,    "time"                          );
  ( "MDUpdateAction",               279,    "md_update_action"              );
  ( "MDReqRejReason",               281,    "md_req_rej_reason"             );
  ( "MDEntryPosNum",                290,    "int"                           );
  ( "SecurityReqID",                320,    "string"                        );
  ( "SecurityReqType",              321,    "security_req_type"             );
  ( "SecurityRespID",               322,    "string"                        );
  ( "SecurityRespType",             323,    "security_resp_type"            );
  ( "LastMsgSeqNumProcessed",       369,    "int"                           );
  ( "RefTagID",                     371,    "fix_fld_type"                  );
  ( "RefMsgType",                   372,    "fix_msg_type"                  );
  ( "SessionRejReason",             373,    "session_rej_reason"            );
  ( "ContraBroker",                 375,    "string"                        );
  ( "ExecRestatementReason",        378,    "exec_restatement_reason"       );
  ( "TotalSecuritiesCount",         393,    "int"                           );
  ( "CxlRejectRespTo",              434,    "cxl_rej_resp_to"               );

  (* XXX: Exetension flds -- formalised for FIX.4.4 but not FIX.4.2:         *)
  ( "ExpireDate",                   432,    "date"                          );
  ( "PartyIDSource",                447,    "party_id_source"               );
  ( "PartyID",                      448,    "string"                        );
  ( "PartyRole",                    452,    "party_role"                    );
  ( "PartyIDsCount",                453,    "int"                           );
  ( "CFI_Code",                     461,    "string"                        );
  ( "SecondaryClOrderID",           526,    "string"                        );
  ( "SecondaryExecID",              527,    "string"                        );
  ( "SecondaryOrderCapacity",       528,    "order_capacity"                );
  ( "OrderStatusReqID",             790,    "string"                        );

  (* XXX: Local extensions:                                                  *)
  ( "TradeNum",                    5001,    "int"                           );
  ( "OrderNum",                    5002,    "int"                           );
  ( "AccruedCoupon",               5003,    "price"                         );
  ( "ExValue",                     5004,    "price"                         );
  ( "ExUserID",                    5005,    "string"                        );
  ( "RepoRate",                    5006,    "price"                         );
  ( "RepoBuyBackPx",               5007,    "price"                         );
  ( "RepoTerm",                    5012,    "string"  (* ??? *)             );
  ( "Repo2Value",                  5013,    "price"                         );
  ( "TradeType",                   5014,    "trade_type"                    );
  ( "UserText",                    9906,    "string"                        );
  ( "TraderID",                   10039,    "string"                        )
];

(*---------------------------------------------------------------------------*)
(* FIX Messages:                                                             *)
(*---------------------------------------------------------------------------*)
(* NB: Msg type codes used to be "Char"s in FIX.4.2, but in FIX.4.4, we need
   "String"s to suppoer 2-letter "User" Msgs:
*)
value fix_msg_def =
[
  ( "HeartBeat",                "0",
    [
      ( "TestReqID",            Option )
    ]
  );

  ( "TestReq",                  "1",
    [
      ( "TestReqID",            Single )
    ]
  );

  ( "ResendReq",                "2",
    [
      ( "BeginSeqNum",          Single );
      ( "EndSeqNum",            Single )
    ]
  );

  ( "Reject",                   "3",
    [
      ( "RefSeqNum",            Single );
      ( "RefTagID",             Option );
      ( "RefMsgType",           Option );
      ( "Text",                 Option );
      ( "SessionRejReason",     Option )
    ]
  );

  ( "SeqReset",                 "4",
    [
      ( "NewSeqNum",            Single );
      ( "GapFillFlag",          Option )
    ]
  );

  ( "LogOut",                   "5",
    [
      ( "Text",                 Option )
    ]
  );

  ( "LogOn",                    "A",
    [
      ( "EncryptMethod",        Single );
      ( "HeartBtInt",           Single );
      ( "ResetSeqNumFlag",      Option )
    ]
  );

  ( "SecurityDefReq",           "c",
    [
      ( "SecurityReqID",        Single );
      ( "SecurityReqType",      Single )
    ]
  );

  ( "SecurityDef",              "d",
    [
      ( "SecurityReqID",        Single );
      ( "SecurityRespID",       Single );
      ( "SecurityRespType",     Option );
      ( "TotalSecuritiesCount", Single );
      ( "Symbol",               Option );
      ( "SecurityExchange",     Option );
      ( "SecurityType",         Option );
      ( "SecurityID",           Option );
      ( "IDSource",             Option );
      ( "Currency",             Option )
    ]
  );

  ( "MarketDataReq",            "V",
    [
      ( "MDReqID",              Single );
      ( "SubscrReqType",        Single );
      ( "MarketDepth",          Single );
      ( "MDUpdateType",         Option );
      ( "MDEntryType",          Group  "MDEntryTypesCount" );
      ( "SymbolEntry",          Group  "RelatedSymCount"   )
    ]
  );

  ( "MarketData",               "W",
    (* XXX: In general, the FIX standard treats "MDReqID"  here as an "Option"
       only. However, if it is missing, we would be unable to distribute data
       to its subscribers properly -- so will be strict:
    *)
    [
      ( "MDReqID",              Single );
      ( "Symbol",               Single );
      ( "IDSource",             Option );
      ( "SecurityID",           Option );
      ( "SecurityExchange",     Option );
      ( "MDEntry",              Group  "MDEntriesCount" )
    ]
  );

  ( "MarketDataIncr",           "X",
    [
      ( "MDReqID",              Option );
      ( "MDEntryIncr",          Group  "MDEntriesCount" )
    ]
  );

  ( "MarketDataReqRej",         "Y",
    [
      ( "MDReqID",              Single );
      ( "MDReqRejReason",       Option );
      ( "Text",                 Option )
    ]
  );

  ( "NewSingleOrder",           "D",
    [
      ( "ClOrderID",            Single );
      ( "HandlInst",            Single );
      ( "Symbol",               Single );
      ( "Side",                 Single );
      ( "TransactTime",         Single );
      ( "OrderType",            Single );
      ( "CFI_Code",             Option ); (* XXX: Mandatory for FIX.4.4      *)
      ( "Account",              Option );
      ( "Price",                Option );
      ( "IDSource",             Option );
      ( "SecurityID",           Option );
      ( "Currency",             Option );
      ( "OrderQty",             Single ); (* Must be a lot size multiple     *)
      ( "TimeInForce",          Option );
      ( "ExDest",               Option );
      ( "ClientID",             Option );
      (* The following flds are currently used by the Direct RTS FIX interface
         for placing orders on RTS Standard etc. They are still FIX.4.4 complt:
      *)
      ( "ExpireDate",           Option );
      ( "SecondaryClOrderID",   Option );
      ( "SettlmntType",         Option );
      ( "SettlmntDate",         Option );
      ( "Party",                OptGroup "PartyIDsCount" )
    ]
  );

  ( "ExecReport",               "8",
    [
      ( "OrderStatus",          Single );
      ( "ExecType",             Single );
      ( "CumQty",               Single );
      ( "LeavesQty",            Single );
      ( "ClOrderID",            Single );
      ( "OrderID",              Single );
      ( "AvgPx",                Single );
      ( "Side",                 Single );
      ( "Symbol",               Single );
      ( "ExecID",               Single );
      ( "OrderQty",             Option ); (* Must be a lot size multiple     *)
      ( "OrderType",            Option );
      ( "LastQty",              Option );
      ( "CFI_Code",             Option ); (* Only in FIX.4.4+                *)
      ( "ExecTransType",        Option ); (* Only in FIX.4.2, not in 4.4     *)
      ( "SecondaryOrderID",     Option );
      ( "SecondaryExecID",      Option );
      ( "OrigClOrderID",        Option );
      ( "OrderStatusReqID",     Option );
      ( "SecondaryClOrderID",   Option );
      ( "Price",                Option );
      ( "LastPx",               Option );
      ( "Account",              Option );
      ( "SecurityID",           Option );
      ( "Currency",             Option );
      ( "ExDest",               Option );
      ( "ClientID",             Option );
      ( "Text",                 Option );
      ( "TradeDate",            Option );
      ( "TransactTime",         Option );
      ( "ContraBroker",         Option );
      ( "SettlmntType",         Option );
      ( "SettlmntCurrency",     Option );
      ( "SettlmntDate",         Option );
      ( "OrderCapacity",        Option );
      ( "Commission",           Option );
      ( "CommType",             Option );
      ( "IDSource",             Option );
      ( "SecurityExchange",     Option );
      ( "SecurityType",         Option );
      ( "TimeInForce",          Option );
      ( "MinQty",               Option );
      ( "OrderRejectReason",    Option );
      ( "HandlInst",            Option );
      ( "ExecRestatementReason",Option );
      (* Non-std FIX extensions:      *)
      ( "UserText",             Option );
      ( "TradeNum",             Option );
      ( "OrderNum",             Option );
      ( "AccruedCoupon",        Option );
      ( "ExValue",              Option );
      ( "ExUserID",             Option );
      ( "RepoRate",             Option );
      ( "RepoBuyBackPx",        Option );
      ( "RepoTerm",             Option );
      ( "Repo2Value",           Option );
      ( "TradeType",            Option );
      ( "TraderID",             Option )
    ]
  );

  ( "OrderCancelReq",               "F",
    [
      ( "OrigClOrderID",        Single ); (* ID of the order to be cancelled *)
      ( "ClOrderID",            Single ); (* ID of the cancellation request  *)
      ( "CFI_Code",             Option ); (* XXX: RTS Non-Std, hence Option  *)
      ( "Symbol",               Single ); (* Must match the original order   *)
      ( "ExDest",               Option ); (* Non-Std -- OSL only             *)
      ( "SecurityID",           Option );
      ( "IDSource",             Option );
      ( "Side",                 Single ); (* Must match the original         *)
      ( "TransactTime",         Single ); (* For THIS request                *)
      ( "Account",              Option );
      ( "OrderQty",             Option )  (* Orig Qty                        *)
    ]
  );

  ( "OrderModifyReq",             "G",
    [
      ( "OrigClOrderID",        Single );
      ( "ClOrderID",            Single );
      ( "CFI_Code",             Option ); (* XXX: Mandatory in FIX.4.4       *)
      ( "Symbol",               Single );
      ( "Side",                 Single ); (* Must match the original         *)
      ( "TransactTime",         Single );
      ( "Account",              Option );
      ( "OrderQty",             Option ); (* XXX: Mandatory in FIX.4.4       *)
      ( "Price",                Single )  (* New Price                       *)
    ]
  );

  ( "OrderStatusReq",             "H",
    [
      ( "ClOrderID",            Single ); (* The ORIG order ID!              *)
      ( "Symbol",               Single );
      ( "CFI_Code",             Option ); (* XXX: Mandatory in FIX.4.4       *)
      ( "Side",                 Single );
      ( "Account",              Option );
      ( "OrderStatusReqID",     Option )
    ]
  );

  ( "OrderCancelReject",          "9",
    [
      ( "OrigClOrderID",        Single ); (* ID of the order not cancelled   *)
      ( "ClOrderID",            Single ); (* ID of the cancellation request  *)
      ( "OrderID",              Single ); (* ???                             *)
      ( "OrderStatus",          Single );
      ( "CxlRejectRespTo",      Single );
      ( "CxlRejectReason",      Option );
      ( "TransactTime",         Option );
      ( "Text",                 Option );
      ( "Account",              Option );
      (* XXX: The remaining flds are non-std OSL extensions:                 *)
      ( "Symbol",               Option );
      ( "Side",                 Option );
      ( "SecurityID",           Option );
      ( "ExDest",               Option )
    ]
  )

  (* Msgs with 2-Letter Codes of FIX.4.4:                                    *)
];

(*---------------------------------------------------------------------------*)
(* FIX Fld Groups:                                                           *)
(*---------------------------------------------------------------------------*)
(* NB: Each group must begin with the fld which is the designated grp delimiter
   according to the FIX protocol. The order of other flds is irrelevant:
*)
value fix_grp_def =
[
  ( "SymbolEntry",              "SE",
    [
      ( "Symbol",               Single );
      ( "IDSource",             Option );
      ( "SecurityID",           Option );
      ( "SecurityExchange",     Option )
    ]
  );

  ( "MDEntry",                  "MDE",
    [
      ( "MDEntryType",          Single );
      ( "MDEntryPx",            Single );
      ( "Currency",             Option );
      ( "MDEntrySize",          Option );    (* Number of securities | lots  *)
      ( "MDEntryTime",          Option );
      ( "MDEntryPosNum",        Option )     (* Pos# of this entry( best=1 ) *)
    ]
  );

  ( "MDEntryIncr",              "MDEI",
    [
      ( "MDUpdateAction",       Single );
      ( "Symbol",               Option );    (* Compulsory for 1st entry     *)
      ( "SecurityID",           Option );
      ( "IDSource",             Option );
      ( "SecurityExchange",     Option );
      ( "MDEntryType",          Option );    (* Compulsory for 1st entry     *)
      ( "MDEntryPx",            Option );    (* Compulsory for 1st entry     *)
      ( "Currency",             Option );
      ( "MDEntrySize",          Option );    (* Compulsory for 1st entry     *)
      ( "MDEntryTime",          Option );
      ( "MDEntryPosNum",        Option )
    ]
  );

  ( "Party",                    "P",
    [
      ( "PartyID",              Single );
      ( "PartyIDSource",        Single );
      ( "PartyRole",            Single )
    ]
  )
];
