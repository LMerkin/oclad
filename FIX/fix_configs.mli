(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                         Configs for FIX Connections:                      *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* Enumerator of all supported Configs:                                      *)
(*---------------------------------------------------------------------------*)
type fix_config_id =
[
  RTS_Direct_Orders_Dry
| RTS_Direct_Orders
| OSL_Quotes_All
| OSL_Orders_All
| OSL_MICEX_Orders_Dry
| TFast_FORTS_Dry
];

value string_of_fix_config_id: fix_config_id -> string;
value parse_fix_config_id    : Stream.t char -> fix_config_id;

(*---------------------------------------------------------------------------*)
(* The FIX Config Type:                                                      *)
(*---------------------------------------------------------------------------*)
type multi_acc  = ( Fix_types.exchange * string * string );
                  (* Exchange,           Acc:1,   ClientID:109               *)

type fix_config =
{
  config_id         : fix_config_id;

  (* Flds of the standard FIX header etc: *)
  fix_version       : string;                            (* E.g. "FIX.4.2"   *)
  our_comp_id       : string;
  server_comp_id    : string;
  accounts          : list multi_acc;
  heart_bt_int      : int;                               (* In sec           *)

  (* Connection params:                   *)
  server_ip         : string;
  server_port       : int;
  reconnect_int     : int;                (* In sec                          *)

  (* FIX session management:              *)
  time_corr         : float;              (* ServerTime - LocalTime, sec     *)
  reset_on_disconn  : bool;               (* Disconnect resets the counters? *)
  max_sent_memo_len : int;

  (* Logging and Debugging. The Bolt Logger config is separate:              *)
  debug             : bool
};

(*---------------------------------------------------------------------------*)
(* "mk_fix_config":                                                          *)
(*---------------------------------------------------------------------------*)
value mk_fix_config: ?debug:bool -> fix_config_id -> fix_config;

