(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*               FIX Application-Level ( API ) Functions                     *)
(*===========================================================================*)
open   ExtString;

module FT = Fix_types;
module FM = Fix_msgs;
module FC = Fix_configs;
module FE = Fix_engine;
module FS = Fix_session;
module BL = Bolt.Level;

(*===========================================================================*)
(* Types:                                                                    *)
(*===========================================================================*)
value mname = "API";

(* Reactor Config: Internal Only Type:                                       *)
type fix_reactor_conf 's =
{
  rc_id     : string;          (* Unique ID of this Reactor                  *)
  rc_engines: Hashtbl.t FC.fix_config_id FE.fix_engine;
  rc_events : mutable list ( Event.event ( FT.fix_msg * FC.fix_config_id ) );
  rc_period : float;           (* Period of "time_cb" invocations            *)
  rc_time_cb: call_back0 's;   (* No-Msg ( time-driven ) Call-Back           *)
  rc_main_cb: call_back1 's    (* Main   ( msg-driven  ) Call-Back           *)
}
(* The No-Msg Call-Back to be invoked when there are no incoming msgs within a
   specified period of time:
*)
and call_back0       's = fix_reactor_conf 's -> 's -> 's

(* Main and On-Error Call-Back Type on FIX Events: The data are FIX msgs recvd;
   the corresp FIX engine is also passed as an arg, since it may be required eg
   for sending FIX msgs back:
*)
and call_back1       's =
    fix_reactor_conf 's -> FE.fix_engine -> 's -> FT.fix_msg -> 's;

(*===========================================================================*)
(* FIX Engine Management:                                                    *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "mk_fix_engine":                                                          *)
(*---------------------------------------------------------------------------*)
value mk_fix_engine: ?debug:bool  -> FC.fix_config_id -> FE.fix_engine =

fun ?{ debug=False } conf_id     ->
  (* Create the Config by its ID:                                            *)
  let conf = FC.mk_fix_config ~{ debug=debug } conf_id in

  (* Buffers Size for user-level recv and kernel-level send and recv buffs:  *)
  let buff_size = 8192*1024 in

  (* The initial FIX Engine object: *)
  let e   =
  {
    FE.conf               = conf;
    FE.socket             = None;
    FE.buff_size          = buff_size;
    FE.send_buff          = Buffer.create   1024;
    FE.send_timeout       = 5.0;
    FE.recv_buff          = String.create buff_size;
    FE.recv_from          = 0;
    FE.parse_from         = 0;
    FE.to_count           = 0;

    FE.sender_thread      = None;
    FE.recver_thread      = None;
    FE.monitor_thread     = None;

    FE.last_unsent        = None;
    FE.sent_memo          = Queue.create      ();
    FE.send_sys_q         = Event.new_channel ();
    FE.send_app_q         = Event.new_channel ();

    FE.curr_mode          = FE.Init;
    FE.next_recv_seq_num  = 1;
    FE.next_send_seq_num  = 1;

    (* NB: The gap window must be small because of real-time contraints:     *)
    FE.gap_window         = Array.make 32 None;
    FE.gap_from           = 0;
    FE.gap_to             = 0;

    FE.reactor_map        = Hashtbl.create 8192;
    FE.dead_msg_ids       = Hashtbl.create 1048576;
    FE.channel_map        = Hashtbl.create 256;
    FE.maps_rwl           = RWLock.create  ()
  }
  in
  do{
    (* Register the Engine for finalisation with the GC:     *)
    Gc.finalise
      ( FS.fix_finalise ~{ graceful=False }
                        ~{ msg="FIX Engine Finalisation" } ) e;

    (* Start the Monitor Thread which connects to the FIX Server and then
       creates and monitors the Sender and Receiver Threads:
    *)
    ignore ( Thread.create FS.fix_monitor_loop e );
    e
  };

(*---------------------------------------------------------------------------*)
(* "shutdown_fix_engine":                                                    *)
(*---------------------------------------------------------------------------*)
(* Seldom required, as an engine is typically finalised automatically. This is
   just a wrapper around the corresp "FS.fix_finalise" function:
*)
value shutdown: FE.fix_engine -> unit =
fun e ->
  FS.fix_finalise ~{ graceful=True } ~{ msg="FIX Engine Scheduled ShutDown" } e;

(*===========================================================================*)
(* Reactor Management:                                                       *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "add_new_channel":                                                        *)
(*---------------------------------------------------------------------------*)
(* Create a new channel (if does not exist already) between the given Engine
   and Reactor. NB: it is assumed that the caller of this function locks the
   Engine  to ensure correct update of the "channel_map",  as the Engine may
   be shared between multiple concurrent Reactors:
*)
value add_new_channel: FE.fix_engine -> fix_reactor_conf 's -> unit =
fun e rconf ->
  if Hashtbl.mem e.FE.channel_map rconf.rc_id
  then
    () (* Nothing to do, the channel already exists                          *)
  else
  (* Create the new channel and the event to be received by the Reactror over
     this channel ( the sender-side events will be created by "Fix_session" ):
  *)
  let ch = Event.new_channel () in
  let ev = Event.receive ch     in
  do{
    (* Add the channel to the Engine, receiver-side event to the Reactor:    *)
    Hashtbl.add  e.FE.channel_map  rconf.rc_id ch;

    rconf.rc_events := [ ev :: rconf.rc_events ]
  };

(*---------------------------------------------------------------------------*)
(* "new_reactor_conf":                                                       *)
(*---------------------------------------------------------------------------*)
(* Creates a Config for a new Reactor. The Engines, if specified, are immedia-
   tely attached to that Reactor:
*)
value new_reactor_conf:
  ?engines: list FE.fix_engine ->
  ~time_cb: call_back0 's      ->
  ~period:  float              ->
  ~main_cb: call_back1 's      ->
  fix_reactor_conf 's          =

fun ?{ engines=[] } ~{ time_cb } ~{ period } ~{ main_cb } ->
  if period <= 0.0
  then
    invalid_arg "Fix_api.new_reactor_conf: Non-positive period"
  else
  let rc_id = FE.new_req_id  () in
  let rconf =
  {
    rc_id       = rc_id;
    rc_engines  = Hashtbl.create ( min 10 ( List.length engines ) );
    rc_events   = [];
    rc_period   = period;
    rc_time_cb  = time_cb;
    rc_main_cb  = main_cb
  }
  in
  do{
    (* Attach the Reactor to all specified Engines, so the latter will be able
       to fwd msgs to the Reactor immediately:
    *)
    List.iter
    (
      fun e ->
      do{
        assert( not( Hashtbl.mem e.FE.channel_map rc_id ) );
        RWLock.lock_wr  e.FE.maps_rwl;
        add_new_channel e rconf;        (* "rconf" is also modified here     *)
        RWLock.unlock   e.FE.maps_rwl;

        (* Also, add this Engine to "rc_engines": *)
        let conf = e.FE.conf.FC.config_id in
        do{
          assert( not( Hashtbl.mem rconf.rc_engines conf ) );
          Hashtbl.add rconf.rc_engines conf e
        }
      }
    )
    engines;
    (* Return the filled in "rconf":                                         *)
    rconf
  };

(*---------------------------------------------------------------------------*)
(* "no_msg":                                                                 *)
(*---------------------------------------------------------------------------*)
(* Part of the "reactor" functionaliy (below)  which is invoked when no events
   are subscribed for, or no messages have arrived within a specified time-out.
   Returns ( NewState, ContinueFlag ):
*)
value no_msg: fix_reactor_conf 's -> 's -> ( 's * bool ) =
fun rconf s0 ->
do{
  (* This is possible: e.g. at Reactor start-up,  or if all msgs have been
     unsubscribed. In hat case, XXX wait a bit to prevent a tight loop, and
     invoke the default call-back:
  *)
  Thread.delay rconf.rc_period;
  try
    ( rconf.rc_time_cb rconf s0, True )
  with
  [ Exit -> ( s0, False )
  | hmm  ->
    do{
      FE.log None mname BL.ERROR
        [ Printf.sprintf "Exception in No-Msg Call-Back: %s\n%s"
        ( Printexc.to_string hmm ) ( Printexc.get_backtrace () ) ];
      ( s0, True )
    }
  ]
};

(*---------------------------------------------------------------------------*)
(* "reactor":                                                                *)
(*---------------------------------------------------------------------------*)
(* Reactor runs in an infinite loop and invokes the provided call-backs on re-
   ceiving the corresp events. It exits on the "Exit" exception raised by any
   call-back:
*)
value rec reactor: fix_reactor_conf 's -> 's -> unit =
fun rconf s0 ->
  let ( s1, continue ) =
    if rconf.rc_events = []
    then
      (* No subscriptions:                                                   *)
      no_msg rconf s0
    else
      (* Wait for any event (an incoming msg_pair ):                         *)
      let mb_msg_pair =
        Event.timed_sync ( Event.choose rconf.rc_events ) rconf.rc_period
      in
      match mb_msg_pair with
      [
        None     ->
          (* No msg within the specified time-out:                           *)
          no_msg rconf s0

      | Some ( msg, conf )  ->
          (* GENERIC CASE: Got a Msg and a ConfID of the Engine from which the
             Msg was originally received. Invoke the call-back:
          *)
          try
            (* Get the Engine from which this Msg was received:              *)
            let e = Hashtbl.find rconf.rc_engines conf  in

            (* Actually apply the Call-Back. It can modify "rconf":          *)
            try
              ( rconf.rc_main_cb rconf e s0 msg, True )
            with
            [ Exit -> ( s0, False )
            | hmm  ->
              do{
                (* An exception in the call-back: log it, but continue:      *)
                FE.log ( Some e ) mname BL.ERROR
                  [ Printf.sprintf "Exception in the Main CallBack: %s\n%s\n%s"
                  ( Printexc.to_string hmm     )
                  ( Printexc.get_backtrace  () )
                  ( FT.string_of_fix_msg msg ) ];
                ( s0, True )
              }
            ]
          with
          [ Not_found ->
            do{
              (* The Engine was not found???  *)
              FE.log None mname BL.ERROR
                [ Printf.sprintf "No Engine found for (%s,\n%s)"
                    ( FC.string_of_fix_config_id conf )
                    ( FT.string_of_fix_msg msg )
                ];
              ( s0, True )
            }
          ]
      ]
  in
  if   continue
  then reactor rconf s1
  else ();

(*===========================================================================*)
(* Engine / Reactor Utils:                                                   *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "submit_app_msg":                                                         *)
(*---------------------------------------------------------------------------*)
(* The client submits an application msg to the Engine for further sending it
   to the Server:
*)
value submit_app_msg: FE.fix_engine -> FT.fix_msg -> unit =
fun e msg ->
  (* NB: "send_app_q" is NOT used for any re-sends, hence "None" below:      *)
  Event.sync ( Event.send e.FE.send_app_q ( None, msg ) );

(*---------------------------------------------------------------------------*)
(* "add_links_for_msg":                                                      *)
(*---------------------------------------------------------------------------*)
(* Make an Engine to forward Msgs  with the given MsgIDs to the given Reactor,
   creating a Channel between them if it does not exist yet. Make the Reactor
   to invoke the given CallBack of receiving msgs with this MsgID:
*)
value add_links_for_msg:
  fix_reactor_conf 's -> FE.fix_engine -> FT.fix_msg  -> unit =

fun rconf e msg ->
  let msg_ids = FS.get_fix_msg_ids msg in
  do{
    (* Lock the maps for possible modification, since the Engine can be shared
       by multiple Reactors running concurrently:
    *)
    RWLock.lock_wr e.FE.maps_rwl;

    List.iter
    (
      fun msg_id ->
        try
          let reactors = Hashtbl.find e.FE.reactor_map msg_id in
          (* Some Reactors are already attached to this Engine wrt this MsgID.
             Check the given Reactor -- XXX linear check here; if it is not in
             the list, attach it:
          *)
          if not ( List.mem rconf.rc_id reactors )
          then     Hashtbl.replace e.FE.reactor_map msg_id
                                     [ rconf.rc_id :: reactors ]
          else   ()
        with
        [ Not_found ->
            (* This Reactor was not connecting to this Engine via this MsgID: *)
            Hashtbl.add  e.FE.reactor_map  msg_id [ rconf.rc_id ]
        ]
    )
    msg_ids;

    (* Also, if there is no Channel yet for this Reactor in this Engine, create
      create and add the corresp Channel (this also affects the Reactor side) :
    *)
    add_new_channel e rconf;

    (* Unlock the Engine maps. NB:  We do not lock the RConf for modification,
       as it can only be updated synchronously (from within the corresponding
       Reactor's call-backs) -- the RConf type is private:
    *)
    RWLock.unlock   e.FE.maps_rwl
};

(*---------------------------------------------------------------------------*)
(* "check_cfi":                                                              *)
(*---------------------------------------------------------------------------*)
(* ISO 10962 syntactic check on CFI Codes:                                   *)

value cfi_x   = "XXXXXX";
value cfi_ex  = "EXXXXX";
value cfi_e   = "ES[VNREX][TUX][OPFX][BRNZAPCUMX]";
value cfi_d   = "D[BCMTWYX]XXXX";
value cfi_r   = "R[AMSWX]XXXX";
value cfi_ox  = "OXXXXX";
value cfi_o   = "O[CP][AEX][SIDCOFCWBMX][CPX][SNX]";
value cfi_fx  = "FXXXXX";
value cfi_fc  = "FC[AEISX][CPX][SNX]X";
value cfi_ff  = "FF[SIDCOFTWBMX][CPX][SNX]X";
value cfi_mx  = "MXXXXX";
value cfi_mm  = "MMXXXX";
value cfi_mr  = "MR[CTRIX]XXX";
value cfi_all =
  Printf.sprintf
  "^(%s)|(%s)|(%s)|(%s)|(%s)|(%s)|(%s)|(%s)|(%s)|(%s)|(%s)|(%s)|(%s)$"
  cfi_x  cfi_ex cfi_e  cfi_d cfi_r cfi_ox cfi_o cfi_fx cfi_fc cfi_ff cfi_mx
  cfi_mm cfi_mr;

value cfi_re   = Pcre.regexp ~{ study = True } cfi_all;

value check_cfi: FE.fix_engine -> string -> option string =
fun e cfi_code ->
  (* FIX.4.2 does not support CFO Codes: *)
  if   cfi_code = "" || ( e.FE.conf.FC.fix_version <> "FIX.4.4" )
  then None
  else
  if   Pcre.pmatch ~{ rex=cfi_re } cfi_code
  then Some cfi_code (* The code is OK!  *)
  else failwith( "ERROR: Invalid CFI Code: "^ cfi_code );

(*---------------------------------------------------------------------------*)
(* "check_ccy":                                                              *)
(*---------------------------------------------------------------------------*)
(* XXX: In some cases, the Currency field, even if valid and known, must NOT be
   provided:
*)
value check_ccy: FE.fix_engine -> FT.currency -> FT.exchange ->
                option FT.currency =
fun e ccy exchange ->
  if  e.FE.conf.FC.config_id = FC.OSL_Quotes_All ||
    ( e.FE.conf.FC.config_id = FC.OSL_Orders_All &&
       exchange <> FT.FORTS  && exchange <> FT.MICEX )
  then None
  else Some ccy;

(*---------------------------------------------------------------------------*)
(* "get_account_info":                                                       *)
(*---------------------------------------------------------------------------*)
(* Returns ( Account, ClientID ) options:                                    *)

value get_account_info:
  FE.fix_engine -> FT.exchange -> string ->
  ( option FT.exchange * option FT.settlmnt_type *
    option string      * option string )        (* Account_1, ClientID_109   *)
=
fun e exchange account ->
  let mb_stype  =
    (* XXX: There is currently only one case when SettlmntType is required:  *)
    if e.FE.conf.FC.config_id = FC.RTS_Direct_Orders_Dry &&
       exchange = FT.RTS_Std
    then Some FT.Regular
    else None
  in
  (* For other params, scan the "multi_acc" for the specified Engine:        *)
  let mb_exchange = ref None in
  let mb_acc      = ref None in
  let mb_cid      = ref None in
  do{
    try
      List.iter
      (
        fun ( ex, acc, cid ) ->
          if  ex = exchange
          then
          do{
            mb_exchange.val := Some ex;
            mb_acc.val      := if acc <> "" then Some acc else None;
            mb_cid.val      := if cid <> "" then Some cid else None;
            raise Exit
          }
          else ()
      )
      e.FE.conf.FC.accounts
    with
      [ Exit -> () ];

    (* If "account" is specified explicitly, it overrides the "exchange"-based
       value:
    *)
    if account <> ""
    then mb_acc.val := Some account
    else ();

    ( mb_exchange.val, mb_stype, mb_acc.val, mb_cid.val )
  };
 
(*===========================================================================*)
(* Market Data Subscription Mgmt:                                            *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "req_securities_list":                                                    *)
(*---------------------------------------------------------------------------*)
value req_securities_list:
  ~req_type: FT.security_req_type ->
  fix_reactor_conf 's             ->
  FE.fix_engine                   ->
  ( string * FT.date_time )       =   (* ReqID, TimeStamp  *)

fun ~{ req_type } rconf engine    ->
  let dt      = FE.get_date_time ( Some engine ) in
  let req_id  = FE.new_req_id ()  in
  let req_msg =
    FT.SecurityDefReq
    {
      FT.security_req_id_c   = req_id;
      FT.security_req_type_c = req_type
    }
  in
  do{
    (* Make the Engine and the Reactor to receive Msgs via the ReqID:        *)
    add_links_for_msg rconf engine req_msg;

    (* Send the order up and return the ReqID:                               *)
    submit_app_msg engine req_msg;
    ( req_id, dt )
  };

(*---------------------------------------------------------------------------*)
(* "subscr_market_data":                                                     *)
(*---------------------------------------------------------------------------*)
(* XXX: We currently permit only 1 symbol per subscription, though the FIX pro-
   tocol generally allows multiple subscriptions at once. The reasons for our
   policy are the following:
   -- Some FIX implementations ( e.g. TFast ) cannot handle more than 1 symbol
      per subscription;
   -- To preserve the network bandwidth, we want to share subscriptions to com-
      mon symbols between different clients ( strategies ), so finer granulari-
      ty would be better than block subscriptions:
*)
value subscr_market_data:
  ~exchange:      FT.exchange               ->
  ?account:       string                    ->
  ~symbol:        string                    ->  (* "Long"  Symbol            *)
  ~security_id:   string                    ->  (* "Short" SecurityID        *)
  ~id_src:        FT.id_source              ->
  ~entry_types:   array FT.md_entry_type    ->
  ~depth:         FT.market_depth           ->
  ~incr_refresh:  bool                      ->
  fix_reactor_conf 's                       ->
  FE.fix_engine                             ->
  ( string * FT.date_time )                 =   (* ReqID, TimeStamp )        *)

fun ~{ exchange } ?{ account="" } ~{ symbol } ~{ security_id } ~{ id_src }
    ~{ entry_types }   ~{ depth } ~{ incr_refresh } rconf engine        ->

  (* Unlike submitting the Orders, in this case the ReqID is not totally random:
     it is a hash value of the subscription params. Thus, we would make only 1
     actual FIX subscription per a given params set, even if there are multiple
     local clients which want to subscribe to it:
     Sort the "entry_types" array because it will be part of the ReqID:
  *)
  let dt          = FE.get_date_time ( Some engine ) in
  let se_types    =
    let se_types' = Array.copy entry_types  in
    do{
      Array.sort compare se_types';
      se_types'
    }
  in
  let update_type = if incr_refresh then FT.IncrRefresh else FT.FullRefresh  in
  let ( mb_exchange, _, _, _ ) =  get_account_info engine exchange account   in
  let sub_params  =
    ( symbol, security_id, mb_exchange, depth, se_types, update_type )
  in
  (* So generate the ReqID: *)
  let req_id      = Printf.sprintf "%x" ( Hashtbl.hash sub_params )          in

  (* Generate the FIX msg for subscription:                                  *)
  let sym_entry   =
  {
    FT.symbol_SE            = symbol;
    FT.id_source_SE         = Some id_src;
    FT.security_id_SE       = Some security_id;
    FT.security_exchange_SE = mb_exchange
  }
  in
  let sub_msg     =
    FT.MarketDataReq
    {
      FT.md_req_id_V        = req_id;
      FT.subscr_req_type_V  = FT.Subscribe;
      FT.market_depth_V     = depth;
      FT.md_update_type_V   = Some update_type;
      FT.md_entry_types_V   = se_types;
      FT.symbol_entries_V   = [| sym_entry |]
    }
  in
  do{
    (* Make the Engine and the Reactor to receive Msgs with this ReqID:      *)
    add_links_for_msg rconf engine sub_msg;

    (* Submit the msg for sending: *)
    submit_app_msg engine sub_msg;

    (* Return the ReqID which can be used, e.g., to unsubscribe later, and also
       the TimeStamp:
    *)
    ( req_id, dt )
  };

(*---------------------------------------------------------------------------*)
(* "unsubscr_market_data":                                                   *)
(*---------------------------------------------------------------------------*)
(* TODO *)

(*===========================================================================*)
(* Orders Management:                                                        *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "send_order":                                                             *)
(*---------------------------------------------------------------------------*)
(* Returns OrderID;
   -- if "price" is not given ( or <= 0.0 ), it is a "MarketOrder",  otherwise
      a "LimitOrder";
   -- "time_in_force" defaults to "Day":
*)
value send_order:
  ~exchange:      FT.exchange               ->
  ?account:       string                    ->
  ~cfi_code:      string                    ->
  ~symbol:        string                    ->
  ~security_id:   string                    ->
  ~id_src:        FT.id_source              ->
  ~side:          FT.side                   ->
  ?price:         float                     ->
  ~currency:      FT.currency               ->
  ~qty:           int                       ->
  ?time_in_force: FT.time_in_force          ->
  fix_reactor_conf 's                       ->
  FE.fix_engine                             ->
  ( string * FT.date_time )                  =

fun ~{ exchange } ?{ account="" } ~{ cfi_code }  ~{ symbol }  ~{ security_id }
    ~{ id_src }   ~{ side } ?{ price=0.0 } ~{ currency } ~{ qty }
    ?{ time_in_force=FT.Day }  rconf  engine ->

  if qty <= 0
  then
    failwith( Printf.sprintf "ERROR: Invalid Qty: %d" qty )
  else
  let dt      = FE.get_date_time ( Some engine )                       in
  let ord_id  = FE.new_req_id    ()                                    in

  let ( mb_exch, mb_stype, mb_acc, mb_cid ) =
        get_account_info   engine  exchange  account                   in
  let mb_cfi                   = check_cfi   engine cfi_code           in
  let mb_ccy                   = check_ccy   engine currency exchange  in
  let ( mb_price, order_type ) =
    if price <= 0.0
    then ( None,       FT.Market )
    else ( Some price, FT.Limit  )
  in
  let ord_msg                  =
    FT.NewSingleOrder
    {
      FT.cl_order_id_D            = ord_id;
      FT.secondary_cl_order_id_D  = None;
      FT.handl_inst_D             = FT.AutoExecPrivate;
      FT.symbol_D                 = symbol;
      FT.side_D                   = side;
      FT.transact_time_D          = dt;
      FT.order_type_D             = order_type;
      FT.account_D                = mb_acc;
      FT.price_D                  = mb_price;
      FT.id_source_D              = Some id_src;
      FT.security_id_D            = Some security_id;
      FT.currency_D               = mb_ccy;
      FT.order_qty_D              = qty;
      FT.time_in_force_D          = Some time_in_force;
      FT.ex_dest_D                = mb_exch;
      FT.client_id_D              = mb_cid;
      (* FIX.4.4:     *)
      FT.cfi_code_D               = mb_cfi;
      FT.settlmnt_type_D          = mb_stype;
      FT.settlmnt_date_D          = None; (* These flds are for out-of-Exch  *)
      FT.expire_date_D            = None; (*   trading only...               *)
      FT.parties_D                = [| |]
    }
  in
  do{
    (* Make the Engine and the Reactor to receive Msgs with this OrdID:      *)
    add_links_for_msg rconf engine ord_msg;

    (* Send the order up and return the OrdID:                               *)
    submit_app_msg engine ord_msg;
    ( ord_id, dt )
  };

(*---------------------------------------------------------------------------*)
(* "req_order_status":                                                       *)
(*---------------------------------------------------------------------------*)
value req_order_status:
  ~exchange:      FT.exchange   ->
  ?account:       string        ->
  ~ord_id:        string        ->
  ~cfi_code:      string        ->
  ~symbol:        string        ->
  ~side:          FT.side       ->
  fix_reactor_conf 's           ->
  FE.fix_engine                 ->
  FT.date_time                  =

fun ~{ exchange } ?{ account="" } ~{ ord_id } ~{ cfi_code } ~{ symbol }
    ~{ side }  rconf  engine    ->

  let dt   =  FE.get_date_time ( Some engine )                       in
  let ( _, _, mb_acc, _ ) = get_account_info engine exchange account in
  let mb_cfi      = check_cfi  engine cfi_code                       in
  let mb_streq_id =
    if   engine.FE.conf.FC.fix_version = "FIX.4.4"
    then Some ( FE.new_req_id () )
    else None
  in
  let streq_msg =
    FT.OrderStatusReq
    {
      FT.cl_order_id_H          = ord_id;
      FT.symbol_H               = symbol;
      FT.cfi_code_H             = mb_cfi;
      FT.side_H                 = side;
      FT.account_H              = mb_acc;
      FT.order_status_req_id_H  = mb_streq_id
    }
  in
  do{
    (* Make the Engine and the Reactor to receive Msgs with the above IDs:   *)
    add_links_for_msg rconf engine streq_msg;

    (* Send the request up: *)
    submit_app_msg engine streq_msg;
    dt
  };

(*---------------------------------------------------------------------------*)
(* "cancel_order":                                                           *)
(*---------------------------------------------------------------------------*)
value cancel_order:
  ~exchange:      FT.exchange       ->
  ?account:       string            ->
  ~ord_id:        string            ->
  ~cfi_code:      string            ->
  ~symbol:        string            ->
  ~security_id:   string            ->
  ~id_src:        FT.id_source      ->
  ~side:          FT.side           ->
  ~orig_qty:      int               ->
  fix_reactor_conf 's               ->
  FE.fix_engine                     ->
  ( string * FT.date_time )         =

fun ~{ exchange } ?{ account="" } ~{ ord_id } ~{ cfi_code } ~{ symbol }
    ~{ security_id }  ~{ id_src } ~{ side }   ~{ orig_qty } rconf  engine ->

  let dt        = FE.get_date_time ( Some engine )                   in
  let cord_id   = FE.new_req_id    ()                                in
  let ( _, _, mb_acc, _ ) = get_account_info engine exchange account in
  let mb_cfi    = check_cfi  engine cfi_code                         in
  let cord_msg  =
    FT.OrderCancelReq
    {
      FT.orig_cl_order_id_F = ord_id;
      FT.cl_order_id_F      = cord_id;
      FT.cfi_code_F         = mb_cfi;
      FT.ex_dest_F          = Some exchange;
      FT.symbol_F           = symbol;
      FT.security_id_F      = Some security_id;
      FT.id_source_F        = Some id_src;
      FT.side_F             = side;
      FT.transact_time_F    = dt;
      FT.account_F          = mb_acc;
      FT.order_qty_F        = Some orig_qty
    }
  in
  do{
    (* Make the Engine and the Reactor to receive Msgs via BOTH the OrdID and
       the COrdID:
    *)
    add_links_for_msg rconf engine cord_msg;

    (* Send the cancel order up and return the COrdID:                       *)
    submit_app_msg engine cord_msg;
    ( cord_id, dt )
  };

(*---------------------------------------------------------------------------*)
(* "cancel_all":                                                             *)
(*---------------------------------------------------------------------------*)
(* TODO *)

(*---------------------------------------------------------------------------*)
(* "modify_order":                                                           *)
(*---------------------------------------------------------------------------*)
(* XXX: In general, any order parameters can be modified; however, in the curr
   implementation we only allow for Price modification:
*)
value modify_order:
  ~exchange:      FT.exchange     ->
  ?account:       string          ->
  ~ord_id:        string          ->
  ~cfi_code:      string          ->
  ~symbol:        string          ->
  ~side:          FT.side         ->
  ~orig_qty:      int             ->
  ~new_price:     float           ->
  fix_reactor_conf 's             ->
  FE.fix_engine                   ->
  ( string * FT.date_time )       =

fun ~{ exchange } ?{ account="" } ~{ ord_id } ~{ cfi_code } ~{ symbol }
    ~{ side } ~{ orig_qty } ~{ new_price } rconf engine    ->

  if new_price <= 0.0
  then
    failwith( Printf.sprintf "Invalid new price: %f" new_price )
  else
  let dt        = FE.get_date_time ( Some engine )                    in
  let mord_id   = FE.new_req_id    ()                                 in
  let ( _, _, mb_acc, _ ) =  get_account_info engine exchange account in
  let mb_cfi    = check_cfi  engine cfi_code                          in
  let mord_msg  =
    FT.OrderModifyReq
    {
      FT.orig_cl_order_id_G = ord_id;
      FT.cl_order_id_G      = mord_id;
      FT.cfi_code_G         = mb_cfi;
      FT.symbol_G           = symbol;
      FT.side_G             = side;
      FT.transact_time_G    = dt;
      FT.account_G          = mb_acc;
      FT.order_qty_G        = Some orig_qty;
      FT.price_G            = new_price
    }
  in
  do{
    (* Make the Engine and the Reactor to receive Msgs via BOTH the OrdID and
       the MOrdID:
    *)
    add_links_for_msg rconf engine mord_msg;

    (* Send the modify order up and return the MOrdID:                       *)
    submit_app_msg engine mord_msg;
    ( mord_id, dt )
  };

(*---------------------------------------------------------------------------*)
(* "remove_from_fwding_map":                                                 *)
(*---------------------------------------------------------------------------*)
(* When we know that we are done with a particular MsgID ( e.g., and order has
   been filled or cancelled ), we can explicitly remove it from the forwarding
   map of the corresp engine so that the latter would not grow indefinitely:
*)
value remove_from_fwding_map: FE.fix_engine -> string -> unit =
fun e msg_id ->
do{
  (* NB: No exceptions are possible in the critical section:                 *)
  RWLock.lock_wr    e.FE.maps_rwl;

  if Hashtbl.mem    e.FE.reactor_map  msg_id
  then
  do{
    Hashtbl.remove  e.FE.reactor_map  msg_id;
    (* Also add this "msg_id" to the "dead pool":                            *)
    Hashtbl.replace e.FE.dead_msg_ids msg_id ()
  }
  else
    FE.log ( Some e ) mname BL.WARN
      [ "Nothing to remove from the Fwding Map: MsgID not found: "; msg_id ];

  RWLock.unlock    e.FE.maps_rwl
};

(*---------------------------------------------------------------------------*)
(* "clean_dead_msg_ids":                                                     *)
(*---------------------------------------------------------------------------*)
value clean_dead_msg_ids: FE.fix_engine -> unit =
fun e ->
  Hashtbl.clear e.FE.dead_msg_ids;

(*---------------------------------------------------------------------------*)
(* "get_engines":                                                            *)
(*---------------------------------------------------------------------------*)
value get_engines: fix_reactor_conf 's -> list FE.fix_engine =
fun rconf ->
  Hashtbl.fold ( fun _ e curr -> [ e :: curr ] ) rconf.rc_engines [];

