(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*    FIX Client Engine: Config, State and Sending / Receiving FIX Msgs      *)
(*===========================================================================*)
module FT = Fix_types;
module FF = Fix_flds;
module FM = Fix_msgs;
module FC = Fix_configs;
module BL = Bolt.Level;

(*===========================================================================*)
(* FIX Engine ( Client ) Object:                                             *)
(*===========================================================================*)
value mname = "Engine";

(* ReConnect / ReSet / LogOut management:                                    *)
type fix_engine_mode =
[
  Init
| Normal
| ReSet
| LogOut_Graceful
| LogOut_Quick
];

type fix_engine =
{
  (*------------------------------------------*)
  (* Config is memoised inside the Engine:    *)
  (*------------------------------------------*)
  conf              : FC.fix_config;

  (*------------------------------------------*)
  (* Low-Level I/O Structs:                   *)
  (*------------------------------------------*)
  (* Server Communication Socket:             *)
  socket            : mutable option Unix.file_descr;

  (* Sizes of kernel and user-level buffs:    *)
  buff_size         : int;

  (* Buffer for assembling msgs to send:      *)
  send_buff         : Buffer.t;

  (* Max send time-out: cannot block oo-ly:   *)
  send_timeout      : float;

  (* Buffer for receiving msgs, and the curr offset within it: *)
  recv_buff         : string;
  recv_from         : mutable int;
  parse_from        : mutable int;

  (* Counter for sequential time-outs on reading:                            *)
  to_count          : mutable int;

  (*------------------------------------------*)
  (* Threads and Multi-Thread Communication:  *)
  (*------------------------------------------*)
  sender_thread     : mutable option Thread.t;
  recver_thread     : mutable option Thread.t;
  monitor_thread    : mutable option Thread.t;

  (* System ( admin, high-priority ) and application-level channels for
     submitting msgs for sending:
  *)
  last_unsent       : mutable option ( ( option int ) * FT.fix_msg );
  sent_memo         : Queue.t        ( int            * FT.fix_msg );
  send_sys_q        : Event.channel  ( ( option int ) * FT.fix_msg );
  send_app_q        : Event.channel  ( ( option int ) * FT.fix_msg );

  (*------------------------------------------*)
  (* Session-Level Structs:                   *)
  (*------------------------------------------*)
  curr_mode         : mutable fix_engine_mode;

  (* Counters of sent and recvd msgs, from 1:                                *)
  next_send_seq_num : mutable int;
  next_recv_seq_num : mutable int;

  (* Window for receiving re-sent msgs:  used only  if gaps were encountered;
     "gap_window" is at least ( gap_to - gap_from + 1 ) long, but should also
     include space for post-gap msgs to be queued:
  *)
  gap_window        : array ( option FT.fix_msg );
  gap_from          : mutable int;
  gap_to            : mutable int;

  (* Queue of recently sent msgs, for re-sending them if necessary:          *)

  (*------------------------------------------*)
  (* Application-Level Structs:               *)
  (*------------------------------------------*)
  (* Map: MsgID => [ ReactorID ], for Events forwarding:                     *)
  reactor_map       : Hashtbl.t string ( list string );

  (* Hash table of MsgIDs previously excluded from the "reactor_map":        *)
  dead_msg_ids      : Hashtbl.t string unit;

  (* Map: ReactorID => Channel, for the same purpose. The elements of thsi map
     really contain Channels used for forwarding msgs along with EngineConfIDs:
  *)
  channel_map       : Hashtbl.t string
                      ( Event.channel ( FT.fix_msg * FC.fix_config_id ) );

  (* The lock protecting updates to the above maps:                          *)
  maps_rwl          : RWLock.t
};

(*===========================================================================*)
(* Error Handling:                                                           *)
(*===========================================================================*)
exception Socket_Error  of string;

(*---------------------------------------------------------------------------*)
(* "string_of_fix_msg_error":                                                *)
(*---------------------------------------------------------------------------*)
(* Creates a formatted FIX error string: *)

value string_of_fix_msg_error: FT.fix_msg_error -> string =
fun err ->
  let buff = Buffer.create 1024  in
  do{
    (* Msg Type: *)
    match  err.FT.err_msg_type with
    [ Some mtype ->
        Printf.bprintf buff "%s: " ( FT.string_of_fix_msg_type mtype )
    | None       ->  ()
    ];
    match  err.FT.err_fld_type with
    [ Some ftype ->
      do{
        (* Fld Type: *)
        Printf.bprintf buff "%s="  ( FT.string_of_fix_fld_type ftype );

        (* Fld Val : *)
        if err.FT.err_fld_val <> ""
        then Buffer.add_string buff  err.FT.err_fld_val
        else Buffer.add_string buff  "?"
      }
    | None       ->  ()
    ];
    match  err.FT.err_reason   with
    [ Some rsn   ->
        (* Session Reject Reason: *)
        Printf.bprintf buff ": %s" ( FT.string_of_session_rej_reason rsn )
    | None       ->  ()
    ];
    (* Text: *)
    if     err.FT.err_text <> ""
    then Printf.bprintf buff ": %s" err.FT.err_text
    else ();
    (* Curr Pos:                  *)
    if     err.FT.err_off >= 0
    then Printf.bprintf buff " @off=%d" err.FT.err_off
    else ();

    (* Return the string:         *)
    Buffer.contents buff
  };

(*---------------------------------------------------------------------------*)
(* "mk_reject":                                                              *)
(*---------------------------------------------------------------------------*)
value mk_reject: FT.fix_msg_error -> option FT.fix_msg =
fun err ->
  if err.FT.err_seq_num >= 1
  then
    (* Got a valid "seq_num" of an erroneous msg, so can do a "reject": *)
    let reject =
    {
      FT.ref_seq_num_3    = err.FT.err_seq_num;
      FT.ref_tag_id_3     = err.FT.err_fld_type;
      FT.ref_msg_type_3   = err.FT.err_msg_type;

      FT.text_3           =
        if err.FT.err_text <> ""
        then Some ( err.FT.err_text )
        else None;

      FT.session_rej_reason_3 = err.FT.err_reason
    }
    in
    Some ( FT.Reject reject )
  else
    None;

(*===========================================================================*)
(* Utils:                                                                    *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "new_req_id":                                                             *)
(*---------------------------------------------------------------------------*)
(* Generates a new ReqID to be used with any msgs which must have such ID:   *)

Random.self_init ();
value req_id_mx = Mutex.create ();


value new_req_id: unit -> string =
fun ()  ->
do{
  Mutex.lock     req_id_mx;
  let rnd = Printf.sprintf "%LX" ( Random.int64 Int64.max_int ) in
  do{
    Mutex.unlock req_id_mx;
    rnd
  }
};

(*---------------------------------------------------------------------------*)
(* "get_date_time":                                                          *)
(*---------------------------------------------------------------------------*)
(* NB: Time correction is taken from the corresp Engine if available, so this
   is a Server time, not a Client time -- values returned by this func may be
   incompatible across multiple Engines:
*)
value get_date_time: option fix_engine -> FT.date_time =
fun mbe ->
  let corr =
    match mbe with
    [ Some engine -> engine.conf.FC.time_corr
    | None        -> 0.0 ]
  in
  FM.get_date_time corr;

(*---------------------------------------------------------------------------*)
(* Determine which Thread the caller is running in:                          *)
(*---------------------------------------------------------------------------*)
(* XXX: Or should we compare Thread IDs?   *)

value in_sender_thread : fix_engine -> bool =
fun e ->
  match e.sender_thread with
  [ None     -> False
  | Some thr -> Thread.id ( Thread.self () ) = Thread.id thr
  ];

value in_recver_thread : fix_engine -> bool =
fun e ->
  match e.recver_thread with
  [ None     -> False
  | Some thr -> Thread.id ( Thread.self () ) = Thread.id thr
  ];

value in_monitor_thread: fix_engine -> bool =
fun e ->
  match e.monitor_thread with
  [ None     -> False
  | Some thr -> Thread.id ( Thread.self () ) = Thread.id thr
  ];

(*---------------------------------------------------------------------------*)
(* "submit_sys_msg":                                                         *)
(*---------------------------------------------------------------------------*)
(* Queue a system ( admin ) msg for sending via the Sender Thread. This func
   should NOT be called from within the Sender thraed itself. XXX: no report
   is sent back regarding the success of sending:
*)
value submit_sys_msg:
  fix_engine -> ?mb_seq_num:( option int ) -> FT.fix_msg -> unit =

fun e ?{ mb_seq_num = None } msg ->
do{
  assert( not ( in_sender_thread e ) );
  Event.sync( Event.send e.send_sys_q ( mb_seq_num, msg ) )
};

(*---------------------------------------------------------------------------*)
(* "get_sock":                                                               *)
(*---------------------------------------------------------------------------*)
value get_sock: fix_engine -> Unix.file_descr =
fun e ->
  match e.socket with
  [ 
    Some sock -> sock
  | None      -> raise( Socket_Error( "Socket closed by another thread?" ) )
  ];

(*===========================================================================*)
(* Socket and File I/O:                                                      *)
(*===========================================================================*)
(* NB: The following functions do not accept any "Unix.msg_flag"s:           *)

external  send_nonblock:
  Unix.file_descr -> string -> int -> int -> int = "send_nonblock";

external  recv_nonblock:
  Unix.file_descr -> string -> int -> int -> int = "recv_nonblock";

(*---------------------------------------------------------------------------*)
(* "log":                                                                    *)
(*---------------------------------------------------------------------------*)
value log: option fix_engine -> string -> BL.t -> list string -> unit =
fun mbe whr level msg_lines ->
try
  (* "DEBUG" and "TRACE" levels are skipped if not in the debug mode:        *)
  let debug =
    match mbe with
    [ Some e -> e.conf.FC.debug
    | None   -> True ]    (* No filtering at the global level                *)
  in
  if ( not debug ) && ( BL.to_int level >= 4 )
  then ()
  else
    (* NB: currently, Server rather than Client (local) time is used for log
       time stamps if the Engine is known:
    *)
    let time_stamp = get_date_time mbe                              in
    let str_stamp  = FF.string_of_date_time  time_stamp             in
    let conf_name  =
      match mbe with
      [ Some e -> FC.string_of_fix_config_id e.conf.FC.config_id
      | None   -> "GLOBAL" ]
    in
    let title      = Printf.sprintf "%s %s\n" str_stamp conf_name   in
    let one_line   =
        ( String.concat "" ( [ title :: msg_lines ] @ [ "\n" ] ) )  in
    Bolt.Logger.log whr level one_line
with
[ _ ->
  () (* "log" must not propagate any exceptions!                             *)
];

(*---------------------------------------------------------------------------*)
(* "send_fix_msg":                                                           *)
(*---------------------------------------------------------------------------*)
(* NB: This is not a client API function -- it is part of the Sender Thread: *)

value send_fix_msg:
  fix_engine -> ?mb_seq_num:( option int ) -> FT.fix_msg -> unit =

fun e  ?{mb_seq_num=None } msg ->
do{
  assert( ( in_sender_thread e )||( in_monitor_thread e ) );

  (*-----------------------*)
  (* Stringify the msg:    *)
  (*-----------------------*)
  (* Is it a possible duplicate with an explicitly-set "SeqNum"?             *)
  let ( seq_num, dup_flag ) =
    match mb_seq_num with
    [
      None ->
        (* Normal case, use the built-in counter: *)
        (  e.next_send_seq_num, False )

    | Some seq_num' ->
        do{
          if e.next_send_seq_num <= seq_num'
          then
            (* XXX: This is strange: explicit "SeqNum"s are provided only for
               re-sending old msgs:
            *)
            log ( Some e ) mname BL.WARN
                [ Printf.sprintf "SeqNum=%d explicitly set, but NextSeqNum=%d"
                                 seq_num' e.next_send_seq_num  ]
          else ();
          (* Still, in all such cases, set "dup_flag" to "True":             *)
          ( seq_num', True )
        }
    ]
  in
  let str_msg =
    try
      FM.string_of_fix_msg
        ~{ fix_version    = e.conf.FC.fix_version    }
        ~{ our_comp_id    = e.conf.FC.our_comp_id    }
        ~{ server_comp_id = e.conf.FC.server_comp_id }
        ~{ msg_seq_num    = seq_num                  }
        ~{ dup_flag       = dup_flag                 }
        ~{ time_corr      = e.conf.FC.time_corr      }
        ~{ buff           = e.send_buff              }
        msg
    with
    [ FT.Invalid_FIX_Msg err ->
      (* This is a serious error condition -- unable to stringify our own msg!
         Log the error and XXX skip the msg.  TODO: propagate an exception to
         the originator of the msg:
      *)
      do{
        log ( Some e ) mname BL.ERROR
            [ "Invalid FIX msg submitted for sending, IGNORED:\n";
              FT.string_of_fix_msg msg; "\n";
              ( string_of_fix_msg_error err ) ];
        ""
      }
      (* Other exceptions will propagate to the top level...   *)
    ]
    in
  if str_msg = ""
  then () (* Nothing to send, exiting. NB: the counter was NOT incremented! *)
  else
  let len  = String.length str_msg in

  (*-----------------------*)
  (* "send_msg_rec":       *)
  (*-----------------------*)
  (* Send the bytes up until the whole msg is transmitted:     *)

  let rec send_msg_rec: int -> unit =
  fun from ->
    (* Wait for the socket writability, but no longer than the time-out given.
       We can then send the data in a non-blocking mode ( more efficient ):
    *)
    let ( _, fdw, _ ) =
      try
        Unix.select [] [ get_sock e ] [] e.send_timeout
      with
      [ Unix.Unix_error err _ _ ->
          raise( Socket_Error( "select-send: "^( Unix.error_message err ) ) )
      ]
    in
    if fdw = []
    then
      raise( Socket_Error( "select-send: time-out" ) )
    else
    do{
      (* The socket must now be writable. Try to send the msg: *)
      assert( from >= 0 && from < len );

      let to_send = len - from  in
      let sent    =
        try send_nonblock ( get_sock e ) str_msg from to_send
        with
        [   Unix.Unix_error err _ _  ->
            raise( Socket_Error
                 ( "send_nonblock: "^( Unix.error_message err ) ) )
        ]
      in
      (* Still check that "sent" is valid: *)
      if   sent = 0
      then raise( Socket_Error "send_nonblock: TCP window closed?" )
      else
      if   sent = to_send
      then ()             (* All done, entire msg sent *)
      else
      do{
        assert( 0 < sent && sent <  to_send );
        (* Tail recursion: send the remaining bytes:   *)
        send_msg_rec ( from + sent )
      }
    }
  in
  (*-----------------------*)
  (* Send up & update:     *)
  (*-----------------------*)
  do{
    (* Send the msg:       *)
    send_msg_rec 0;

    (* Also, memoise the msg in the queue of recently sent msgs.   If the queue
       length exceeds the limit, remove the oldest saved msg. We ignore the sys
       msgs:
    *)
    match msg with
    [
      (* The following sys ( admin ) msgs are not re-sent, so not memoised:  *)
      FT.HeartBeat _
    | FT.TestReq   _
    | FT.ResendReq _
    | FT.SeqReset  _
    | FT.LogOn     _
    | FT.LogOut    _ -> ()
    | _              ->
      (* All other msgs can be re-sent. XXX: we deliberately keep the queue of
         sent msgs short  --  there is probably no point in re-sending old app
         msgs anyway:
      *)
      do{
        Queue.push ( e.next_send_seq_num, msg )   e.sent_memo;

        if Queue.length e.sent_memo > e.conf.FC.max_sent_memo_len
        then ignore ( Queue.pop e.sent_memo )
        else ()
      }
    ];
    (* Now increment the "next_send_seq_num" counter:                        *)
    e.next_send_seq_num := e.next_send_seq_num + 1;

    (* Log the msgs sent ( will only appear in the debug mode ):             *)
    let len = String.length str_msg  in
    log ( Some e ) mname BL.DEBUG
        [ Printf.sprintf "SENT %d bytes:\n" len;
          FM.raw_fix_msg_to_print str_msg 0 len
        ]
  }
};

(*---------------------------------------------------------------------------*)
(* "recv_fix_msgs":                                                          *)
(*---------------------------------------------------------------------------*)
(* Receive next msg(s) and return them as
   [ ( msg, seq_num, last_ok, dup_flag ) ].
   This function is part of the Receiver Thread:
*)
value rec recv_fix_msgs:
  fix_engine -> list ( FT.fix_msg * int * int * bool )  =
fun e        ->
do{
  assert( in_recver_thread e );

  let len  = String.length e.recv_buff  in

  (* Wait until the non-blocking socket becomes readable or the time-out
     expires.   In the latter case, will send a "TestReq" to the server,
     and if no response 2 times, will disconnect:
  *)
  let ( fdr, _, _ ) =
    let max_delay   = 1.2 *. ( float_of_int e.conf.FC.heart_bt_int ) in
    try
      Unix.select [ get_sock e ] [] [] max_delay
    with
    [ Unix.Unix_error err _ _ ->
        raise( Socket_Error( "select-recv: "^( Unix.error_message err ) ) )
    ]
  in
  if fdr = []
  then
    (* Time-out:     *)
    if e.to_count >= 2
    then
    do{
      (* Re-set the time-out counter but propagate an exception which will
         cause the connection to be closed and re-initialised:
      *)
      e.to_count  := 0;
      raise( Socket_Error "No response from the server" )
    }
    else
    do{
      (* Increment the time-out counter and send a "TestReq". XXX: We currently
         do not record its ID, any response is OK to break the time-out:
      *)
      e.to_count  := e.to_count + 1;
      let test_req_msg = FT.TestReq { FT.test_req_id_1 = new_req_id () } in
      submit_sys_msg e test_req_msg;

      (* And wait again: *)
      recv_fix_msgs  e
    }
  else
  do{
    (* The socket must now be readable, and the buff must be long enough:    *)
    e.to_count := 0;
    assert
    ( 0 <= e.parse_from && e.parse_from <= e.recv_from && e.recv_from < len );

    (*=======================*)
    (* "recv_available":     *)
    (*=======================*)
    (* Read the data into the "recv_buff" while the data are readily available
       and the buff is not full; advance the "recv_from" pos:
    *)
    let rec recv_available: unit -> unit =
      fun () ->
        let got =
          try recv_nonblock
              ( get_sock e ) e.recv_buff  e.recv_from ( len - e.recv_from )
          with
          [   Unix.Unix_error err _ _ ->
              raise( Socket_Error
                   ( "recv_nonblock: "^( Unix.error_message err ) ) )
          ]
        in
        do{
          if got > 0
          then
            log ( Some e ) mname BL.TRACE
                [ Printf.sprintf "RECEIVED %d bytes: [ %d..%d )"  got
                                 e.recv_from ( e.recv_from + got )
                ]
          else ();

          if  got = 0
          then
            ()    (* No more data immeditely available,  return *)
          else
          do{
            (* Adjust the buff position:   *)
            assert( got > 0 );
            e.recv_from := e.recv_from + got;

            if ( e.recv_from < len )
            then
              (* There is a space for more data, try to get it: *)
              recv_available  ()
            else ()     (* No buff space for more data, return  *)
          }
        }
    in
    (*-----------------------*)
    (* Run the Receiver:     *)
    (*-----------------------*)
    recv_available ();

    (* NB:  Although we waited in "select" for the socket readability,  it does
       not guarantee that we actually got a non-"" read -- it might be that the
       socket was closed by the Server. If we got nothing to parse, disconnect:
    *)
    if e.parse_from = e.recv_from
    then
      raise( Socket_Error "recv: Connection closed by the Server" )
    else
    (*=======================*)
    (* "parse_msgs":         *)
    (*=======================*)
    (* Got some data in the buffer. NB: the last msg may be partial. Parse the
       FIX msgs from the "parse_from" pos on. If an invalid ( unparsable ) msg
       occurs and its length is not known, all msgs to the end of the buff are
       skipped:
    *)
    let rec parse_msgs:
        int ->
        list ( FT.fix_msg * int * int * bool ) ->
        list ( FT.fix_msg * int * int * bool ) =
    fun  pos curr ->
    do{
      (* NB: "pos" going beyond "recv_from" would be a serious error condition
         -- invalid msg length arithmetic in the parser:
      *)
      assert( e.parse_from <= pos && pos <= e.recv_from );

      (*---------------------*)
      (* End of Input:       *)
      (*---------------------*)
      if pos = e.recv_from
      then
      do{
        (* All done, return the reversed list. If we reached the physical end
           of the buffer, start from the beginning:
        *)
        if e.recv_from = len
        then e.recv_from := 0
        else ();
        e.parse_from     := e.recv_from;
        curr
      }
      else
      (*---------------------*)
      (* Generic Case:       *)
      (* pos < e.recv_from : *)
      (*---------------------*)
      (* Try to parse the msg and update "curr" and "pos": *)
      let ( curr', pos' ) =
        try
          let me =
            FM.fix_msg_of_string
              ~{ fix_version    = e.conf.FC.fix_version    }
              ~{ our_comp_id    = e.conf.FC.our_comp_id    }(* Still our one! *)
              ~{ server_comp_id = e.conf.FC.server_comp_id }
              ~{ debug          = e.conf.FC.debug          }
              e.recv_buff
              pos
              e.recv_from
          in
          do{
            (*---------------------*)
            (* Successful parsing: *)
            (*---------------------*)
            (* In the debug mode, log the parsed msgs:     *)
            log ( Some e ) mname BL.DEBUG
                [ "PARSED:\n";
                  FM.raw_fix_msg_to_print e.recv_buff pos me.FM.msg_next
                ];
            assert( pos < me.FM.msg_next && me.FM.msg_next <= e.recv_from );

            (* The modified list and buff pos: *)
            ( [ ( me.FM.msg, me.FM.msg_seq_num, me.FM.msg_last_ok,
                  me.FM.msg_dup_flag )       :: curr
              ],
              me.FM.msg_next
            )
          }
        with
        [ (*---------------------*)
          (* "Invalid_FIX_Msg":  *)
          (*---------------------*)
          FT.Invalid_FIX_Msg err ->
          do{
            (* Parsing error on an incoming msg. First, log it:  *)
            log ( Some e ) mname BL.ERROR
                [ "Invalid FIX msg received:\n";
                  FM.raw_fix_msg_to_print e.recv_buff pos e.recv_from;
                  "\n"; ( string_of_fix_msg_error  err )
                ];
            (* If we can form a "Reject" msg, send it -- but  XXX  not in the
               Debug mode, since in that case the errors may be due to faults
               on the client side:
            *)
            match mk_reject err with
            [
              Some reject when not e.conf.FC.debug -> submit_sys_msg e reject
            | _ -> ()
            ];

            (* "next_pos" may or may not be known:               *)
            let next_pos =
              if   err.FT.err_next_msg_pos > pos
              then err.FT.err_next_msg_pos
              else
                (* XXX: This is a serious error condition, since the msg bounds
                   have become blurred -- most likely due to incorrect  prefix
                   of the curr msg. Try to find the next msg beginning, failing
                   that, raise an exception:
                *)
                raise( Socket_Error( Printf.sprintf
                       "Cannot determine msg boundary after %d" pos ) )
            in
            (* "seq_num" may also be known; if it is, we will replace the
               failed msg by a placeholder in order not to mix up the seq:
            *)
            let seq_num =
              if   err.FT.err_seq_num >= 1
              then err.FT.err_seq_num
              else 0
            in
            do{
              assert( next_pos <= e.recv_from );
              log ( Some e ) mname BL.INFO
                  [ "Attempting recovery from off=";
                      string_of_int ( next_pos - pos ) ];

              let  curr' =
                if seq_num >= 1
                then
                  (* Synthesise a placeholder is order to avoid a SeqNum
                     error:
                  *)
                  [ ( FT.HeartBeat
                    { FT.test_req_id_0 = Some "WARNING:PLACEHOLDER" },
                      seq_num, 0, False
                    ) :: curr    ]
                else curr
              in
              ( curr',  next_pos )
            }
          }
        | (*---------------------*)
          (* "Partial_FIX_Msg":  *)
          (*---------------------*)
          FF.Partial_FIX_Msg ->
          do{
            (* This is a completely valid situation: the last msgs was received
               only partially.    If there is no more space in the buffer, move
               the received part to the beginning:
            *)
            log ( Some e ) mname BL.TRACE
                [ "Partial FIX Msg received @off="; string_of_int pos ];

            assert( pos < e.recv_from );
            if e.recv_from = len
            then
            do{
              log ( Some e ) mname BL.TRACE
                  [ "Relocating last msg chunk to the beginning" ];

              for i = pos to ( e.recv_from - 1 )
              do{
                e.recv_buff.[ i-pos ] := e.recv_buff.[ i ]
              };
              e.recv_from  := e.recv_from - pos;
              e.parse_from := 0
            }
            else
              (* Next parse ( after the msg is fully read in ) will continue
                 from the current pos:
              *)
              e.parse_from := pos;

            (* Nothing new to return: *)
            ( curr, pos )
          }
        ]
      in
      if pos' > pos
      then
        (* A valid "pos'", continue parsing from there:       *)
        parse_msgs  pos' curr'
      else
        (* Return what we got ( e.g. on a partial last msg ): *)
        curr'
    }
    in
    (*-----------------------*)
    (* Run the Parser:       *)
    (*-----------------------*)
    List.rev ( parse_msgs e.parse_from [] )
  }
};

(*---------------------------------------------------------------------------*)
(* "fix_connect":                                                            *)
(*---------------------------------------------------------------------------*)
(* Creates the communication socket and makes a TCP connection with the FIX
   Server. Runs as part of the Monitor Thread:
*)
value fix_connect: fix_engine -> unit =
fun e ->
do{
  (* Create the socket. Before that, the socket should not exist:            *)
  assert( in_monitor_thread e && e.socket = None );

  let sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
  do{
    (* Make the socket non-blocking and set some socket options:             *)
    Unix.set_nonblock      sock;
    Unix.setsockopt        sock Unix.TCP_NODELAY True;
    Unix.setsockopt_int    sock Unix.SO_SNDBUF   e.buff_size;
    Unix.setsockopt_int    sock Unix.SO_RCVBUF   e.buff_size;
    Unix.setsockopt_optint sock Unix.SO_LINGER   None;

    (* Connect  to the server. NB: it is perfectly normal for a non-blocking
      "connect" to raise "EINPROGRESS":
    *)
    log ( Some e ) mname BL.INFO
        [ "CONNECTING TO "; e.conf.FC.server_ip; ":";
           string_of_int    e.conf.FC.server_port ];

    try   Unix.connect  sock
        ( Unix.ADDR_INET ( Unix.inet_addr_of_string e.conf.FC.server_ip )
          e.conf.FC.server_port )
    with
    [ Unix.Unix_error Unix.EINPROGRESS _ _ -> ()
    | Unix.Unix_error err _ _              ->
        raise( Socket_Error ( "connect: "^( Unix.error_message err ) ) )
    ];

    (* Wait for socket readability or writability to complete the "connect" --
       with a time-out:
    *)
    let ( fdr, fdw, _ ) =
      try
        Unix.select [ sock ] [ sock ] [] e.send_timeout
      with
      [ Unix.Unix_error err _ _ ->
        raise( Socket_Error ( "select-connect: "^( Unix.error_message err ) ) )
      ]
    in
    if fdr = [] && fdw = []
    then
      raise( Socket_Error ( "select-connect: time-out" ) )
    else
      assert( fdr = [ sock ] || fdw = [ sock ] );

    (* Connection is complete, but it is not known whether it was successful or
       refused, due to non-blocking socket used; errors may manifest themselves
       on the first data transaction. Save the "sock" in the Engine:
    *)
    e.socket := Some sock;
  }
};

(*---------------------------------------------------------------------------*)
(* "fix_close":                                                              *)
(*---------------------------------------------------------------------------*)
(* Close and invalidate the socket ignoring any errors:                      *)
value fix_close: fix_engine -> unit =
fun e ->
do{
  try  Unix.close   ( get_sock e )
  with[ _ -> () ];
  e.socket := None; (* Should be OK -- atomic update *)
};

