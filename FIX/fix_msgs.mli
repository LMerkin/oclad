(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                      Encoding / Decoding of FIX Msgs:                     *)
(*===========================================================================*)
(* Getting the type of each FIX msg:          *)

value get_fix_msg_type: Fix_types.fix_msg -> Fix_types.fix_msg_type;

(* Conversion of a FIX msg into a low-level string format ready for sending;
   "buff" is a tmp Buffer used to assemble the msg parts efficiently.    NB:
   this function has nothing in common with "Fix_types.string_of_fix_msg";
   the latter returns a string representation of the OCaml term for  msg:
*)
value string_of_fix_msg:
  ~fix_version:    string   ->
  ~our_comp_id:    string   ->
  ~server_comp_id: string   ->
  ~msg_seq_num:    int      ->
  ~dup_flag:       bool     ->
  ~time_corr:      float    ->
  ~buff:           Buffer.t ->
  Fix_types.fix_msg         ->
  string;

(* Conversion of a raw FIX msgs ( e.g. one received from server, or the result
   of "string_of_fix_msg" ) into a printablle form -- replace unplrintable SOH
   fld separators with '|'s:
*)
value raw_fix_msg_to_print: string -> int -> int -> string;
(*                         raw_msg  from_pos until_pos                       *)

(* Parsing of a FIX msg. The args include the from-pos in the msg buffer, since
   FIX runs ovr TCP and FIX msgs are not aligned at datagram boundaries. Return
   value: ( fix_msg, msg_seq_num, dup_flag, next_pos ).
   Exceptions:
   Fix_flds.Partial_FIX_Msg   -- if the msg has been fragmented;
   Fix_types.Invalid_FIX_Msg  -- if any other parsing error occured:
*)
type fix_msg_etc =
{
  msg         : Fix_types.fix_msg;
  msg_seq_num : int;
  msg_last_ok : int;
  msg_dup_flag: bool;
  msg_next    : int
};

value fix_msg_of_string:
  ~fix_version:    string   ->
  ~our_comp_id:    string   ->
  ~server_comp_id: string   ->
  ~debug:          bool     ->
  string                    ->                (* Buffer                   *)
  int                       ->                (* From Pos                 *)
  int                       ->                (* Logical Buffer Len       *)
  fix_msg_etc;

(* Misc Utils:              *)
value get_date_time: float  -> Fix_types.date_time;
                                              (* Arg: TimeCorrection, sec *)
value string_of_exn: exn    -> string;

