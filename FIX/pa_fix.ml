(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*    OCaml Parser Extension for Generating FIX Data Types and Functions     *)
(*===========================================================================*)
(* "Fix_defs" module contains the defs from which the stuff is generated:    *)

(*===========================================================================*)
(* Common Utils:                                                             *)
(*===========================================================================*)
Printexc.record_backtrace True;
module FD = Fix_defs;

value fatal_error: exn -> 'a =
fun exn ->
do{
  prerr_endline ( Printexc.to_string exn    );
  prerr_endline ( Printexc.get_backtrace () );
  flush stderr;
  exit 1
};

(*---------------------------------------------------------------------------*)
(* "from_humps":                                                             *)
(*---------------------------------------------------------------------------*)
(* Convert an external Fld or Msg name ( e.g. "MDReqID" ) into the generic LC
   form.   For Flds, no posifix or plural form is installed here -- it is not
   known at this point how the corresp Fld is going to be used.
   NB: the function memeoises its results in a "from_humps_ht":
*)
value from_humps_ht: Hashtbl.t string string = Hashtbl.create 512;

value from_humps   : string -> string =
fun  fn ->
  if fn = ""
  then failwith  "ERROR: Empty Name to convert to LC"
  else
  if Hashtbl.mem from_humps_ht fn
  then
    (* Use the memoised value: *)
    Hashtbl.find from_humps_ht fn
  else
  (* Do the actual conversion: *)
  let buff      = Buffer.create 64                  in
  let is_cap    = fun c -> ( c >= 'A' && c <= 'Z' ) in
  let is_low    = fun c -> ( c >= 'a' && c <= 'z' ) in
  (* NB: "prev_char" is the original char @ [ i-1 ], or '_', depending on what
     has been output last:
  *)
  let prev_char = ref fn.[ 0 ]                      in
  let last_pos  = String.length fn - 1              in
  do{
    if  not ( is_cap fn.[ 0 ] )
    then failwith( "ERROR: FIX Fld name must begin with a Capital: "^fn )
    else Buffer.add_char buff ( Char.lowercase fn.[ 0 ] );

    for i = 1 to last_pos
    do{
      let this_cap = is_cap fn.[ i ]          in
      let lc       = Char.lowercase fn.[ i ]  in

      let prev_cap = is_cap prev_char.val     in
      let prev_low = is_low prev_char.val     in

      (* Whether to install a '_':      *)
      if this_cap && prev_cap && i <> last_pos
      then
      do{
        (* 2 caps -- install '_' AFTER this char, UNLESS it is followed by a
           '_' anyway, or followed by a single cap and an '_':
        *)
        Buffer.add_char buff lc;
        if fn.[ i+1 ] <> '_' &&
           ( not( i <= last_pos - 2 && is_cap fn.[ i+1 ] && fn.[ i+2 ] = '_' ) )
        then
        do{
          Buffer.add_char buff '_';
          prev_char.val :=     '_'
        }
        else
          prev_char.val := fn.[ i ]
      }
      else
      if this_cap && prev_low
      then
      do{
        (* Hump: lower -> upper; install '_' BEFORE this char: *)
        Buffer.add_char buff '_';
        Buffer.add_char buff lc;
        prev_char.val   := fn.[ i ]
      }
      else
      do{
        (* Generic case:     *)
        Buffer.add_char buff lc;
        prev_char.val   := fn.[ i ]
      }
    };
    (* Memoise and return the lower-case Fld name: *)
    let res = Buffer.contents buff in
    do{
      Hashtbl.add from_humps_ht fn res;
      res
    }
  };

(*===========================================================================*)
(* "mk_fix_enum_type":                                                       *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "fix_fld_subtypes_ht":                                                    *)
(* "get_fix_fld_subtype_def":                                                *)
(*---------------------------------------------------------------------------*)
type  fix_fld_subtype_def = ( FD.codec * ( list ( string * string ) ) );
value fix_fld_subtypes_ht:  Hashtbl.t  string fix_fld_subtype_def =
                            Hashtbl.create 64;
List.iter
(
  fun ( stn, codec, stvals ) ->
    if   Hashtbl.mem fix_fld_subtypes_ht stn
    then failwith( "ERROR: Repeated FIX Fld SubType: " ^ stn )
    else Hashtbl.add fix_fld_subtypes_ht stn ( codec, stvals )
)
FD.fix_fld_subtypes_def;


value get_fix_fld_subtype_def: string -> fix_fld_subtype_def =
fun stn ->
  try Hashtbl.find fix_fld_subtypes_ht stn
  with[ Not_found ->
        failwith( "ERROR: Invalid FIX type name: "^stn )  ];
  
(*---------------------------------------------------------------------------*)
(* Generating ADTs ( sum types ) with 0-arg ctors, typically to hold values of
   various FIX Flds:
*)
value mk_fix_enum_type =
fun _loc type_name    ->
try
  (* Select the projection func which yields the list of 0-arg ctors:        *)
  let ( ctors, prefix ) =
    (* First, 2 special cases:             *)
    if   type_name = "fix_fld_type"
    then ( List.map ( fun ( fn, _, _ ) -> fn ) FD.fix_fld_def, "T_" )
    else
    if   type_name = "fix_msg_type"
    then ( List.map ( fun ( fn, _, _ ) -> fn ) FD.fix_msg_def, "M_" )
    else
    (* Now search all other fld sub-types for  "type_name": *)
    let ( _, stvals ) = get_fix_fld_subtype_def type_name  in
    ( List.map fst stvals, "" )
  in
  <:ctyp<
    [
      $list: List.map ( fun ctor -> ( _loc, prefix ^ ctor, [], None ) ) ctors $
    ]
  >>
with[ any -> fatal_error any ];

(*===========================================================================*)
(* Generation of FIX Msg and Fld Grp Record Types:                           *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "fix_msgs_ht":                                                            *)
(* "get_fix_msg_def":                                                        *)
(* "fix_gmsgs_ht":                                                           *)
(* "get_fix_gmsg_def":                                                       *)
(*---------------------------------------------------------------------------*)
(* Provide STRUCTURE and ENCODING of Msgs and Msgs+Grps. For Grps, the code is
   SOH:
*)
type  fix_gmsg_def = ( string * list ( string * FD.sog ) );
(*                     code          fld_name fld_sog *)

(* Construct the maps: MsgTypeName       => fix_gmsg_def
                 and   {Msg,Grp}TypeName => fix_gmsg_def
*)
value fix_msgs_ht:  Hashtbl.t string fix_gmsg_def = Hashtbl.create 64;
value fix_gmsgs_ht: Hashtbl.t string fix_gmsg_def = Hashtbl.create 64;

do{
  (* Install Msgs in both HashTables:        *)
  List.iter
  (
    fun ( m, c, flds ) ->
    do{
      if   Hashtbl.mem fix_gmsgs_ht m
      then failwith( "ERROR: Repeated FIX Msg/Grp def: "^ m )
      else Hashtbl.add fix_gmsgs_ht m ( c, flds );

      assert( not( Hashtbl.mem fix_msgs_ht m ) );
      Hashtbl.add fix_msgs_ht  m ( c, flds   )
    }
  )
  FD.fix_msg_def;

  (* Install Grps in the 2nd HashTable only: *)
  List.iter
  (
    fun ( gn, s, flds ) ->
      if   Hashtbl.mem fix_gmsgs_ht gn
      then failwith( "ERROR: Multiply-defined FIX Grp/Msg name: "^gn )
      else Hashtbl.add fix_gmsgs_ht gn ( s, flds )
  )
  FD.fix_grp_def
};

(* Getting the defs grom "fix_[g]msgs_ht":   *)

value get_fix_msg_def: string -> fix_gmsg_def =
fun m ->
  try Hashtbl.find fix_msgs_ht m
  with[ _ -> failwith( "ERROR: FIX Msg def not found: "^ m ) ];


value get_fix_gmsg_def: string -> fix_gmsg_def =
fun mg    ->
  try Hashtbl.find fix_gmsgs_ht mg
  with[ _ -> failwith( "ERROR: FIX Msg/Grp def not found: "^ mg ) ];


(*---------------------------------------------------------------------------*)
(* "fix_flds_ht" :                                                           *)
(* "fix_gflds_ht":                                                           *)
(* "get_fix_fld_def" :                                                       *)
(* "get_fix_gfld_def":                                                       *)
(*---------------------------------------------------------------------------*)
(* All converted   Fld and Grp names: map: Humped => lc_spaced.
   "fix_flds_ht":  Flds only
   "fix_gflds_ht": Flds and Grps together ( tag=-1 for Grps )
*)
type  fix_gfld_def = (  string * int * string );
                     (* lc_name, tag,  ftype *)

value fix_flds_ht : Hashtbl.t string fix_gfld_def = Hashtbl.create 512;
value fix_gflds_ht: Hashtbl.t string fix_gfld_def = Hashtbl.create 512;

(* Fill them in: *)
do{
  (* Install elementary Fld defs: *)
  List.iter
  (
    fun ( fn, ftag, ftype ) ->
      let fdef = ( from_humps fn, ftag, ftype ) in
      do{
        if not ( Hashtbl.mem  fix_gflds_ht fn )
        then     Hashtbl.add  fix_gflds_ht fn fdef
        else failwith( "ERROR: Multiply-defined FIX Fld/Grp name: "^fn  );

        assert( not ( Hashtbl.mem fix_flds_ht fn ) );
        Hashtbl.add fix_flds_ht   fn  fdef
     }
  )
  FD.fix_fld_def;

  (* Install names and types ( obviously no valid tags ) for FIX Fld Grps:   *)
  List.iter
  (
    fun ( gn, _, _ ) ->
      if not ( Hashtbl.mem fix_gflds_ht gn )
      then
        let gtype = from_humps   gn      in
        Hashtbl.add fix_gflds_ht gn ( gtype, -1, gtype )
      else
        failwith( "ERROR: Multiply-defined FIX Grp/Fld name: "^gn )
  )
  FD.fix_grp_def
};


value get_fix_fld_def : string -> fix_gfld_def =
fun fn ->
  try Hashtbl.find fix_flds_ht  fn
  with[ _ -> failwith( "ERROR: Invalid FIX Fld name: "    ^fn ) ];


value get_fix_gfld_def: string -> fix_gfld_def =
fun fn ->
  try Hashtbl.find fix_gflds_ht fn
  with[ _ -> failwith( "ERROR: Invalid FIX Fld/Grp name: "^fn ) ];


(*---------------------------------------------------------------------------*)
(* "not_structured_fld_grp":                                                 *)
(*---------------------------------------------------------------------------*)
value not_structured_fld_grp: string -> bool =
fun fn ->
  if not ( Hashtbl.mem fix_gflds_ht fn )
  then     failwith( "ERROR: Invalid FIX Fld/Grp name: "^fn )
  else     Hashtbl.mem fix_flds_ht  fn;
 
(*---------------------------------------------------------------------------*)
(* "get_gmsg_fld_info":                                                      *)
(*---------------------------------------------------------------------------*)
(* Getting converted and postfixed fld names for use in FIX Msg and FIX Fld Grp
   types, from the above "fix_flds_ht".   NB: This Fld info is already specific
   to the Msg where the Fld is installed:
*)
value get_gmsg_fld_info: string -> string -> FD.sog -> ( string  * string ) =
                                                       (* lc_name, ftype *)
fun c fn sog ->
  let ( lcm0, _, ftype ) = get_fix_gfld_def fn in

  (* If this is a Grp, translate the Fld name into plural for use in record: *)
  let lcm1 =
    match sog with
    [
      FD.Group    _
    | FD.OptGroup _ ->
        let n = String.length lcm0 in
        do{
          assert( n >= 2  );
          if   lcm0.[ n-1 ] = 'y'
          then ( String.sub lcm0 0 ( n-1 ) )^"ies"
          else lcm0 ^ "s"
        }
    | _ -> lcm0
    ]
  in
  (* Install the postfix to avoid name clashes between Flds in different Msg or
     Grp types:
  *)
  let lcm2 = Printf.sprintf "%s_%s" lcm1 c in
  ( lcm2, ftype );

(*---------------------------------------------------------------------------*)
(* "get_fix_fld_tag":                                                        *)
(*---------------------------------------------------------------------------*)
(* Getting FIX flt tags from "fix_flds_ht": *)

value get_fix_fld_tag: string -> int =
fun fn ->
  let ( _, ftag, _ ) =
    try Hashtbl.find fix_flds_ht fn
    with[ _  -> failwith(  "ERROR: Invalid FIX Fld name: "^fn ) ]
  in
  ftag;

(*---------------------------------------------------------------------------*)
(* "mk_gmsg_fld_type":                                                       *)
(*---------------------------------------------------------------------------*)
(* NB: This function also applies to FIX Fld Grps. Constructs the actual Msg
   Fld type from the type name -- the actual type may also be an "option" or
   an "array", depening on the Fld declaration in the Msg:
*)
value mk_gmsg_fld_type  =
fun _loc ftype sog ->
  match sog with
  [
    FD.Single      ->
      <:ctyp< $lid:ftype$ >>                   (* Just the type ident itself *)
  | FD.Option      ->
      <:ctyp< option $lid:ftype$ >>            (* "option ftype"             *)
  | FD.Group    _
  | FD.OptGroup _  ->
      <:ctyp< array  $lid:ftype$ >>            (* "array  ftype"             *)
  ];

(*---------------------------------------------------------------------------*)
(* "mk_fix_gmsg":                                                            *)
(*---------------------------------------------------------------------------*)
(* Constructing a FIX Msg or a FIX Fld Grp record type:                      *)

value mk_fix_gmsg   =
fun  _loc gmsg_ctor ->
try
  (* Get the Msg /  Grp code and Flds: *)
  let ( c, flds ) = get_fix_gmsg_def gmsg_ctor in
  <:ctyp<
  {
    $list:
      (* Install the record Flds:      *)
      List.map
      (
        fun  ( fn, sog ) ->
          (* Fld name as it will appear in the Msg type record: *)
          let ( rf, ftype ) = get_gmsg_fld_info c   fn    sog in

          (* Type of the Fld:                                   *)
          let rftype        = mk_gmsg_fld_type _loc ftype sog in

          (* NB: All Msg Flds are non-mutable :                 *)
          ( _loc, rf, False, rftype )
      )
      flds
    $
  }
  >>
with[ any -> fatal_error any ];

(*===========================================================================*)
(* Generation of Top-Level FIX Fld and FIX Msg Sum Types:                    *)
(*===========================================================================*)
value mk_fix_top =
fun _loc  t      ->
try
  let ctors =
    match t with
    [
      "fix_fld"  ->
        (* For Flds, their types are pre-configured:       *)
        List.map
          ( fun ( ctor, _, ftype )  -> ( ctor, <:ctyp< $lid:ftype$ >> ) )
          FD.fix_fld_def

    | "fix_msg"  ->
        (* For Msgs, their types are converted Ctor names: *)
        List.map
        (
          fun ( mctor, _, _ ) ->
            let mtype = from_humps   mctor in
            ( mctor, <:ctyp< $lid:mtype$ >> )
        )
        FD.fix_msg_def

    | _ ->
      failwith( "ERROR: Invalid Top-Level Def: "^t )
    ]
  in
  <:ctyp<
  [
    $list: List.map( fun ( ctor, ctype ) ->
                   ( _loc, ctor, [ ctype ], None ) ) ctors$  (* XXX: None?? *)
  ]
  >>
with[ any -> fatal_error any ];

(*---------------------------------------------------------------------------*)
(* Generation of the Map: FIXFld => FIXFldType or FIXMsg => FIXMsgType:      *)
(*---------------------------------------------------------------------------*)
value mk_fix_proj =
fun _loc tm       ->
try
  let ctors =
    match tm with
    [ "T_" -> (* FIX Flds: *)
        List.map ( fun ( fn, _, _ ) -> fn ) FD.fix_fld_def
    | "M_" -> (* FIX Msgs: *)
        List.map ( fun ( mn, _, _ ) -> mn ) FD.fix_msg_def
    | _    -> failwith( "ERROR: Invalid prefix: "^tm )
    ]
  in
  let cases =
    List.map
    (
      fun ctor ->
        (* Make explicit qualification with "FT = Fix_types" module:   *)
        let c   = <:patt< FT.$uid:ctor$   _    >> in
        let ct  = <:expr< FT.$uid:( tm^ctor )$ >> in
        ( c, None (* No guards *), ct )
    )
    ctors
  in
  <:expr< fun[ $list:cases$  ] >>
with[ any -> fatal_error any ];

(*===========================================================================*)
(* Generation of FIX Fld Sub-Type Encoding/Decoding Funcs:                   *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* Encoder for sub-types:                                                    *)
(*---------------------------------------------------------------------------*)
value mk_subtype_enc    =
fun _loc codec stn vals ->
  let prefix    =
    match codec with
    [ FD.Char   -> "char_of_"
    | FD.Int    -> "int_of_"
    | FD.String -> "string_of_"
    ]
  in
  let func_name = <:patt< $lid:( prefix ^ stn ) $>> in
  let cases     =
    List.map
    (
      fun ( vn, vs ) ->
        let lhs = <:patt< FT.$uid:vn$  >> in
        let rhs =
          match codec with
          [ FD.Char   -> <:expr< $chr:vs$ >>
          | FD.Int    -> <:expr< $int:vs$ >>
          | FD.String -> <:expr< $str:vs$ >>
          ]
        in
        ( lhs, None (* No guards *), rhs )
    )
    vals
  in
  let func_body = <:expr< fun[ $list:cases$  ] >> in
  let entry     = [ ( func_name, func_body ) ]    in

  <:str_item< value $flag:False$ (* Non-rec *) $list:entry$ >>;

(*---------------------------------------------------------------------------*)
(* Decoder for sub-types:                                                    *)
(*---------------------------------------------------------------------------*)
value mk_subtype_dec    =
fun _loc codec stn vals ->
do{
  (* Sub-types with vals=[] are special cases, MUST NOT appear here:         *)
  assert( vals <> [] );

  (* NB: decoding is alsways performed "_of_string":    *)
  let func_name = <:patt< $lid:( stn ^"_of_string" )$ >> in

  (* Cases with a valid LHS on parsing:                 *)
  let cases0    =
    List.map
    (
      fun ( vn, vs ) ->
        let lhs =
          match codec with
          [ FD.Char   -> <:patt< $chr:vs$ >>
          | FD.Int    -> <:patt< $int:vs$ >>
          | FD.String -> <:patt< $str:vs$ >>
          ]
        in
        let rhs = <:expr< FT.$uid:vn$ >> in
        ( lhs, None, rhs )
    )
    vals
  in
  (* Catch-all case -- invalid LHS.  This results in the "invalid_fix_fld"
     invocation with the function param given by ident  "s". NB: "stn" can
     also be "fix_fld_type", in which case conversion failure results in a
     special exception:
  *)
  let any_other = <:patt< _ >>                                    in
  let err_ctor  =
    match stn with
    [ "fix_fld_type" -> <:expr< $uid:"InvalidTagNum"$     >>
    | _              -> <:expr< $uid:"ValueRangeError"$   >>
    ]
  in
  let err_func  = <:expr< invalid_fix_fld FT.$err_ctor$ s >>      in
  let cases1    = cases0 @ [ ( any_other, None, err_func ) ]      in

  (* Func body containing the "match" expr with "cases1": *)
  let func_body =
    match codec with
    [ FD.Char   ->
      <:expr< fun s ->
                if String.length s = 1
                then match s.[ 0 ] with  [ $list:cases1$ ]
                else invalid_fix_fld FT.ValueFormatError s
      >>
    | FD.Int    ->
      <:expr< fun s -> match int_val_of_string s with [ $list:cases1$ ] >>

    | FD.String ->
      <:expr< fun s -> match s with [ $list:cases1$ ]   >>
    ]
  in
  (* Th etop-level entry: *)
  let entry = [ ( func_name, func_body ) ] in

  <:str_item< value $flag:False$ (* Non-rec *) $list:entry$ >>
};

(*---------------------------------------------------------------------------*)
(* Top-Level Generator of Multiple "str_item"s:                              *)
(*---------------------------------------------------------------------------*)
value mk_fix_fld_subtype_codecs =
fun _loc ->
try
  (* Codec for "fix_fld_type": *)
  let ff = List.map
            ( fun ( fn, ftag, _ )  -> ( "T_"^fn, string_of_int  ftag  ) )
            FD.fix_fld_def
  in
  let ff_enc = mk_subtype_enc _loc FD.Int  "fix_fld_type"   ff in
  let ff_dec = mk_subtype_dec _loc FD.Int  "fix_fld_type"   ff in

  (* Codec for "fix_msg_type": *)
  let fm = List.map
            ( fun ( mn, mcode, _ ) -> ( "M_"^mn, Printf.sprintf "%s" mcode ) )
            FD.fix_msg_def
  in
  let fm_enc = mk_subtype_enc _loc FD.String "fix_msg_type" fm in
  let fm_dec = mk_subtype_dec _loc FD.String "fix_msg_type" fm in

  (* Codecs for all other subtypes:         *)
  let all    =
    List.fold_left
    (
      fun curr ( stn, codec, stvals ) ->
        (* NB: ignore sub-types with stvals=[]: they are special cases, treated
           above and elsewhere:
        *)
        if stvals = []
        then curr
        else
          let t_enc = mk_subtype_enc _loc codec stn stvals     in
          let t_dec = mk_subtype_dec _loc codec stn stvals     in
          [ t_enc; t_dec :: curr ]
    )
    [ ff_enc; ff_dec; fm_enc; fm_dec ]
    FD.fix_fld_subtypes_def
  in
  (* A single module-level "declare" entry: *)
  <:str_item< declare $list:all$ end >>

with[ any -> fatal_error any ];

(*---------------------------------------------------------------------------*)
(* Core of the top-level encoder for FIX Flds:                               *)
(*---------------------------------------------------------------------------*)
value mk_fix_fld_encoder =
fun _loc ->
try
  let cases =
    List.map
    (
       fun ( fname, tag, ftype ) ->
        let lhs  = <:patt< FT.$uid:fname$ x >>           in
        let stag = <:expr< $str:( string_of_int tag )$>> in
        let rhs  =
          match ftype with
          [
            "int"          ->
              <:expr< if x >= 0
                      then Printf.bprintf buff "%s=%d\\x01" $stag$ x
                      else invalid_fix_fld FT.ValueRangeError( string_of_int x )
              >>
          | "price"        ->
              <:expr< Printf.bprintf buff "%s=%s\\x01" $stag$
                                          ( string_of_price x )              >>
          | "bool"         ->
              <:expr< Printf.bprintf buff "%s=%c\\x01" $stag$
                                          ( if x then 'Y' else 'N' )         >>
          | "string"       ->
              <:expr< Printf.bprintf buff "%s=%s\\x01" $stag$
                                          ( check_string    x )              >>
          | "date"
          | "time"
          | "date_time"    ->
              <:expr< Printf.bprintf buff "%s=%s\\x01" $stag$
                      ( $lid:( "string_of_"^ ftype )$  x )                   >>
          | other          ->
              (* Other types are listed sub-types; encode them according to the
                 configured encoding type and function.  The following includes
                 all special cases as well:
              *)
              let   codec = fst ( get_fix_fld_subtype_def other )            in
              match codec with
              [ FD.Char    ->
                  <:expr< Printf.bprintf buff "%s=%c\\x01" $stag$
                          ( $lid:( "char_of_"   ^ ftype )$  x   )            >>

              | FD.Int     ->
                  <:expr< Printf.bprintf buff "%s=%d\\x01" $stag$
                          ( $lid:( "int_of_"    ^ ftype )$  x   )            >>

              | FD.String  ->
                  <:expr< Printf.bprintf buff "%s=%s\\x01" $stag$
                          ( $lid:( "string_of_" ^ ftype )$  x   )            >>
              ]
          ]
        in
        ( lhs, None (* No guards *), rhs )
    )
    FD.fix_fld_def
  in
  <:expr< match fld with [ $list:cases$ ] >>

with[ any -> fatal_error any ];

(*---------------------------------------------------------------------------*)
(* Core of the top-level decoder for FIX Flds:                               *)
(*---------------------------------------------------------------------------*)
value mk_fix_fld_decoder =
fun _loc ->
try
  let cases =
    List.map
    (
      fun ( fname, _, ftype ) ->
        let lhs             = <:patt< FT.$uid:( "T_"^fname) $ >>  in
        let conv            =
          match ftype with
          [
            "int"    -> "nat_of_string"
          | "bool"   -> "yn"
          | "string" -> "check_string"
          | other    ->  other ^"_of_string"
          ]
        in
        let rhs             = <:expr< FT.$uid:fname$ ( $lid:conv$ s ) >> in
        ( lhs, None (* No guards *), rhs )
    )
    FD.fix_fld_def
  in
  <:expr< match ftype with [ $list:cases$ ] >>

with[ any -> fatal_error any ];

(*===========================================================================*)
(* Splitting Functions for Sub-Types:                                        *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "is_empty_grp_ok":                                                        *)
(*---------------------------------------------------------------------------*)
(* NB: "Group"s must not be empty, but "OptGroup"s may. Returns a Expr, not a
   Bool const:
*)
value is_empty_grp_ok =
fun _loc sog ->
  match  sog with
  [ FD.Group    _ -> <:expr< $uid:"False"$ >>
  | FD.OptGroup _ -> <:expr< $uid:"True" $ >>
  | _             -> assert False
  ];

(*---------------------------------------------------------------------------*)
(* "mk_split_lets":                                                          *)
(*---------------------------------------------------------------------------*)
(* Returns a list of exprs which can then be used to produce a chain of "let...
   in..." exprs for splitting a Msg into a list of Flds. Recursively processes
   Grps:
*)
value rec mk_split_lets    =
fun  _loc rec_level mgtype ->
  (* The base for intermediate exprs: just "e" at the top recursion level, "ee"
     at the 1st one, and so on:
  *)
  let base = String.make ( rec_level + 1 ) 'e'      in

  (* Get the Msg/Grp code and Flds -- the latter need to be reversed. The code
     is SOH for Grps:
  *)
  let ( c, flds )      = get_fix_gmsg_def mgtype    in
  let rflds            = List.rev         flds      in
  let ( ne, expr_lst ) =
    List.fold_left
    (
      fun ( n, curr )( fn, sog ) ->
        let rf = fst ( get_gmsg_fld_info c fn sog ) in
        (* XXX: Difficult point:
           Names of the prev and curr list exprs. At the top recursion level,
           we start generation with "e1" and set "ep=e0" to [].   For nested
           recursion calls, e.g. "ee0" is a function param, and a valid name:
        *)
        let ep =
          if n = 1 && rec_level = 0
          then <:expr< [] >>
          else <:expr< $lid:( Printf.sprintf "%s%d" base ( n-1 ) ) $>>
        in
        (* The method of generating output Flds depends on "sog". NB: "Single"
           and "Option" Flds can only be elementary ones ( that is, "fn" is a
           valid "fix_fld" ctor ), but "Group"s can be made of both Elementary
           and Structured Flds ( the latter are listed in "fix_grp_def" rather
           than in "fix_fld_def" -- then recursion is required ):
        *)
        match sog with
        [
          FD.Single  ->
          do{
            assert( Hashtbl.mem fix_flds_ht fn );
            (* Apply the "fn" ctor to "m.rf" and add it to the list: *)
            let add =
              <:expr< [ FT.$uid:fn$ m.FT.$lid:rf$ :: $ep$ ] >>
            in
            ( n+1, [ add :: curr ] )
          }
        | FD.Option  ->
          do{
            assert( Hashtbl.mem fix_flds_ht fn );
            (* Add the value to the list if not "None":              *)
            let add =
              <:expr<
                match m.FT.$lid:rf$ with
                [ None   -> $ep$
                | Some x -> [ ( FT.$uid:fn$ x ) :: $ep$ ]
                ]
              >>
            in
            ( n+1, [ add :: curr ] )
          }
        | FD.Group    counter
        | FD.OptGroup counter ->
          (* Will generate two exprs -- the nested Flds output and the counter
             output:
          *)
          let empty_ok     = is_empty_grp_ok _loc sog in
          let group_expr   =
            (* Traverse all entries of the corresp array, in the reverse order.
               Elementary entries are installed as Flds, structured ones require
               recursive calls:
            *)
            if not_structured_fld_grp fn
            then
              (* Elementary entries with ctor "fn": *)
              <:expr<
                arr_fold_left_rev $empty_ok$
                  ( fun curr x -> [ ( FT.$uid:fn$ x ) :: curr ] )
                  $ep$
                  m.FT.$lid:rf$
              >>
            else
              (* Structured entries. XXX: don't ask why they are generated
                 exactly this way -- this is simply done by emulating hand-
                 writen code; "fn" is the Grp ctor in this case.   We also
                 construct the "base0" for the NEXT recursion level:
              *)
              let nbase  = String.make  ( rec_level + 2 ) 'e'                in
              let nbase0 = <:patt< $lid:( Printf.sprintf "%s0" nbase )$ >>   in
              let nested = mk_split_lets _loc ( rec_level+1 ) fn             in
              <:expr<
                arr_fold_left_rev $empty_ok$
                  ( fun $nbase0$ m -> $nested$ )
                  $ep$
                  m.FT.$lid:rf$
              >>
          in
          (* Now install the counter. BUT: for "OptGroup"s of 0 length, do not
             install it at all:
          *)
          let en         = <:expr< $lid:( Printf.sprintf "%s%d" base n )$ >> in
          let count_expr =
            match sog with
            [ FD.Group _    ->
              <:expr<
                [ ( FT.$uid:counter$ ( Array.length m.FT.$lid:rf$ ) ) :: $en$ ]
              >>
            | FD.OptGroup _ ->
              <:expr<
                let len = Array.length m.FT.$lid:rf$   in
                if  len = 0
                then $en$
                else [ ( FT.$uid:counter$ len ) :: $en$ ]
              >>
            | _ -> assert False
            ]
          in
          ( n+2, [ count_expr; group_expr :: curr ] )
        ]
    )
    ( 1, [] )
    rflds
  in
  (* Convert "expr_lst" into a chain of "let... in..." exprs: *)
  match expr_lst with
  [
    []          -> failwith( "ERROR: Empty Msg/Grp type: "^ mgtype )
  | [ eh::exs ] ->
      let ( _, lets ) =
        List.fold_left
        (
          fun ( n, curr ) rhs ->
            let lhs = <:patt< $lid:( Printf.sprintf "%s%d" base n )$ >>  in
            ( n-1, <:expr< let $lhs$ = $rhs $ in $curr$              >>  )
        )
        ( ne-2, eh )
        exs
      in
      lets
  ];

(*---------------------------------------------------------------------------*)
(* "mk_split_top":                                                           *)
(*---------------------------------------------------------------------------*)
(* Generates the "fix_msg_to_flds" function:                                 *)

value mk_split_top =
fun _loc        ->
  let fname  = <:patt< $lid:"fix_msg_to_flds"$ >>   in
  let fcases =
    List.map
    (
      fun ( mtype, _, _ ) ->
        let lhs   = <:patt< FT.$uid:mtype$   x >>   in
        let lc    = "split_"^( from_humps mtype )   in
        let rhs   = <:expr< $lid:lc$         x  >>  in
        ( lhs, None (* No guards *), rhs )
    )
    FD.fix_msg_def
  in
  let fbody  = <:expr< fun[ $list:fcases$ ] >>      in
  let fentry = [ ( fname, fbody ) ] in
  <:str_item< value $flag:False$ (* Non-rec *) $list:fentry$ >>;

(*---------------------------------------------------------------------------*)
(* All Split Entries Together:                                               *)
(*---------------------------------------------------------------------------*)
value mk_fix_subtype_splits =
fun _loc ->
try
  let all =
    List.fold_left
    (
      fun curr ( mctor, _, _ ) ->
        let lc_type     = from_humps   mctor                        in
        let split_fname = <:patt< $lid:( "split_"^lc_type )$    >>  in
        let split_lets  = mk_split_lets _loc  0   mctor             in
        let split_cases = [ ( <:patt< m >>, None, split_lets ) ]    in
        let split_body  = <:expr<   fun [ $list:split_cases$ ]  >>  in
        let split_entry = [ ( split_fname, split_body ) ]           in
        let split_value =
            <:str_item< value $flag:False$ (* Non-rec *) $list:split_entry$ >>
        in
        [ split_value :: curr ]
    )
    (* Start with the top-level -- it will come last on the list: *)
    [ mk_split_top   _loc     ]
    ( List.rev FD.fix_msg_def )
  in
  <:str_item< declare $list:all$ end >>

with[ any -> fatal_error any ];

(*===========================================================================*)
(* "Assembler" Functions for Sub-Types:                                      *)
(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "mk_gmsg_fld_type_mut":                                                   *)
(*---------------------------------------------------------------------------*)
(* Similar to "mk_gmsg_fld_type", but for types of mutable record Flds:
   * Enum types do not have distinguishable empty values, so "option" is added
     to them, empty value = "None";
   * Array types are for Grps, empty arrays are in general allowed ( counter
     being 0 ) but they must still be present, so "option" is added  and the
     default empty value is "None":
*)
value mk_gmsg_fld_type_mut  =
fun _loc fn ftype sog       ->
  match sog with
  [
    FD.Single    ->
      match ftype with
      [
        (* Types with distinguishable empty values: *)
        "int"
      | "string"    ->
        <:ctyp< $lid:ftype$ >>

        (* Same but with the "FT" qualifier:        *)
      | "price"
      | "date"
      | "time"
      | "date_time" ->
        <:ctyp< FT.$lid:ftype$ >>

      | "bool"      ->
        (* "bool" requires special treatment, as it has empty value: *)
        <:ctyp< option bool >>

        (* All other types ( e.g. enums ) are wrapped into "option": *)
      | _           ->
        <:ctyp< option FT.$lid:ftype$ >>
      ]
  | FD.Option  ->
    (* "option" with or without the "FT" qualifier: *)
    match ftype with
    [
      "int"
    | "string"
    | "bool"      ->
      <:ctyp< option $lid:ftype$    >>
    | _           ->
      <:ctyp< option FT.$lid:ftype$ >>
    ]
  | FD.Group    _
  | FD.OptGroup _ ->
    (* "option array": if the Grp Entry is a structured type, we must install
       its "_mut" counter-part, otherwise the type is an FT.enum; for the lat-
       ter, there is no default value, hence "option" in array elements:
    *)
    if not_structured_fld_grp fn
    then
      (* "Elementary" Grp entry: *)
      <:ctyp< array ( option FT.$lid:ftype$ ) >>
    else
      (* "Structured" Grp entry: *)
      <:ctyp< array $lid:( ftype^"_mut" )$    >>
  ];

(*---------------------------------------------------------------------------*)
(* "mk_gmsg_fld_empty":                                                      *)
(*---------------------------------------------------------------------------*)
value mk_gmsg_fld_empty =
fun _loc    ftype   sog ->
  match sog with
  [
    FD.Single    ->
      match ftype with
      [
        (* Types with well-defined "empty" values:  *)
        "int"       -> <:expr< FT.n_a_nat       >>
      | "string"    -> <:expr< ""               >>
      | "price"     -> <:expr< FT.n_a_price     >>
      | "date"      -> <:expr< FT.n_a_date      >>
      | "time"      -> <:expr< FT.n_a_time      >>
      | "date_time" -> <:expr< FT.n_a_date_time >>

        (* All other types are wrapped into "option", hence "None":          *)
      | _           -> <:expr< None >>
      ]
  | FD.Option       -> <:expr< None >>
  | FD.Group    _
  | FD.OptGroup _   -> <:expr< [||] >>
  ];

(*---------------------------------------------------------------------------*)
(* "mk_gmsg_fld_val":                                                        *)
(*---------------------------------------------------------------------------*)
(* Wrappinng a non-empty value into a mutable record value:                  *)

value mk_gmsg_fld_val =
fun _loc ftype sog   ->
  match sog with
  [
    FD.Single ->
      match ftype with
      [
        (* For the following types, the value is installed directly. XXX: this
           is different from treatment of OPTIONAL "date"/"time" Flds below:
        *)
        "int"
      | "string"
      | "price"
      | "date"
      | "time"
      | "date_time"   -> <:expr< x      >>
        (* Other types ( e.g. "bool", enums ) are wrapped into "option"s:    *)
      | _             -> <:expr< Some x >>
      ]
  | FD.Option ->
      (* XXX: non-standard OSL FIX extension: "n_a" "x" values are to be
         skipped -- install "None" in those cases:
      *)
      match ftype with
      [
        "int"       ->
          <:expr<  if x = FT.n_a_nat       then None else Some x >>
      | "price"     ->
          <:expr<  if x = FT.n_a_price     then None else Some x >>
      | "date"      ->
          <:expr<  if x = FT.n_a_date      then None else Some x >>
      | "time"      ->
          <:expr<  if x = FT.n_a_time      then None else Some x >>
      | "date_time" ->
          <:expr<  if x = FT.n_a_date_time then None else Some x >>
      | "currency"  ->
          <:expr<  if x = FT.n_a_currency  then None else Some x >>
      | _           ->
          <:expr< Some x >>
      ]
  | FD.Group    _
  | FD.OptGroup _   ->
      (* Here we install an array corresponding to Grp entries ( extra code
         will be required elsewhere to fill it in, of course ):
      *)
      <:expr< es >>
  ];

(*---------------------------------------------------------------------------*)
(* "mk_fix_gmsg_mut":                                                        *)
(*---------------------------------------------------------------------------*)
(* Similar to "mk_fix_gmsg", but for records with mutable Flds. Also generates
   the function returning the empty value of this type, and top-level entries:
*)
value mk_fix_gmsg_mut =
fun  _loc mgctor     ->
  (* Get the Msg /  Grp code and Flds: *)
  let ( c, flds ) = get_fix_gmsg_def mgctor                         in

  (* Create the record type:           *)
  let ( rftypes, empties ) =
    List.fold_left
    (
      fun ( curr_t, curr_e ) ( fn, sog ) ->
        (* Fld name as it will appear in the Msg type record: *)
        let ( rf, ftype ) = get_gmsg_fld_info     c   fn       sog  in

        (* Type of the Fld:                                   *)
        let rftype        = mk_gmsg_fld_type_mut _loc fn ftype sog  in

        (* NB: All Msg Flds are MUTABLE here:                 *)
        let fld_dcl       = ( _loc, rf, True, rftype )              in

        (* Empty value of this Fld:                           *)
        let empty_val     = mk_gmsg_fld_empty    _loc ftype    sog  in
        let fld_empty     = ( <:patt< $lid:rf$ >>, empty_val )      in

        (* Attach the exprs generated:                        *)
        (
          [ fld_dcl   :: curr_t ],
          [ fld_empty :: curr_e ]
        )
    )
    ( [], [] )
    ( List.rev flds )
  in
  let rtype      = <:ctyp< { $list:rftypes$ } >>  in

  (* Create the module-level entry:    *)
  let conv       = from_humps mgctor              in
  let rtype_name = conv ^ "_mut"                  in
  let type_decl  =
  {
    MLast.tdNam  = ( _loc, rtype_name );
    MLast.tdPrm  = [];
    MLast.tdPrv  = False;
    MLast.tdDef  = rtype;
    MLast.tdCon  = []
  }
  in
  let type_entry = <:str_item< type $list:[ type_decl ] $    >>     in

  (* Now generate the function which returns the empty value of this type:   *)

  let efname     = <:patt< $lid:( "empty_"^conv^"_mut" )$    >>     in
  let efbody     = <:expr< fun () -> { $list:empties$ }      >>     in
  let efun       = [ ( efname, efbody ) ]                           in
  let ev_entry   =
    <:str_item< value $flag:False$ (* Non-rec *) $list:efun$ >>     in

  ( type_entry, ev_entry );

(*---------------------------------------------------------------------------*)
(* "mk_copy_flds":                                                           *)
(*---------------------------------------------------------------------------*)
(* Generates initialiser of the immutable record from a mutable one:         *)

value rec mk_copy_flds  =
fun _loc mgctor ->
  (* Get the Msg /  Grp code and Flds: *)
  let ( c, flds ) = get_fix_gmsg_def mgctor                 in
  List.map
  (
    fun ( fn, sog ) ->
      let ( rf, ftype ) = get_gmsg_fld_info c fn sog        in
      let lhs           = <:patt< FT.$lid:rf$ >>            in
      let mut           = <:expr<  m.$lid:rf$ >>            in
      let empty         = mk_gmsg_fld_empty _loc ftype sog  in
      let copy_single   =
        fun emut ->
          match ftype with
          [ "int"
          | "string"
          | "price"
          | "date"
          | "time"
          | "date_time" ->
            <:expr<
              if   $emut$ = $empty$
              then missing_fld mtype FT.$uid:( "T_"^fn )$
              else $emut$
            >>
          | _ ->
            <:expr<
              match $emut$ with
              [ None   -> missing_fld mtype FT.$uid:( "T_"^fn )$
              | Some x -> x
              ]
            >>
          ]
      in
      let rhs           =
        match sog with
        [
          FD.Single           ->
            (* Check whether the mandatory values have been set, by comparing
               them with empty defaults. NB: no need to apply more strict va-
               lidity checks at that point ( e.g. that int >= 0 ), since this
               has already been done when Flds were formed:
            *)
            copy_single mut

        | FD.Option           ->
            (* Just copy the option value as it is: *)
            <:expr< $mut$  >>

        | FD.Group    counter
        | FD.OptGroup counter ->
            (* NB: As Grps do not have the corresp Fld T-types, missing Grp is
               signalled as a missing Counter:
            *)
            let empty_ok = is_empty_grp_ok _loc sog in
            if  not_structured_fld_grp fn
            then
              let copy_e = copy_single <:expr< e >> in
              <:expr<
                if   ( not $empty_ok$ ) && ( Array.length $mut$ = 0 )
                then missing_fld mtype FT.$uid:( "T_"^counter ) $
                else Array.map ( fun e -> $copy_e$ ) $mut$
              >>
            else
              let sub_copy = mk_copy_flds _loc fn in
              <:expr<
                if   ( not $empty_ok$ ) && ( Array.length $mut$ = 0 )
                then missing_fld mtype FT.$uid:( "T_"^counter ) $
                else Array.map ( fun m -> { $list:sub_copy$ } ) $mut$
                (* NB: overriding "m" with a local binding!     *)
              >>
        ]
      in
      ( lhs, rhs )
  )
  flds;

(*---------------------------------------------------------------------------*)
(* "mk_mutables_top":                                                        *)
(*---------------------------------------------------------------------------*)
(* Adds module-level declarations  for mutable versions  of Msg Types and Sub-
   Types to the "curr0" list. Such declarations can be generated "en mass" (as
   opposed to non-mutable record types which are generated individually) since
   they do not require "TyWith" extensions:
*)
value mk_mutables_top =
fun _loc curr0 ->
  (* FIX Msgs:       *)
  let curr1 =
    List.fold_left
      ( fun curr ( mgctor, _, _ ) ->
          let (  type_entry, ev_entry )  = mk_fix_gmsg_mut _loc mgctor in
          [ type_entry; ev_entry :: curr ]
      )
      curr0
      ( List.rev FD.fix_msg_def )
  in
  (* FIX Fld Grps in the order given in "fix_defs.ml"  -- the author of the
     latter is responsible for the correct order. They will eventually come
     BEFORE FIX Msgs, as the latter use FIX Fld Grps:
  *)
  let curr2 =
    List.fold_left
      ( fun curr ( mgctor, _, _ )    ->
          let (  type_entry, ev_entry )  = mk_fix_gmsg_mut _loc mgctor in
          [ type_entry; ev_entry :: curr ]
      )
      curr1
      ( List.rev FD.fix_grp_def )
  in
  curr2;

(*===========================================================================*)
(* "mk_asm_func":                                                            *)
(*===========================================================================*)
(* Assembler functions generation for a single Msg / Grp type. NB: the static
   "mgctor" arg is used to generate the code  for the corresp "asm" function,
   but there is also a dynamically-passed "mgctor" arg to the generated func.
   The dynamic arg is passed down to funcs assembling Fld Grps, so they would
   know the top-level type they are working on:
*)
value mk_asm_func =
fun _loc in_group mgctor ->
  let ( c, flds ) = get_fix_gmsg_def mgctor in
  (*-------------------------------------------------------------------------*)
  (* Fld-Fetching Cases:                                                     *)
  (*-------------------------------------------------------------------------*)
  let fcases0     =
    List.map
    (
      fun ( ( fn, sog ) as fld ) ->
        let ( rf, ftype ) = get_gmsg_fld_info c fn sog       in
        (* The mutable value to be assigned from this Fld is INDEXED if we are
           within a Fld Grp:
        *)
        let rexpr =
          if in_group
          then <:expr< es.( i ).$lid:rf$ >>
          else <:expr< m.$lid:rf$        >>
        in
        (* The match pattern: for Grps, we need to match the Counter!        *)
        let lhs           =
          match sog with
          [
            FD.Single
          | FD.Option           -> <:patt< FT.$uid:fn$      x >>
          | FD.Group    counter
          | FD.OptGroup counter -> <:patt< FT.$uid:counter$ n >>
          ]
        in
        (* The empty value of this record Fld: *)
        let empty         = mk_gmsg_fld_empty _loc ftype sog in
        (* The "x"-value   of this record Fld: *)
        let xval          = mk_gmsg_fld_val   _loc ftype sog in
        (*-------------------------------------------------------------------*)
        (* The rest depends on "sog":                                        *)
        (*-------------------------------------------------------------------*)
        let rhs           =
          match sog with
          [
            FD.Single
          | FD.Option  ->
              (* Assign "xval" if this Fld was not already encountered. Other-
                 wise, it is normally a "Repeated" fld error, EXCEPT for a 1st
                 Fld in a Grp -- this simply means that the curr entry is done
                 ( "Stop" ):
              *)
              let else_case =
                if in_group && ( fld = List.hd flds )
                then <:expr< Stop     >>
                else <:expr< Repeated >>
              in
              <:expr<
                if       $rexpr$  = $empty$
                then do{ $rexpr$ := $xval$; Continue fs }
                else $else_case$
              >>
          | FD.Group    _
          | FD.OptGroup _ ->
              (* Here the matter is more complex.   Recall that in this case we
                 match the COUNTER from "f",  and "n" is the numbet of subsequ-
                 ent Grp Entries ( elementary | structured ) to be matched from
                 the remaining "fs" Flds.
                 NB: At this moment, we do not check whether the "Grp"  is non-
                 empty : this is done when  it is stored back in the immutable
                 record:
              *)
              if  not_structured_fld_grp fn
              then
                (*-----------------------------------------------------------*)
                (* "Elementary" Grp Entries:                                 *)
                (*-----------------------------------------------------------*)
                (* The ctor for each entry is "fn". Once the Grp has been enco-
                   untered, the entries are mandatory, ie "Single":
                *)
                let ector  = <:patt< FT.$uid:fn$ >>                 in
                let eempty = mk_gmsg_fld_empty _loc ftype FD.Single in
                let exval  = mk_gmsg_fld_val   _loc ftype FD.Single in
                <:expr<
                  if  $rexpr$ = $empty$
                  then
                    (* Fetch the next "n" list Flds and store them in "es":  *)
                    let es    = Array.init n ( fun _ -> $eempty$ )  in

                    let rec store_entries =
                    fun  i fs0 ->
                      if i >= n
                      then fs0 (* All done, return the remains of the list   *)
                      else
                        match fs0 with
                        [ [ f::fs1 ]    ->
                            match f with
                            [ $ector$ x ->
                              do{
                                es.( i ) := $exval$;
                                store_entries ( i+1 ) fs1   (* Tail-rec call *)
                              }
                            | _ -> improper_fld  mtype f (* Unexpected Fld   *)
                            ]
                          | []  -> short_fld_lst mtype   (* Cannot fetch "i" *)
                        ]
                    in
                    (* Apply this func: *)
                    let fs' = store_entries 0 fs in
                    do{
                      $rexpr$ := es;
                      Continue   fs'
                    }
                  else Repeated
                >>
              else
                (*-----------------------------------------------------------*)
                (* "Structured" Grp Entries:                                 *)
                (*-----------------------------------------------------------*)
                (* Here we call an external func rather than a local closure in
                   order to fill them in:
                *)
                let conv        = from_humps fn                             in
                let fempty      = <:expr< $lid:( "empty_"^conv^"_mut" )$ >> in
                let asm_grp     = <:expr< $lid:( "asm_"  ^conv        )$ >> in
                <:expr<
                  if    $rexpr$ = $empty$
                  then
                    let es  = Array.init n ( fun _ -> $fempty$ () )         in
                    (* Call the generated function for the Fld Grp: *)
                    let fs' = $asm_grp$  mtype es fs 0 n                    in
                    do{
                      $rexpr$ := es;
                      Continue   fs'
                    }
                  else Repeated
                >>
          ]
        in
        ( lhs, None (* No guards! *), rhs )
    )
    flds
  in
  (* Now append the catch-all case. Normally, this is an error  ( "Improper"
     fld, except for the last entry in a Grp -- in that case the Grp is done
     ( "Stop" ):
  *)
  let catch_all =
    if in_group
    then <:expr< if i = n-1 then Stop else Improper >>
    else <:expr< Improper >>
  in
  let fcases1 = fcases0 @ [ ( <:patt< _ >>, None, catch_all ) ] in

  (*-------------------------------------------------------------------------*)
  (* The Asm Func:                                                           *)
  (*-------------------------------------------------------------------------*)
  let conv    = from_humps mgctor  in
  let fname   = "asm_"^ conv       in
  let fbody   =
    if not in_group
    then
      (*---------------------------------------------------------------------*)
      (* Top-level Msg type:                                                 *)
      (*---------------------------------------------------------------------*)
      let copy_flds = mk_copy_flds _loc mgctor             in
      <:expr<
        fun mtype flds ->
          (* Start with an empty mutable record: *)
          let m     = $lid:( "empty_"^conv^"_mut" )$ ()    in

          (* Fill in the "m" record:             *)
          let res =
            apply_flds mtype
              ( fun f fs -> match f with [ $list:fcases1$ ] )
              flds
          in
          do{
            (* At the end, there must be no unused Flds:            *)
            assert( res = [] );
            (* Set the Flds of the immutable record to be returned: *)
            { $list:copy_flds$ }
          }
      >>
    else
      (*---------------------------------------------------------------------*)
      (* A Fld Grp type:                                                     *)
      (*---------------------------------------------------------------------*)
      (* There is no "M_"-type for a Fld Grp, so use the enclosing Msg Type: *)
      <:expr<
        fun  mtype es fs0 i n ->
          if i >= n
          then fs0 (* All Grp Entries done  *)
          else
          if   fs0  = []
          then
            (* Empty src list -- will not be able to fetch the "i"th entry:  *)
            short_fld_lst mtype
          else
          let fs1 =
            (* Process the "i"th Grp Entry: *)
            apply_flds    mtype
              ( fun f fs -> match f with [ $list:fcases1$ ] )
              fs0
          in
          (* To the next Grp Entry:         *)
          $lid:fname$ mtype es fs1 ( i+1 ) n

          (* NB: for Grp Entries, we not not need to fill the resulting non-mut
            record, so nothing more to do here!
          *)
      >>
  in
  (*-------------------------------------------------------------------------*)
  (* Top-Level Entry:                                                        *)
  (*-------------------------------------------------------------------------*)
  let asm_entry     = [ ( <:patt< $lid:fname$ >>, fbody ) ]       in
  if  not in_group
  then
    <:str_item< value $flag:False$ (* Non-rec *) $list:asm_entry$ >>
  else
    (* For Grps, the asm funcs are recursive: *)
    <:str_item< value $flag:True$  (* Rec!    *) $list:asm_entry$ >>;

(*===========================================================================*)
(*---------------------------------------------------------------------------*)
(* "mk_asm_funcs":                                                           *)
(*---------------------------------------------------------------------------*)
(* Attach asm func entries to the "curr0" lst. Similar to "mk_mutables_top": *)

value mk_asm_funcs =
fun _loc curr0    ->
  (* FIX Msgs:       *)
  let curr1 =
    List.fold_left
      ( fun curr ( mgctor, _, _ ) ->
          [ ( mk_asm_func _loc False mgctor ) :: curr ]
      )
      curr0
      ( List.rev FD.fix_msg_def )
  in
  (* FIX Fld Grps:   *)
  let curr2 =
    List.fold_left
      ( fun curr ( mgctor, _, _ ) ->
          [ ( mk_asm_func _loc True  mgctor ) :: curr ]
      )
      curr1
      ( List.rev FD.fix_grp_def )
  in
  curr2;

(*---------------------------------------------------------------------------*)
(* "mk_asm_top":                                                             *)
(*---------------------------------------------------------------------------*)
(* Generates the "fix_msg_of_flds" function:                                 *)

value mk_asm_top =
fun _loc        ->
  let fname  = <:patt< $lid:"fix_msg_of_flds"$ >>    in
  let fcases =
    List.map
    (
      fun ( mtype, _, _ ) ->
        let lhs   = <:patt< FT.$uid:( "M_"^mtype )$ >> in
        let lc    = "asm_"^( from_humps mtype )        in
        let rhs   = <:expr< FT.$uid:mtype$ ( $lid:lc$ mtype flds ) >> in
        ( lhs, None (* No guards *), rhs )
    )
    FD.fix_msg_def
  in
  let fbody  = <:expr< fun mtype flds -> match mtype with [ $list:fcases$ ] >>
  in
  let fentry = [ ( fname, fbody ) ] in
  <:str_item< value $flag:False$ (* Non-rec *) $list:fentry$ >>;

(*---------------------------------------------------------------------------*)
(* Assembler Entries All Together:                                           *)
(*---------------------------------------------------------------------------*)
value mk_fix_subtype_asms =
fun _loc ->
try
  (* Generate the top-level "fix_msg_of_flds":  *)
  let lst0  = [ mk_asm_top    _loc ]         in

  (* Generate all asm functions:                *)
  let lst1  = mk_asm_funcs    _loc lst0      in

  (* Generate all mutable types:                *)
  let lst2  = mk_mutables_top _loc lst1      in


  (* All entries together:                      *)
  <:str_item< declare $list:lst2$ end >>

with[ any  -> fatal_error any ];

(*===========================================================================*)
(* Attach the Extensions:                                                    *)
(*===========================================================================*)
(* NB: Record types for FIX Msgs and FIX Fld Grps are constructed by the same
   function ( "mk_fix_gmsg" ):
*)
EXTEND
  Pcaml.ctyp:
  [
    (* Generation of 0-Ctor Enums:                                           *)
    [ "FIX_ENUM_TYPE"; tn = STRING -> mk_fix_enum_type            _loc tn ]

    (* Generation of FIX Fld Grp types:                                      *)
  | [ "FIX_FLD_GROUP"; gn = STRING -> mk_fix_gmsg                 _loc gn ]

    (* Generation of FIX Fld Msg   sub-types:                                *)
  | [ "FIX_MSG";       m  = STRING -> mk_fix_gmsg                 _loc m  ]

    (* Generation of top-level FIX Fld and Fix Msg sum types:                *)
  | [ "FIX_TOP";       t  = STRING -> mk_fix_top                  _loc t  ]
  ];

  Pcaml.expr:
  [
    (* Map "fix_fld => fix_fld_type" or "fix_msg" => "fix_msg_type":         *)
    [ "FIX_PROJ";      tm = STRING -> mk_fix_proj                 _loc tm ]

    (* Core of the top-level FIX Fld Encoder:                                *)
  | [ "FIX_FLD_ENCODER"            -> mk_fix_fld_encoder          _loc    ]

    (* Core of the top-level FIX Fld Decoder:                                *)
  | [ "FIX_FLD_DECODER"            -> mk_fix_fld_decoder          _loc    ]
  ];

  Pcaml.str_item:
  [
    (* Fld Sub-Type Codecs:                                                  *)
    [ "FIX_FLD_SUBTYPE_CODECS"     -> mk_fix_fld_subtype_codecs   _loc    ]

    (* Sub-Types Splitting / Assembly from Flds:                             *)
  | [ "FIX_SUBTYPES_SPLIT"         -> mk_fix_subtype_splits       _loc    ]
  | [ "FIX_SUBTYPES_ASM"           -> mk_fix_subtype_asms         _loc    ]
  ];
END;
