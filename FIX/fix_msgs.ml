(* vim:ts=2:syntax=ocaml
*)
module FT  = Fix_types;
module FF  = Fix_flds;

open ExtString;

(*===========================================================================*)
(* Stringification ( Encoding ) of FIX Msgs:                                 *)
(*===========================================================================*)
(*-----------------------------------------*)
(* "get_fix_msg_type":                     *)
(*-----------------------------------------*)
(* Returns the type of each FIX msg:       *)

value get_fix_msg_type:  FT.fix_msg -> FT.fix_msg_type = FIX_PROJ "M_";

(*-----------------------------------------*)
(* "get_date_time":                        *)
(*-----------------------------------------*)
(* Returns the FIX-stringified form of the curr date and time. Time correction
   can be applied to adjust the client time to the server time:
*)
value get_date_time: float -> FT.date_time  =
fun corr ->
  let clock = Unix.gettimeofday () +. corr  in
  let frac  = clock -. ( floor clock )      in
  let tm    = Unix.gmtime ( clock )         in
  let date  =
    ( tm.Unix.tm_year + 1900, tm.Unix.tm_mon + 1, tm.Unix.tm_mday )
  in
  let time  =
    ( tm.Unix.tm_hour,        tm.Unix.tm_min,
      float_of_int tm.Unix.tm_sec +. frac   )
  in
  ( date, time );

(*-----------------------------------------*)
(* "msg_check_sum":                        *)
(*-----------------------------------------*)
value msg_check_sum: string -> int -> int -> int =
fun buff from len ->
  let csr  = ref  0         in
  let last = from + len - 1 in
  do{
    for i = from to last
    do{
      csr.val := csr.val + ( Char.code buff.[ i ] )
    };
    csr.val mod 256
  };

(*-----------------------------------------*)
(* "string_of_exn":                        *)
(*-----------------------------------------*)
value string_of_exn: exn -> string =
fun any ->
  let s  =  Printexc.to_string any    in
  let bt =  Printexc.get_backtrace () in
  if  bt <> ""
  then  s^"\n"^bt
  else  s;
 
(*=========================================*)
(* "string__of_fix_msg":                   *)
(*=========================================*)
(* Stringification of a Msg -- getting it ready for sending: *)

value string_of_fix_msg:
  ~fix_version:    string   -> 
  ~our_comp_id:    string   ->
  ~server_comp_id: string   ->
  ~msg_seq_num:    int      ->
  ~dup_flag:       bool     ->
  ~time_corr:      float    ->
  ~buff:           Buffer.t ->
  FT.fix_msg                ->
  string                    =

fun ~{ fix_version } ~{ our_comp_id } ~{ server_comp_id } ~{ msg_seq_num }
    ~{ dup_flag }    ~{ time_corr}    ~{ buff }      msg  ->

  (* Check the args:                  *)
  if String.length fix_version <> 7 || String.sub fix_version 0 4 <> "FIX."
  then
    failwith( "Invalid FIX ver: "^fix_version )
  else
  if msg_seq_num <= 0
  then
    failwith( Printf.sprintf "Invalid SeqNum: %d" msg_seq_num )
  else
  (* Get the msg type and flds:       *)
  let mtype = get_fix_msg_type   msg in
  let flds  = FF.fix_msg_to_flds msg in

  (* Output buffer. Most of the FIX msgs are quite small. We will limit the
     size of the outbound msgs ( excl. flds 8,9,10 ) by 999 bytes anyway,
     so the following buffer is appropriate:
  *)
  do{
    (* Standard FIX header, with the msg length ( LLL ) still unknown:    *)
    Buffer.clear   buff;
    Array.iter
      ( FF.fix_fld_to_buffer buff )
      [|
        FT.BeginString  fix_version;
        FT.BodyLength   999;         (* Placeholder, will be over-written *)
        FT.MsgType      mtype;
        FT.SenderCompID our_comp_id;
        FT.TargetCompID server_comp_id;
        FT.SeqNum       msg_seq_num;
        FT.SendingTime  ( get_date_time  time_corr )
      |];
    (* The duplicate flag if set:     *)
    if dup_flag
    then FF.fix_fld_to_buffer buff ( FT.PossDupFlag True )
    else ();

    (* Now output appl-level fields:  *)
    List.iter ( FF.fix_fld_to_buffer buff ) flds;

    (* Placeholder for the check-sum: *)
    FF.fix_fld_to_buffer buff ( FT.CheckSum 999 );

    (* Extract the raw msg and fill in the BodyLength and CheckSum flds: *)
    let str_msg = Buffer.contents buff        in

    (* NB: "body_len" is the msg length w/o the flds "BeginString", "BodyLength"
       ( prefix ) and "CheckSum" ( postfix ). It must not be greater than 999 in
       the current impl.  XXX: In the following calculations, we have to use the
       lengths of the standard tags:
    *)
    let blen_off = String.find  str_msg "999" in
    let len8910  = blen_off + 11              in
    let body_len = ( String.length str_msg ) - len8910        in
    if  body_len < 0 || body_len > 999
    then
       failwith( Printf.sprintf "Invalid msg body len: %d" body_len )
    else
    let blen_str = Printf.sprintf "%03d" body_len             in
    do{
      (* Install the actual "body_len" in place of "999": *)
      str_msg.[ blen_off     ]  := blen_str.[ 0 ];
      str_msg.[ blen_off + 1 ]  := blen_str.[ 1 ];
      str_msg.[ blen_off + 2 ]  := blen_str.[ 2 ];

      (* We can now compute the check-sum from msg start until the check-sum
         fld iteself:
      *)
      let cs     = msg_check_sum  str_msg 0 ( body_len + 16 ) in
      let cs_str = Printf.sprintf "%03d"  cs                  in
      let cs_off = blen_off + body_len + 7                    in
      do{
        assert( ( String.sub str_msg cs_off 3 ) = "999" );
        str_msg.[ cs_off     ]  := cs_str.[  0 ];
        str_msg.[ cs_off + 1 ]  := cs_str.[  1 ];
        str_msg.[ cs_off + 2 ]  := cs_str.[  2 ]
      };

      (* The result:  *)
      str_msg
    }
  };

(*-----------------------------------------*)
(*  "raw_fix_msg_to_print":                *)
(*-----------------------------------------*)
(*  Make a raw FIX msg suitable for logging or printing -- replace unplrintable
    <SOH> fld separators:
*)
value raw_fix_msg_to_print: string -> int -> int -> string =
fun buff m n ->
do{
  assert( m >= 0 && n >= m && ( n <= String.length buff ) );

  let len = n - m in
  let s   = String.sub buff m len in
  do{
    for i = 0 to ( len-1 )
    do{
      if   s.[ i ]  = '\x01'
      then s.[ i ] := '|'
      else ()
    };
    s
  }
};

(*===========================================================================*)
(* Parsing of FIX Msgs:                                                      *)
(*===========================================================================*)
(*-----------------------------------------*)
(* Current state of the FIX Msg Parser:    *)
(*-----------------------------------------*)
type fix_parser_state =
{
  (* Consts to verify the std hdrs:        *)
  fix_version   : string;
  our_comp_id   : string;
  server_comp_id: string;

  (* Buffer positions:      *)
  from_pos:       int;                      (* Initial      parsing pos      *)
  chunk_end:      int;                      (* "The beyond" parsing pos      *)
  curr_pos:       mutable int;              (* Current parsing pos           *)
  cs_pos:         mutable int;              (* Location of the next CheckSum *)
  next_msg_pos:   mutable int;              (* Beginning ofthe next msg      *)

  (* Important msg flds:    *)
  seq_num:  mutable int;                    (* SeqNum of this Msg            *)
  last_ok_seq_num:
            mutable int;                    (* Last Msg SeqNum Processed     *)
  dup_flag: mutable bool;                   (* Was DupFlag set for this msg? *)
  mtype:    mutable option FT.fix_msg_type; (* MsgType of this msg, if known *)
  ftype:    mutable option FT.fix_fld_type; (* Curr fld type,       if known *)
  fval:     mutable string;                 (* Value of the curr fld,  ditto *)
  flds:     mutable list   FT.fix_fld;      (* All non-hdr flds: accumulator *)
  stdh_ok:  Hashtbl.t      FT.fix_fld_type  ( bool * bool )
                                            (* ( Compuls,Encounter) std hdrs *)
};

(*-----------------------------------------*)
(* FIX Standard Header Flds:               *)
(*-----------------------------------------*)
(* These are precisely the flds for which "FT.fix_fld_type" ctors exist,  BUT
   there are no corresponding "FT.fix_fld" ctors. This is because the vals of
   such flds are given special treatment in the msg parsing process  --  they
   affect the "fix_parser_state". Most of them are compulsory, some are not:
*)
value std_hdr_flds: array ( FT.fix_fld_type * bool ) =
[|
  ( FT.T_BeginString,             True  );
  ( FT.T_BodyLength,              True  );
  ( FT.T_CheckSum,                True  );
  ( FT.T_SeqNum,                  True  );
  ( FT.T_MsgType,                 True  );
  ( FT.T_SenderCompID,            True  );
  ( FT.T_SendingTime,             True  );
  ( FT.T_TargetCompID,            True  );
  ( FT.T_PossDupFlag,             False );
  ( FT.T_LastMsgSeqNumProcessed,  False );
  ( FT.T_OrigSendingTime,         False )
|];

(*-----------------------------------------*)
(* "init_parser_state":                    *)
(*-----------------------------------------*)
(* NB: "from" is the initial parser pos; "stdh_ok" holds flags for already-en-
   countered std hdr flds listed above :
*)
value init_parser_state:
  ~fix_version:    string -> 
  ~our_comp_id:    string ->
  ~server_comp_id: string ->
  ~from:           int    ->
  ~chunk_end:      int    ->
  fix_parser_state        =

fun ~{ fix_version } ~{ our_comp_id } ~{ server_comp_id } ~{ from }
    ~{ chunk_end }        ->
{
  fix_version     = fix_version;
  our_comp_id     = our_comp_id;
  server_comp_id  = server_comp_id;
  from_pos        = from;
  chunk_end       = chunk_end;
  curr_pos        = -1; (* XXX: or "from" *)
  cs_pos          = -1;
  next_msg_pos    = -1;
  seq_num         = -1;
  last_ok_seq_num = -1;
  dup_flag        = False;
  mtype           = None;
  ftype           = None;
  fval            = "";
  flds            = [];
  stdh_ok         = 
    (* Initially all "encountered" flags are "off":       *)
    let hash = Hashtbl.create ( Array.length std_hdr_flds ) in
    do{
      (* FldType => ( ComplulsoryFlag, EncounteredFlag ): *)
      Array.iter
        ( fun ( f, comp ) -> Hashtbl.add hash f ( comp, False ) ) std_hdr_flds;
      hash
    }
};

(*-----------------------------------------*)
(* "invalid_fix_msg":                      *)
(*-----------------------------------------*)
value invalid_fix_msg:
  fix_parser_state -> option FT.session_rej_reason -> string -> 'a =

fun st reason err_msg ->
  let err =
  {
    FT.err_seq_num      = st.seq_num;
    FT.err_msg_type     = st.mtype;
    FT.err_fld_type     = st.ftype;
    FT.err_fld_val      = st.fval;
    FT.err_text         = err_msg;
    FT.err_reason       = reason;
    FT.err_next_msg_pos = st.next_msg_pos;
    FT.err_off          =
      (*  NB: if "curr_pos" is invalid, do not subtract "ftom" from it: *)
      if   st.curr_pos >= st.from_pos
      then st.curr_pos -  st.from_pos
      else -1
  }
  in raise( FT.Invalid_FIX_Msg err );

(*-----------------------------------------*)
(* "encountered_stdh_fld":                 *)
(*-----------------------------------------*)
(* What to do when an std hdr fld is encountered during parsing. Updates the
   parser state:
*)
value encountered_stdh_fld:
  ?must:( option FT.fix_fld_type ) -> fix_parser_state -> FT.fix_fld_type  ->
  ( unit -> unit )                 -> unit             =

fun  ?{ must  = None } st f action ->
  match must with
  [
    Some mf when mf <> f ->
      (* Encountered a different fld than expected:     *)
      let smust  = FT.string_of_fix_fld_type  mf        in
      invalid_fix_msg st ( Some FT.RequiredTagMissing ) smust

  | _ ->
      (* All other cases: "must" not set or is correct: *)
      do{
        assert( Hashtbl.mem st.stdh_ok f );
        let ( comp, enc ) = Hashtbl.find st.stdh_ok  f  in
        if    enc
        then
          let srep =  FT.string_of_fix_fld_type  f   in
          invalid_fix_msg st ( Some FT.ImproperTag ) ( "RepeatedTag="^srep )
        else
        do{
          (* Mark this fld as "encountered" and run the "action" on it: *)
          Hashtbl.replace st.stdh_ok f ( comp, True );
          action ()
        }
      }
  ];

(*-----------------------------------------*)
(* "check_stdh_flds":                      *)
(*-----------------------------------------*)
(* Verifying that all required std hdr flds were indeed present:     *)

value check_stdh_flds: fix_parser_state -> unit  =
fun st ->
  Hashtbl.iter
  (
    fun f ( comp, enc ) ->
      if    comp && ( not enc )   (* Compulsory, but not Encountered *)
      then
        let  smust   =  FT.string_of_fix_fld_type   f  in
        invalid_fix_msg st ( Some FT.RequiredTagMissing ) smust
      else ()
  )
  st.stdh_ok;

(*=========================================*)
(*  "parse_msg":                           *)
(*=========================================*)
(* "n"   is the 0-base number of the curr field  to be parsed out;
   "pos" is the parsing offset within the "buff".
   Returns the "pos" of the next msg, or raises "Partial_FIX_Msg".

   This function provides specific treatment for std hdr flds, and
   accumulates all "application-level" flds in the "st",  which is
   updated IN-PLACE:
*)
value rec parse_msg: fix_parser_state -> string -> int -> int -> int  =
fun st buff n pos ->
do{
  st.curr_pos := pos;

  if  st.next_msg_pos > 0 (* valid! *) && pos >= st.next_msg_pos
  then
    (*------------------*
       TERMINATION COND:
     *------------------*
      Actually, "pos" must get exactly on "next_msg_pos" if all length
      arithmetic is correct:
    *)
    if pos > st.next_msg_pos
    then
      invalid_fix_msg
        st None
        ( Printf.sprintf
           "Mismatching total msg len: expected = %d, actual >= %d"
           ( st.next_msg_pos - st.from_pos ) ( pos - st.from_pos )
        )
    else
    do{
      check_stdh_flds st; (* Check that all required std hdrs were resent *)
      assert( pos = st.next_msg_pos );
      pos                 (* All done, exiting with the next msg pos      *)
    }
  else
  do{
    (*------------------*
       GENERIC CASE:
     *------------------*
       Get and process the next fld. First, re-set fld-specific vars so that
       their old values do not confuse the exception handler if an exception
       is raised in "FF.parse_fix_fld" below:
    *)
    st.ftype := None;
    st.fval  := "";

    let ( f, fval, next_fld_pos ) = FF.parse_fix_fld buff pos st.chunk_end  in
    do{
      (* Memoise these vals for the purpose of exception handling: *)
      st.ftype := Some f;
      st.fval  := fval;

      (* The first  3 flds are pre-defined: *)

      (*------------------*)
      if n = 0
      (*------------------*)
      then  (* Expected: "BeginString":     *)
        encountered_stdh_fld ~{ must=( Some FT.T_BeginString ) } st f
        (
          fun () ->
            if   fval <> st.fix_version
            then invalid_fix_msg st None fval
            else ()
        )
      else
      (*------------------*)
      if n = 1
      (*------------------*)
      then  (* Expected: "BodyLength":      *)
        encountered_stdh_fld ~{must=( Some FT.T_BodyLength  ) } st f
        (
          fun () ->
            (* "BodyLength" is equal to the msg length from the next fld until
               the check-sum,  so memoise the check-sum position.  The check-
               sum itself is 7 bytes long:
            *)
            let body_len  =
              try int_of_string fval
              with[ _ -> invalid_fix_msg st( Some FT.ValueFormatError )fval ]
            in
            if   body_len <= 0
            then invalid_fix_msg st ( Some FT.ValueRangeError  ) fval
            else
            do{
              st.cs_pos       := next_fld_pos + body_len;
              st.next_msg_pos := st.cs_pos    + 7;

              if st.next_msg_pos  > st.chunk_end
              then raise FF.Partial_FIX_Msg
              else
              (* Verify that at "cs_pos", we indeed have a check-sum fld --
                 this will confirm that,  even if errors in msg flds are
                 encountered, the over-all length is correct:
              *)
              if ( String.sub buff st.cs_pos 3) <> "10=" ||
                 buff.[ st.next_msg_pos - 1   ] <> '\x01'
              then
                invalid_fix_msg st None
                                ( Printf.sprintf "No CheckSum found @off=%d"
                                ( st.cs_pos - st.from_pos ) )
              else ()
            }
        )
      else
      (*------------------*)
      if n = 2
      (*------------------*)
      then  (* Expected: "MsgType":         *)
        encountered_stdh_fld ~{ must=( Some FT.T_MsgType ) } st f
        (
          fun () ->
            try st.mtype  :=  Some ( FF.fix_msg_type_of_string fval )
            with[ _  -> invalid_fix_msg st ( Some FT.InvalidMsgType ) fval ]
        )
      else
      (* All other flds are position-independent ( can come in any relative
         order ), except the CheckSum which must be the last fld:
      *)
      match f with
      [
        (*------------------*)
        FT.T_SeqNum
      | FT.T_LastMsgSeqNumProcessed ->
        (*------------------*)
        encountered_stdh_fld st f
        (
          fun () -> (* SeqNums must be >= 1: *)
            let sn  =
              try int_of_string fval
              with[ _ -> invalid_fix_msg st ( Some FT.ValueFormatError ) fval ]
            in
            if   sn <= 0
            then invalid_fix_msg st ( Some FT.ValueRangeError ) fval
            else
            if   f = FT.T_SeqNum
            then st.seq_num         := sn
            else st.last_ok_seq_num := sn
        )
        (*------------------*)
      | FT.T_OrigSendingTime ->
        (*------------------*)
          encountered_stdh_fld st f ( fun () -> () )    (* Currently ignored *)

        (*------------------*)
      | FT.T_PossDupFlag  ->
        (*------------------*)
        encountered_stdh_fld st f
        (
          fun () ->
            match fval with
            [ "Y" | "y" -> st.dup_flag := True
            | "N" | "n" -> st.dup_flag := False
            | _         ->
              invalid_fix_msg st ( Some FT.ValueFormatError ) fval
            ]
        )
        (*------------------*)
      | FT.T_SenderCompID ->
        (*------------------*)
        encountered_stdh_fld st f
        (
          fun () -> (* Must be equal to the *server* CompID: *)
            if fval <> st.server_comp_id
            then invalid_fix_msg st ( Some FT.CompIDError  )
                                    ( "SenderCompID="^fval )
            else ()
        )
        (*------------------*)
      | FT.T_TargetCompID ->
        (*------------------*)
        encountered_stdh_fld st f
        (
          fun () -> (* Must be equal to *our* CompID: *)
            if fval <> st.our_comp_id
            then invalid_fix_msg st ( Some FT.CompIDError  )
                                    ( "TargetCompID="^fval )
            else ()
        )
        (*------------------*)
      | FT.T_SendingTime  ->
        (*------------------*)
        encountered_stdh_fld st f
        (
          fun () ->    (* XXX: Parsed but not semantically checked: *)
            try ignore ( FF.date_time_of_string fval )
            with[ _ -> invalid_fix_msg st ( Some FT.SendingTimeError ) fval ]
        )
        (*------------------*)
      | FT.T_CheckSum ->
        (*------------------*)
        (* It may only occur at a known pos at the msg end: *)
        if pos = st.cs_pos
        then
        do{
          (* By construction, the following conditions must hold: *)
            assert( n >= 3                                  &&
                    st.from_pos     <  pos                  &&
                    pos              = st.next_msg_pos - 7  &&
                    st.next_msg_pos <= st.chunk_end );

            encountered_stdh_fld st f
            (
              fun () ->
                let cs  = msg_check_sum    buff          st.from_pos
                                           ( st.cs_pos - st.from_pos )  in
                let fcs =
                  try int_of_string fval
                  with[ _ ->
                        invalid_fix_msg st ( Some FT.ValueFormatError ) fval ]
                in
                if  cs <> fcs
                then
                  invalid_fix_msg
                     st None
                     ( Printf.sprintf
                       "Invalid CheckSum: %s, must be %d" fval cs
                     )
                else ()
            )
          }
          else
            (* Premature of misplaced "CheckSum" *)
            invalid_fix_msg
              st None
              ( Printf.sprintf
                  "Unexpected CheckSum @off=%d; should be @off=%d" pos st.cs_pos
              )
        (*------------------*)
      | _ ->
        (*------------------*)
        (* Any other flds are "application-level" ones; they are assembled
           and accumulated ( IN THE REVERSE ORDER ) in the "flds" list:
        *)
        let fld  = FF.fix_fld_of_string f fval  in
        st.flds := [ fld :: st.flds ]
      ]
    };

    (* Next field -- tail recursion:       *)
    parse_msg st buff ( n+1 ) next_fld_pos
  }
};

(*=========================================*)
(* "fix_msg_of_string":                    *)
(*=========================================*)
type fix_msg_etc =
{
  msg         : FT.fix_msg;
  msg_seq_num : int;
  msg_last_ok : int;
  msg_dup_flag: bool;
  msg_next    : int
};

(* Top-Level Parsing of a FIX Msg. The args include the from-pos in the msg
   buffer, since FIX runs over TCP and FIX msgs are not aligned at datagram
   boundaries:
*)
value fix_msg_of_string:
  ~fix_version:    string ->
  ~our_comp_id:    string ->
  ~server_comp_id: string ->
  ~debug:          bool   ->
  string                  ->
  int                     ->
  int                     ->
  fix_msg_etc             =

fun ~{ fix_version } ~{ our_comp_id } ~{ server_comp_id } ~{ debug }  buff
  from   chunk_end        ->

  (* Create the initial parser state:      *)
  let st = init_parser_state
           ~{ fix_version    = fix_version    }
           ~{ our_comp_id    = our_comp_id    }
           ~{ server_comp_id = server_comp_id }
           ~{ from           = from           }
           ~{ chunk_end      = chunk_end      }
  in
  try
  do{
    (* Verify the "buff" consistency. In particular, there must be at least
       1 byte actually to parse:
    *)
    assert( 0 <= from && from < chunk_end && chunk_end <= String.length buff );

    (* Do the actual parsing:              *)
    let next_msg_pos = parse_msg st buff 0 from in
    do{
      assert( st.next_msg_pos = next_msg_pos && st.seq_num >= 1 );

      (* Reverse the flds accumulated:     *)
      st.flds := List.rev st.flds;

      (* Re-set some vars so that they would not confuse the exception handler
         if an error is encountered during fld assembling:
      *)
      st.curr_pos := -1;
      st.ftype    := None;
      st.fval     := "";

      (* "mtype" must be set by now:       *)
      let mtype =
        match  st.mtype  with
        [ Some mtype' -> mtype'
        | None        -> assert False
        ]
      in
      (* Assemble the msg according to its type: *)
      let msg = FF.fix_msg_of_flds  mtype st.flds    in

      (* The final result:     *)
      {
        msg          = msg;
        msg_seq_num  = st.seq_num;
        msg_last_ok  = st.last_ok_seq_num;
        msg_dup_flag = st.dup_flag;
        msg_next     = st.next_msg_pos
      }
    }
  }
  with
  [
    (*---------------------------------------*)
    (* Exception Handling                    *)
    (*---------------------------------------*)
    FT.Invalid_FIX_Msg err ->
      (* Re-raise this exception, possibly with more info. But take care not
         to over-write valid info by invalid or re-set vals:
      *)
      let err' =
      {
        ( err ) with
        FT.err_seq_num  =
          if   st.seq_num >= 1  then st.seq_num  else err.FT.err_seq_num;

        FT.err_msg_type =
          if   st.mtype <> None then st.mtype    else err.FT.err_msg_type;

        FT.err_fld_type =
          if   st.ftype <> None then st.ftype    else err.FT.err_fld_type;

        FT.err_fld_val  =
          if   st.fval  <> ""   then st.fval     else err.FT.err_fld_val;

        FT.err_off      =
          if   st.curr_pos >= from
          then st.curr_pos -  from
          else err.FT.err_off;

        FT.err_next_msg_pos =
          if   st.next_msg_pos >  from
          then st.next_msg_pos
          else err.FT.err_next_msg_pos;

        FT.err_text     =
          if debug
          then err.FT.err_text^"\n"^( Printexc.get_backtrace () )
          else err.FT.err_text
      }
      in raise( FT.Invalid_FIX_Msg err' )

    | FF.Partial_FIX_Msg as err -> raise err

    | other                     ->
        invalid_fix_msg st None ( string_of_exn other )
  ];

