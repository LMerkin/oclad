(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(* Error Handling:                                                           *)
(*===========================================================================*)
module FT = Fix_types;

exception Partial_FIX_Msg;

(* The following func raises an exception with partial informarion about the
   error.   The caller may have more details aboutthe error location, and in
   that case will fill in other record flds:
*)
value invalid_fix_fld: FT.session_rej_reason -> string -> 'a =
fun rsn fval ->
  let err    =
  {
    FT.err_seq_num      = -1;
    FT.err_msg_type     = None;
    FT.err_fld_type     = None;
    FT.err_fld_val      = fval;
    FT.err_text         = "";
    FT.err_reason       = Some rsn;
    FT.err_off          = -1;
    FT.err_next_msg_pos = -1
  }
  in
  raise( FT.Invalid_FIX_Msg err );

(*===========================================================================*)
(* Codec Utils:                                                              *)
(*===========================================================================*)
(* Map: FIX Msg => FIX Msg Type: *)
value get_fix_fld_type: FT.fix_fld -> FT.fix_fld_type = FIX_PROJ "T_";


(* "int_of_string" with a special exception type: *)
value int_val_of_string: string -> int =
fun s ->
  try int_of_string s
  with[ _ -> invalid_fix_fld FT.ValueFormatError s ];


(* "check_string":
   Used in both encoding and decoding of FIX flds:
*)
value check_string: string -> string =
fun s ->
  let n = ( String.length s ) - 1 in
  do{
    for i = 0 to n
    do{
      if s.[ i ] = '\x01'
      then invalid_fix_fld FT.ValueFormatError s
      else ()
    };
    s
  };

(*---------------------------------------------------------------------------*)
(* "price" handling, incl non-std extensions:                                *)
(*---------------------------------------------------------------------------*)
value string_of_price: FT.price -> string =
fun f ->
  if f = FT.n_a_price
  then "N/A"              (* XXX: OSL FIX extension! *)
  else
  let  s = string_of_float f in
  if   f > 0.0
  then s
  else invalid_fix_fld FT.ValueRangeError s;


value price_of_string: string -> FT.price =
fun  s ->
  if s = "N/A"
  then FT.n_a_price       (* XXX: OSL FIX extension! *)
  else
  let  f  =
    try float_of_string s
    with[ _ -> invalid_fix_fld FT.ValueFormatError s ]
  in
  if   f  >= 0.0
  (* XXX: This is an OSL extension as well. Normally, must be a strict ">":  *)
  then f
  else invalid_fix_fld FT.ValueRangeError s;

(*---------------------------------------------------------------------------*)
(* Date and Time handling:                                                   *)
(*---------------------------------------------------------------------------*)
value check_time: int -> int -> float -> unit =
fun hh mm ss ->
  (* NB: Leap seconds are allowed:      *)
  if hh >= 0 && hh <= 23 && mm >= 0 && mm <= 59 && ss >= 0.0 && ss < 61.0
  then ()
  else invalid_fix_fld FT.SendingTimeError
                       ( Printf.sprintf "%d:%d:%f" hh mm ss );


value check_date: int -> int -> int   -> unit =
fun year month day ->
  (* Some restrictions on the acceptable dates: *)
  let month_days   =
    match month with
    [
      1 | 3 | 5 | 7 | 8 | 10 | 12 -> 31
    | 4 | 6 | 9 | 11              -> 30
    | 2 ->  if year mod 4 <> 0 then  28 else 29
    | _ ->  assert False
    ]
  in
  if year >= 2000 && year <= 2099 && month >= 1 && month <= 12 && day  >= 1 &&
     day  <= month_days
  then ()
  else invalid_fix_fld FT.SendingTimeError
                       ( Printf.sprintf "%04d%02d%02d" year month day );


value string_of_time: FT.time   -> string =
fun ( ( hh, mm, ss )  as time ) ->
  if   time = FT.n_a_time
  then
    "N/A"       (* XXX: OSL FIX extension! *)
  else
  do{
    check_time hh mm ss;
      Printf.sprintf "%02d:%02d:%06.3f" hh mm ss
  };


value string_of_date:  FT.date     -> string =
fun ( ( year, month, date ) as d ) ->
  if   d = FT.n_a_date
  then
    "N/A"       (* XXX: OSL FIX entension! *)
  else
  do{
    check_date year month date;
    Printf.sprintf "%04d%02d%02d" year month date
  };


value string_of_date_time: FT.date_time -> string   =
fun ( ( date, time ) as dt ) ->
  if  dt = FT.n_a_date_time
  then
    "N/A"       (* XXX: OSL FIX extension! *)
  else
    ( string_of_date date )^"-"^( string_of_time time );


value time_of_string: string -> FT.time =
fun s ->
  if s = "N/A"  (* XXX: OSL FIX extension! *)
  then
    FT.n_a_time
  else
    let get_time: int -> int -> float -> FT.time =
    fun  hh mm ss ->
      do{
        check_time hh mm ss;
        ( hh, mm, ss )
      }
    in
    Scanf.sscanf s "%02d:%02d:%02.3f" get_time;


value date_of_string: string -> FT.date =
fun s ->
  if s = "N/A"  (* XXX: OSL FIX extension! *)
  then
    FT.n_a_date
  else
    let get_date: int -> int -> int -> FT.date =
    fun year month day ->
      do{
        check_date year month day;
        ( year, month, day )
      }
    in
    Scanf.sscanf s "%04d%02d%02d" get_date;


value date_time_of_string: string -> FT.date_time =
fun s ->
  if s = "N/A"  (* XXX: OSL FIX extension! *)
  then
    FT.n_a_date_time
  else
    let get_date_time:
        int -> int -> int -> int -> int -> float -> FT.date_time =
    fun year month day hh mm ss  ->
      do{
        check_date year month day;
        check_time hh   mm    ss;
        ( ( year, month, day ), ( hh, mm, ss ) )
      }
    in
    Scanf.sscanf s "%04d%02d%02d-%02d:%02d:%02.3f" get_date_time;


value compare_dts: FT.date_time -> FT.date_time -> int =
fun ( ( year1, mon1, day1 ), ( hour1, min1, sec1 ) )
    ( ( year2, mon2, day2 ), ( hour2, min2, sec2 ) )  ->

    let  c_year = compare year1 year2 in
    if   c_year <> 0
    then c_year
    else
    let  c_mon  = compare mon1  mon2  in
    if   c_mon  <> 0
    then c_mon
    else
    let  c_day  = compare day1  day2  in
    if   c_day  <> 0
    then c_day
    else
    let  c_hour = compare hour1 hour2 in
    if   c_hour <> 0
    then c_hour
    else
    let  c_min  = compare min1  min2  in
    if   c_min  <> 0
    then c_min
    else
    compare sec1  sec2;

(*---------------------------------------------------------------------------*)
(* ALL AUTO-GENERATED CODECS COME HERE:                                      *)
(*---------------------------------------------------------------------------*)
FIX_FLD_SUBTYPE_CODECS;

(*---------------------------------------------------------------------------*)
(* Special Cases:                                                            *)
(*---------------------------------------------------------------------------*)
(* "SecurityType":
   Override "security_type_of_string" -- it requires several extra cases to be
   handled manually:
*)
value sectype_of_string_auto:  string -> FT.security_type =
  security_type_of_string;

value security_type_of_string: string -> FT.security_type =
fun s ->
  try sectype_of_string_auto s
  with
  [ hmm ->
    match String.lowercase s with
    [ "equity"     -> FT.CS   (* XXX: always assuming CommonStock? *)
    | "currencies" -> FT.FOR
    | "future"     -> FT.FUT
    | "option"     -> FT.OPT
    | _            -> raise hmm
    ]
  ];

(* "MarketDepth": *)

value int_of_market_depth   :  FT.market_depth    -> int =
fun
[
  FT.FullBook          -> 0
| FT.BookTop           -> 1
| FT.BestPriceTiers  i ->
    if   i >= 2
    then i
    else invalid_fix_fld FT.ValueRangeError ( string_of_int i )
];

value market_depth_of_string: string -> FT.market_depth =
fun s ->
  match int_val_of_string s with
  [
    0             -> FT.FullBook
  | 1             -> FT.BookTop
  | i when i >= 2 -> FT.BestPriceTiers i
  | _             -> invalid_fix_fld FT.ValueRangeError s
  ];

(*===========================================================================*)
(* "fix_fld_to_buffer":                                                      *)
(*===========================================================================*)
(* Externally-Visible Field Encoding Func: *)

value fix_fld_to_buffer: Buffer.t -> FT.fix_fld -> unit  =
fun buff  fld ->
  try  FIX_FLD_ENCODER
  with
  [ FT.Invalid_FIX_Msg err ->
      (* Modify the exception by installing the field type and the backtrace: *)
      let ftype = get_fix_fld_type fld  in
      let err'  =
      {
        ( err ) with
        FT.err_fld_type = Some ftype;
        FT.err_text     = Printexc.get_backtrace ()
      }
      in
      raise( FT.Invalid_FIX_Msg err' )
  ];

(*===========================================================================*)
(* Parsing ( Decoding ) of FIX Flds:                                         *)
(*===========================================================================*)
(*-----------------------------------------*)
(* Some Utils:                             *)
(*-----------------------------------------*)
(* "yn"                   *)
value yn: string -> bool =
fun s ->
  match s with
  [
    "Y" | "y" -> True
  | "N" | "n" -> False
  | _         -> invalid_fix_fld FT.ValueFormatError s
  ];

(* "nat_of_string"        *)
value nat_of_string: string -> int =
fun s ->
  if s = "N/A"  (* XXX: OSL FIX extension! *)
  then FT.n_a_nat
  else
  let  i  = int_val_of_string s in
  if   i >= 0
  then i
  else invalid_fix_fld FT.ValueRangeError s;

(*-----------------------------------------*)
(* "fix_fld_of_string":                    *)
(*-----------------------------------------*)
(* Actually, it requires 2 args: fld type and the string arg: *)

value fix_fld_of_string: FT.fix_fld_type -> string -> FT.fix_fld =
fun ftype s ->
  FIX_FLD_DECODER;

(*-----------------------------------------*)
(* "parse_fix_fld":                        *)
(*-----------------------------------------*)
(* Starting from a given "pos" in the "buff",  find the next delimeter and
   return ( ftype: FT.fix_fld_type, fval: string, next_pos: int ).
   NB: Since FIX protocol runs over TCP, it is NOT guaranteed that the msgs
   are not fragmented. I.e., parsing of a partially-received msg or fld may
   be attempted. Exception "Partial_FIX_Msg" is raised in that case:
*)
value parse_fix_fld:
      string -> int -> int -> ( FT.fix_fld_type * string * int ) =
fun buff from chunk_end ->
do{
  (* Consistency of the buffer is to be assured to the caller. There MUST
     be at least 1 byte to parse:
  *)
  assert( from <= chunk_end && chunk_end <= String.length buff );

  (* "scan_chars":
    Find the end of the field ( SOH ) and the preceding '=':
  *)
  let eq_pos = ref ( -1 ) in

  let rec scan_chars: int -> int =
  fun pos ->
    if pos >= chunk_end
    then
    do{
      (* End of string, but the fld terminator ( SOH ) not found: *)
      assert( pos = chunk_end );
      raise Partial_FIX_Msg
    }
    else
    if buff.[ pos ] = '\x01'
    then pos      (* Found SOH, return the next pos and exit scan *)
    else
    do{
      if buff.[ pos ] = '=' then eq_pos.val := pos else ();
      (* Next char by tail recursion: *)
      scan_chars ( pos + 1 )
    }
  in
  (* Run the scanner and verify the result: *)
  let soh_pos = scan_chars from      in
  let fld_len = soh_pos  - from + 1  in

  if eq_pos.val = -1 || eq_pos.val = from
  then
    (* No tag=value structure at all or empty tag: *)
    invalid_fix_fld FT.UndefinedTag ( String.sub buff from fld_len )
  else
  if soh_pos = eq_pos.val + 1
  then
    (* "tag=" is present, but no value:            *)
    invalid_fix_fld FT.TagWithoutValue ( String.sub buff from fld_len )
  else
  do{
    assert( from < eq_pos.val && eq_pos.val < soh_pos-1 );
    (* Get the fld tag and value:                  *)
    let stag = String.sub buff from ( eq_pos.val - from )                in
    let f    = fix_fld_type_of_string stag                               in
    let fval = String.sub buff ( eq_pos.val+1 ) ( soh_pos-eq_pos.val-1 ) in

    (* The fld has been parsed out, but not fully decoded yet: "f" is still
       an "int" and "fval" is a "string":
    *)
    ( f, fval, soh_pos+1 )
  }
};

(*===========================================================================*)
(* Conversion of Type-Specific FIX Msgs into Fld Lists:                      *)
(*===========================================================================*)
(* NB: The funcs below DO NOT verify the semantic validity of the flds.  The
   latter is done at the point of flds stringification.   Advantages of this
   indirect way of FIX msgs stringification ( msg -> list of flds -> buffer ):
   ( * ) provides an abstraction layer ( flds can be shared between multipler
         msgs;
   ( * ) is type-safe ( flds are fully typed );
   disadvatage:
   ( * ) less efficient that direct outputting of record msgs types to a buff.
*)
(*-----------------------------------------*)
(* "arr_fold_left_rev":                    *)
(*-----------------------------------------*)
(* Folding an array backwards. XXX: Functional interface, imperative impl:   *)

value arr_fold_left_rev: bool  -> ( 'a -> 'b -> 'a ) -> 'a -> array 'b -> 'a =

fun empty_ok f a0 barr         ->
  let n  = Array.length barr  in
  if  n  = 0 && ( not empty_ok )
  then failwith "ERROR: Empty Group"
  else
  let ac = ref a0             in
  do{
    for i  = 1 to n
    do{
      ac.val := f ac.val barr.( n-i )
    };
    ac.val
  };

(*---------------------------------------------------------------------------*)
(* Auto-Generate Split Functions for FIX Sub-Types ( conversion to lists     *)
(* of Flds ):                                                                *)
(*---------------------------------------------------------------------------*)
FIX_SUBTYPES_SPLIT;

(*===========================================================================*)
(* Assembly of Type-Specific FIX Msgs from Flds:                             *)
(*===========================================================================*)
(*-----------------------------------------*)
(* Raising Errors:                         *)
(*-----------------------------------------*)
value fld_error:
  string -> FT.session_rej_reason -> FT.fix_msg_type -> FT.fix_fld_type
  -> 'a   =
fun err_txt rsn mt ft ->
  let err =
  {
    FT.err_seq_num      = -1;
    FT.err_msg_type     = Some mt;
    FT.err_fld_type     = Some ft;
    FT.err_fld_val      = "";   (* XXX: difficult to stringify once parsed! *)
    FT.err_text         = err_txt;
    FT.err_reason       = Some rsn;
    FT.err_off          = -1;
    FT.err_next_msg_pos = -1
  }
  in
  raise( FT.Invalid_FIX_Msg err );


value improper_fld:  FT.fix_msg_type -> FT.fix_fld      -> 'a =
fun mt fld ->
  fld_error "" FT.ImproperTag mt ( get_fix_fld_type fld );


value repeated_fld:  FT.fix_msg_type -> FT.fix_fld      -> 'a =
fun mt fld ->
  fld_error "Repeated Tag" FT.ImproperTag mt ( get_fix_fld_type fld );


value missing_fld:   FT.fix_msg_type -> FT.fix_fld_type -> 'a =
fun mt ft  ->
  fld_error "" FT.RequiredTagMissing mt ft;


(* An error is also raised if the Fld list appears to be not long enough to
   fetch the Fld Grp of the previously-specified length:
*)
value short_fld_lst: FT.fix_msg_type -> 'a =
fun mt ->
  let err =
  {
    FT.err_seq_num      = -1;
    FT.err_msg_type     = Some mt;
    FT.err_fld_type     = None;
    FT.err_fld_val      = "";
    FT.err_text         = "Premature Msg End";
    FT.err_reason       = Some FT.RequiredTagMissing;
    FT.err_off          = -1;
    FT.err_next_msg_pos = -1
  }
  in
  raise( FT.Invalid_FIX_Msg err );

(*-----------------------------------------*)
(* "apply_flds":                           *)
(*-----------------------------------------*)
(* Result of trying to match and consume an "fix_fld" and set the corresp type-
   specific record fld:
*)
type action_res =
[
  Continue of list FT.fix_fld   (* 1+ flds consumed OK, continue with others *)
| Stop          (* Fld not consumed, but that is OK -- end of a sub-group    *)
| Improper      (* Fld does not belong to the record type being constructed  *)
| Repeated      (* The corresp record fld was already set                    *)
];

(* The "apply_flds" func traverses the list of flds,  and on each fld runs the
   "action" which tries to use this fld to set up some fld in the record being
   constructed. NB: "action" may consume more flds than 1 if it matches a sub-
   group, hence the following logic:
*)
value rec apply_flds:
  FT.fix_msg_type    ->
  ( FT.fix_fld       -> list FT.fix_fld -> action_res ) ->
  list FT.fix_fld    ->
  list FT.fix_fld    =

fun mt action flds   ->
  match flds with
  [
    []               -> []  (* All done, exiting *)
  | [ f::fs ]        ->
    do{
      match action f fs with
      [
        Continue fs' -> apply_flds mt action fs'   (* Tail-recursion       *)
      | Stop         -> flds                       (* Return unused flds   *)
      | Improper     -> improper_fld  mt f
      | Repeated     -> repeated_fld  mt f
      ]
    }
  ];

(*---------------------------------------------------------------------------*)
(* Auto-Generated Assembly Functions:                                        *)
(*---------------------------------------------------------------------------*)
FIX_SUBTYPES_ASM;

