/* vim:ts=2:syntax=c
*/
/*===========================================================================*/
/*           Low-Level Non-Blocking Socket I/O for the FIX Engine:           */
/*===========================================================================*/
#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/fail.h>
#include <caml/custom.h>
#include <caml/signals.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <errno.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

#define  Nothing  ( ( value )0 )
extern void uerror( char * cmdname, value arg ) Noreturn;

/* Because the socket is non-blocking, we do not need to copy OCaml buffers to/
	 from C buffers, or do "enter_/leave_blocking_section" -- more efficient. GC
	 cannot be triggered within these functions:
*/
/*-----------------------*/
/* "send_nonblock":      */
/*-----------------------*/
value send_nonblock( value sock, value buff, value from, value len )
{
	int flags = 0;

	/* "buff" is a "string":         */
	assert( Is_block( buff ) && Tag_val( buff ) == String_tag &&
					Is_long(  from ) && Is_long( len  ) && Is_long( sock ) );

	long  f    = Long_val( from );
	long  n    = Long_val( len  );
	assert( f >= 0 && n > 0 && f + n <= caml_string_length( buff ) );

	/* Now perform the actual I/O:   */
	const char * sp = String_val( buff ) + f;

#ifdef __linux__
	flags = MSG_NOSIGNAL;
#endif
	/* XXX: MSG_NOSIGNAL may not be available for other systems, so need to block
		 SIGPIPE!
	*/
	int ret = send( Int_val( sock ), sp, ( int ) n, flags );
	
	if( ret < 0 )
		if( errno == EAGAIN )
			return Val_int( 0 );	/* No data available       */
		else
			uerror( "send_nonblock", Nothing );
	else
		return Val_int( ret );	/* Maybe some actual data? */
}

/*-----------------------*/
/* "recv_nonblock":      */
/*-----------------------*/
value recv_nonblock( value sock, value buff, value from, value len )
{
	int flags = 0;

	/* "buff" is a "string":       */
	assert( Is_block( buff ) && Tag_val( buff ) == String_tag &&
					Is_long(  from ) && Is_long( len  ) && Is_long( sock ) );

	long f = Long_val( from );
	long n = Long_val( len  );
	assert( f >= 0 && n > 0 && f + n <= caml_string_length( buff ) );

	/* Now perform the actual I/O: */
	char * sp = String_val( buff ) + f;

#ifdef __linux__
	flags = MSG_NOSIGNAL;
#endif
	/* XXX: MSG_NOSIGNAL may not be available for other systems, so need to block
		 SIGPIPE!
	*/
	int ret = recv( Int_val( sock ), sp, ( int ) n, flags );

	if( ret < 0 )
		if( errno == EAGAIN )
			return Val_int( 0 );  /* No data available       */
		else
			uerror( "send_nonblock", Nothing );
	else
		return Val_int( ret );  /* Maybe some actual data? */
}

/*===========================================================================*/
/*                          POSIX RWLocks Support:                           */
/*===========================================================================*/
/* The following implementation is modeled on that of the standard OCaml Mutex
   module but allows for both Read ( Read-Only ) and Write ( Read-Write ) locks:
*/
#define RWLock_val( v ) ( *( ( pthread_rwlock_t ** )( Data_custom_val( v ) ) ) )
#define MAX_RWLOCKS 1024

/*-----------------------*/
/* "rwlock_check":       */
/*-----------------------*/
static void rwlock_check( int retcode, const char *msg )
{
	const char * err;
  int errlen, msglen;
  value str;

	if( retcode == 0 ) return;
  err    = strerror( retcode );
  msglen = strlen( msg );
  errlen = strlen( err );
	str    = caml_alloc_string( msglen + 2 + errlen );
	memmove( &Byte(  str, 0         ), msg,  msglen );
	memmove( &Byte(  str, msglen    ), ": ", 2      );
	memmove( &Byte(  str, msglen + 2), err,  errlen );
	caml_raise_sys_error( str );
}

/*-----------------------*/
/* "rwlock_ops":         */
/*-----------------------*/
static void rwlock_finalise( value wrapper )
{
	pthread_rwlock_t * rwl = RWLock_val( wrapper );
	pthread_rwlock_destroy( rwl );
	caml_stat_free( rwl );
}

static int  rwlock_compare( value wrapper1, value wrapper2 )
{
	pthread_rwlock_t * rwl1 = RWLock_val( wrapper1 );
	pthread_rwlock_t * rwl2 = RWLock_val( wrapper2 );
	return ( rwl1 == rwl2 ) ? 0 : ( rwl1 < rwl2 ) ? -1 : 1;
}

static struct custom_operations rwlock_ops =
{
	"_rwlock",
	rwlock_finalise,
	rwlock_compare,
	custom_hash_default,
	custom_serialize_default,
	custom_deserialize_default
};

/*-----------------------*/
/* "rwlock_create":      */
/*-----------------------*/
value rwlock_create( value unit )
{
	pthread_rwlock_t * rwl = caml_stat_alloc( sizeof( pthread_rwlock_t ) );
	value          wrapper = Val_unit;
	int            retcode = pthread_rwlock_init( rwl, NULL );
	rwlock_check(  retcode, "RWLock.create" );
	wrapper =
		alloc_custom( &rwlock_ops, sizeof( pthread_rwlock_t * ), 1, MAX_RWLOCKS );
	RWLock_val( wrapper )  = rwl;
	return wrapper;
}

/*-----------------------*/
/* "rwlock_lock_rd":     */
/*-----------------------*/
value rwlock_lock_rd( value wrapper )
{
	pthread_rwlock_t * rwl = RWLock_val( wrapper );
	/* PR#4351: first try to acquire rwlock w/o releasing the master mutex:    */
	int retcode =  pthread_rwlock_tryrdlock( rwl );
	if( retcode == 0 ) return Val_unit;

	/* If unsuccessful, block on the master mutex: */
	Begin_root( wrapper )						/* Prevent the de-allocation of the rwlock */
		caml_enter_blocking_section();
		retcode = pthread_rwlock_rdlock( rwl );
		caml_leave_blocking_section();
	End_roots();
	rwlock_check( retcode, "RWLock.lock_rd" );
	return Val_unit;
}

/*-----------------------*/
/* "rwlock_lock_wr":     */
/*-----------------------*/
value rwlock_lock_wr( value wrapper )
{
	pthread_rwlock_t * rwl = RWLock_val( wrapper );
	/* PR#4351: first try to acquire rwlock w/o releasing the master mutex:    */
	int retcode =  pthread_rwlock_trywrlock( rwl );
	if( retcode == 0 ) return Val_unit;

	/* If unsuccessful, block on the master mutex: */
	Begin_root( wrapper )						/* Prevent the de-allocation of the rwlock */
		enter_blocking_section();
		retcode = pthread_rwlock_wrlock( rwl );
		leave_blocking_section();
	End_roots();
	rwlock_check( retcode, "RWLock.lock_wr" );
	return Val_unit;
}

/*-----------------------*/
/* "rwlock_unlock":      */
/*-----------------------*/
value rwlock_unlock( value wrapper )
{
	pthread_rwlock_t * rwl = RWLock_val( wrapper );
	/* PR#4351: no need to release and reacquire master lock */
	int retcode = pthread_rwlock_unlock( rwl );
	rwlock_check( retcode, "RWLock.unlock" );
	return Val_unit;
}

/*-----------------------*/
/* "rwlock_try_lock_rd": */
/*-----------------------*/
value rwlock_try_lock_rd( value wrapper )
{
	pthread_rwlock_t * rwl = RWLock_val( wrapper );
	int retcode = pthread_rwlock_tryrdlock(  rwl );
	if( retcode == EBUSY || retcode == EAGAIN ) return Val_false;
	rwlock_check( retcode, "RWLock.try_lock_rd" );
	return Val_true;
}

/*-----------------------*/
/* "rwlock_try_lock_wr": */
/*-----------------------*/
value rwlock_try_lock_wr( value wrapper )
{
	pthread_rwlock_t * rwl = RWLock_val( wrapper );
	int retcode = pthread_rwlock_trywrlock(  rwl );
	if( retcode == EBUSY || retcode == EAGAIN ) return Val_false;
	rwlock_check( retcode, "RWLock.try_lock_wr" );
	return Val_true;
}
