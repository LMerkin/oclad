(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                           Build Language Types                            *)
(*===========================================================================*)
(* EXPANDABLE VARIABLES:
   The following variables are automatically expanded in all file names and
   attrs in "build.conf" files:

    ${ARCH}
    ${OS}
    ${TOOLS_TOP}
    ${OCAML_LIB}
    ${ASM_TOP}
    ${ASM_LIB}
    ${SRC_TOP}
    ${OSRC_TOP}
    ${BUILD_TOP}
    ${OBUILD_TOP}
    ${PKGS}
    ${ASM}
    ${OBJ}
    ${LIB}
    ${DLL}
    ${EXE}

    FILE EXTENSIONS:

    .mli  : OCaml module signature
    .ml   : OCaml module body
    .cmx  : native-code OCaml object
    .cmo  : byte-code   OCaml object
    .cmxa : bative-code OCaml library
    .cma  : byte-code   OCaml library
    .c    : C   src file
    .cpp  : C++ src file

   ${OBJ}: object  file   ( expands to ".obj" on Windows, ".o"  on UNIX )
   ${LIB}: static library ( expands to ".lib" on Windows, ".a"  on UNIX )
   ${DLL}: shared library ( expands to ".dll" on Windows, ".so" on UNIX )
   ${EXE}: executable     ( expands to ".exe" on Windows, ""    on UNIX )
*)
(*--------------------*)
(* "src_attr":        *)
(*--------------------*)
(* Currently, the attributes are just strings representing extra compilation
   options, possibly containing expandable vars (see below):
*)
type src_attr =
[
  (* Additive Attr: Gets added to all previously specified src attrs, incl
     those inherited from upper dirs. There are 3 kinds of src atrrs: those
     for OCaml ( *_ML ); for C++/C compilation ( *_CPP ); any others ( e.g.
     for custom commands -- *_OTHER ). In addition, there is a special form
     of OCaml attrs ( *_PKGS ) which specify FindLib packages required for
     compilation. Packages required for building of libraries & executables
     are then assembled automatically from this info:
  *)
  AA_ML    of string
| AA_PKGS  of list string
| AA_CPP   of string
| AA_OTHER of string

  (* Replacement Src Attr: All src attrs for the same type of compilation
     ( OCaml or C++/C ) specified before this one are dropped:
  *)
| RA_ML    of string
| RA_PKGS  of list string
| RA_CPP   of string
| RA_OTHER of string
]
with parse;

(*--------------------*)
(* "src":             *)
(*--------------------*)
type src =
[
  (* Simple source: just a file name or path, possibly containing expandable
     variables:
  *)
  S  of string

  (* Source file with attributes :*)
| SA of string and list src_attr
]
with parse;

(*--------------------*)
(* "targ_attr":       *)
(*--------------------*)
type targ_attr =
[
  (* Additive Target Attr: Gets added to all previously specified target attrs,
     including those inherited from upper dirs. Since all kinds of output files
     ( e.g., libs: CMXA or CMA; compound OBJ files; execs: EXE or DLL ) are ge-
     nerated by the same subsystem ( the native OCaml compiler ), we can  main-
     tain a single set of target attrs for all of them:
  *)
  AA_OUT of string

  (* Replacemenmt Target Attributes: All target attributes specified before
     this one are dropped:
  *)
| RA_OUT of string
]
with parse;

(*--------------------*)
(* "targ":            *)
(*--------------------*)
type targ =
[
  (* Simple target: just a file name or path, possibly containing expandable
     variables:
  *)
  T  of string

  (* Target with attributes: *)
| TA of string and list targ_attr
]
with parse;

(*--------------------*)
(* "*conf*":          *)
(*--------------------*)
type conf_entry =
[
  (* Global declarations are attributes which have directory-wide effect, and
     are inherited by sub-directories:
  *)
  GSA        of src_attr

  (* GTA is a Global Target Attribute: *)
| GTA        of targ_attr

  (* BUILD rule: Target and Sources  : *)
| BUILD      of targ and list src

  (* EXTRA_DEPS: similar to BUILD, but not a stand-alone rule. Adds dependen-
     cies to the "targ"    ( which does not need to appear explicitly in any
     BUILD ).  Typically used to declare dependencies which cannot be automa-
     tically inferred by "ocamldep" ( e.g. for MLL/MLY/MLP files ). Args are
     just file names:
  *)
| EXTRA_DEPS of string and list string
]
with parse;

(*---------------------*)
(* The top-level type: *)
(*---------------------*)
(* Terms of this type are the content of "build.conf" files: *)

type  build_conf = list conf_entry with parse;
value build_conf = "build.conf";
