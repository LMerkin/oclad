(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                        Build Rules Implementation:                        *)
(*===========================================================================*)
open ExtString;

(*--------------------*)
(* Accumulated Builds *)
(*--------------------*)
type where_from = string;
type attr_where = ( string * where_from );
type aattrs     =
{
  ml_attrs    : list attr_where;
  pkg_attrs   : list attr_where;
  cxx_attrs   : list attr_where;
  other_attrs : list attr_where;
  out_attrs   : list attr_where
};

(* Empty attr lists: *)
value empty_aattrs: aattrs =
{
  ml_attrs    = [];
  pkg_attrs   = [];
  cxx_attrs   = [];
  other_attrs = [];
  out_attrs   = []
};

(* Dictionary of global attrs for all dirs: *)
value dir_attrs_val: Hashtbl.t string aattrs = Hashtbl.create 64;

(* Externally-visible look-up: *)
value dir_attrs    : string -> aattrs =
fun rdir ->
  try
    Hashtbl.find dir_attrs_val rdir
  with
  [ Not_found ->
      failwith ( "ERROR: No attrs for "^rdir )
  ];

(* Dictionary of build rules:
   key = target_file, val = build_rule
   NB: "where_from" is the location of the corresp "build.conf" file. It is
   associated with each src individually, since srcs are moved across rules
   when the latter are re-written:
*)
type build_src  =
{
  src_file   : U.file;
  src_attrs  : list L.src_attr;
  src_aattrs : list attr_where;
  src_pkgs   : list attr_where;
  src_where  : where_from;
  src_term   : bool
};

type build_rule =
{
  targs      : list U.file;
  targ_attrs : list L.targ_attr;
  targ_aattrs: list attr_where;
  targ_pkgs  : list attr_where;
  targ_where : where_from;
  srcs       : list build_src;
  command    : string
};

value empty_rule: build_rule =
{
  targs      = [];
  targ_attrs = [];
  targ_aattrs= [];
  targ_pkgs  = [];
  targ_where = "";
  srcs       = [];
  command    = ""
};

value build_rules: Hashtbl.t U.file build_rule      = Hashtbl.create 2048;

(* Dictionary of extra deps: *)
value extra_deps : Hashtbl.t U.file ( list U.file ) = Hashtbl.create  256;

(*--------------------*)
(* Attrs Mgmt:        *)
(*--------------------*)
(* "merge_src_attrs": *)

value merge_src_attrs : where_from -> aattrs -> L.src_attr  -> aattrs =
fun where_from aa  sa ->
  match sa with
  [
    (* Additive Attrs:  *)
    L.AA_ML    v  ->
      { ( aa ) with ml_attrs    = aa.ml_attrs    @ [( v, where_from )] }
  | L.AA_PKGS  ps ->  
      { ( aa ) with pkg_attrs   = aa.pkg_attrs   @
                                ( List.map ( fun p -> ( p, where_from ) ) ps )
      }
  | L.AA_CPP   v  ->
      { ( aa ) with cxx_attrs   = aa.cxx_attrs   @ [( v, where_from )] }
  | L.AA_OTHER v  ->
      { ( aa ) with other_attrs = aa.other_attrs @ [( v, where_from )] }

    (* Replacing Attrs: *)
  | L.RA_ML    v  ->
      { ( aa ) with ml_attrs    = [( v, where_from )] }
  | L.RA_PKGS  ps ->
      { ( aa ) with pkg_attrs   = List.map ( fun p -> ( p, where_from ) ) ps }
  | L.RA_CPP   v  ->
      { ( aa ) with cxx_attrs   = [( v, where_from )] }
  | L.RA_OTHER v  ->
      { ( aa ) with other_attrs = [( v, where_from )] }
  ];

(* "merge_targ_attrs":  *)

value merge_targ_attrs: where_from -> aattrs -> L.targ_attr -> aattrs =
fun where_from aa ta ->
  match ta with
  [
    (* Additive Attrs:  *)
    L.AA_OUT v ->
      { ( aa ) with out_attrs = aa.out_attrs @ [( v, where_from )] }
    (* Replacing Attrs: *)
  | L.RA_OUT v ->
      { ( aa ) with out_attrs = [( v, where_from )] }
  ];

(* "merge_conf_attrs":
   Merges the "aattrs" from the given "conf_entry" into "aattrs". NB: No dir
   re-writing ( such as path normalisation ) is performed for attrs:
*)
value merge_conf_attrs: string -> aattrs -> L.conf_entry -> aattrs =
fun   rdir aa ce ->
  let where_from = rdir ^"/"^ L.build_conf in
  match ce with
  [
    L.GSA sa ->
      merge_src_attrs  where_from aa sa (* Global Src Attr   *)

  | L.GTA ta ->
      merge_targ_attrs where_from aa ta (* Global Targ Attr  *)

  | _ ->  aa                            (* Unchanged         *)
  ];

(*--------------------*)
(* "get_rule":        *)
(*--------------------*)
value get_rule: U.file -> string -> build_rule =
fun targ comment ->
  try
    Hashtbl.find build_rules targ
  with
  [
    Not_found ->
      failwith ( "ERROR: Cannot get the build rule for "^( U.string_of targ )^
               ": "^comment )
  ];

(*--------------------*)
(* "install_rule":    *)
(*--------------------*)
(* Installs the given "rule" in "build_rules" under EACH of the "targs",
   replacing the existing bindings if any:
*)
value install_rule: build_rule -> unit =
fun rule ->
  List.iter ( fun targ -> Hashtbl.replace build_rules targ rule ) rule.targs;


(*--------------------*)
(* Initial BuildRules *)
(*--------------------*)
(* "add_build":
   Adds target / src lists from a BUILD in "conf_entry" to the global  "build_
   rules" dict, possibly modifying the paths in the process. XXX: it has other
   side-effects due to "U.mk_src_path" ( as detailed below ):
*)
value add_build: string -> string -> string -> L.conf_entry -> unit =
fun   rdir src_adir where_conf ce ->
  match ce with
  [
    L.BUILD targ srcs ->
      (* Get the filename and the attrs of the "targ": *)
      let ( t_path, t_attrs ) =
        match targ with
        [
          L.T  tf     -> ( U.mk_targ_path rdir tf, []  )
        | L.TA tf tas -> ( U.mk_targ_path rdir tf, tas )
        ]
      in
      (* Now scan the srcs.   XXX: "U.mk_src_path" has side effects: src files
         which are indeed found under the src tree are symlinked into the corr
         dirs of the build tree, and marked as Terminals ( Leaves ):
      *)
      let srcs': list build_src =
        List.map
        (
          fun src ->
          let ( ( s_file, is_term ), s_attrs ) =
              match src with
              [
                L.S  sf     -> ( U.mk_src_path rdir src_adir sf, []  )
              | L.SA sf sas -> ( U.mk_src_path rdir src_adir sf, sas )
              ]
            in
            (* Create the "build_src"; "src_aattrs" and "src_pkgs" installed
               later:
            *)
            {
              src_file  = s_file;
              src_attrs = s_attrs;
              src_aattrs= [];
              src_pkgs  = [];
              src_where = where_conf;
              src_term  = is_term
            }
        )
        srcs
      in
      do{
        (* Install the build info; "targ_aattrs" and "targ_pkgs" are installed
           later:
        *)
        assert( not( Hashtbl.mem  build_rules t_path ) );
        install_rule
        {
          targs      = [ t_path ]; (* Store the key itself in the value *)
          targ_attrs = t_attrs;
          targ_aattrs= [];
          targ_pkgs  = [];
          targ_where = where_conf;
          srcs       = srcs';
          command    = ""          (* Will be generated later *)
        }
      }

  | L.EXTRA_DEPS targ srcs ->
      (* Similar to BUILD, but simpler: *)
      let targ' = U.mk_targ_path rdir targ in
      let srcs' =
        List.map
          ( fun sf -> fst ( U.mk_src_path ~no_symlink:True rdir src_adir sf ) )
          srcs
      in
      (* Existing dependencies for the same "targ"? *)
      let old_srcs =
        if   Hashtbl.mem  extra_deps targ'
        then Hashtbl.find extra_deps targ'
        else []
      in
      (* Unified list of old and new srcs:          *)
      let new_srcs =
        List.fold_left
        (
          fun  curr sf ->
            if not ( List.mem sf curr )
            then curr @ [ sf ]
            else curr
        )
        old_srcs srcs'
      in
      (* Install them: *)
      Hashtbl.replace extra_deps targ' new_srcs

  | _ ->
      () (* Nothing to do here *)
  ];

(*--------------------*)
(* Traverse Src Dirs: *)
(*--------------------*)
(* "traverse_dirs":
   Traverse ALL dirs under "src_top"  ( i.e., NOT only those configured through
   "build.conf" files ), create ( if necessary ) a parallel dir structure under
   "build_top". Also collect "dir_attrs_val" and "build_rules" in the process:
*)
value rec traverse_dirs: aattrs -> string -> unit =
fun aattrs rdir ->
  (* "rdir"     is a   relative src   path ( wrt "src_top" or "build_top" );
     "src_adir" is the absolute src   path;
     "aattrs"   are the global attrs inherited from upper dirs
  *)
  let src_adir = U.normalise_path ( P.src_top ^"/"^ rdir ) in
  do {
    (* "src_adir" is an absolute path. It must exist, and be a directory: *)
    assert( not( Filename.is_relative src_adir )&& Sys.is_directory src_adir );

    (* If the build dir does not exist, create it: *)
    U.ensure_dir_exists rdir;

    (* Is there a config file in "src_adir"?  *)
    let conf_file = U.normalise_path ( src_adir ^"/"^ L.build_conf ) in
    let aattrs'   =
      if  Sys.file_exists conf_file
      then
        (* Yes, config file found - parse it: *)
        let bc    = C.parse_conf_file conf_file  in

        (* Merge the config with the curr "aattrs": *)
        let aattrs'' = List.fold_left ( merge_conf_attrs rdir ) aattrs bc in
        do {
          (* Memoise the "aattrs'" for this "rdir": *)
          assert( not( Hashtbl.mem dir_attrs_val rdir   ) );
          Hashtbl.add  dir_attrs_val rdir  aattrs'';

          (* Now process the BUILD part, if present, updating "build_rules" AND
            symlinking the srcs into the corresp build dir:
          *)
          List.iter ( add_build rdir src_adir conf_file ) bc;

          (* Modified "aattrs'" to be passed to sub-dirs: *)
          aattrs''
        }
      else
        (* No config in this dir: *)
        aattrs
    in
    (* Recurse into all sub-dirs of "src_adir", passing new "aattrs'" down: *)

    let src_dir_conts = Array.to_list ( Sys.readdir src_adir ) in
    let subdirs   =
      List.filter
        ( fun f -> Sys.is_directory ( src_adir ^"/"^ f ) &&
              f <> ".svn" )
        src_dir_conts
    in
    (* Creating a rel path to "f" from the CWD ( build top ): *)
    let rel_subdirs =
      List.map ( fun f -> U.normalise_path ( rdir ^"/"^ f ) ) subdirs
    in
    List.iter ( traverse_dirs aattrs' ) rel_subdirs
  };

(* Top-level invocation of the above: *)
value parse_build_confs: unit -> unit =
fun () ->
  traverse_dirs empty_aattrs ".";


(*--------------------*)
(* Printing Rules:    *)
(*--------------------*)
(* XXX: not very efficient -- a Buffer would better be used: *)

value str_attrs: string -> list U.file -> list attr_where -> string =
fun title fs aws ->
  match aws with
  [
    [] -> ""

  | _  ->
    let s0 =
      Printf.sprintf "#\t%s( %s ) =\n"
                     title ( String.concat " " ( List.map U.string_of fs ) )
    in
    let s1 =
      String.concat ""
        ( List.map
          ( fun ( a, w ) -> ( Printf.sprintf "#\t[ %s @ %s ]\n" a w ) ) aws
        )
    in
    s0^s1^"\n"
  ];


value string_of: ?short:bool -> build_rule -> string =
fun  ?( short = False ) rule ->
  (* Targets:      *)
  let s0 = String.concat " " ( List.map U.string_of rule.targs ) in

  (* Srcs:         *)
  let s1 = String.concat " "
           ( List.map ( fun src -> U.string_of src.src_file ) rule.srcs )
  in
  (* Rule:         *)
  let s2 = s0 ^": "^ s1 ^ "\n" in

  if short
  then
    s2  (* That's all! *)
  else
  (* Command:      *)
  let s3 =
    if rule.command = ""
    then s2
    else s2 ^ "\t" ^ rule.command ^ "\n"
  in
  (* Targ Attrs:   *)
  let s4 = s3 ^ ( str_attrs "TARG_ATTRS" rule.targs rule.targ_aattrs ) in

  (* Targ Pkgs :   *)
  let s5 = s4 ^ ( str_attrs "TARG_PKGS"  rule.targs rule.targ_pkgs   ) in

  (* Srcs Attrs:   *)
  let s6 =
      s5 ^( String.concat ""
          ( List.map
            ( fun src -> str_attrs "SRC_ATTRS" [ src.src_file ] src.src_aattrs )
            rule.srcs
          ) )
  in
  (* Src Pkgs  :   *)
  s6   ^ ( String.concat ""
         ( List.map
            ( fun src -> str_attrs "SRC_PKGS"  [ src.src_file ] src.src_pkgs )
            rule.srcs
         ) );


value print_rules: out_channel -> unit =
fun outch ->
do{
  Hashtbl.iter
  (
    fun targ rule ->
    do{
      assert ( List.mem targ rule.targs );
      Printf.fprintf outch "%s\n" ( string_of rule )
    }
  )
  build_rules;
  Printf.fprintf outch "\n\n";
  flush outch
};


(*--------------------*)
(* Utils: "get_src(s)"*)
(*--------------------*)
(* "get_srcs":
   Returns all srcs of the given sub-types:
*)
value get_srcs: list ( string -> U.file ) -> build_rule -> list build_src =
fun  ctors rule ->
  List.filter ( fun src -> U.has_subtype ctors src.src_file ) rule.srcs;
    

(* "get_src":
   Returns the ( presumabley unique ) src of the given sub-types:
*)
value get_src:  list ( string -> U.file ) -> build_rule -> build_src =
fun  ctors rule ->
  match get_srcs ctors rule with
  [
    [ src ] -> src
  | _       -> 
    let str_ctors = 
        String.concat ", "
          ( List.map ( fun ctor -> U.string_of ( ctor "*" ) ) ctors )
    in
    do{             
      U.ocmake_out stderr ~do_flush:True ( string_of rule );
      failwith( "ERROR: Multiple or missing src(s) of type(s) "^str_ctors^
                " in the rule" )
    }           
  ];


(*--------------------*)
(* External Depends:  *)
(*--------------------*)
(* The following function updates "build_rules" by running the external
   "ocamldep" program. It can only be invoked when all "aattrs" are
   filled in, otherwise it will not know which pre-processor to use
   in "ocamldep":
*)
value pp_re = Pcre.regexp ~study:True "-pp\\s+'(.*?)'";

value install_external_deps: unit -> unit =
fun () ->
  (* NB: "ocamldep" must be able to parse ML and MLI files, and for that, it
     must know which pre-processor options to use:
  *)
  let mlpp_parts: Hashtbl.t string U.FileSet.t =   (* Map: PP_Attr => Srcs *)
      Hashtbl.create 16
  in
  do{
    (* Fill in the "mlpp_parts" from "aattrs" of each ML/MLI in each rule.
       XXX: if multiple PP attrs are specified for a given file, it will come
       into all respective partitions, probably resulting in "ocamldep" errs:
    *)
    Hashtbl.iter
    (
      fun _ rule ->
        (* Special case: If this rule contains an src of the form "CMO pa_*",
           then it requires a custom-built parser, so "ocamldep" will almost
           certainly fail. Skip this rule altogether -- any missed dependen-
           cies must be configured manually:
        *)
        let has_pa_cmo =
          List.exists
            ( fun src ->
                match src.src_file with
                [
                  U.CMO f -> String.starts_with ( Filename.basename f ) "pa_"
                | _       -> False
            ] )
            rule.srcs
        in
        if has_pa_cmo
        then
          (* Skip this rule!  *)
          if C.ocmake_verbose ()
          then
            U.ocmake_out stdout
              ( Printf.sprintf "NOTE: Skipping the rule from ocamldep:\n%s\n"
                               ( string_of ~short:True rule )
              )
          else ()
        else
        (*---------------*)
        (* Generic case: *)
        (*---------------*)
        List.iter
        (
          fun src ->
            match src.src_file with
            [
              U.MLI _ | U.ML _ ->
                (* MLI/ML src: scan aattrs: *)
                let ppas =
                  List.fold_left
                  (
                    fun curr ( attr, _ ) ->
                    try
                      let ppa = ( Pcre.extract ~rex:pp_re attr ).( 1 ) in
                      if not ( List.mem ppa curr )
                      then [ ppa :: curr ]
                      else curr
                    with
                    [ Not_found  -> curr ]
                  )
                  [] src.src_aattrs
                in
                (* Analyse the PPAs. There may only be 0 or 1 PPA present,
                   otherwise there is a conflict.    If there are no PPAs,
                   use the "" string for the "mlpp_parts" key:
                *)
                let ppa =
                  match ppas with
                  [
                    []    -> ""
                  | [ a ] -> a
                  | _     ->
                    let err =
                      Printf.sprintf
                        "ERROR: Multiple PP attrs found for %s in %s: %s"
                        ( U.string_of src.src_file ) src.src_where
                        "CHECK INHERITANCE"
                    in
                    failwith( err )
                  ]
                in
                if Hashtbl.mem mlpp_parts ppa
                then
                  (* The partition for this "ppa" already exists: *)
                  let part = Hashtbl.find mlpp_parts ppa in
                  if  U.FileSet.mem  src.src_file   part
                  then
                    () (* This src is already in its partition, via another
                          rule. Nothing to do. *)
                  else
                    (* Insert this src into "part" and store the partition
                       back in the map:
                    *)
                    let part' =  U.FileSet.add src.src_file part in
                    Hashtbl.replace mlpp_parts ppa part'
                else
                  (* No partition for this "ppa" yet, create it:  *)
                  let part = U.FileSet.singleton src.src_file    in
                  Hashtbl.add mlpp_parts ppa part

            | _ ->
                () (* Any other src: Nothing to do *)
            ]
        )
        rule.srcs
    )
    build_rules;

    (* Now: For each partition constructed, invoke "ocamldep" with the part-
       specific PP attrs.  XXX: thus, the dependencies can only be inferred for
       MLI/ML srcs with identical PP attrs -- this is an "ocamldep" limitation:
    *)
    let quiet = not ( C.ocmake_verbose () ) in
    Hashtbl.iter
    (
      fun ppa srcs ->
        (* Run "ocamldep", get the inferred dependencies and merge them with
           the "extra_deps":
        *)
        U.mk_deps ~quiet:quiet ppa ( U.FileSet.elements srcs ) extra_deps
    )
    mlpp_parts;

    (* Process the integrated "extra_deps", amending the rules: *)
    Hashtbl.iter
    (
      fun dtarg dsrcs ->
        (* Get the rule for "dtarg". XXX: there are terrible problems with
           "ocamldep" ( at least 3.11.0 ):
           it generates dependencies for CMO files even in the "-native"
           mode -- such depndencies are unnecessary:
        *)
        match dtarg with
        [
          U.CMO _ -> ()  (* Dropped! *)

        | _       ->
          (* Retrieve the old "drule" for this "dtarg" -- it must be there since
             "dtarg" is a non-terminal; but the srcs may need to be amended. The
             only exception is if "dtarg" is a CMX auto-generated by "ocamldep"
             from a CMO. XXX: for the moment, if "drule" does not exist, we just
             drop it:
          *)
          let   mb_drule =
            try Some ( get_rule dtarg "R.install_external_deps" )
            with[ _ -> None ]
          in
          match mb_drule with
          [
            Some drule ->
              (* Process the actual dependencies -- add missing ones to the
                 "drule" srcs:
              *)
              let dsrcs_full: list build_src =
                List.fold_left
                (
                  fun ( curr: list build_src ) ( dsfile: U.file ) ->
                    let curr_files =
                      List.map  ( fun ds -> ds.src_file )  curr
                    in
                    (* "dsfile" may be a CMO -- in fact, the CMI is needed. This
                       typically happens when "dtarg" is a CMI as well:
                    *)
                    let dsfile' =
                      match dsfile with
                      [
                        U.CMO base -> U.CMI base
                      | _          -> dsfile   ]
                    in
                    if List.mem dsfile' curr_files
                    then curr                 (* "dsfile'" is already there *)
                    else
                      let new_src  =
                      {
                        src_file   = dsfile';
                        src_attrs  = [];
                        src_aattrs = [];      (* Later!                     *)
                        src_pkgs   = [];      (* Later!                     *)
                        src_where  = "( ocamldep/extra_deps )";
                        src_term   = False    (* XXX: Always False???       *)
                      }
                      in
                      curr @ [ new_src ]
                )
                drule.srcs dsrcs
              in
              (* Install the modified "drule": *)
              let drule' =  { ( drule ) with srcs = dsrcs_full } in
              if  drule' <> drule
              then
                install_rule drule'
              else  ()

          | None -> ()
          ]
        ]
    )
    extra_deps
  };

