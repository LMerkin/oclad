(*===========================================================================*)
(*           Dependencies and  Topological Sorting of Non-Terminals:         *)
(*===========================================================================*)
(* Topological sorting of rules:
   the "bool" arg is the flag indicating whether the external "ocamldep" is to
   be invoked as well ( can only be done after rules re-writing ).
   Verifies that there are no dependency loops or Non-Terminals without build
   rules.
   Returns the array of NonTerminals in the build order, and a list of roots,
   i.e. ultimate Non-Terminals which do not appear as srcs in any rules:
*)
value rules_topol_sort: bool -> ( array U.file * list U.file );

(* For debugging only:      *)
value print_rules_ord : out_channel -> array U.file -> unit;
