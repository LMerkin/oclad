(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                    Static ( Pre-Processor-Level ) Config:                 *)
(*===========================================================================*)
(*-----------------------------------*)
(* Debug / Release Mode:             *)
(*-----------------------------------*)
type dr_mode =
[
  Debug
| Release
];
value dr_mode      : dr_mode;

(*-----------------------------------*)
(* Platform-dependent file suffixes: *)
(*-----------------------------------*)
value asm          : string;
value obj          : string;
value lib          : string;
value dll          : string;
value exe          : string;

(*-----------------------------------*)
(* Paths:                            *)
(*-----------------------------------*)
(* Statically pre-configured paths:  *)
value tools_top    : string;
value src_top      : string;
value build_top    : string;

(* Directory of the OCaml compilers and libs proper:        *)
value ocaml_dir    : string;

(* Standard OCaml library:    *)
value ocaml_lib    : string;

(* 3rd-Party OCaml Packages:  *)
value pkgs_top     : string;

(* Where Debug or Release-specific binaries are installed: *)
value install_top  : string;

