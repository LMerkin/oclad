(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                  "Main" module ( the Driver ) of "OCMake"                 *)
(*===========================================================================*)
(* Read in the hierarchy of "build.conf"s. This creates the initial build rules
   in "R.build_rules". This is required not only in "ConfigMode" but in all
   other modes as well, since this refreshes the src files  copied into the
   build dir:
*)
R.parse_build_confs ();

if C.ocmake_mode () = C.ConfigMode
then
do{
  (* Perform dependency analysis and multi-stange re-writing of the rules.
     This updates "R.build_rules":
  *)
  let ( targs_sorted, roots ) = Rw.rewrite_build_rules ()  in

  (* Generate the build commands, updating "R.build_rules" *)
  B.generate_build_cmds targs_sorted roots
}
else
  (* Just run the build commands -- the target is specified in the command
     line:
  *)
  B.run_build ();

U.close_log   ();
exit 0;
