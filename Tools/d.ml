(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*           Dependencies and  Topological Sorting of Non-Terminals:         *)
(*===========================================================================*)
(*--------------------*)
(* File + Where From: *)
(*--------------------*)
type wfile =
{
  wf_file : U.file;
  wf_where: R.where_from;
  wf_term : bool
};

module WFOrd =
struct
  type  t = wfile;
  value compare: t -> t -> int =
    fun x y -> Pervasives.compare x.wf_file y.wf_file;
end;

(* Set of "wfile", used for a set of NonTerms: *)
module WFSet = Set.Make WFOrd;

(*--------------------*)
(* Stable Sort:       *)
(*--------------------*)
(* Stable sorting function for any array of ( U.file + something ), where files
   are a subset of those in "ntarr"; a sorted COPY is returned.
   XXX: No loop detection yet, so may produce an unsorted array:
*)
value stable_sort:
  Hashtbl.t U.file int   -> ( 'a -> U.file ) -> array ( array int ) ->
  array 'a  -> array 'a  =

fun rmap get_file part_order xfiles ->
  (* "xfiles" may contain both Terms and Non-Terms. Terms are not to be
     sorted, but attached in the original order at the end.
     For Non-Terms, construct 2 initial permutations:
     (+) perm_global.( i ) is the original index ( in the global "ntarr" )
         of the Non-Term at curr index "i" in the array being sorted; used
         for COMPARISONS;
     (+) perm_local.( j )  is the original index ( in "xfiles" ) of the
         Non-Term at curr index "i" in the array being sorted; used for
         re-constructing the result:
  *)
  let ( _, ts_lst, gperm_lst, lperm_lst ) =
    Array.fold_left
    (
      fun ( i, curr_ts, curr_gperm, curr_lperm ) xf ->
        try
          (* If the following succeeds,   "xf" a Non-Term: *)
          let p = Hashtbl.find rmap ( get_file xf )   in
          ( i+1, curr_ts, [ p :: curr_gperm ], [ i :: curr_lperm ] )
        with
        [ Not_found ->
          (* "xf" is a Term: *)
          ( i+1, [ xf :: curr_ts ], curr_gperm, curr_lperm )
        ]
    )
    ( 0, [], [], [] ) xfiles
  in
  let ts_arr      = Array.of_list ( List.rev    ts_lst ) in
  let perm_global = Array.of_list ( List.rev gperm_lst ) in
  let perm_local  = Array.of_list ( List.rev lperm_lst ) in
  let m           = Array.length perm_global             in
  let max_swaps   = m*( m-1 )/2                          in

  (* "sort_pass":
     One pass of the sorting algorithm from "i" to the end of the array.
     Returns the number of swaps performed:
  *)
  let sort_pass: int -> int =
  fun i ->
    let swc = ref 0   in
    do{
      for j = i to ( m-1 )
      do{
        (* Compare ( i ) and ( j ); must go back to their ORIGINAL indices to
           use the partial ordering matrix.  NB: The loop for "j" starts from
           ( i ) itself, not from ( i+1 );   this allows us to detect depends
           of a file on itself:
        *)
        if part_order.( perm_global.( i ) ).( perm_global.( j ) ) = 1
        then
          (* Wrong order -- do swap in both "perm"s: *)
          let tmpg = perm_global.( i ) in
          let tmpl = perm_local.(  i ) in
          do{
            perm_global.( i ) := perm_global.( j );
            perm_global.( j ) := tmpg;

            perm_local.(  i ) := perm_local.(  j );
            perm_local.(  j ) := tmpl;

            swc.val := swc.val + 1
          }
        else
          () (* No swap *)
      };
      swc.val
    }
  in
  (* "sort_iter":
     Repeat "sort_pass" from the same position "i" if swaps were encountered
     during the previous pass; guard for infinite loops:
  *)
  let rec sort_iter: int -> int -> int =
  fun i cum_swc ->
    if  cum_swc > max_swaps
    then
      failwith( "ERROR: Cyclical dependency in the build rules" )
    else
      let swc = sort_pass i in
      if  swc = 0
      then
        cum_swc (* No more swaps, all done *)
      else
        (* Another pass from the same "i": *)
        sort_iter i ( cum_swc + swc )
  in
  do{
    (* Sorting top level. Invoke "sort_iter" for each initial position "i": *)
    let cum_swc = ref 0 in
    for i = 0 to ( m-2 )
    do{
      cum_swc.val := sort_iter i cum_swc.val
    };

    (* The result: *)
    Array.init ( Array.length xfiles )
    (
      fun i ->
        if i < m
        then xfiles.( perm_local.( i ) )
        else ts_arr.( i-m )
    )
  };


(*------------------*)
(* NTs & PartOrder: *)
(*------------------*)
(* "mk_ntarr":
   Creates an array of NonTerms:
*)
value mk_ntarr: unit -> array wfile =
fun () ->
  let non_terms: WFSet.t =
    (* Scan all "build_rules": *)
    Hashtbl.fold
    (
      fun targ rule curr_nt ->
      do {
        assert ( List.mem targ rule.R.targs );
        (* Scan the targ and all srcs within a rule, to get all its files: *)
        let wfs0 =
          List.map
            ( fun targ ->
              {
                wf_file  = targ;
                wf_where = rule.R.targ_where;
                wf_term  = False
              }
            )
            rule.R.targs
        in
        let wfs1 =
          List.map
            ( fun src ->
              {
                wf_file  = src.R.src_file;
                wf_where = src.R.src_where;
                wf_term  = src.R.src_term
              }
            )
            rule.R.srcs
        in
        let wfs = wfs0 @ wfs1 in

        List.fold_left
        (
          fun curr_nt' cand ->
            if   not cand.wf_term (* Not a Terminal! *)
            then WFSet.add cand curr_nt'
            else curr_nt'
        )
        curr_nt wfs
      }
    )
    R.build_rules WFSet.empty
  in
  (* Return "non_terms" as an array: *)
  Array.of_list ( WFSet.elements non_terms );


(* "mk_part_order":
   Returns:
   ==> the array of NonTerms;
   ==> the Reverse Lookup Map ( to obtain the Non-Term pos in the array by its
       name );
   ==> the Partial Order Matrix:
*)
value mk_part_order:
  bool ->
  ( ( array wfile ) * ( Hashtbl.t U.file int ) * ( array ( array int ) ) ) =
fun with_full_deps  ->
  let ntarr: array wfile = mk_ntarr ()               in
  let n    : int = Array.length ntarr                in

  (* "rmap": Inverse mapping of file entries to their array indices: *)
  let rmap : Hashtbl.t U.file int = Hashtbl.create n in

  (* Partial order matrix for "ntarr": *)
  let part_order: array ( array int ) = Array.make_matrix n n 0 in
  do{
    (* Fill in the "rmap": *)
    Array.iteri
    (
      fun ( i: int ) ( nt: wfile ) ->
      do{
        (* Install the "rmap" entry: *)
        Hashtbl.add rmap nt.wf_file i
      }
    )
    ntarr;

    (* Traverse the "ntarr", filling in the "part_order" matrix: *)
    Array.iteri
    (
      fun ( i: int ) ( nt: wfile ) ->
        (* Get the rule which builds "nt" -- missing rules are tolerated unless
           in the final mode:
        *)
        let msg  = "D.mk_part_order: "^ nt.wf_where
        in
        let rule =
          try  R.get_rule nt.wf_file msg
          with [ _ when not with_full_deps -> R.empty_rule ]
        in
        if  rule = R.empty_rule
        then
          () (* Just slip a non-existing rule *)
        else
        do{
          assert ( Hashtbl.find rmap nt.wf_file = i );

          (* Scan all srcs of this "rule". If the "src" appears to be a
             Non-Term, install it in the partial ordering under construction so
             that it will precede the "nt":
          *)
          List.iter
          (
            fun src ->
              let swf =
              {
                wf_file  = src.R.src_file;
                wf_where = src.R.src_where;
                wf_term  = src.R.src_term
              }
              in
              if Hashtbl.mem rmap swf.wf_file
              then
                (* This "swf" is indeed a "NonTerm": *)
                let j = Hashtbl.find rmap swf.wf_file in
                do{
                  part_order.( j ).( i ) := -1;
                  part_order.( i ).( j ) :=  1
                }
              else ()
          )
          rule.R.srcs
        }
    )
    ntarr;

    (* The result: *)
    ( ntarr, rmap, part_order )
  };


(*--------------------*)
(* Sorting Srcs:      *)
(*--------------------*)
(* Within each rule, "srcs" also need to be sorted -- same way as Non-Terms
   are globally sorted. This is because, e.g., in a command producing CMXA
   from CMX files, the srcs must also come in the topological order.
   NB: This is only done for targs in "ntarr" -- unused rules are simply
   ignored:
*)
value sort_rule_srcs:
  bool -> array wfile -> Hashtbl.t U.file int -> array ( array int ) -> unit =

fun with_full_deps ntarr rmap part_order ->
  Array.iter
  (
    fun nt ->
      (* NB: Missing "rule" is allowed unless in the "full_deps" mode: *)
      let rule      =
        let msg     = "D.sort_rule_srcs: "^nt.wf_where                in
        try  R.get_rule nt.wf_file msg
        with [ _ when not with_full_deps -> R.empty_rule ]
      in
      if  rule = R.empty_rule
      then
        ()  (* No "rule" -- nothing to do *)
      else
        let get_file  = fun src -> src.R.src_file                     in
        let srcs_arr  = Array.of_list rule.R.srcs                     in
        let srcs_arr' = stable_sort rmap get_file part_order srcs_arr in
        let srcs'     = Array.to_list srcs_arr'                       in
        let rule'     = { ( rule ) with R.srcs = srcs' }              in
        R.install_rule rule'
  )
  ntarr;


(*--------------------*)
(* Constructing Roots *)
(*--------------------*)
(* Mark the rows ( i ) in "part_order" which do not contain "-1", that is,
   the "i"th original non-terminal is not smaller than any others, i.e.
   they are the "roots" -- ultimate Non-Terminals, as opposed to "leaves"
   which are Terminals:
*)
value mk_roots: array wfile -> array ( array int ) -> list U.file =

fun ntarr part_order ->

  let n = Array.length part_order  in
  let inds_lst: list int  = Array.to_list( Array.init n ( fun i -> i ) ) in
  (* simply [ 0..( n-1 ) ] *)

  let root_inds: list int =
  (
    List.filter
    (
      fun i ->
        let not_smaller =
          List.for_all ( fun v -> v <> -1)( Array.to_list part_order.( i ) )
        in
        (* XXX: Do not classify "elementary" files -- OBJ, CMI, CMO and CMX --
           as roots: due to various limitations, this may result in lost depen-
           dencies after "extraneous" roots are eliminated:
        *)
        if not not_smaller
        then
          False
        else
          (* Check that it's not an "elementary" type: *)
          let   file = ntarr.( i ).wf_file in
          match file with
          [ U.CMI _ | U.CMO _ | U.CMX _ | U.OBJ _ -> False
          |       _                               -> True ]
    )
    inds_lst
  )
  in
  (* NB: "res_roots" do not depend on the topological sorting -- only on
     the "part_order"; internal ordering of roots is irrelevant:
  *)
  List.map ( fun r -> ntarr.( r ).wf_file ) root_inds;


(*--------------------*)
(* Extraneous Roots:  *)
(*--------------------*)
(* The Roots which are not valid top-level files ( currently COBJ, CMXS,
   EXE and DLL ) are to be eliminated along with their rules. Do so re-
   cursively until no more eliminations are done. Returns the remaining
   VALID roots:
*)
value rec elim_extra_roots:
  bool ->
  ( ( array wfile ) * ( Hashtbl.t U.file int ) * ( array ( array int ) ) *
    ( list U.file ) )
=
fun with_full_deps ->
  let ( ntarr, rmap, part_order ) = mk_part_order  with_full_deps in
  let all_roots                   = mk_roots ntarr part_order     in

  (* There must be at least one root, otherwise there are cyclical
     dependencies in the rules:
  *)
  if all_roots = []
  then
    failwith "ERROR: No Root Non-Terms: Cyclical Dependecies???"
  else
    (* Split the roots into valid and extraneous: *)
    let extra_roots =
      List.filter
      (
        fun root ->
          match root with
          [ U.COBJ _ | U.CMXS _ | U.EXE _ | U.DLL _ | U.LIB _ -> False
          | _                                                 -> True ]
      )
      all_roots
    in
    if extra_roots = []
    then
      (* All done, all curr roots are valid:    *)
      ( ntarr, rmap, part_order, all_roots )
    else
    do{
      (* Eliminate the rules for "extra_roots": *)
      List.iter
      (
        fun eroot ->
        do{
          if C.ocmake_verbose ()
          then
            U.ocmake_out stdout ( Printf.sprintf "Eliminating the rule for %s\n"
                                ( U.string_of eroot ) )
          else ();
          Hashtbl.remove R.build_rules eroot
        }
      )
      extra_roots;

      (* New iteration: *)
      elim_extra_roots with_full_deps
    };


(*--------------------*)
(* Propagating Pkgs:  *)
(*--------------------*)
(* Propagates the pkg into from srcs to targets in the dependency graph: *)

value add_pkg: R.attr_where -> list R.attr_where -> list R.attr_where =
fun pkg pkgs ->
  (* O(N) membership test: never mind, the list is typically short here: *)
  if List.mem pkg pkgs
  then pkgs
  else [ pkg :: pkgs ];


value propagate_pkgs: U.file -> unit =
fun curr ->
  (* The info is to be installed in the "curr" from its srcs. Get the rule for
     the "curr" ( must be present,  "curr" is a non-term ):
  *)
  let rule0 = R.get_rule curr "D.propagate_pkgs" in

  (* Go through all srcsi and make a union of their own and transitive pkg: *)
  let pkgs0 =
    List.fold_left
    (
      fun pkgs1 src ->
        (* Make the union of pkgs listed directly for this "src":  *)
        let pkgs2 =
          List.fold_left
            ( fun pkgs3 pa -> add_pkg pa pkgs3 ) pkgs1 src.R.src_pkgs
        in
        (* Add pkgs from the build rule for this "src", if exists: *)
        let rule1 =
          try  R.get_rule src.R.src_file "D.propagate_pkgs"
          with [ _ -> R.empty_rule ]
        in
        List.fold_left
          ( fun pkgs4 pa -> add_pkg pa pkgs4 ) pkgs2 rule1.R.targ_pkgs
    )
    [] rule0.R.srcs
  in
  (* Install the resulting set of pkgs in "rule0". NB: they have been accumed
     in the reverse order:
  *)
  let rule2 = { ( rule0 ) with R.targ_pkgs = List.rev pkgs0 } in
  R.install_rule  rule2;


(*--------------------*)
(* Rules Topol Sort:  *)
(*--------------------*)
(* TOP-LEVEL function of this module. Sort "non-terminals" according to the
   partial ordering imposed by "R.build_rules". Return an array of non-terms
   in a topologically ascending order, and a list of all "root" non-terminals
   ( order is irrelevant ):
*)
value rules_topol_sort: bool -> ( array U.file * list U.file ) =
fun with_full_deps ->
do{
  (* If requested, update the "build_rules" from external dependencies: *)
  if with_full_deps
  then
    R.install_external_deps ()
  else ();

  (* Eliminate extraneous roots and retain valid ones:      *)
  let ( ntarr, rmap, part_order, valid_roots ) =
    elim_extra_roots with_full_deps                           in

  (* Perform stable sort of "ntarr":  *)
  let get_file = fun nt -> nt.wf_file                         in
  let ntarr'   = stable_sort rmap get_file part_order ntarr   in
  let ntarr''  = Array.map   get_file ntarr'                  in
  do{
    (* Sort srcs in all rules:        *)
    sort_rule_srcs with_full_deps ntarr rmap part_order;

    (* If "with_full_deps" is set, also propagate pkg info: *)
    if with_full_deps
    then Array.iter propagate_pkgs ntarr''
    else ();
      
    (* The result:                    *)
    ( ntarr'', valid_roots )
  }
};


(*--------------------*)
(* For debugging only *)
(*--------------------*)
value print_rules_ord: out_channel -> array U.file -> unit =
fun outch targs ->
do{
  Array.iter
  (
    fun targ ->
      let rule = R.get_rule targ "D.print_rules_ord" in
      if  rule.R.command = C.empty_command
      then
        (* This is not ""; this specifically means that such a rule is to be
           skipped:
        *)
        ()
      else
      do{
        assert ( List.mem targ rule.R.targs );
        Printf.fprintf outch "%s\n" ( R.string_of rule )
      }
  )
  targs;
  Printf.fprintf outch "\n";
  flush outch
};

