(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                             Config Signature:                             *)
(*            Command-Line Settings and Parsing of the Build Language        *)
(*===========================================================================*)
(*--------------------*)
(* Command Line Modes *)
(*--------------------*)
(* "ocmake" can be invoked in 3 modes:

 --config : only creates the Makefile for any of the following Modes;
 --build  : build the whole project tree;
 --clean  : clean-up;
 --debug  : prints the graph of dependencies and the attributes of all srcs and
            targets;
 --verbose: more verbose output ( in conjuction with --build )
*)
type ocmake_mode =
[
  ConfigMode
| BuildMode
| InstallMode
| CleanMode
];

(* Current "ocmake" mode: *)
value ocmake_mode   : unit -> ocmake_mode;
value ocmake_debug  : unit -> bool;
value ocmake_verbose: unit -> bool;


(*--------------------*)
(* Conf File Parsing: *)
(*--------------------*)
value parse_conf_file: string -> L.build_conf;

(*--------------------*)
(* Misc utils:        *)
(*--------------------*)
(* The string which, when used as a build command, indicates that the corresp
   build rule si to be skipped altother:
*)
value empty_command : string;

(* Some useful commands ( UNIX-platform settings ): *)
value cp            : string;
value mv            : string;
value rm            : string;
value touch         : string;
value install       : string;
value mkdir         : string;
value make          : string;
value ar            : string;

