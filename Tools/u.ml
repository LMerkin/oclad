(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                                Path Utils:                                *)
(*         XXX: Currently, all values of this module are exported:           *)
(*===========================================================================*)
open ExtString;

(* Global replacement of substrings, e.g. in path rewriting: *)
value rec replace_all: string -> string -> string -> string =
fun str sub by ->
  let ( did, res ) = String.replace ~str:str ~sub:sub ~by:by in
  if    did
  then  replace_all res sub by
  else  res;


(* Logging:
   If the log file is enabled, all stdout and stderr output is copied into this
   file ( and may or may not be displayed on the console as well ):
*)
value ocmake_log_ch    : ref ( option out_channel ) = ref None;
value ocmake_quiet_ref : ref bool                   = ref False;


value open_log  : string -> unit =
fun log_file ->
  ocmake_log_ch.val := Some ( open_out log_file );


value close_log : unit   -> unit =
fun () ->
  match ocmake_log_ch.val with
  [
    Some log_ch ->
    do{
      try close_out log_ch with [ _ -> () ];
      ocmake_log_ch.val := None
    }
  | _           -> ()
  ];


value ocmake_out:
      out_channel -> ?quiet:bool -> ?do_flush:bool   -> string -> unit =

fun   orig_ch  ?( quiet = False )   ?( do_flush = False )  msg ->
do{
  (* Output the line on the "orig_ch" if it's stderr, otherwise do so unless we
     are in the quiet mode. NB: quiet mode has nothing to to with the verbosty
     of the output -- both "quiet" and "verbose" can be set,  in this case the
     ( verbose ) output is sent to th elog file only:
  *)
  if ( ( not ocmake_quiet_ref.val ) && ( not quiet ) ) || ( orig_ch = stderr )
  then
  do{
    output_string orig_ch msg;
    if do_flush then flush orig_ch else ()
  }
  else ();
  (* If the log file is enabled, put the msg into it: *)
  match ocmake_log_ch.val with
  [
    Some log_ch ->
    do{
      output_string log_ch msg;
      if do_flush then flush log_ch else ()
    }
  | _ -> ()
  ]
};


(* Getting all keys of a Hash Table. The order of the keys is irrelevant: *)
value keys: Hashtbl.t 'k 'v -> list 'k =
fun ht ->
  Hashtbl.fold ( fun key _ keys -> [ key::keys ] ) ht [];


(* The ADT of file paths classified wrt their recognised extensions. The arg
   of each ctor is the file path without the siffix.
   Arg: path ( rel or abs ) to the file incl its basename but w/o the suffix:
*)
type file =
[
  MLI  of string  (* OCaml module interface              *)
| ML   of string  (* OCaml module                        *)
| MLL  of string  (* OCamlLex                            *)
| MLY  of string  (* OCamlYACC                           *)
| MLP  of string  (* OCaml src for custom pre-processing *)
| CMI  of string  (* Compiled interface                  *)
| CMO  of string  (* Byte-code compiled OCaml            *)
| CMX  of string  (* Native    compiled OCaml            *)
| CMA  of string  (* Byte-code archive                   *)
| CMXA of string  (* Native    archive                   *)
| CMXS of string  (* DLL plugin                          *)
| COBJ of string  (* Compound top-level native OCaml OBJ *)
| H    of string  (* C/C++  Header                       *)
| C    of string  (* C   comp unit                       *)
| CPP  of string  (* C++ comp unit                       *)
| ASM  of string  (* ASM file for C compilation          *)
| OBJ  of string  (* Object                              *)
| LIB  of string  (* Library                             *)
| EXE  of string  (* Native executable                   *)
| DLL  of string  (* General DLL                         *)
];

(* "string_of":
   "file" ADT to a "string" path:
*)
value string_of: file -> string =
fun f ->
  match f with
  [
    MLI  base -> base ^ ".mli"
  | ML   base -> base ^ ".ml"
  | MLL  base -> base ^ ".mll"
  | MLY  base -> base ^ ".mly"
  | MLP  base -> base ^ ".mlp"
  | CMI  base -> base ^ ".cmi"
  | CMO  base -> base ^ ".cmo"
  | CMX  base -> base ^ ".cmx"
  | CMA  base -> base ^ ".cma"
  | CMXA base -> base ^ ".cmxa"
  | CMXS base -> base ^ ".cmxs"
  | COBJ base -> base ^ ".cobj"
  | H    base -> base ^ ".h"
  | C    base -> base ^ ".c"
  | CPP  base -> base ^ ".cpp"
  | ASM  base -> base ^ P.asm
  | OBJ  base -> base ^ P.obj
  | LIB  base -> base ^ P.lib
  | EXE  base -> base ^ P.exe
  | DLL  base -> base ^ P.dll
  ];

(* "of_string":
   "file" ADT from a "string" path. If the path does not have any extensions,
   it is considered to be of EXE type ( this allows for portable build langu-
   age exprs on both Windows and UNIX ):
*)
value of_string: string -> file =
fun path ->
  let ( base, ext ) =
    try
      let n     = String.length path             in
      let base' = Filename.chop_extension path   in
      let n'    = String.length base'            in

      ( base', String.sub path n' ( n-n') )
    with
    [
      Invalid_argument _ -> ( path, "" )
      (* Other exceptions are propagated *)
    ]
  in
  match ext with
  [
    (* Platform-independent suffixes: *)
    ".mli"  -> MLI  base
  | ".ml"   -> ML   base
  | ".mll"  -> MLL  base
  | ".mly"  -> MLY  base
  | ".mlp"  -> MLP  base
  | ".cmi"  -> CMI  base
  | ".cmo"  -> CMO  base
  | ".cmx"  -> CMX  base
  | ".cma"  -> CMA  base
  | ".cmxa" -> CMXA base
  | ".cmxs" -> CMXS base
  | ".cobj" -> COBJ base
  | ".h"    -> H    base
  | ".c"    -> C    base
  | ".cpp"  -> CPP  base
  | _       ->
    (* Platform-dependent suffixes: *)
    if   ext = P.asm
    then ASM base
    else
    if   ext = P.obj
    then OBJ base
    else
    if   ext = P.lib
    then LIB base
    else
    if   ext = P.exe
    then EXE base
    else
    if   ext = P.dll
    then DLL base
    else
      failwith( "Unrecognised file suffix: "^path )
  ];

(* "project":    *)
value project: file -> file =
fun f ->
  match f with
  [
    MLI  _  -> MLI  ""
  | ML   _  -> ML   ""
  | MLL  _  -> MLL  ""
  | MLY  _  -> MLY  ""
  | MLP  _  -> MLP  ""
  | CMI  _  -> CMI  ""
  | CMO  _  -> CMO  ""
  | CMX  _  -> CMX  ""
  | CMA  _  -> CMA  ""
  | CMXA _  -> CMXA ""
  | CMXS _  -> CMXS ""
  | COBJ _  -> COBJ ""
  | H    _  -> H    ""
  | C    _  -> C    ""
  | CPP  _  -> CPP  ""
  | ASM  _  -> ASM  ""
  | OBJ  _  -> OBJ  ""
  | LIB  _  -> LIB  ""
  | EXE  _  -> EXE  ""
  | DLL  _  -> DLL  ""
  ];

(* Set of files: *)
module FileOrd =
struct
  type  t = file;
  value compare: t -> t -> int = Pervasives.compare;
end;
module FileSet = Set.Make FileOrd;


(* "has_subtype":
   Returns True iff the "file" has the same subtype as the one generated
   by any of the "ctors" ( each ctor may be like "fun x -> ML x" ):
*)
value has_subtype: list ( string -> file ) -> file -> bool =
fun ctors f ->
  List.exists
  (
    fun ctor -> ctor "" = project f
  )
  ctors;


(* "getcwd":
  Returns the CWD with "/" separators:
*)
value getcwd: unit -> string =
fun () ->
  replace_all ( Sys.getcwd () ) "\\" "/";


(* "normalise_path":
   Removes unnecessary path components: "." components are removed and ".."
   ones mapped, if possible, to the previous ones:
*)
value normalise_path: ?dot_to_cwd: bool -> string -> string =
fun ?( dot_to_cwd = False )  path ->

  let path_split = Array.of_list ( String.nsplit path "/" ) in
  let n          = Array.length path_split in
  do {
    assert ( n >= 1 );

    let rec normalise_path_split: int -> int -> string =
    fun  from_pos to_pos ->
      if from_pos  = n
      then
        (* The path is processed: "to_pos" is then the length of the normalised
           path. Merge it back:
        *)
        String.concat "/" ( Array.to_list ( Array.sub path_split 0 to_pos ) )
      else
        let curr = path_split.( from_pos ) in

        if  curr = ".." && to_pos >= 1
        then
          (* The PREVIOUS component will be overwritten by the next one: *)
          normalise_path_split ( from_pos+1 ) ( to_pos-1 )
        else
        if  curr = "."  && from_pos >= 1
        then
          (* An intermediate "." -- skip it:    *)
          normalise_path_split ( from_pos+1 ) to_pos
        else
        if  curr = "."  && from_pos =  0 && dot_to_cwd
        then
        do{
          (* Replace the starting "." with CWD: *)
          path_split.( to_pos ) := getcwd ();
          normalise_path_split ( from_pos+1 ) ( to_pos+1 )
        }
        else
        do {
          (* Generic case: *)
          path_split.( to_pos ) := curr;
          normalise_path_split ( from_pos+1 ) ( to_pos+1 )
        }
    (* End of "normalise_path_split" *)
    in
    normalise_path_split 0 0
  };


(* "mk_targ_path":
   If "tfile" is not an abs path, it is considered to be relative to the build
   dir ( which is given as "rdir" relative to "build_top",   which is the curr
   dir ); in that case, the result is relative to "build_top" as well:
*)
value mk_targ_path: string -> string -> file =
fun rdir tfile ->
  if   Filename.is_relative tfile
  then
    of_string ( normalise_path ( rdir ^"/"^ tfile ) )
  else
    of_string ( normalise_path tfile );


(* "mk_src_path":
   If "sfile" is not an abs path, we first consider it to be relative to the
   "src_adir". If it is found there, it is SYMLINKED nto the build dir ( for
   simplicity of the build process -- XXX: side-effect!). If there is no src
   file in the "src_adir", it is assumed to be dynamically generated (as a
   target of another rule ).   If "sfile" is an abs path, it will not be sym-
   linked.
   Returns the new "file" and the flag indicating whether it is a Terminal
   ( Leaf, symlinked from the orig src tree ):
*)
value mk_src_path:
  ?no_symlink:bool -> string -> string -> string -> ( file * bool ) =
fun   ?( no_symlink=False ) rdir src_adir sfile  ->
  if  Filename.is_relative sfile
  then
    (* The path is considered to be realtive to the curr src dir ( whose abs
       path is "src_adir" and the path relative to "src_top" is "rdir" ):
    *)
    let sapath = normalise_path ( src_adir ^"/"^ sfile ) in
    if  Sys.file_exists sapath
    then
      (* IMPORTANT: "sfile" may contain a ( presumably relative )   path on its
         own, but that path WILL BE DROPPED for the symlink destination, so the
         file will be symlinked into "rdir" itself in any case.
         This functioality is useful for creating multiple platform-specific
         versions of the same file, stored in platform-specific sub-dirs. At
         config time, the actual file is selected and symlinked into "rdir":
      *)
      let sbase  = Filename.basename sfile               in
      let trpath = normalise_path ( rdir ^"/"^ sbase )   in
      do{
        (* Copy the file over: *)
        if not no_symlink
        then
        do{
          try Unix.unlink     trpath with [ _ -> () ];
          Unix.symlink sapath trpath
        }
        else ();
        ( of_string trpath, True )
      }
    else
      (* "sfile" does not exist -- we assume it will  be created using the
         FULL REALTIVE PATH -- no mapping to "rdir" itself as above, and no
         symlinking:
      *)
      let trpath = normalise_path ( rdir ^"/"^ sfile )   in
      ( of_string trpath, False )
  else
    (* Absolute path -- still normalise it, no symlinking. It is a terminal
       iff it exists:
    *)
    let sapath        = normalise_path sfile             in
    ( of_string sapath, Sys.file_exists sapath );


(* "is_term":
   Checks whether the given file is a Terminal. The check is only performed
   for srcs in rules generated in th ere-writing process ( for the original
   rules, the flags returned by "mk_src_path" above are used ). The check is
   heuristical and not always accurate ( will return a false negative for
   OS- and Arch-specific files located in subdirs of the main src dirs XXX ):
*)
value is_term: file -> bool =
fun file ->
  let path = string_of file in
  if Filename.is_relative path
  then
    (* It is relative wrt "P.build_top"; check the corresp original src: *)
    let opath = Filename.concat P.src_top path in
    Sys.file_exists opath
  else
    (* Check the abs "path" itself: *)
    Sys.file_exists path;


(* "rm_rec":
   Remove a file, or recursively remove a dir with all its contents. If the
   "ignore_errs" flag is set, the recursive operation continues even if some
   files / subdirs could not be deleted ( e.g. they are still in use ):
*)
value rec rm_rec: ?ignore_errs: bool -> string -> unit =
fun ?( ignore_errs = False ) path ->
  if  Sys.is_directory path
  then
    let conts = Sys.readdir path in
    do{
      (* Recursively remove the dir entries: *)
      Array.iter
      (
        fun entry ->
          let epath = Filename.concat path entry in
          rm_rec ~ignore_errs:ignore_errs  epath
      )
      conts;
      (* Now try to remove the dir itself  : *)
      try  Unix.rmdir path
      with [ _ when ignore_errs -> () ]
    }
  else
    (* A single file -- try to delete it. NB: Do not use "of_string" and
       pattern matching, as there may be files of unrecognised types:
    *)
    try  Sys.remove path
    with [ _ when ignore_errs -> () ];


(* "ensure_dir_exists":
   Creates the dir if it does not exist ( or if it was not a dir ):
*)
value ensure_dir_exists: ?uniq_name:bool -> string -> unit =
fun   ?( uniq_name = False ) dir ->

  let ( path_is_dir, path_exists ) =
  try
    ( Sys.is_directory dir,   True )
  with
    [ Sys_error _ -> ( False, False ) ]
  in
  do{
    if path_exists
    then
      if not path_is_dir
      then
        (* An ordinary file in place of the "dir"; remove it: *)
        Sys.remove dir
      else
      if uniq_name
      then
        (* The "dir" exists, but its name was supposed to be unique: error: *)
        failwith
          ( Printf.sprintf "U.ensure_dir_exists: Dir name not uniq: %s" dir )
      else ()
    else ();

    if not path_is_dir
    then
      (* The "dir" did not exist as a dir -- create it now:   *)
      Unix.mkdir dir 0o755
    else ();

    assert( Sys.is_directory dir );
  };


(* "run":
   Run an external executable and return its stdout as a list of strings
   ( lines without EOL chars ):
*)
value run: ?quiet:bool -> string -> list string =
fun   ?( quiet = False ) cmd     ->
do{
  (* The cmd is invoked via a shell. For any shell, "2>&1" will direct
     stderr into stdout ( which is in turn directed into the "pipe" );
     "cmd" is printed out unless quiet:
  *)
  ocmake_out stdout ~quiet:quiet ( cmd ^"\n" );

  let pipe: in_channel = Unix.open_process_in ( cmd ^" 2>&1" ) in

  (* Read from the pipe until EOF -- this will mean the co-process has
     terminated:
  *)
  let rec read_lines: list string -> list string =
  fun lines ->
    let ( line, ok ) =
      try (  input_line pipe, True )
      with [ End_of_file -> ( "", False ) ]
    in
    if ok
    then
    do {
      (* The line is copied to stdout -- unless quiet:   *)
      ocmake_out stdout ~quiet:quiet ( line ^"\n" );

      (* Read more lines recursively, accumulating them: *)
      read_lines [ line::lines ]
    }
    else
      (* Return the lines received: *)
      lines
  in
  (* Read the lines from the pipe, then close it; NB: "res" contains the lines
     in the reverse order:
  *)
  let res = List.rev ( read_lines [] ) in
  do{
    ocmake_out stdout ~quiet:quiet ~do_flush:True "";

    if ( Unix.close_process_in pipe <> ( Unix.WEXITED 0 ) )
    then failwith ( "ERROR: Invocation of "^cmd^" failed" )
    else res
  }
};


(* "exec":
   Simular to "run", but does not capture the output:
*)
value exec: array string -> unit =
fun args ->
  (* Prepare the command line: *)
  let args' =
    Array.map
    (
      fun arg ->
        let ( prefix, main ) =
          if  arg <> "" && ( arg.[ 0 ] = '/' || arg.[ 0 ] = '-' )
          then
            ( "-", String.slice ~first:1 arg )
          else
            ( "",  arg )
        in
        let main' = replace_all main "\\" "/" in
        prefix ^ main'
    )
    args
  in
  let cmd  = args'.( 0 ) in
  let cmdl = String.concat " " ( Array.to_list args' ) in
  do{
    (* Print out the command line: *)
    print_endline ( "+ "^ cmdl );
    flush stdout;
    try
      (* Create the process:       *)
      let pid = Unix.create_process  cmd args'
                  Unix.stdin Unix.stdout Unix.stderr
      in
      (* Wait for completion:      *)
      let ( pid', status ) = Unix.waitpid [ Unix.WUNTRACED ] pid in
      (* Examine the status:       *)
      if pid' <> pid || status <> Unix.WEXITED 0
      then
        failwith ( "ERROR: Abnormal termination of "^cmd )
      else
        exit 0   (* All done!      *)
    with
    [ Unix.Unix_error err _ _ ->
        failwith ( Printf.sprintf "ERROR: Invocation of %s failed: %s\n"
                   cmd ( Unix.error_message err ) )
    ]
  };


(* "mk_deps":
   Runs "ocamldep" on the specified list of files, and fills in the hash table
   ( Arg 3 ) with the dependencies found:
*)
module StrSet = Set.Make String;

value mk_deps:
  ?quiet:bool -> string -> list file -> Hashtbl.t file ( list file ) -> unit =

fun ?( quiet=False ) pp   files deps ->
  (* Construct the command line. Since the files for which the dependencies are
     constructed, are typically located not in the current dir,  -I options for
     "ocamldep" are required:
  *)
  let incls: StrSet.t =
    List.fold_left
    (
      fun curr file ->
        let dir    = Filename.dirname ( string_of file ) in
        StrSet.add ( "-I "^dir ) curr
    )
    StrSet.empty files
  in
  (* The "cmd" may contain the pre-processor invocation: *)
  let opts = String.concat " " ( StrSet.elements incls    ) in
  let args = String.concat " " ( List.map string_of files ) in
  let cmd  =
    if pp <> ""
    then Printf.sprintf "ocamldep.opt -slash -pp \"%s\" %s %s" pp opts args
    else Printf.sprintf "ocamldep.opt -slash %s %s"               opts args
  in
  let out  = run ~quiet:quiet cmd in

  (* Some lines are terminated by "\\", merge them: *)
  let lines =
    List.rev
    (
      List.fold_left
      (
        fun curr raw_line ->
          let   line = String.strip raw_line in
          match curr with
          [
            []       ->  [ line ]
          | [ h::t ] ->
              if   String.ends_with h "\\"
              then [ ( ( String.rchop h ) ^ line ) :: t ]
              else [ line::curr ]
          ]
      )
      [] out
    )
  in
  (* Now parse the "lines" and fill in the hash table: *)
  List.iter
  (
    fun line ->
    try
      let ( raw_targ, raw_srcs ) =
        if String.ends_with line ":"
        then
          (* Special case: empty dependencies. Treat it separately in order
             not to confuse the parsing process:
          *)
          ( String.slice ~last:( -1 ) line, "" )
        else
          String.split  line    ": "
      in
      let str_targ               = String.strip  raw_targ     in
      let str_srcs               = String.strip  raw_srcs     in
      let lst_srcs               = String.nsplit str_srcs " " in
      let targ                   = of_string ( str_targ )     in
      let new_srcs =
        List.map ( fun rs -> of_string ( String.strip rs ) ) lst_srcs
      in
      (* Store the mapping targ => srcs in the hash table, amending existing
         mapping if any:
      *)
      let old_srcs =
        if   Hashtbl.mem  deps targ
        then Hashtbl.find deps targ
        else []
      in
      Hashtbl.replace deps targ ( old_srcs @ new_srcs )
    with
      [ _ -> failwith ( "ERROR: Invalid ocamldep output line:\n" ^ line ) ]
  )
  lines;


(* "find_in_path":
   Find the directory ( from the PATH environment ) containing a given file.
   The result always has UNIX-style ( "/" ) path separators:
*)
value find_in_path: string -> string =
fun   file ->
  (* The "file" must itself be just a filename, no dir components: *)
  if  Filename.basename file <> file
  then
    failwith ( "ERROR: U.find_in_path: Not a plain file name: "^file )
  else
  (* Split the PATH according to the OS conventions: *)
  let path =
    try
      Sys.getenv "PATH"
    with
    [ Not_found -> failwith "ERROR: U.find_in_path: PATH env var not set" ]
  in
  let  path_sep = ":" in
  let  path_split = String.nsplit path path_sep in
  let  pdir =
    try
      List.find
        ( fun dir -> Sys.file_exists ( Filename.concat dir file ) )
        path_split
    with
    [ Not_found ->
      failwith ( "ERROR: U.find_in_path: Not Found: "^ file )
    ]
  in
  (* Make the result to follow UNIX-style separators: *)
  replace_all pdir "\\" "/";


(* "read_term_file":
   Read in the content of a file, remove comments from it, return the remaining
   string (presumably representing an OCaml term):
*)
value comms_re       =
  Pcre.regexp ~study:True ~flags:[ `DOTALL ] "\\(\\*.*?\\*\\)";

value read_term_file: string -> string =
fun fname ->
  let inch =
    try open_in_bin fname
    with[ _ -> failwith ( "ERROR: Cannot read file: "^fname ) ]
  in
  try
    let len  = in_channel_length inch in
    let cont = String.create len      in
    do{
      really_input inch cont 0 len;
      close_in inch;
      Pcre.replace ~rex:comms_re ~templ:"" cont
    }
  with
  [ _ ->
    do{
      close_in_noerr inch;
      failwith ( "ERROR: Cannot read file: "^fname )
    }
  ];

