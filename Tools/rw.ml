(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                              Rules Re-Writing:                            *)
(*===========================================================================*)
(* If a rule compiles ML -> CMO or ML -> CMX, and there is no corresp MLI file,
  then this rule also produces the CMI; otherwise, CMI is produced by separate
  MLI compilation.
  The following function amends the "targs" list in this case. NB: multiple ML
  srcs in a rule are allowed, since this function can be invoked in the middle
  of rule re-writing:
*)
value maybe_also_cmi: list U.file -> list R.build_src -> list U.file =
fun targs srcs ->
  List.fold_left
  (
    fun curr src ->
      match src.R.src_file with
      [
        U.ML base ->
          (* So: this is an ML src. Is there the corresp MLI File? *)
          if Sys.file_exists ( U.string_of ( U.MLI base ) )
          then
            (* The MLI already exists, nothing to do: the CMI will be created
               in the ordinary way:
            *)
            curr
          else
            (* Add the CMI target for this ML. The order of targs is not
               important:
            *)
            let  cmi = U.CMI base in
            if   not ( List.mem cmi curr )
            then [ cmi :: curr ]
            else curr

      | _     -> curr  (* Not an ML src *)
      ]
  )
  targs srcs;


(* In addition to the explicit build rules constructed, there are multiple
   implicit ones. E.g.:
   -- If a CMXA / CMA is built from other CMXAs / CMAs, it is is fact built
      from the components of those CMXAs / CMAs;
   -- convert all MLL, MLY, MLP files into ML;
   -- if a CMXA lib is built from MLI, ML, MLL, MLY and/or MLP srcs, there
      are implicit rules about compiling   MLI->CMI, ML->CMX,  and finally
   -- linking all CMX files into CMXA;
   -- implicit generation of CMI files ( see "maybe_also_cmi" above );
   --  ...
   So rewrite the rules. XXX: the following "rewrite" functions have a side-
   effect of updating "R.build_rules":
*)
value rewrite_rule_once: bool -> U.file -> bool =
fun final targ ->
  (* Get the build rule for this "targ". If it is missing and we are not in
     the "final" mode, simply ignore it and return False -- no re-writing:
  *)
  let rule =
    try  R.get_rule targ "Rw.rewrite_rule"
    with [ _ when not final -> R.empty_rule ]
  in
  if  rule = R.empty_rule
  then
    False
  else
  (* The flag indicating whether any re-writing or rule addition has taken
     place. XXX: sorry, an updatable var:
  *)
  let rw: ref bool = ref False in

  (* What kind of targ is it? Is it "compound" ( CMXA/CMA/EXE/DLL/... )? *)
  let cmxa_targ = match targ with [ U.CMXA _ -> True | _ -> False ] in
  let cmxs_targ = match targ with [ U.CMXS _ -> True | _ -> False ] in
  let cobj_targ = match targ with [ U.COBJ _ -> True | _ -> False ] in
  let exe_targ  = match targ with [ U.EXE  _ -> True | _ -> False ] in
  let dll_targ  = match targ with [ U.DLL  _ -> True | _ -> False ] in
  let cma_targ  = match targ with [ U.CMA  _ -> True | _ -> False ] in
  let lib_targ  = match targ with [ U.LIB  _ -> True | _ -> False ] in

  (* OCaml object targs: *)
  let cmx_targ  = match targ with [ U.CMX  _ -> True | _ -> False ] in
  let obj_targ  = match targ with [ U.OBJ  _ -> True | _ -> False ] in
  let cmo_targ  = match targ with [ U.CMO  _ -> True | _ -> False ] in

  (* Other targs:        *)
  let ml_targ   = match targ with [ U.ML   _ -> True | _ -> False ] in

  let compound_targ =
    cmxa_targ || cmxs_targ || cobj_targ || cma_targ || exe_targ || dll_targ ||
    lib_targ
  in
  (* Components of the new rule: *)
  let ( new_tattrs, new_srcs ) =
    List.fold_left
    (
      fun ( new_tattrs', new_srcs' ) src ->
        (*--------------------*)
        (* "trans_arch_rule": *)
        (*--------------------*)
        (* Applies when "targ" is a CMA/CMXA, and the "src" is such as well.
           Then the rule to build the "src" is found and its own 2nd-order
           srcs inserted instead of the original 1st-order "src" in the rule
           to build the "targ". The attrs of the 1st-order "src" are ignored
           ( should be empty anyway, since it's an archive ):
        *)
        let trans_arch_rule: unit -> ( list L.targ_attr * list R.build_src ) =
        fun () ->
          let msg      = "Rw.rewrite_rule: " ^ src.R.src_where in
          let src_rule = R.get_rule src.R.src_file msg         in

          (* NB: target attrs of "src_rule" are PREpended to those of the
             original "targ". Return the combines lists of attrs and srcs:
          *)
          (
            src_rule.R.targ_attrs @ new_tattrs',
            new_srcs'             @ src_rule.R.srcs
          )
        in
        (*--------------------*)
        (* "trans_src_rule":  *)
        (*--------------------*)
        (* Applies when the original "src" is to be replaced ( for the "targ"
           under consideration ) by a "new_src".    The rule to generate the
           "new_src" is inferred and added, the modified attrs and srcs of the
           original rule ( for the "targ" ) returned:
        *)
        let trans_src_rule: U.file -> ( list L.targ_attr * list R.build_src ) =
        fun new_src ->
          let inferred_where = src.R.src_where^"( inferred )" in

          (* Generate "new_src_srcs": the srcs of the "new_src". They typically
             include the original "src" which is thus trabsferred to the rule
             for the "new_src" ( from the rule for the "targ" ):
          *)
          let new_src_srcs =
            match new_src with
            [
              U.CMX base 
            | U.CMO base ->
                (* Check if the corresp MLI exists, in that case "new_src" will
                   also depend on its CMI. NB: MLI files are never auto-genera-
                   ted, so this test is always valid.  CMI files typically have
                   no attrs ( but the corresp MLI may have them ):
                *)
                if Sys.file_exists ( U.string_of ( U.MLI base ) )
                then
                  let cmi_src   =
                  {
                    R.src_file  = U.CMI base;
                    R.src_attrs = [];
                    R.src_aattrs= [];            (* Later! *)
                    R.src_pkgs  = [];            (* Later! *)
                    R.src_where = inferred_where;
                    R.src_term  = False          (* CMI is never a Term! *)
                  }
                  in [ cmi_src; src ]
                else
                  [ src ]

            | U.OBJ base ->
                (* Check for the corresp H -- if exists, add it as well. XXX:
                   In that case, we assume it to be a Terminal. Attrs are not
                   needed since H files are not compiled on theor own:
                *)
                let h_file      =  U.H base in
                if Sys.file_exists ( U.string_of h_file )
                then
                  let h_src     =
                  {
                    R.src_file  = h_file;
                    R.src_attrs = [];
                    R.src_aattrs= [];                  (* Later! *)
                    R.src_pkgs  = [];                  (* Later! *)
                    R.src_where = inferred_where;
                    R.src_term  = U.is_term h_file     (* Usually True     *)
                  }
                  in [ h_src; src ]
                else
                  [ src ]

            | _ ->
              (* All other cases: "new_src" just depends on the old "src": *)
              [ src ]
            ]
          in
          (* Install the rule to generate "new_src" as yet another target.
             NB: No new targ attrs, but global ones may apply:
          *)
          do{
            let new_rule =
            {
              R.targs      = maybe_also_cmi [ new_src ] new_src_srcs;
              R.targ_attrs = [];
              R.targ_aattrs= [];                       (* Later! *)
              R.targ_pkgs  = [];                       (* Later! *)
              R.targ_where = inferred_where;
              R.srcs       = new_src_srcs;
              R.command    = ""                        (* Later! *)
            }
            in
            do{
              R.install_rule new_rule;
              rw.val := True
            };
            (* To insert "new_src" into the list of srcs for the target: since
               "new_src" is already "compiled" compared to the old one,  there
               are typically no attrs or pkgs installed here:
            *)
            (
              new_tattrs',
              new_srcs' @ [ { R.src_file   = new_src;
                              R.src_attrs  = [];
                              R.src_aattrs = [];         (* Later! *)
                              R.src_pkgs   = [];         (* Later! *)
                              R.src_where  = inferred_where;
                              R.src_term   = U.is_term new_src
                          } ]
            )
          }
        in
        (*--------------------*)
        (* Actual re-writing: *)
        (*--------------------*)
        (* For the current "src" of "targ", generate new attrs and new srcs.
           NB: there are MYRIADS of invalid src/targ combinations. They are
           mostly NOT checked here -- but only when the actual build cmds are
           generated by the back-end:
        *)
        match src.R.src_file with
        [
          U.CMXS _ | U.COBJ _ | U.DLL _ ->
            (* NB: CMXS, COBJ and DLL files are intended as ultimate "targ"s
               only. They are not for structuring the build process. However,
               EXE files are different, since they may be used as generators
               of other files:
            *)
            failwith( "ERROR: "^( U.string_of src.R.src_file )^
                      " can be a top-level target ( Root Non-Term ) only" )

        | U.CMXA  _  when cmxa_targ ->
            trans_arch_rule ()

        | U.CMA   _  when cma_targ ->
            trans_arch_rule ()

        | U.LIB   _  when lib_targ ->
            trans_arch_rule ()

        | U.MLL base when ( not ml_targ ) ->
            (* Generation of ML from MLL/MLY/MLP: *)
            trans_src_rule ( U.ML base )

        | U.MLY base when ( not ml_targ ) ->
            trans_src_rule ( U.ML base )

        | U.MLP base when ( not ml_targ ) ->
            trans_src_rule ( U.ML base )

        | U.MLI base when ( compound_targ || cmx_targ || cmo_targ ) ->
            (* An OCaml Interface file as a src for a library or exec. In fact,
               the "targ" depends on the corresp CMI, so insert the rule for
               CMI. It might eventually happen that the targ does not depend on
               the CMI directly ( if the targ is an archive ), but the CMI rule
               is to be installed in any case:
            *)
            trans_src_rule ( U.CMI base )

        | U.ML base  when compound_targ ->
            (* OCaml src for a library or executable. In fact, the targ depends
               on the corresp CMX or CMO. We only build native executables, so
               e.g. EXE "targ" implies CMX "new_src":
            *)
            let new_src =
              if   cmxa_targ || cmxs_targ || cobj_targ || exe_targ || obj_targ
              then U.CMX base
              else U.CMO base
            in
            trans_src_rule new_src

        | U.CPP base when compound_targ ->
            (* C++/C/ASM src for a library or executable. In fact, the targ
               depends on the corresp OBJ:
            *)
            trans_src_rule ( U.OBJ base )

        | U.C   base when compound_targ ->
            trans_src_rule ( U.OBJ base )

        | U.ASM base when compound_targ ->
            trans_src_rule ( U.OBJ base )

        | _ ->
          (* Any other "targ"/"src": just keep the original "src": *)
          ( new_tattrs', new_srcs' @ [ src ] )
        ]
    )
    ( rule.R.targ_attrs, [] ) rule.R.srcs
  in
  (* Construct a modified rule for the "targ" using the new "tattrs" and
     "srcs", and replace the orig "rule" in the hash table. If the "targ"
     is "compound", explicit dependencies on the specs ( CMI and H ) are
     to be dropped by the "filter" below:
  *)
  let filtered_new_srcs =
    if compound_targ
    then
      List.filter
        ( fun src ->
            match src.R.src_file  with
            [ U.CMI _ | U.H _ -> False
            | _               -> True ]
        )
        new_srcs
    else
      new_srcs
  in
  (* The modified rule -- may actually be identical to the old one: *)
  let rule' =
  {
    ( rule ) with
      R.targs       = maybe_also_cmi rule.R.targs filtered_new_srcs;
      R.targ_attrs  = new_tattrs;
      R.srcs        = filtered_new_srcs
  }
  in
  do{
    if rule' <> rule
    then
    do{
      (* Actually replace the re-written rule in the cache, for all targs: *)
      assert ( List.mem targ rule'.R.targs );
      R.install_rule rule';
      rw.val := True
    }
    else ();

    (* Return the flag: *)
    rw.val
  };
 

(* "rewrite_rule_deep":
   Iterative re-writing of the same rule until a fixed point is reached. Thus,
   re-writing must be depath-first, otherwise inconsistent intermediate rules
   may be created.
   As for "rewrite_rule_once", returns the flag indicating whether at least 1
   re-writing has occurred:
*)
value rewrite_rule_deep: bool -> U.file -> bool =
fun final targ ->
  let rec rewrite_rule_rec: bool -> bool =
  fun did_rw ->
    let did_rw' = rewrite_rule_once final targ in
    if  did_rw'
    then
      (* Do it again: *)
      rewrite_rule_rec True
    else
      (* Return the flag accumulated so far: *)
      did_rw
  in
  rewrite_rule_rec False;


(*--------------------*)
(* Targ & Src AAttrs: *)
(*--------------------*)  
(* "mk_aattrs":
   This function finally installs per-file attrs taken from the user-specified
   ones and the dir environment:
*)
value mk_aattrs: R.build_rule -> R.build_rule =
fun rule ->
  (*-------------*)
  (* Targ attrs: *)
  (*-------------*)
  (* If multiple targs are specified in the same rule, they must all be in the
     same dir:
  *)
  let t_rdirs   =
    List.fold_left
    (
      fun curr targ ->
        let  trd = Filename.dirname ( U.string_of targ ) in
        if   not ( List.mem trd curr )
        then [ trd :: curr ]
        else curr
    )
    [] rule.R.targs
  in
  let t_rdir =
    match t_rdirs with
    [
      []      -> failwith( "ERROR: No targets in rule:\n"^( R.string_of rule ) )
    | [ trd ] -> trd
    | _       -> failwith( "ERROR: Targets in different dirs in rule:\n"^
                         ( R.string_of rule ) )
    ]
  in
  let t_where   = rule.R.targ_where                      in

  (* Get the "aattrs" from the "t_rdir" ( THEY MUST EXIST ) and merge them
     with the explicit ones:
  *)
  let t_aattrs  = R.dir_attrs t_rdir                     in
  let t_aattrs' = List.fold_left ( R.merge_targ_attrs t_where )
                                   t_aattrs rule.R.targ_attrs
  in
  (* From the curr and inherited "t_aattrs'", only one field could actually
     apply to THIS group of targs -- depending on the "master targ" type.
     Currently, the "out_attrs" apply to any COMPOUND targs generated by the
     OCaml compiler:
  *)
  let apply_tattrs: bool =
    List.exists
    (
      fun targ ->
        match targ with
        [
          U.CMA _ | U.CMXA _ | U.CMXS _ | U.COBJ _ | U.EXE _
        | U.DLL _ -> True
        | _       -> False ]
    )
    rule.R.targs
  in
  let  t_res: list R.attr_where =
    if   apply_tattrs
    then t_aattrs'.R.out_attrs
    else []
  in
  (*-------------*)
  (* Src  attrs: *)
  (*-------------*)
  (* Modify all "srcs": *)
  let srcs' =
    List.map
    (
      fun src ->
        let s_file    = src.R.src_file                          in
        let s_rdir    = Filename.dirname ( U.string_of s_file ) in
        let s_where   = src.R.src_where                         in

        (* Get the "aattrs" from the "s_rdir" ( THEY MUST EXIST ) and merge
           them with the explicit ones:
        *)
        let s_aattrs  = R.dir_attrs s_rdir in
        let s_aattrs' = List.fold_left ( R.merge_src_attrs s_where )
                                         s_aattrs  src.R.src_attrs
        in
        (* From "s_aattrs'", only one field could actually apply to THIS src
           -- depending on the latter's type:
        *)
        let s_res: list R.attr_where =
          match s_file with
          [
            U.MLI _
          | U.ML  _ -> s_aattrs'.R.ml_attrs

          | U.ASM _ (* XXX: currently, ASM and C/C++ srcs have same flags *)
          | U.C   _
          | U.CPP _ -> s_aattrs'.R.cxx_attrs

          | _       -> s_aattrs'.R.other_attrs
          ]
        in
        (* Install "s_res" in the "src". NB: We also install the pkgs info;
           it is then propagated to all transitive targets when the rules
           are topologically sorted with full dependencies inferred:
        *)
        { ( src ) with R.src_aattrs = s_res;
                       R.src_pkgs   = s_aattrs'.R.pkg_attrs
        }
    )
    rule.R.srcs
  in
  (* Modified rule: *)
  { ( rule ) with
    R.targ_aattrs = t_res;
    R.srcs        = srcs'
  };

(*===========================================================================*)
(*                                 Top-Level:                                *)
(*===========================================================================*)
value rec rule_rewriter: bool -> array U.file -> unit =
fun final targs ->
  (* Traverse all curr targs, re-writing their rules if necessary; "did_rw"
     indicates whether at least one rule has been re-written or added:
  *)
  let did_rw: bool =
    Array.fold_left
    (
      fun   did_rw' targ ->
        (* Re-write the rule for this "targ" depth-first: *)
        let did_rw'' = rewrite_rule_deep final targ in
        did_rw' || did_rw''
    )
    False targs
  in
  (* If at least one re-writing occurred, do yet another iteration from the
     beginning of "targs":
  *)
  if   did_rw
  then rule_rewriter final targs
  else ();


value rewrite_build_rules: unit -> ( array U.file * list U.file ) =
fun () ->
  (* Get the initial array of Non-Terms, via the initial topol sorting: *)
  let targs0: array U.file = fst ( D.rules_topol_sort False ) in
  do{
    (* Do the initial re-writing of the rules: *)
    rule_rewriter False targs0;

    (* Perform topological sorting of the rules, eliminate extraneous ones, and
       return the new array of Non-Terms. External dependencies are not yet in:
    *)
    let targs1  : array U.file = fst ( D.rules_topol_sort False ) in
    do{
      (* ONLY NOW we can install "aattrs" in the rules. This, in turn, allows
         us to integrate the external dependencies:
      *)
      Array.iter
      (
        fun targ ->
          let rule  = R.get_rule targ "Rw.rewrite_build_rules" in
          let rule' = mk_aattrs rule                           in
          R.install_rule rule'
      )
      targs1;

      (* Sort the rules again, this time invoking external dependencies. In
         particular, this will get the relative order of CMX srcs in CMXA
         rules correct:
      *)
      let res = D.rules_topol_sort True  in
      do{
        (* Do final re-writing: XXX: is it really necessary? *)
        rule_rewriter True ( fst res );
        res
      }
    }
  };
