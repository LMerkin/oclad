(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                    AND RE-WRITING OF THE BUILD RULES:                     *)
(*===========================================================================*)
(* The following function constructs the depndencies between build targs,
   re-writes the build rules ( updating "R.build_rules" in place ) and returns
   the topologically-sorted build targets in the build order:
*)
value rewrite_build_rules: unit -> ( array U.file * list U.file );
