(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*                       Dynamic Config Implementation:                      *)
(*===========================================================================*)
open ExtString;

(*--------------------*)
(* Command Line:      *)
(*--------------------*)
type ocmake_mode =
[
  ConfigMode
| BuildMode
| InstallMode
| CleanMode
];
value ocmake_mode_ref   : ref ocmake_mode = ref ConfigMode;
value ocmake_debug_ref  : ref bool        = ref False;
value ocmake_verbose_ref: ref bool        = ref False;

value string_of: ocmake_mode -> string =
fun mode ->
  match mode with
  [
    ConfigMode  -> "ConfigMode"
  | BuildMode   -> "LibsMode"
  | InstallMode -> "InstallMode"
  | CleanMode   -> "CleanMode"
  ];


(* Externally-visible "ocmake_mode": *)
value ocmake_mode    : unit -> ocmake_mode =
  fun () -> ocmake_mode_ref.val;

value ocmake_debug   : unit -> bool =
  fun () -> ocmake_debug_ref.val;

value ocmake_verbose : unit -> bool =
  fun () -> ocmake_verbose_ref.val;

(* Admissible command-line args: *)
value usage_msg      : string = "PARAMETERS:";

value cmd_args = 
[
  ( "-config",
    Arg.Unit( fun () -> ocmake_mode_ref.val := ConfigMode ),
    "Creates the Makefile for the whole OCaml tree ( DEFAULT MODE )"
  );
  ( "-build",
    Arg.Unit( fun () -> ocmake_mode_ref.val := BuildMode ),
    "Build the whole project"
  );
  ( "-install",
    Arg.Unit( fun () -> ocmake_mode_ref.val := InstallMode ),
    "Install the executables built"
  );
  ( "-clean",
    Arg.Unit( fun () -> ocmake_mode_ref.val := CleanMode ), "Clean-Up"
  );
  ( "-debug",
    Arg.Set ocmake_debug_ref,   "Print various debugging info"
  );
  ( "-v",
    Arg.Set ocmake_verbose_ref, "Verbose build output"
  );
  ( "-q",
    Arg.Set U.ocmake_quiet_ref,
    "Quiet mode ( output to log file only, if enabled )"
  );
  ( "-log",
    Arg.String U.open_log, "Log File"
  )
];

(* Actually parse the command-line args: XXX: this is a side-effiecting
   computation performed at the module init time:
*)
Arg.parse
  cmd_args
  ( fun _ -> do{ Arg.usage cmd_args usage_msg; exit 1 } ) usage_msg;

(*====================*)
(* Set the Env:       *)
(*====================*)
(* From now on, work in "build_top": *)
Sys.chdir P.build_top;

(* Some self-diagnostics: *)
if ocmake_verbose ()
then U.ocmake_out stdout ( Printf.sprintf "\nOCMake: %s, CWD=%s\n\n"
                     ( string_of  ( ocmake_mode () ) ) ( U.getcwd () ) )
else ();

(* Create paths to the OCaml compilers and tools:   *)
value path =
  let ocaml_bin  = P.ocaml_dir^"/bin"  in
  let curr_path  = Sys.getenv  "PATH"  in
  String.concat ":" [ ocaml_bin; curr_path ];

Unix.putenv "PATH" path;

(*--------------------*)
(* Conf Parsing:      *)
(*--------------------*)
(* Recognised expandable vars and their expansions: *)
value expandable_vars: list ( string * string ) =
[
  ( "${TOOLS_TOP}",  P.tools_top    );
  ( "${OCAML_LIB}",  P.ocaml_lib    );
  ( "${SRC_TOP}",    P.src_top      );
  ( "${BUILD_TOP}",  P.build_top    );
  ( "${PKGS}",       P.pkgs_top     );
  ( "${ASM}",        P.asm          );
  ( "${OBJ}",        P.obj          );
  ( "${LIB}",        P.lib          );
  ( "${DLL}",        P.dll          );  (* Currently not used... *)
  ( "${EXE}",        P.exe          )
];

(* NB: "build_conf" files have syntax different from the one parseable by
   automatically-inferred functions on the above types. Some syntax adjust-
   ments are required before parsing:
*)
(* RegExp to install ()s around string literals in 1-arg ctors: *)
value string_re      = Pcre.regexp ~study:True "[A-Z]\\s*\\\".*?\\\"";

(* RegExp to install ()s around lists in 1-arg ctors: *)
value list_re        = Pcre.regexp ~study:True "[A-Z]\\s*\\[.*?\\]";

(* "insert_pars":
   Action on the substrings selected by "string_re" or "list_re":
*)
value insert_pars : string -> string =
fun s ->
  (* "s" is guaranteed to be non-empty here; s.[0] is an upper-case letter,
     after which ()s are installed:
  *)
  ( String.sub s 0 1 )^"("^( String.sub s 1 ( String.length s - 1 ) )^")";


(* "parse_conf_file":
   Given the abs path of a "build.conf" file, this function reads, transforms
   and parses that file, including expansion of variables:
*)
value parse_conf_file: string -> L.build_conf =
fun   conf_path ->
  (* Read the file in, remove the comments from it:    *)
  let conf0  = U.read_term_file conf_path in

  (* Insert ()s around string literals in 1-arg ctors: *)
  let conf1  = Pcre.substitute ~rex:string_re ~subst:insert_pars conf0 in

  (* Insert ()s around list args in 1-arg ctors:       *)
  let conf2  = Pcre.substitute ~rex:list_re   ~subst:insert_pars conf1 in

  (* Expand the std vars. XXX: this is done purely syntactically, without
     checking the semantics of vars expanded:
  *)
  let conf3  =
    List.fold_left
      ( fun conf ( vname, vval ) -> U.replace_all conf vname vval )
      conf2 expandable_vars
  in
  (* Now actually parse the contents: *)
  try
    L.parse_build_conf ( Stream.of_string conf3 )
  with
  [ _ ->
    failwith ( "ERROR: Invalid config syntax: "^conf_path )
  ];

(* "empty_command":
   Still syntactically valid:
*)
value empty_command: string = "#    SKIP";

(* Standard UNIX commands: *)
value cp     : string = "cp";
value mv     : string = "mv";
value rm     : string = "rm -f";
value touch  : string = "touch";
value mkdir  : string = "mkdir -p";
value install: string = "install";
value make   : string = "make";
value ar     : string = P.tools_top ^ "/BinUtils/Current/bin/ar";

