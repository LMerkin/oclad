(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*              Parsing of Build Language into Build Rules:                  *)
(*              Targets, Sources, Directories, Attributes                    *)
(*===========================================================================*)
(*--------------------*)
(* Accumulated Builds *)
(*--------------------*)
(* The result of traversing and parsing of user-provided "build.conf" files.
   For each directory  traversed, the following config info is accumulated:
   -- accumulated attrs ( incl those inherited from upper dirs ); for each
      such attr, memoise its origin for debugging purposes ( "attr_where" );
   -- build rules ( targets and srcs -- converted to absolute paths ):
*)
type where_from = string;
type attr_where = ( string * where_from );   (* Attr value, ConfFileName *)

type aattrs     =
{
  ml_attrs   : list attr_where; (* For ML  and MLI files *)
  pkg_attrs  : list attr_where; (* Ditto                 *)
  cxx_attrs  : list attr_where; (* For C   and CPP files *)
  other_attrs: list attr_where; (* Misc...               *)
  out_attrs  : list attr_where  (* For all OCaml compilation outputs     *)
};

(* Looking up the accumulated attributes for a given directory: *)
value dir_attrs: string -> aattrs;

(* The following functions can also be used externally for merging per-file
   src or targ attrs with per-directory "aattrs":
*)
value merge_src_attrs : where_from -> aattrs -> L.src_attr  -> aattrs;
value merge_targ_attrs: where_from -> aattrs -> L.targ_attr -> aattrs;

(* Build rules: each rule has 1 principal target, although multiple files can
   actually be constructed ( e.g., building CMXA implies a LIB as well ). But
   those secondary targets are always implied by the primary ones, and do not
   need to be accessed separately:
   NB: "where_from" is the location of the corresp "build.conf" file. It is
   associated with each src individually, since srcs are moved across rules
   when the latter are re-written:
*)
type build_src  =
{
  src_file  : U.file;
  src_attrs : list L.src_attr;   (* Local attrs *)
  src_aattrs: list attr_where;   (* All accumulated attrs: global and local  *)
  src_pkgs  : list attr_where;   (* Pkgs required to compile this src        *)
  src_where : where_from;        (* Where this src originally appeared       *)
  src_term  : bool               (* Exists in the orig tree?                 *)
};

(* XXX: Currently, there is exactly 1 build command which implements each
   rule. There are no "phony" targets or rules, so there must be AT LEAST
   1 command. If multiple commands are required to build the target, they
   must be concatenated as a sequence, so that they run on the same shell.
   The "command"s are installed by the back-end:
*)
type build_rule =
{
  targs      : list U.file;      (* Multiple targs can be built by same rule *)
  targ_attrs : list L.targ_attr; (* Local attrs                              *)
  targ_aattrs: list attr_where;  (* All accumulated attrs: global and local  *)
  targ_pkgs  : list attr_where;  (* Accumulated pkgs from dir & trans srcs   *)
  targ_where : where_from;       (* Where this targ and its rule appeared    *)
  srcs       : list build_src;
  command    : string
};

value empty_rule : build_rule;   (* A placeholding constant *)

(* Build rules by target:   *)
value build_rules: Hashtbl.t U.file build_rule;

(* Regexp used to recognise the OCaml pre-processor directives in attrs: *)
value pp_re: Pcre.regexp;

(* Amend existing ( and fully attributed ) rules  by running "ocamldep": *)
value install_external_deps: unit -> unit;

(* Get the rule from "build_rules", fail with err msg if not there :     *)
value get_rule    : U.file -> string -> build_rule;

(* Srcs from a rule, satisfying at least one of the sub-types given by the
   "ctors":
*)
value get_srcs: list ( string -> U.file ) -> build_rule -> list build_src;
value get_src : list ( string -> U.file ) -> build_rule ->      build_src;

(* Install a rule in "build_rules" replacing any previous bindings for itsr
   "targs":
*)
value install_rule: build_rule  -> unit;

(* XXX: For debugging only: *)
value string_of   : ?short:bool -> build_rule -> string;

(* Top-level function: Parses the hierarchy of "build.conf" files, creates
   initial set of "build_rules":
*)
value parse_build_confs: unit -> unit;
