(* vim:ts=2:syntax=ocaml
*)
(*===========================================================================*)
(*              Back-End: Construct and Run the Build Commands:              *)
(*===========================================================================*)
open ExtString;

(*--------------------*)
(* Tools:             *)
(*--------------------*)
(* They are already in the PATH: *)

(* OCaml compilers:   *)
value ocamlc_base   = "ocamlfind ocamlc   -w Ae -warn-error Ae";
value ocamlopt_base = "ocamlfind ocamlopt -w Ae -warn-error Ae";

value ocamlc   =
  let oc1 =
    if not ( C.ocmake_verbose () )
    then ocamlc_base
    else ocamlc_base ^" -verbose"
  in
  let oc2 =
    if P.dr_mode = P.Debug
    then oc1 ^" -g"
    else oc1 ^" -unsafe"
  in
  oc2;

value ocamlopt =
  let oo1 =
    if not ( C.ocmake_verbose () )
    then ocamlopt_base
    else ocamlopt_base ^" -verbose"
  in
  let oo2 =
    if P.dr_mode = P.Debug
    then oo1 ^" -g"
    else oo1 ^" -unsafe"
  in
  oo2;

(* Configs of both compilers: must be the same: *)
value ocaml_conf_b : Hashtbl.t string string = Hashtbl.create 32;
value ocaml_conf_n : Hashtbl.t string string = Hashtbl.create 32;

value mk_ocaml_conf: string -> Hashtbl.t string string -> unit =
fun comp hconf ->
  let conf = U.run ~quiet:True ( comp ^" -config" ) in
  List.iter
  (
    fun line ->
    try
      (* Split the "line" into "key: value": *)
      let ( rkey, rval ) = String.split line ":" in
      let k = String.strip rkey in
      let v = String.strip rval in
      do{
        assert ( not( Hashtbl.mem hconf k ) );
        Hashtbl.add hconf k v
      }
    with
    [ _ -> () ] (* Line not in "key: value" format: just skip it *)
  )
  conf;

(* XXX: side-effecting computation at the module init time: *)
mk_ocaml_conf ocamlc   ocaml_conf_b;
mk_ocaml_conf ocamlopt ocaml_conf_n;

value copt_re: Pcre.regexp = Pcre.regexp ~study:True "-O[0-9]?";
                                                    (* Opt flags for GCC *)

(* We can now get the C, C++ and ASM compilers. The ByteCode and native C
   compilers may have different flags, so use the Native one:
*)
value cc_base =
  let ccn = Hashtbl.find ocaml_conf_n "native_c_compiler"   in

  (* Replace "/" flags with "-"s and add extra flags to "ccn":
     Default include paths to OCaml run-time headers:
  *)
  let ccm  = U.replace_all ccn " /" " -"                  in

  (* Remove any optimisation flags, since they will be different depending
     on the Debug or Release mode:
  *)
  let ccno = Pcre.replace ~rex:copt_re ~templ:"" ccm      in
  let ccd  =
    ccno ^
    (
      match P.dr_mode with
      [
        P.Debug   ->" -O0 -g  -UNDEBUG -DDEBUG"
      | P.Release ->" -O3 -DNDEBUG"
      ]
    )
  in
  (* Extra include paths: to avoid conflicts between the local and global
     OCaml RTS headers,  do NOT automatically include the path to the std
     OCaml headers. So nothing extra for the moment:
  *)
  ccd;

value cxx_base =
  (* We use "gcc" for C and "g++" for C++, and same opts:  *)
  let ( rccomp, rcopts ) = String.split cc_base " " in
  let   ccomp = String.strip rccomp in
  let   copts = String.strip rcopts in
  let cxxcomp =
    if String.ends_with ccomp "gcc"
    then ( String.slice ~first:0 ~last:( -3 ) ccomp ) ^ "g++"
    else failwith ( "Error: Unknown C Compiler: "^ccomp )
  in
  cxxcomp^" "^copts;


value add_verb: string -> string =
fun base ->
  if C.ocmake_verbose ()
  then base ^" -v -Wl,-v"
  else base;

value cc  = add_verb cc_base;
value cxx = add_verb cxx_base;
value asm = cc;   (* XXX! *)


(* Some integrity checks: *)
assert
(
  P.obj           = Hashtbl.find ocaml_conf_b "ext_obj" &&
  P.lib           = Hashtbl.find ocaml_conf_b "ext_lib" &&
  P.obj           = Hashtbl.find ocaml_conf_n "ext_obj" &&
  P.lib           = Hashtbl.find ocaml_conf_n "ext_lib"
);

(*--------------------*)
(*  Utils:            *)
(*--------------------*)
(* Src Types Counter: *)
type scount =
{
  mli  : int; ml   : int; mll  : int; mly  : int; mlp  : int; cmi  : int;
  cmo  : int; cmx  : int; cma  : int; cmxa : int; h    : int; c    : int;
  cpp  : int; obj  : int; lib  : int; cmxs : int; exe  : int; cobj : int;
  dll  : int; asm  : int
};

value count_srcs: R.build_rule -> scount =
fun rule ->
  List.fold_left
  (
    fun scount src ->
      match src.R.src_file with
      [
        U.MLI  _ -> { ( scount ) with mli  = scount.mli  + 1 }
      | U.ML   _ -> { ( scount ) with ml   = scount.ml   + 1 }
      | U.MLL  _ -> { ( scount ) with mll  = scount.mll  + 1 }
      | U.MLY  _ -> { ( scount ) with mly  = scount.mly  + 1 }
      | U.MLP  _ -> { ( scount ) with mlp  = scount.mlp  + 1 }
      | U.CMI  _ -> { ( scount ) with cmi  = scount.cmi  + 1 }
      | U.CMO  _ -> { ( scount ) with cmo  = scount.cmo  + 1 }
      | U.CMX  _ -> { ( scount ) with cmx  = scount.cmx  + 1 }
      | U.CMA  _ -> { ( scount ) with cma  = scount.cma  + 1 }
      | U.CMXA _ -> { ( scount ) with cmxa = scount.cmxa + 1 }
      | U.CMXS _ -> { ( scount ) with cmxs = scount.cmxs + 1 }
      | U.H    _ -> { ( scount ) with h    = scount.h    + 1 }
      | U.C    _ -> { ( scount ) with c    = scount.c    + 1 }
      | U.CPP  _ -> { ( scount ) with cpp  = scount.cpp  + 1 }
      | U.ASM  _ -> { ( scount ) with asm  = scount.asm  + 1 }
      | U.OBJ  _ -> { ( scount ) with obj  = scount.obj  + 1 }
      | U.LIB  _ -> { ( scount ) with lib  = scount.lib  + 1 }
      | U.COBJ _ -> { ( scount ) with cobj = scount.cobj + 1 }
      | U.EXE  _ -> { ( scount ) with exe  = scount.exe  + 1 }
      | U.DLL  _ -> { ( scount ) with dll  = scount.dll  + 1 }
      ]
  )
  {
    mli  = 0; ml   = 0; mll  = 0; mly = 0; mlp = 0; cmi = 0; cmo = 0; cmx = 0;
    cma  = 0; cmxa = 0; cmxs = 0; h   = 0; c   = 0; cpp = 0; obj = 0; lib = 0;
    cobj = 0; exe  = 0; dll  = 0; asm = 0
  }
  rule.R.srcs;


(* "join_attrs":
   Merge the string vals of the accumulated attrs together:
*)
value join_attrs: list R.attr_where -> string =
fun aws ->
  String.concat " " ( List.map fst aws );


(* "join_pkgs":
   Merge the pkg strings:
*)
value join_pkgs : list R.attr_where -> string =
fun pws ->
  String.concat "," ( List.map fst pws );


(* "joing_srcs":
   Merge the string vals of the given srcs together:
*)
value join_srcs: list R.build_src   -> string =
fun srcs ->
  String.concat " " ( List.map ( fun src -> U.string_of src.R.src_file ) srcs );


(* "insert_parsers":
   Insert parser(s) invocation into the OCaml compilation line. Returns:
      ( new_attrs, pp_cmd_line )
   where "pp_cmd_line" is for calling the pre-processor directly:
*)
value insert_parsers:
  R.build_rule -> string -> list string -> ( string * string ) =

fun rule attrs cmo_paths ->
  let all_cmos =  String.concat " " [ "-I ." :: cmo_paths ] in
  let pp_cmdl  =  ref ""                                    in
  let attrs'   =
    Pcre.substitute_substrings ~rex: R.pp_re
    ~subst:
    (
      fun substrs ->
        let n = Pcre.num_of_subs substrs  in
        do{
          assert( n = 2 );

          if   pp_cmdl.val  = ""
          then pp_cmdl.val := ( Pcre.get_substring substrs 1 )^" "^all_cmos
          else failwith( Printf.sprintf
                        "Cannot install the Parser: %s: multiple PP in\n%s"
                         all_cmos ( R.string_of rule ) );

          Printf.sprintf "-pp '%s'" pp_cmdl.val
        }
    )
    attrs
  in
  if pp_cmdl.val <> ""
  then
    ( attrs', pp_cmdl.val )  (* Successfully installed *)
  else
    failwith
    ( Printf.sprintf "Cannot install the Parser: %s: no PP known in\n%s"
                     all_cmos ( R.string_of rule ) );


  (* "all_srcs_attrs":
     Checks whether ths src attrs are appropriate:
  *)
  value all_srcs_attrs: R.build_rule -> bool -> ( string * string ) =
  fun rule with_attrs ->
    let ( srcs_lst, attrs_lst ) =
      List.fold_left
      (
        fun ( curr_srcs, curr_attrs )  src ->
          (
            curr_srcs  @ [ U.string_of ( src.R.src_file ) ],
            curr_attrs @ ( List.map  fst src.R.src_aattrs )
          )
      )
      ( [], [] ) rule.R.srcs
    in
    if ( not with_attrs ) && ( attrs_lst <> [] )
    then
    do{
      (* There must be no src attrs in this rule: *)
      U.ocmake_out stderr ~do_flush:True ( R.string_of rule );
      failwith "ERROR: Unexpected src attrs"
    }
    else
      let str_srcs  = String.concat " " srcs_lst   in
      let str_attrs = String.concat " " attrs_lst  in
      ( str_srcs, str_attrs );


(*--------------------*)
(* OCaml include dirs *)
(*--------------------*)
(* All OCaml files are compiled from the same top dir ( "build_top" ),  so
   we need paths to all underlying dirs in order to be able to include the
   CMI files.
   Similarly, construct C/C++ include dirs from the locations of H files:
*)
module StrSet = Set.Make String;

value include_dirs: array U.file -> ( StrSet.t * StrSet.t ) =
fun targs_sorted ->
  Array.fold_left
  (
    fun  ( ocaml_set, c_set ) targ ->
      (* NB: CMI files are targs but H files are srcs: *)
      match targ with
      [
        U.CMI base ->
        ( StrSet.add ( " -I "^( Filename.dirname base ) ) ocaml_set, c_set )

      | U.OBJ _    ->
        (* Extract the srcs and find teh H files among them: *)
        let rule   = R.get_rule targ "B.include_dirs" in
        let c_set' =
          List.fold_left
          (
            fun c_set'' src ->
              match src.R.src_file with
              [
                U.H base  ->
                  StrSet.add ( " -I "^( Filename.dirname base ) ) c_set''
              | _ -> c_set''
              ]
          )
          c_set rule.R.srcs
        in
        ( ocaml_set, c_set' )

      | _ ->
        ( ocaml_set, c_set  )
      ]
  )
  ( StrSet.empty, StrSet.empty ) targs_sorted;


(*--------------------*)
(* Generate Cmds:     *)
(*--------------------*)
(* Update "R.build_rules" -- install "command"s in each of them.  HOWEVER,
   because there are rules with multiple targs, they would appear multiple
   times while iterating over "targs_sorted". For such rules, only the 1st
   occurrence will generate a command, others produce the empty stub:
*)
value generate_build_cmds: array U.file -> list U.file -> unit =
fun targs_sorted roots ->
do{
  let ocaml_include_dirs = fst ( include_dirs targs_sorted ) in
  let ocaml_includes     =
    String.concat " " ( StrSet.elements ocaml_include_dirs )
  in
  Array.iter
  (
    fun targ ->
      let rule = R.get_rule targ "B.generate_build_cmds" in
      let sc   = count_srcs rule                         in
      if  rule.R.command <> ""
      then
        (* Command stub already installed for another targ of this rule --
           nothing to do:
        *)
        ()
      else
        (* Generic Case: *)
        let targ_path = U.string_of targ in
        let cmd       =
          (*--------------------*)
          (* Custom EXE Cmd:    *)
          (*--------------------*)
          if sc.exe <> 0
          then
          do{
            (* There is an EXE in the srcs; this means that the target is to be
             generated by invocation of that EXE, which must be unique ( this
             is guaranteed by "R.get_src" ):
            *)
            let exe = R.get_src [ fun b -> U.EXE b ] rule       in

            (* All other srcs: *)
            let other_srcs =
              List.filter ( fun src -> src <> exe ) rule.R.srcs in

            (* Make the command line. All attrs are taken into account. The exe
               comes first, FOLLOWED by its own attrs:
            *)
            let cmd0 =
              Printf.sprintf "%s %s"
                ( U.string_of exe.R.src_file ) ( join_attrs  exe.R.src_aattrs )
            in
            (* Then come "other_srcs", PREFIXED by their resp attrs: *)
            let cmd1 =
              List.fold_left
              (
                fun curr src ->
                  curr ^( Printf.sprintf " %s %s"
                          ( join_attrs  src.R.src_aattrs )
                          ( U.string_of src.R.src_file   ) )
              )
              cmd0 other_srcs
            in
            (* Finally, comes the targ, PREFIXED by its attrs: *)
            cmd1 ^( Printf.sprintf " %s %s"
                    ( join_attrs  rule.R.targ_aattrs ) ( U.string_of targ ) )
          }
          else
          match targ with
          [
            (*--------------------*)
            (* ML generation:     *)
            (*--------------------*)
            (* There must be exactly 1 MLL, MLY or MLP src; XXX: MLP is not
               implemented yet, and no src or target attrs are allowed:
            *)
            U.ML  _ ->
            do{
              if sc.cmo  > 1 || sc.cmx  <> 0 || sc.cma <> 0 || sc.cmxa <> 0 ||
                 sc.h   <> 0 || sc.c    <> 0 || sc.cpp <> 0 || sc.obj  <> 0 ||
                 sc.lib <> 0 || sc.cmxs <> 0 || sc.exe <> 0 || sc.cobj <> 0 ||
                 sc.dll <> 0 || sc.asm  <> 0 || sc.ml  <> 0 || sc.mli  <> 0 ||
                 sc.cmi <> 0 || ( sc.mll + sc.mly + sc.mlp <> 1 )           ||
                 ( rule.R.targ_aattrs <> [] )||
                 ( List.exists
                    ( fun src -> src.R.src_aattrs <> [] ) rule.R.srcs
                 )
              then
              do{
                U.ocmake_out stderr ~do_flush:True ( R.string_of rule );
                failwith  "ERROR: Invalid rule( 0 )"
              }
              else ();

              if sc.mll = 1
              then
                (* MLL: ocamllex:  *)
                let src = R.get_src  [ fun b -> U.MLL b ] rule in
                Printf.sprintf "ocamllex  %s" ( U.string_of src.R.src_file )
              else
              if sc.mly = 1
              then
                (* MLY: ocamlyacc: *)
                let src = R.get_src  [ fun b -> U.MLY b ] rule in
                Printf.sprintf "ocamlyacc %s" ( U.string_of src.R.src_file )
              else
                (* MLP: XXX: currently no general way of constructing the ML
                   file. Assume this has already been done:
                *)
                let src = R.get_src  [ fun b -> U.MLP b ] rule in
                Printf.sprintf "echo %s should already exist"
                               ( U.string_of src.R.src_file )
            }
            (*--------------------*)
            (* OCaml compilation: *)
            (*--------------------*)
          | U.CMI _
          | U.CMX _
          | U.CMO _ ->
            let is_cmi = match targ with [ U.CMI _ -> True | _ -> False ] in
            let is_cmx = match targ with [ U.CMX _ -> True | _ -> False ] in
            let is_cmo = match targ with [ U.CMO _ -> True | _ -> False ] in
            do{
              assert ( is_cmi || is_cmx || is_cmo );
  
              (* There is 1 primary src ( ML or MLI ),  and an arbitrary number
                 of CMIs and CMXs are allowed for dependencies -- they will not
                 be in the command. CMOs are allowed for pre-processing.  There
                 must be no targ attrs:
              *)
              if sc.cma <> 0 || sc.cmxa <> 0 || sc.h   <> 0 || sc.c    <> 0 ||
                 sc.cpp <> 0 || sc.obj  <> 0 || sc.lib <> 0 || sc.cmxs <> 0 ||
                 sc.exe <> 0 || sc.cobj <> 0 || sc.dll <> 0 || sc.mll  <> 0 ||
                 sc.mly <> 0 || sc.mlp  <> 0 || sc.asm <> 0 ||
                 (   is_cmi   && ( sc.mli  + sc.ml <> 1 ) ) ||
                 ( ( is_cmx   || is_cmo ) && sc.ml <> 1 )   ||
                 rule.R.targ_aattrs <> []
              then
              do{
                U.ocmake_out stderr ~do_flush:True ( R.string_of rule );
                failwith  "ERROR: Invalid rule( 1 )"
              }
              else ();
  
              (* There is exactly 1 ML or MLI src. Get it with attrs. Note that
                 CMI can actually depend on ML if there is no MLI file:
              *)
              let main     =
                if is_cmi
                then R.get_src [ fun b -> U.MLI b; fun b -> U.ML b ] rule
                else R.get_src [ fun b -> U.ML  b ]                  rule
              in
              let main_attrs = join_attrs  main.R.src_aattrs in
              let main_path  = U.string_of main.R.src_file   in

              let targ_pkgs  = join_pkgs   rule.R.targ_pkgs  in
              let pkgs       =
                if targ_pkgs = ""
                then ""
                else "-package "^targ_pkgs
              in
              (* The compiler: native or byte-code. Use native if there is a
                 CMX among ALL "targs" of this rule, and byte-code otherwise:
              *)
              let has_cmx =
                List.exists ( fun[ U.CMX _ -> True | _ -> False ] ) rule.R.targs
              in
              let comp =
                if   has_cmx
                then ocamlopt
                else ocamlc
              in
              (* If there are CMO srcs, they are used as a parser with whatever
                 pre-processor configured for this src. The PP directives must
                 come in a special format ( with ''s ) to be properly handled:
              *)
              if sc.cmo = 0
              then
                (* Generic case: *)
                Printf.sprintf
                  "%s -c %s %s -o %s %s %s"
                  comp ocaml_includes main_attrs targ_path pkgs main_path
              else
                (* With Parser:  *)
                let cmos       = R.get_srcs [ fun b -> U.CMO b ] rule      in
                let cmo_paths0 =
                  List.map ( fun cmo -> U.string_of cmo.R.src_file ) cmos
                in
                let ( pp0, pp_cmdl0 ) =
                    insert_parsers rule main_attrs cmo_paths0              in
                let cmd0       =
                  Printf.sprintf "%s %s -c %s -o %s %s %s"
                    comp pp0 ocaml_includes targ_path pkgs main_path       in

                (* Also, if we use our own pre-processor module, it's a good
                   idea to produce a listing of the ML file after PP:
                *)
                let pp_cmdl1    = pp_cmdl0 ^" pr_r.cmo pr_ro.cmo"          in
                Printf.sprintf "%s -o %s.lst %s && %s"
                               pp_cmdl1 main_path main_path cmd0
            }
            (*--------------------*)
            (* C/C++ compilation  *)
            (*--------------------*)
          | U.OBJ _ ->
            (* Case 1:
               ASM/C/C++ compilation into an OBJ.  There must be exactly 1 ASM,
               C or CPP file and any number of Hs. There must be no targ attrs.
               Get the ASM, C or C++ compiler, main src, and src attrs:
            *)
            do{
              if sc.cmo  <> 0 || sc.cmx <> 0 || sc.cma  <> 0 || sc.cmxa <> 0 ||
                 sc.obj  <> 0 || sc.lib <> 0 || sc.cmxs <> 0 || sc.exe  <> 0 ||
                 sc.cobj <> 0 || sc.ml  <> 0 || sc.mli  <> 0 || sc.cmi  <> 0 ||
                 sc.dll  <> 0 || sc.mll <> 0 || sc.mly  <> 0 || sc.mlp  <> 0 ||
                 ( sc.c + sc.cpp + sc.asm )  <> 1 || rule.R.targ_aattrs <> []
              then
              do{
                U.ocmake_out stderr ~do_flush:True ( R.string_of rule );
                failwith "ERROR: Invalid rule( 2 )"
              }
              else ();

              let ( comp, main ) =
                if sc.c   = 1
                then ( cc,  R.get_src [ fun b -> U.C   b ] rule )
                else
                if sc.cpp = 1
                then ( cxx, R.get_src [ fun b -> U.CPP b ] rule )
                else
                     ( asm, R.get_src [ fun b -> U.ASM b ] rule )
              in
              let  main_attrs = join_attrs  main.R.src_aattrs       in
              let  main_path  = U.string_of main.R.src_file         in
  
              (* Switch for the output file: *)
              let osw  = "-o "
              in
              (* The command. We automatically provide the include path to the
                 OCaml run-time system headers:
              *)
              Printf.sprintf "%s -I %s -c %s %s%s %s"
                           comp P.ocaml_lib main_attrs osw targ_path main_path
            }

            (*--------------------*)
            (* OCaml archives:    *)
            (*--------------------*)
          | U.CMXA _
          | U.CMA  _ ->
            let is_cmxa = match targ with [ U.CMXA _ -> True | _ -> False ] in
            let is_cma  = match targ with [ U.CMA  _ -> True | _ -> False ] in
            do{
              (* There must be only CMX, CMO, LIB or OBJ srcs ( XXX: do they
                 make sense for CMA archives?
              *)
              if sc.cma <> 0 || sc.cmxa <> 0 || sc.cmxs <> 0 || sc.exe <> 0 ||
                 sc.ml  <> 0 || sc.mli  <> 0 || sc.cmi  <> 0 || sc.c   <> 0 ||
                 sc.cpp <> 0 || sc.h    <> 0 || sc.cobj <> 0 || sc.dll <> 0 ||
                 sc.mll <> 0 || sc.mly  <> 0 || sc.mlp  <> 0 || sc.asm <> 0 ||
                 ( is_cmxa && sc.cmo  <> 0 ) || ( is_cma && sc.cmx  <> 0 )  ||
                 ( is_cmxa && sc.cmx  =  0 ) || ( is_cma && sc.cma  =  0 )
              then
              do{
                U.ocmake_out stderr ~do_flush:True ( R.string_of rule );
                failwith "ERROR: Invalid rule( 3 )"
              }
              else ();
  
              (* OCaml compiler: *)
              let comp =
                if is_cmxa 
                then ocamlopt
                else ocamlc
              in
              (* For such srcs, there should be no attrs. But the targ attrs
                 may exist:
              *)
              let str_srcs = fst ( all_srcs_attrs rule False ) in
              let t_attrs  = join_attrs rule.R.targ_aattrs     in

              (* The command: NB: targ attrs are coming at the end: *)
              Printf.sprintf "%s -a -o %s -linkall %s %s"
                             comp targ_path str_srcs t_attrs
            }
            (*--------------------*)
            (* Exec |PlugIn |DLL: *)
            (*--------------------*)
          | U.EXE  _
          | U.CMXS _
          | U.DLL  _ ->
            do{
              (* There must only be CMX, CMXA, OBJ and LIB files: *)
              if sc.cma  <> 0 || sc.cmo <> 0 || sc.cmxs <> 0 || sc.exe <> 0 ||
                 sc.cobj <> 0 || sc.dll <> 0 || sc.ml   <> 0 || sc.mli <> 0 ||
                 sc.cmi  <> 0 || sc.c   <> 0 || sc.cpp  <> 0 || sc.h   <> 0 ||
                 sc.mll  <> 0 || sc.mly <> 0 || sc.mlp  <> 0 || sc.asm <> 0
              then
              do{
                U.ocmake_out stderr ~do_flush:True ( R.string_of rule );
                failwith "ERROR: Invalid rule( 4 )"
              }
              else ();
  
              let flag =
                match targ with
                [
                  U.EXE  _ -> "" (* No extra libs etc for the moment... *)
                | U.DLL  _ -> "-output-obj -linkall"
                | U.CMXS _ -> "-shared"
                | _        -> assert False
                ]
              in
              (* No src attrs, but there are targ attrs: *)
              let  str_srcs  = fst ( all_srcs_attrs rule False ) in
              let  t_attrs   = join_attrs rule.R.targ_aattrs     in

              (* Packages:    *)
              let  targ_pkgs = join_pkgs rule.R.targ_pkgs        in
              let  pkgs      =
                if targ_pkgs = ""
                then ""
                else "-package "^targ_pkgs^" -linkpkg"
              in
              (* The command: *)
              Printf.sprintf "%s %s -o %s %s %s %s"
                             ocamlopt flag targ_path str_srcs t_attrs pkgs
            }
            (*----------------*)
            (* Compound OBJs: *)
            (*----------------*)
          | U.COBJ base ->
              (* For COBJ: We actually create a compound OBJ ( because the
                 OCaml compiler requires this extension ), and on Windows,
                 move it into a LIB ( because then it's actually a LIB --
                 clients will work with that LIB ), and install a CLIB stamp:
              *)
            do{
              (* There must only be CMX or CMXA files. LIBs and OBjs are not
                 allowed as they are silently ignored by the OCaml compiler
                 in this mode:
              *)
              if sc.cma  <> 0 || sc.cmo <> 0 || sc.cmxs <> 0 || sc.exe <> 0 ||
                 sc.cobj <> 0 || sc.dll <> 0 || sc.ml   <> 0 || sc.mli <> 0 ||
                 sc.cmi  <> 0 || sc.c   <> 0 || sc.cpp  <> 0 || sc.h   <> 0 ||
                 sc.mll  <> 0 || sc.mly <> 0 || sc.mlp  <> 0 || sc.asm <> 0 ||
                 sc.obj  <> 0 || sc.lib <> 0
              then
              do{
                U.ocmake_out stderr ~do_flush:True ( R.string_of rule );
                failwith "ERROR: Invalid rule( 5 )"
              }
              else ();

              (* There must be no src attrs; targ attrs may exist: *)
              let srcs_str   = fst ( all_srcs_attrs rule False ) in
              let t_attrs    = join_attrs rule.R.targ_aattrs     in

              (* Packages: *)
              let  targ_pkgs = join_pkgs rule.R.targ_pkgs        in
              let  pkgs      =
                if targ_pkgs = ""
                then ""
                else "-package "^targ_pkgs^" -linkpkg"
              in
              let obj      = U.string_of( U.OBJ base )           in
              (* Generate the physical OBJ: *)
              Printf.sprintf "%s -output-obj -linkall -o %s %s %s %s"
                             ocamlopt obj srcs_str pkgs t_attrs
            }
            (*----------------*)
            (* LIB from OBJs: *)
            (*----------------*)
            (* This rule is for building LIBs from OBJs which themeselves come
               from C/CPP/ASM ( not from OCaml srcs ):
            *)
          | U.LIB _ ->
            do{
              (* There must only be OBJ files: *)
              if sc.cma  <> 0 || sc.cmo  <> 0 || sc.cmxs <> 0 || sc.exe <> 0 ||
                 sc.cobj <> 0 || sc.dll  <> 0 || sc.ml   <> 0 || sc.mli <> 0 ||
                 sc.cmi  <> 0 || sc.c    <> 0 || sc.cpp  <> 0 || sc.h   <> 0 ||
                 sc.mll  <> 0 || sc.mly  <> 0 || sc.mlp  <> 0 || sc.asm <> 0 ||
                 sc.cmx  <> 0 || sc.cmxa <> 0 || sc.lib  <> 0
              then
              do{
                U.ocmake_out stderr ~do_flush:True ( R.string_of rule );
                failwith "ERROR: Invalid rule( 6 )"
              }
              else ();
  
              (* No src or target attrs: *)
              let ( str_srcs, str_attrs ) = all_srcs_attrs rule True in
              let t_attrs   = join_attrs rule.R.targ_aattrs          in

              if  ( str_attrs <> "" ) || ( t_attrs <> "" )
              then
                  failwith( Printf.sprintf
                            "ERROR: Inappropriate attrs for %s: %s %s"
                            targ_path str_attrs t_attrs )
              else
                Printf.sprintf "%s%s %s" C.ar targ_path str_srcs
            }

          | _ ->
            do{
              U.ocmake_out stderr ~do_flush:True ( R.string_of rule );
              failwith "ERROR: Invalid rule( 7 )"
            }
          ]
      in
      (* Update "R.build_rules" -- install the "cmd", but only once;  for all
         tail targs of this rule, install the empty stub as the command.  For
         this reason, cannot use "R.install_rule" here:
      *)
      match rule.R.targs with
      [
        [ htarg :: ttargs ] ->
        do{
          Hashtbl.replace R.build_rules htarg
                          { ( rule ) with R.command = cmd };
          List.iter
          (
            fun ttarg ->
              Hashtbl.replace R.build_rules ttarg
                          { ( rule ) with R.command = C.empty_command }
          )
          ttargs
        }
      | _ -> assert False (* Empty "targs", impossible *)
      ]
  )
  targs_sorted;

  (*--------------------*)
  (* Verify the "roots":*)
  (*--------------------*)
  (* Only the following files can be "roots":  COBJ, LIB, DLL, CMXS, EXE.
     EXE, DLL and CMXS files are considered to be "progs" which should be
     "installed" at the end of the build; others are called "libs".  NB:
     there may also exist non-"root" EXE files!
  *)
  let ( progs, libs ) =
    List.partition
    (
      fun f ->
        match f with
        [
          U.EXE  _ | U.DLL  _ | U.CMXS _ -> True
        | U.COBJ _ | U.LIB  _            -> False
        | _ ->
          failwith ( "INTERNAL ERROR: Not a Root: "^( U.string_of f ) )
        ]
    )
    roots
  in
  (*--------------------*)
  (* Output the Cmds:   *)
  (*--------------------*)
  (* XXX: Currently this is done by generating a Makefile and running "make" --
     this facilitates debugging and simplifies the task of "ocmake".
  *)
  let mf = open_out "Makefile" in
  do{
    (* 2 groups of targets to be built: *)
    Printf.fprintf mf "all: progs libs\n";
  
    (* Progs: *)
    Printf.fprintf mf "progs:";
    List.iter ( fun f -> Printf.fprintf mf " %s" ( U.string_of f ) ) progs;
    Printf.fprintf mf "\n\n";
  
    (* Libs:  *)
    Printf.fprintf mf "libs:";
    List.iter ( fun f -> Printf.fprintf mf " %s" ( U.string_of f ) ) libs;
    Printf.fprintf mf "\n\n";
  
    (* Install:  Currently, "progs" and only them are installable: *)
    Printf.fprintf mf "install: progs\n";
    Printf.fprintf mf "\t%s %s && %s %s %s\n\n"
                  C.mkdir
                  P.install_top
                  C.install
                  ( String.concat " " ( List.map U.string_of progs ) )
                  P.install_top;
  
    (* All rules: *)
    D.print_rules_ord mf targs_sorted;
  
    (* To clean up everything: *)
    Printf.fprintf mf "clean:\n\t%s" C.rm;
    Array.iter
      ( fun f ->
          (* NB: Files to be deleted: Possibly more than just "f": *)
          match f with
          [
            U.CMXA base ->
              (* Delete CMX[A] and LIB: *)
              Printf.fprintf mf " %s %s"
                             ( U.string_of f ) ( U.string_of ( U.LIB base ) )
  
          | U.CMX  base ->
              (* Delete CMX and OBJ:    *)
              Printf.fprintf mf " %s %s"
                             ( U.string_of f ) ( U.string_of ( U.OBJ base ) )
  
          | _ ->
              (* Delete "f" itself:     *)
              Printf.fprintf mf " %s" ( U.string_of f )
          ]
      )
      targs_sorted;
    Printf.fprintf mf "\n";
  
    close_out mf
  }
};
  
(*----------------------*)
(* Finally, run "make": *)
(*----------------------*)
(* Invoke "make" on the created Makefile according to the mode specified in the
   command line. NB: "ConfigMode" only creates the Makefile without running it:
*)
value run_build: unit -> unit =
fun () ->
  if C.ocmake_mode () = C.ConfigMode
  then ()
  else
    let mode_target =
      match C.ocmake_mode () with
      [
        C.ConfigMode  -> assert False
      | C.BuildMode   -> " all"
      | C.InstallMode -> " install"
      | C.CleanMode   -> " clean"
      ]
    in
    ignore ( U.run ( C.make ^ mode_target ) );

