(* Vim:ts=2:syntax=ocaml
*)
(*============================================================================*)
(*      Back-End of "OCMake": Generation and Running of Build Commands        *)
(*============================================================================*)
(* Generate the build commands, updating "R.build_rules". The arg is the
   topologically-sorted array of build targets:
*)
value generate_build_cmds: array U.file -> list U.file -> unit;

(* Actually run the build: *)
value run_build: unit -> unit;

