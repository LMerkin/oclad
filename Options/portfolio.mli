
type portfolio = 'a;

(* Portfolio interface *)

value empty : portfolio;

value portfolio_of_list : list Options.fx_option -> portfolio;

value add_option : portfolio -> Options.fx_option -> portfolio;

value add_portfolio : ~pto:portfolio -> ~psub:portfolio -> portfolio;

value iter : ( Options.fx_option -> unit ) -> portfolio -> unit;

value fold_left : ( 'a -> Options.fx_option -> 'a ) -> 'a -> portfolio -> 'a;

value rev : portfolio -> portfolio;

value map : ( Options.fx_option -> 'a ) -> portfolio -> list 'a;

value filter : ( Options.fx_option -> bool ) -> portfolio -> portfolio;

value find_all : ( Options.fx_option -> bool ) -> portfolio -> list Options.fx_option;

value sort : ( Options.fx_option -> float ) -> ?aggregator:( list float -> float ) -> portfolio -> portfolio;

value count : portfolio -> int;

value flatten : portfolio -> portfolio;

value hd : portfolio -> Options.fx_option;

value tl : portfolio -> portfolio;

(* Portfolio storage interface *)

value store_portfolio : string -> portfolio -> unit;

value retrieve_portfolio : string -> portfolio;

value remove_portfolio : string -> unit;

value remove_all_portfolios : unit -> unit;

value list_portfolios : unit -> array string;
