type currency = 
[
  ATS
| AUD
| BEF
| CAD
| CHF
| DEM
| DKK
| ESP
| FIM
| FRF
| GBP
| HKD
| IEP
| ITL
| JPY 
| LUF
| NLG
| NOK
| NZD
| PTE
| SEK
| USD
| XEU 
| SGD
| GRD
| MYR
| TWD
| THB (* Thai Baht          *)
| IDR (* Indonesian Rupiah  *)
| AU  (* Gold               *)
| TRL (* Turkish Lira       *)
| RUR (* Russian Rouble     *)
| PLN (* Polish Zloty       *)
| PHP (* Philippine Peso    *)
| UAH (* Ukranian hryvnia   *)
| LBP (* Lebanese pound     *)
| INR (* Indian Rupee       *)
| KRW (* Korean Wong        *)
| ZAR (* South African Rand *)
| RUB (* New Russian Rouble *)
| KZT (* Kazakhstani Tenge *)
| MXN (* Mexican Peso       *)
| ILS (* Israeli Shekel     *)
| CZK (* Czech Koruna       *)
| HUF (* Hungarian Forint   *)
| BRL (* Brazil Real        *)
| ARS (* Argentine Peso     *)
| VEB (* Venezuelan Bolivar *)
| UYP (* Urugay NewPeso     *)
| COP (* Columbian Peso     *)
| CLP (* Chilean Peso       *)
| EUR (* Euro               *)
| CNY (* Chinese Renminbe   *)
| SAR (* Saudi Arabian Real *)
| SKK (* Slovak Koruna      *)
| TRY (* new Turkish ccy *)
| AG (* Silver *)
| HRK(* Croatia *)
| BAL (* Aluminium *)
| BCU (* Copper *)
| BZN (* Zinc *)
| BNI (* Nickel *)
| BPB (* Lead *)
| BSN (* Tin *)
| BAA (* Aluminium Alloy *)
| BNA (* Aluminium NAASAC *)
| BSM (* Steel Med *)
| BSF (* Steel Far East *)
| CCU (* Comex copper *)
| BLL (* Polyethylene *)
| BPP (* Polypropylene *)
| XPT (* Platinum *)
| XPD (* Palladium *)
| GSI (* Goldman Sachs Index *)
| CO2 (* Carbon dioxide *)
| WTI (* WTI *)
| NRB (* UnleadGasoline *)
| NHO (* HeatingOil *)
| NNG (* NatGaz *)
| LGO (* Gasoil *)
| LCO (* Brent *)
| DTD (* Dated Brent *)
| BFO (*HSFO 3.5 FOB Rotterdam Barges*)
| EPU (*European Gasoline*)
| SGO (*Singapore Gasoil*)
| SFO (* HSFO 180 CST Singapore Cargoes *)
| SFT (* HSFO 380 CST Singapore Cargoes *)
| MGF (* Gasoil 0.2 FOB MED Cargoes *)
| MGC (* Gasoil 0.2 CIF Med Cargoes *)
| MGG (* Gasoil 0.1 FOB MED Cargoes *)
| MGD (* Gasoil 0.1 CIF Med Cargoes *)
| EJC (* Jet CIF NWE Cgs *)
| SKF (* Kerosene FOB Sing Cgs *)
| RFO (*HSFO 3.5 FOB Rotterdam Barges*)
| NGR (* Nymex Unl Gasoline RFG *)
| GGP (* USGC Gasoline Unl 87 Pipe *)
| GHO (* USGC Heating Oil No2 Pipe *)
| GDP (* USGC LS Diesel Pipe *)
| GDQ (* ULSD 15ppm US Gulf Coast Pipeline *)
| LAJ (* Jet LA Pipe *)
| GJP (* USGC Jet 54 Pipe *)
| UCO (* Urals CIF Med CG *)
| EFC (* HSFO 3.5 CIF NWE CG *)
| EFF (* HSFO 3.5 FOB NWE CG *)
| MFF (* HSFO 3.5 FOB Med CG *)
| MFC (* HSFO 3.5 CIF Med CG *)
| ELF (* LSFO 1.0 FOB NWE CG *)
| BLF (* LSFO 1.0 FOB Rdam BG *)
| ELC (* LSFO 1.0 CIF NWE CG *)
| MLC (* LSFO 1.0 CIF Med CG *)
| MLF (* LSFO 1.0 FOB Med CG *)
| AFO (* US FO 1PC FOB NY CG *)
| AFH (* US FO .3HP NY CG *)
| AFT (* US FO 2.2 NY CG *)
| AFC (* US FO 3PC FOB NY CG *)
| GFW (* USGC HSFO 3.0 Waterborne *)
| ENC (* Naphta CIF NWE CG *)
| SNF (* Naphta FOB Sing CG *)
| BNF (* Naphta FOB Rdam BG *)
| MNF (* Naphta FOB Med CG *)
| MNC (* Naphta CIF Med CG *)
| APC (* Propane CIF Argus ARA NWE CG *)
| EMG (* MOG 95R Unl 10ppm Argus FOB NWE BG *)
| MPF (* Prem Unl 50ppm FOB Med CG *)
| MPC (* Prem Unl 50ppm CIF Med CG *)
| EPC (* Prem Unl 10ppm CIF NWE CG *)
| EGC (* Gasoil 0.2 CIF NWE CG *)
| EGF (* Gasoil 0.2 FOB NWE CG *)
| BGF (* Gasoil 0.2 FOB Rdam BG *)
| EGD (* Gasoil 0.1 CIF NWE CG *)
| EGG (* Gasoil 0.1 FOB NWE CG *)
| BGG (* Gasoil 0.1 FOB Rdam BG *)
| MJF (* Jet FOB Med CG *)
| BJF (* Jet FOB Rdam BG *)
| EUF (* ULSD 50 ppm CIF NWE CG *)
| MUC (* ULSD 50 ppm CIF Med CG *)
| BUF (* ULSD 50 ppm FOB Rdam BG *)
| EUO (* ULSD 10 ppm CIF NWE CG *)
| MUF (* ULSD 50 ppm FOB Med CG *)
| BUO (* ULSD 10 ppm FOB Rdam BG *)
| DCO (* Dubai Crude *)
| TCO (* Tapis Crude *)
| HSL (* HSL_RHEINSCHIENE *)
| FEP (* Propane Argus Far East Index *)
| TBP (* Propane Argus Tet Belvieu *)
| HEL (* HEL RHEINSCHIENE *)
| HSC (* Houston Ship Canal *)
| EPG (* El Paso San Juan *)
| NWP (* North West Pipeline Rockies *)
| PEP (* Panhandle Eastern Pipeline *)
| BLU (* Power UK Baseload *)
| PKU (* Power UK Peaks *)
| GBL (* Power Germany Baseload *)
| GPK (* Power Germany Peaks *)
| FBL (* Power France Baseload *)
| FPK (* Power France Peaks *)
| TTF (* Holland Natural Gas *)
| ZEE (* Belgium Natural Gas *)
| NBP (* UK Natural Gas *)
| ISK (* Iceland Krona *)
| RON (* Romanian Krona *)
| BGN (* Bulgarian Lewa *)
| SIT (* Slovenian Tollar *)
| PEN (* Peruvian new sol*)
| KWD (* Kuwaiti Dinar*)  
| OMR (* Omani Rial*)
| QAR (* Qatari Rial*)
| BHD (* Bahraini Dinar*)
| AED (* United Arab Emirates Dirham*)
| MAD (* Moroccan Dirham*)
| TND (* Tunisian Dinar*)
| EGP (* Egyptian Pound*)  
| RUA (* Offshore Russian Rouble *)
| COA (* Coal API2 NWE CIF *)
| CAR (* Coal API4 RB FOB *)
| WHE (* Wheat *)
| RDW (* Red wheat *)
| COR (* Corn *)
| COF (* Coffee *)
| SOY (* Soybeans *)
| SOO (* Soybean oil *)
| SOM (* Soybean meal *)
| OAT (* CBOT OAT*)
| SUG (* Sugar *)
| COT (* Cotton *)
| LEH (* Lean Hogs *)
| LIC (* Live Cattle *)
| FEC (* Feeder Cattle *)
| COC (* Cocoa *)
| MAI (* Corn euro *)
| BLE (* Wheat euro *)
| CLZ (* Rapeseed *)
| CLO (* Rapeseed oil*)
]
with string_of, parse;


type ccypair =
{
  a_ccy: currency;
  b_ccy: currency
};


value make_ccypair: currency -> currency -> ccypair =
    fun a b ->
      { a_ccy=a; b_ccy=b };


value ccypair_of_string: string -> ccypair =
    fun str ->
      try
        let a:string = String.sub str 0 3 in
        let b:string = String.sub str 3 3 in
          make_ccypair (parse_currency (Stream.of_string a)) (parse_currency (Stream.of_string b))
      with
          [ Invalid_argument _ -> failwith (Printf.sprintf "ccypair_of_string: Invalid input string; %s" str)];


value string_of_ccypair: ccypair -> string = 
    fun p ->
      (string_of_currency p.a_ccy)^(string_of_currency p.b_ccy);


value parse_ccypair: Stream.t char -> ccypair =
    fun s ->
      let string_of_stream: Stream.t char -> string = 
        fun s ->
          let buffer = Buffer.create 6 in
          do
          {
            Stream.iter (Buffer.add_char buffer) s;
            Buffer.contents buffer
          }
      in
        ccypair_of_string (string_of_stream s);


type day_count_type =
[
  UNKNOWN_DCT
| ACT_365f          (* Actual/365 (fixed)             *)
| EBOND             (* Eurobond or 30E/360            *)
| ACT_360           (* Actual/360                     *)
| ACT_ACT           (* Actual/365 or Actual/Actual    *)
| EBONDP            (* 30E+/360 day count             *)
| BOND              (* Bond or 30/360                 *)
| ACT_365L          (* Act/365 (Long)                 *)
| EQUAL_COUPONS     (* Number of months divided by 12 *)
| UNIT_DCT          (* Day count fraction is always 1 *)
| EXT_EXT           (* A French market convention: the denominator is 365
                       unless the accrual period includes 29 Feb           *)
| ACT_NL            (* Japanese market convention like act/365f w/o 29 Feb *)
| ACT_ACTTSY        (* Act/Act on a treasury basis    *)
| ACT_365dot25      (* Actual/365.25                  *)
]
with string_of, parse;

type ir_futures_type =
[
  UNKNOWN_IRFUT
| IRFUT_IMM
| IRFUT_AUD
| IRFUT_CAD
| IRFUT_NZD
]
with string_of, parse;

type bus_day_conv =
[
  UNKNOWN_BDC
| FOLLOWING         (* Next business day              *)
| PRECEDING         (* Previous business day          *)
| MOD_FOL           (* Next b.d. or last  b.d. of month, whichever is sooner *)
| MOD_PRE           (* Prev b.d. or first b.d. of month, whichever is later  *)
| UNADJ             (* Constrained to lie in the same month; b.days ignored  *)
| NO_ADJ            (* No adjustments at all          *)
]
with string_of, parse;

type currency_limit_group =
[
  GROUP_I
| GROUP_IIa
| GROUP_IIb
| GROUP_III
| GROUP_IV
| GROUP_UNKNOWN
]
with string_of, parse;

(* NB: Non-Currency assets are not in use: *)
type asset_type =
[
  CurrencyE
| MetalE
| PreciousE
| EnergyE
| PowerUKE
| PowerEuroE
| GasEuroE
| SoftE
]
with string_of, parse;

type interp_meth =
[
  LINEAR
| PIECEWISE
| SPLINE
]
with string_of, parse;

type currency_info =
{
  description:          string;
  iso_code:             currency;
  alt_code:             string;
  two_let_code:         string;
  hols:                 int;
  inverse_fx_quote:     bool;
  futures_tick_size:    float;
  pip_size:             float;
  cash_days_to_spot:    int;
  swap_days_to_spot:    int;
  prec:                 int;
  fx_dct:               day_count_type;
  swap_dct:             day_count_type;
  fut_type:             ir_futures_type;
  bdc:                  bus_day_conv;
  atype:                asset_type;
  under_interp_method:  interp_meth;
  vol_interp_method:    interp_meth;
  discount_price:       bool;
  discount_delta:       bool;
  roll_lag:             int;
  group:                currency_limit_group
}
with string_of;

