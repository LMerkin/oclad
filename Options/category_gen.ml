

value load_options: string -> list Options_utils.option_info =
fun file ->

  let inc: in_channel = open_in file in
  let rec loader: list Options_utils.option_info -> 
                  in_channel -> list Options_utils.option_info =

    fun data inc ->
      let new_opt: option Options_utils.option_info =
      try
        let line: string = input_line inc in
        let opt: Options_utils.option_info = 
          Options_utils.parse_option_info (Stream.of_string line)
        in
          Some opt
      with [ _ -> do { close_in inc; None }]
  in
    match new_opt with
    [
      None -> data
    | Some opt -> loader [ opt :: data ] inc
    ]
  in
    List.sort (fun a b -> String.compare a.Options_utils.short_code b.Options_utils.short_code) (loader [] inc);


value tag_of_string: string -> string = fun a -> "${" ^ a ^ "}";

value data_file: string = Sys.argv.(1);

value options: list Options_utils.option_info = load_options data_file;

value constructor_def: Options_utils.option_info -> string = 
fun o ->
  Printf.sprintf "%s of %s_required and list optional_argument" o.Options_utils.short_code (String.lowercase o.Options_utils.short_code);


value create_typedef: (Options_utils.option_info -> string) -> list Options_utils.option_info -> string =
fun f options ->
  Libfx_extract.typedef_of_list (List.map f options);


value record_creation_list: list Options_utils.option_info -> string = 
fun options ->
  String.concat "\n" (List.map (fun o -> o.Options_utils.required) options);


value cat_of_type: Options_utils.option_info -> string = 
fun o ->
  Printf.sprintf "T_%s" o.Options_utils.short_code;


value cat_type: Options_utils.option_info -> string = 
fun o ->
  Printf.sprintf "%s _ _ -> ( C_%s, T_%s )" o.Options_utils.short_code o.Options_utils.category o.Options_utils.short_code;


value constructors: Options_utils.option_info -> string = 
fun o -> 
  Printf.sprintf "T_%s -> %s %s (list_of_optional_parameters op)" o.Options_utils.short_code o.Options_utils.short_code o.Options_utils.constructor;


value params: Options_utils.option_info -> string = 
fun o -> 
  Printf.sprintf "%s o l -> %s" o.Options_utils.short_code o.Options_utils.params;


value category: string = (List.hd options).Options_utils.category;


value tags: list string = [ (tag_of_string "LID");
                            (tag_of_string "Category"); 
                            (tag_of_string "Category_required"); 
                            (tag_of_string "Category_cat_type"); 
                            (tag_of_string "Make_Category"); 
                            (tag_of_string "Make_Category_Params");
                            (tag_of_string "Category_of_type")
                          ];

value replacements: list string =[ ( String.lowercase category );
                                   ((create_typedef constructor_def) options);
                                   ( record_creation_list options);
                                   ((create_typedef cat_type) options);
                                   ((create_typedef constructors) options);
                                   ((create_typedef params) options);
                                   ((create_typedef cat_of_type) options) ^ " -> Some C_" ^ category
                                 ];


value category_gen_file: string = Sys.argv.(2);

value output_file: string = (Filename.chop_suffix data_file ".mlp") ^ ".ml";

Libfx_extract.generate_file category_gen_file tags replacements output_file;
