open ExtString;

value copy_file_with_line_filter: in_channel -> out_channel -> (string -> string) -> unit =
    fun inc outc filter ->
      while True do
      {
        output_string outc ( (filter (input_line inc)) ^ "\n" )
      };

value replace_all: string -> string -> string -> (bool * string)=
    fun input tag replacement ->
      let rec worker: bool -> string -> string -> string -> ( bool * string ) =
        fun state str sub by ->
          match String.replace ~{ str=str } ~{ sub=sub } ~{ by=by } with
          [
            (True, s) ->  worker True s sub by 
          | (False, s) -> ( (state || False) , s)
          ]
      in
        worker False input tag replacement;
          

value replace_filter: list string -> list string -> string -> string = 
  fun pattern replacement input ->
    let results: list (bool * string) = List.map2 (fun a b -> replace_all input a b) pattern replacement in
    try
      List.assoc True results
    with
        [ Not_found -> input ];


value file_contents: string -> string = 
    fun file_name ->
      let inc: in_channel = open_in file_name in
      let len: int = in_channel_length inc in
      let file_contents: string = String.create len in
      do  
      {
        really_input inc file_contents 0 (len - 1);
        close_in inc;
        file_contents
      };


value typedef_of_list: list string -> string =
    fun str_list -> 
      let hd_str:string = Printf.sprintf "    %s\n" (List.hd str_list) in
      let tl_strs:list string = List.map (Printf.sprintf "   |  %s") (List.tl str_list) in
        hd_str ^ (String.concat "\n" tl_strs);


value file_entries: string -> string -> string -> list string =
    fun file reg1 reg2 ->
      let r1:Pcre.regexp = Pcre.regexp reg1 in
      let r2:Pcre.regexp = Pcre.regexp reg2 in
      let a:string = (Pcre.extract ~{ rex=r1 } (file_contents file)).(1) in
      Array.to_list (Array.map (fun x -> x.(1))
                               (Pcre.extract_all ~{ rex=r2 } a));


(* Reads in the template file and replaces the tag with replacement in the output file *)
value generate_file: string -> list string -> list string  -> string -> unit = 
    fun template_file tag replacement out_file ->
      let templc: in_channel = open_in template_file in
      let outc: out_channel = open_out out_file in      
      try
        do
        {
          copy_file_with_line_filter templc outc (replace_filter tag replacement);
        }
      with 
          [ _ -> do { close_in templc; close_out outc } ];
