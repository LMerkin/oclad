
type market_parameters =
{
  spot: float;
  mparameters: Hashtbl.t Argument.argument_type Argument.market_argument
};

value empty : unit -> market_parameters;

value string_of_market_parameters: market_parameters -> string;


type spot_shift =
[
  RelativeSS of float
| AbsoluteSS of float
];


value apply_spot_shift : market_parameters -> spot_shift -> market_parameters;


type vol_shift =
[
  ParallelVS of float
];


value apply_vol_shift : market_parameters -> vol_shift -> market_parameters;


value clear_shocked_volsurfaces : unit -> unit;


type depo_shift =
[
  ParallelDS of float
];


value apply_base_depo_shift : market_parameters -> depo_shift -> market_parameters;


value apply_dealt_depo_shift : market_parameters -> depo_shift -> market_parameters;


value clear_shocked_depos : unit -> unit;


value shifted_market_parameters : market_parameters -> Oper.mloper -> market_parameters;
