
type portfolio_item =
[
  Option of Options.fx_option
| SubPortfolio of list portfolio_item
]
with string_of, parse;

type portfolio = list portfolio_item with string_of, parse;


(* Portfolio interface *)

value rec iter: ( Options.fx_option -> unit ) -> portfolio -> unit =
fun f p ->
  List.iter 
  ( fun i ->
      match i with
      [
        Option o -> f o
      | SubPortfolio sp -> iter f sp
      ]
  ) p;


value empty : portfolio = [];


value portfolio_of_list : list Options.fx_option -> portfolio = 
fun l -> 
  List.rev (List.rev_map (fun o -> Option o) l);


value add_option : portfolio -> Options.fx_option -> portfolio = 
fun p o ->
  p @ [ Option o ];


value add_portfolio : ~pto:portfolio -> ~psub:portfolio -> portfolio =
fun ~{ pto }  ~{ psub } ->
  pto @ [ SubPortfolio psub ];


value rec fold_left : ( 'a -> Options.fx_option -> 'a ) -> 'a -> portfolio -> 'a =
fun f aggr p ->
  List.fold_left 
  ( fun a i ->
      match i with
      [
        Option o -> f a o
      | SubPortfolio sp -> fold_left f a sp
      ]
  ) aggr p;


value map : ( Options.fx_option -> 'a ) -> portfolio -> list 'a =
fun f p ->
  List.rev (fold_left ( fun a o ->  [ (f o) :: a ] ) [] p);


value flatten : portfolio -> portfolio =
fun p ->
  portfolio_of_list (map (fun o -> o) p);


value hd : portfolio -> Options.fx_option =
fun p ->
  List.hd ( map (fun o -> o ) p );


value tl: portfolio -> portfolio =
fun p ->
  portfolio_of_list ( List.tl ( map (fun o -> o ) p ) );


value rec rev : portfolio -> portfolio =
fun p ->
  List.fold_left
  ( fun a i ->
      match i with
      [
        Option _ -> [ i :: a ]
      | SubPortfolio sp -> [ (SubPortfolio (rev sp)) :: a ]
      ]
  ) [] p;


value rec filter : ( Options.fx_option -> bool ) -> portfolio -> portfolio =
fun f p ->
  let p' = List.fold_left 
  ( fun a i ->
      match i with
      [
         Option o -> if f o then [ i :: a ] else a
      |  SubPortfolio sp -> [ (SubPortfolio (filter f sp)) :: a ]
      ]
  ) [] p
  in
    rev p';


value find_all : ( Options.fx_option -> bool ) -> portfolio -> list Options.fx_option =
fun f p ->
  List.rev (fold_left (fun a o -> if f o then [ o :: a ] else a) [] p);


value list_sum : list float -> float = List.fold_left ( \+. ) 0.0;


value sort : ( Options.fx_option -> float ) -> ?aggregator:( list float -> float ) -> portfolio -> portfolio =
fun f ?{ aggregator=list_sum } p ->
  let rev_cmp : (float * portfolio_item) -> ( float * portfolio_item) -> int =
      fun i1 i2 -> compare (fst i2) (fst i1)
  in
  let rec weight_and_sub_sort : portfolio_item -> ( float * portfolio_item ) =
  fun i ->
    match i with
    [
      Option o -> ( f o, i )
    | SubPortfolio sp -> let ( w , sp') = sort_rec sp in ( w, SubPortfolio sp' )
    ]
  and
  sort_rec : portfolio -> (float * portfolio ) =
  fun p ->
    let top : list ( float * portfolio_item ) = List.rev_map weight_and_sub_sort p in
    let atop : array ( float * portfolio_item ) = Array.of_list top in
    do
    {
      Array.sort rev_cmp atop;
      let ( aw, ai ) : ( list float * portfolio) = 
        Array.fold_left ( fun (caw, cai) (w, i) -> ( [w :: caw], [ i :: cai ]) ) ([], []) atop
      in
        ( aggregator aw, ai )
    }
  in
   snd (sort_rec p);


value count: portfolio -> int = fold_left (fun a _ -> a + 1 ) 0;


(* Portfolio storage interface *)

exception DoesNotExist of string;

value store_size : int = 20;

value portfolio_store : Hashtbl.t string portfolio = Hashtbl.create store_size;


value store_portfolio : string -> portfolio -> unit =
fun k p ->
  Hashtbl.replace portfolio_store (String.lowercase k) p;
  
Callback.register "Portfolio.store_portfolio" store_portfolio;


value retrieve_portfolio : string -> portfolio =
fun k ->
  try
    Hashtbl.find portfolio_store (String.lowercase k)
  with
    [ Not_found -> raise (DoesNotExist k) ];

value remove_portfolio : string -> unit =
fun k ->
  Hashtbl.remove portfolio_store (String.lowercase k);

Callback.register "Portfolio.remove_portfolio" remove_portfolio;

value remove_all_portfolios : unit -> unit =
fun _ ->
  Hashtbl.clear portfolio_store;

Callback.register "Portfolio.remove_all_portfolios" remove_all_portfolios;

value list_portfolios : unit -> array string =
fun _ ->
  Array.of_list (Hashtbl.fold (fun k _v l -> [ k :: l ]) portfolio_store []);

Callback.register "Portfolio.list_portfolios" list_portfolios;

