
type fx_option =
[
    Accrual               of Accrual.fx_accrual
  | AmericanDigital       of American_digital.fx_americandigital
  | AverageOption         of Average_option.fx_averageoption
  | BarrierOption         of Barrier_option.fx_barrieroption
  | EuropeanDigital       of European_digital.fx_europeandigital
  | ExoticKnockIn         of Exotic_knockin.fx_exoticknockin
  | Forward               of Forward.fx_forward
  | ForwardStartDigital   of Forward_start_digital.fx_forwardstartdigital
  | ForwardStartOption    of Forward_start_option.fx_forwardstartoption
  | HurdleOption          of Hurdle_option.fx_hurdleoption
  | Lookback              of Lookback.fx_lookback
  | Miscellaneous         of Miscellaneous.fx_miscellaneous
  | Resettable            of Resettable.fx_resettable
  | SimpleOption          of Simple_option.fx_simpleoption
  | TransatlanticOption   of Transatlantic_option.fx_transatlanticoption
  | VolatilityProduct     of Volatility_product.fx_volatilityproduct
  | WindowAmericanDigital of Window_american_digital.fx_windowamericandigital
  | WindowBarrierOption   of Window_barrier_option.fx_windowbarrieroption
  | WindowEuropeanDigital of Window_european_digital.fx_windoweuropeandigital
]
with string_of, parse;


exception CategoryOfType of string;

value category_of_type: Options_type.fx_option_type -> Options_type.fx_category =
fun t ->
  let convertors: list (Options_type.fx_option_type -> option Options_type.fx_category) =
    [ 
      Accrual.category_of_fx_accrual;
      American_digital.category_of_fx_americandigital;
      Average_option.category_of_fx_averageoption;
      Barrier_option.category_of_fx_barrieroption;
      European_digital.category_of_fx_europeandigital;
      Exotic_knockin.category_of_fx_exoticknockin;
      Forward.category_of_fx_forward;
      Forward_start_digital.category_of_fx_forwardstartdigital;
      Forward_start_option.category_of_fx_forwardstartoption;
      Hurdle_option.category_of_fx_hurdleoption;
      Lookback.category_of_fx_lookback;
      Miscellaneous.category_of_fx_miscellaneous;
      Resettable.category_of_fx_resettable;
      Simple_option.category_of_fx_simpleoption;
      Transatlantic_option.category_of_fx_transatlanticoption;
      Volatility_product.category_of_fx_volatilityproduct;
      Window_american_digital.category_of_fx_windowamericandigital;
      Window_barrier_option.category_of_fx_windowbarrieroption;
      Window_european_digital.category_of_fx_windoweuropeandigital
    ]
  in
  let results: list (bool * Options_type.fx_category) = 
    List.map (fun f -> match (f t) with [ Some c -> (True, c) | None -> (False, Options_type.C_SimpleOption)]) convertors
  in
    List.assoc True results;


value cat_type_of_option: fx_option -> ( Options_type.fx_category * Options_type.fx_option_type ) =
fun o ->
  match o with
  [
    Accrual a                 -> Accrual.cat_type_of_fx_accrual a
  | AmericanDigital ad        -> American_digital.cat_type_of_fx_americandigital ad
  | AverageOption av          -> Average_option.cat_type_of_fx_averageoption av
  | BarrierOption bo          -> Barrier_option.cat_type_of_fx_barrieroption bo
  | EuropeanDigital ed        -> European_digital.cat_type_of_fx_europeandigital ed
  | ExoticKnockIn eki         -> Exotic_knockin.cat_type_of_fx_exoticknockin eki
  | Forward fwd               -> Forward.cat_type_of_fx_forward fwd
  | ForwardStartDigital fsd   -> Forward_start_digital.cat_type_of_fx_forwardstartdigital fsd
  | ForwardStartOption fso    -> Forward_start_option.cat_type_of_fx_forwardstartoption fso
  | HurdleOption ho           -> Hurdle_option.cat_type_of_fx_hurdleoption ho
  | Lookback lb               -> Lookback.cat_type_of_fx_lookback lb
  | Miscellaneous m           -> Miscellaneous.cat_type_of_fx_miscellaneous m
  | Resettable r              -> Resettable.cat_type_of_fx_resettable r
  | SimpleOption so           -> Simple_option.cat_type_of_fx_simpleoption so
  | TransatlanticOption t     -> Transatlantic_option.cat_type_of_fx_transatlanticoption t
  | VolatilityProduct vp      -> Volatility_product.cat_type_of_fx_volatilityproduct vp
  | WindowAmericanDigital wad -> Window_american_digital.cat_type_of_fx_windowamericandigital wad
  | WindowBarrierOption wbo   -> Window_barrier_option.cat_type_of_fx_windowbarrieroption wbo
  | WindowEuropeanDigital wed -> Window_european_digital.cat_type_of_fx_windoweuropeandigital wed
  ];


value make: Options_type.fx_option_type -> Options_utils.option_parameters -> fx_option =
fun t params ->     
  match category_of_type t with
  [
    Options_type.C_Accrual               -> Accrual (Accrual.make_fx_accrual t params)
  | Options_type.C_AmericanDigital       -> AmericanDigital (American_digital.make_fx_americandigital t params)
  | Options_type.C_AverageOption         -> AverageOption (Average_option.make_fx_averageoption t params)
  | Options_type.C_BarrierOption         -> BarrierOption (Barrier_option.make_fx_barrieroption t params)
  | Options_type.C_EuropeanDigital       -> EuropeanDigital (European_digital.make_fx_europeandigital t params)
  | Options_type.C_ExoticKnockIn         -> ExoticKnockIn (Exotic_knockin.make_fx_exoticknockin t params)
  | Options_type.C_Forward               -> Forward (Forward.make_fx_forward t params)
  | Options_type.C_ForwardStartDigital   -> ForwardStartDigital (Forward_start_digital.make_fx_forwardstartdigital t params)
  | Options_type.C_ForwardStartOption    -> ForwardStartOption (Forward_start_option.make_fx_forwardstartoption t params)
  | Options_type.C_HurdleOption          -> HurdleOption (Hurdle_option.make_fx_hurdleoption t params)
  | Options_type.C_Lookback              -> Lookback (Lookback.make_fx_lookback t params)
  | Options_type.C_Miscellaneous         -> Miscellaneous (Miscellaneous.make_fx_miscellaneous t params)
  | Options_type.C_Resettable            -> Resettable (Resettable.make_fx_resettable t params)
  | Options_type.C_SimpleOption          -> SimpleOption (Simple_option.make_fx_simpleoption t params)
  | Options_type.C_TransatlanticOption   -> TransatlanticOption (Transatlantic_option.make_fx_transatlanticoption t params)
  | Options_type.C_VolatilityProduct     -> VolatilityProduct (Volatility_product.make_fx_volatilityproduct t params)
  | Options_type.C_WindowAmericanDigital -> WindowAmericanDigital (Window_american_digital.make_fx_windowamericandigital t params)
  | Options_type.C_WindowBarrierOption   -> WindowBarrierOption (Window_barrier_option.make_fx_windowbarrieroption t params)
  | Options_type.C_WindowEuropeanDigital -> WindowEuropeanDigital (Window_european_digital.make_fx_windoweuropeandigital t params)
  ];


value option_params: fx_option -> Options_utils.option_parameters =
fun option ->
  match option with
  [
    Accrual o               -> Accrual.make_fx_accrual_params o
  | AmericanDigital o       -> American_digital.make_fx_americandigital_params o
  | AverageOption o         -> Average_option.make_fx_averageoption_params o
  | BarrierOption o         -> Barrier_option.make_fx_barrieroption_params o
  | EuropeanDigital o       -> European_digital.make_fx_europeandigital_params o
  | ExoticKnockIn o         -> Exotic_knockin.make_fx_exoticknockin_params o
  | Forward o               -> Forward.make_fx_forward_params o
  | ForwardStartDigital o   -> Forward_start_digital.make_fx_forwardstartdigital_params o
  | ForwardStartOption o    -> Forward_start_option.make_fx_forwardstartoption_params o
  | HurdleOption o          -> Hurdle_option.make_fx_hurdleoption_params o
  | Lookback o              -> Lookback.make_fx_lookback_params o
  | Miscellaneous o         -> Miscellaneous.make_fx_miscellaneous_params o
  | Resettable o            -> Resettable.make_fx_resettable_params o
  | SimpleOption o          -> Simple_option.make_fx_simpleoption_params o
  | TransatlanticOption o   -> Transatlantic_option.make_fx_transatlanticoption_params o
  | VolatilityProduct o     -> Volatility_product.make_fx_volatilityproduct_params o
  | WindowAmericanDigital o -> Window_american_digital.make_fx_windowamericandigital_params o
  | WindowBarrierOption o   -> Window_barrier_option.make_fx_windowbarrieroption_params o
  | WindowEuropeanDigital o -> Window_european_digital.make_fx_windoweuropeandigital_params o
  ];

