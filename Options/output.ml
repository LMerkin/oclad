
type option_output =
[
  AccruedNotional
| AlphaPrice
| AlphaPricePercentBase
| AlphaPricePercentDealt
| AlphaPriceAssetPips
| AlphaPricePips
| AnalyticVegas
| AssetZero
| Bar1
| Bar1ToT1
| Bar1ToT2
| Bar2
| Bar2ToT1
| Bar2ToT2
| BarCompCP
| BarCompStrike
| BarCompStrikeIsAsset
| BarMinMax
| BarrierDate
| BarrierDateToT2
| BarrierInfo
| BarrierTable
| BaseDepo
| BaseDepoToPayOut
| BaseDepoToStandardDelivery
| BaseDepoToT1
| BaseDepoToT2
| BaseGrannaExp
| BaseNotional
| BaseRhoBucketed
| BaseRhoExp
| BaseRhoExpPercentBase
| BaseRhoExpPercentDealt
| BaseRhoExpAssetPips
| BaseRhoExpPips
| BaseRhoPayout
| BaseRhoToT1
| BaseRhoToT1PercentBase
| BaseRhoToT1PercentDealt
| BaseRhoToT1AssetPips
| BaseRhoToT1Pips
| BaseRhoToT2
| BaseRhoToT2PercentBase
| BaseRhoToT2PercentDealt
| BaseRhoToT2AssetPips
| BaseRhoToT2Pips
| BaseZCRhoBucketed
| BaseZCRhoExp
| BaseZCRhoToT1
| BaseZCRhoToT2
| BaseZero
| BaseZeroToPayOut
| BaseZeroToStandardDelivery
| BaseZeroToT1
| BaseZeroToT2
| BidAsk
| BidAskDetails
| BidAskDetailsDrillDown
| BidAskVolPrice
| BreakEven
| CanHaveNegativeValue
| CheckFixings
| CompStrike
| COQS_10DeltaRegaPercentA
| COQS_10DeltaRegaPercentB
| COQS_10DeltaRegaA
| COQS_10DeltaRegaAPips
| COQS_10DeltaRegaB
| COQS_10DeltaRegaBPips
| COQS_10DeltaSegaPercentA
| COQS_10DeltaSegaPercentB
| COQS_10DeltaSegaA
| COQS_10DeltaSegaAPips
| COQS_10DeltaSegaB
| COQS_10DeltaSegaBPips
| COQS_1DThetaPercentA
| COQS_1DThetaPercentB
| COQS_1DThetaA
| COQS_1DThetaAPips
| COQS_1DThetaB
| COQS_1DThetaBPips
| COQS_25DeltaRegaPercentA
| COQS_25DeltaRegaPercentB
| COQS_25DeltaRegaA
| COQS_25DeltaRegaAPips
| COQS_25DeltaRegaB
| COQS_25DeltaRegaBPips
| COQS_25DeltaSegaPercentA
| COQS_25DeltaSegaPercentB
| COQS_25DeltaSegaA
| COQS_25DeltaSegaAPips
| COQS_25DeltaSegaB
| COQS_25DeltaSegaBPips
| COQS_AlphaPercentA
| COQS_AlphaPercentB
| COQS_AlphaA
| COQS_AlphaAPips
| COQS_AlphaB
| COQS_AlphaBPips
| COQS_BidAskNetDetailsPercentA
| COQS_BidAskNetDetailsPercentB
| COQS_BidAskNetDetailsA
| COQS_BidAskNetDetailsAPips
| COQS_BidAskNetDetailsB
| COQS_BidAskNetDetailsBPips
| COQS_BidAskSpreadPercentA
| COQS_BidAskSpreadPercentB
| COQS_BidAskSpreadA
| COQS_BidAskSpreadAPips
| COQS_BidAskSpreadB
| COQS_BidAskSpreadBPips
| COQS_BidAskSPricePercentA
| COQS_BidAskSPricePercentB
| COQS_BidAskSPriceA
| COQS_BidAskSPriceAPips
| COQS_BidAskSPriceB
| COQS_BidAskSPriceBPips
| COQS_CreditRiskPercentA
| COQS_CreditRiskPercentB
| COQS_CreditRiskA
| COQS_CreditRiskAPips
| COQS_CreditRiskB
| COQS_CreditRiskBPips
| COQS_DefaultSPricePercentA
| COQS_DefaultSPricePercentB
| COQS_DefaultSPriceA
| COQS_DefaultSPriceAPips
| COQS_DefaultSPriceB
| COQS_DefaultSPriceBPips
| COQS_DeltaPercentA
| COQS_DeltaPercentB
| COQS_DeltaA
| COQS_DeltaAPips
| COQS_DeltaB
| COQS_DeltaBPips
| COQS_DeltaGapDownPercentA
| COQS_DeltaGapDownPercentB
| COQS_DeltaGapDownA
| COQS_DeltaGapDownAPips
| COQS_DeltaGapDownB
| COQS_DeltaGapDownBPips
| COQS_DeltaGapUpPercentA
| COQS_DeltaGapUpPercentB
| COQS_DeltaGapUpA
| COQS_DeltaGapUpAPips
| COQS_DeltaGapUpB
| COQS_DeltaGapUpBPips
| COQS_DerivePercentA
| COQS_DerivePercentB
| COQS_DeriveA
| COQS_DeriveAPips
| COQS_DeriveB
| COQS_DeriveBPips
| COQS_DVega15DSPricePercentA
| COQS_DVega15DSPricePercentB
| COQS_DVega15DSPriceA
| COQS_DVega15DSPriceAPips
| COQS_DVega15DSPriceB
| COQS_DVega15DSPriceBPips
| COQS_DVegaAtHit15DSPricePercentA
| COQS_DVegaAtHit15DSPricePercentB
| COQS_DVegaAtHit15DSPriceA
| COQS_DVegaAtHit15DSPriceAPips
| COQS_DVegaAtHit15DSPriceB
| COQS_DVegaAtHit15DSPriceBPips
| COQS_DVegaAtHitSPricePercentA
| COQS_DVegaAtHitSPricePercentB
| COQS_DVegaAtHitSPriceA
| COQS_DVegaAtHitSPriceAPips
| COQS_DVegaAtHitSPriceB
| COQS_DVegaAtHitSPriceBPips
| COQS_DVegaSDeltaPercentA
| COQS_DVegaSDeltaPercentB
| COQS_DVegaSDeltaA
| COQS_DVegaSDeltaAPips
| COQS_DVegaSDeltaB
| COQS_DVegaSDeltaBPips
| COQS_DVegaSPricePercentA
| COQS_DVegaSPricePercentB
| COQS_DVegaSPriceA
| COQS_DVegaSPriceAPips
| COQS_DVegaSPriceB
| COQS_DVegaSPriceBPips
| COQS_DVegaSVegaPercentA
| COQS_DVegaSVegaPercentB
| COQS_DVegaSVegaA
| COQS_DVegaSVegaAPips
| COQS_DVegaSVegaB
| COQS_DVegaSVegaBPips
| COQS_FwdDeltaPercentA
| COQS_FwdDeltaPercentB
| COQS_FwdDeltaA
| COQS_FwdDeltaAPips
| COQS_FwdDeltaB
| COQS_FwdDeltaBPips
| COQS_GammaPercentA
| COQS_GammaPercentB
| COQS_GammaA
| COQS_GammaAPips
| COQS_GammaB
| COQS_GammaBPips
| COQS_GusSPricePercentA
| COQS_GusSPricePercentB
| COQS_GusSPriceA
| COQS_GusSPriceAPips
| COQS_GusSPriceB
| COQS_GusSPriceBPips
| COQS_OfficialPercentA
| COQS_OfficialPercentB
| COQS_OfficialA
| COQS_OfficialAPips
| COQS_OfficialB
| COQS_OfficialBPips
| COQS_PricePercentA
| COQS_PricePercentB
| COQS_PriceA
| COQS_PriceAPips
| COQS_PriceB
| COQS_PriceBPips
| COQS_RealSThetaPercentA
| COQS_RealSThetaPercentB
| COQS_RealSThetaA
| COQS_RealSThetaAPips
| COQS_RealSThetaB
| COQS_RealSThetaBPips
| COQS_RealThetaPercentA
| COQS_RealThetaPercentB
| COQS_RealThetaA
| COQS_RealThetaAPips
| COQS_RealThetaB
| COQS_RealThetaBPips
| COQS_ReplSPricePercentA
| COQS_ReplSPricePercentB
| COQS_ReplSPriceA
| COQS_ReplSPriceAPips
| COQS_ReplSPriceB
| COQS_ReplSPriceBPips
| COQS_RhoAPercentA
| COQS_RhoAPercentB
| COQS_RhoAA
| COQS_RhoAAPips
| COQS_RhoAB
| COQS_RhoABPips
| COQS_RhoBPercentA
| COQS_RhoBPercentB
| COQS_RhoBA
| COQS_RhoBAPips
| COQS_RhoBB
| COQS_RhoBBPips
| COQS_S10DeltaRegaPercentA
| COQS_S10DeltaRegaPercentB
| COQS_S10DeltaRegaA
| COQS_S10DeltaRegaAPips
| COQS_S10DeltaRegaB
| COQS_S10DeltaRegaBPips
| COQS_S10DeltaSegaPercentA
| COQS_S10DeltaSegaPercentB
| COQS_S10DeltaSegaA
| COQS_S10DeltaSegaAPips
| COQS_S10DeltaSegaB
| COQS_S10DeltaSegaBPips
| COQS_S25DeltaRegaPercentA
| COQS_S25DeltaRegaPercentB
| COQS_S25DeltaRegaA
| COQS_S25DeltaRegaAPips
| COQS_S25DeltaRegaB
| COQS_S25DeltaRegaBPips
| COQS_S25DeltaSegaPercentA
| COQS_S25DeltaSegaPercentB
| COQS_S25DeltaSegaA
| COQS_S25DeltaSegaAPips
| COQS_S25DeltaSegaB
| COQS_S25DeltaSegaBPips
| COQS_SDeltaPercentA
| COQS_SDeltaPercentB
| COQS_SDeltaA
| COQS_SDeltaAPips
| COQS_SDeltaB
| COQS_SDeltaBPips
| COQS_SDeltaGapDownPercentA
| COQS_SDeltaGapDownPercentB
| COQS_SDeltaGapDownA
| COQS_SDeltaGapDownAPips
| COQS_SDeltaGapDownB
| COQS_SDeltaGapDownBPips
| COQS_SDeltaGapUpPercentA
| COQS_SDeltaGapUpPercentB
| COQS_SDeltaGapUpA
| COQS_SDeltaGapUpAPips
| COQS_SDeltaGapUpB
| COQS_SDeltaGapUpBPips
| COQS_SDerivePercentA
| COQS_SDerivePercentB
| COQS_SDeriveA
| COQS_SDeriveAPips
| COQS_SDeriveB
| COQS_SDeriveBPips
| COQS_SFwdDeltaPercentA
| COQS_SFwdDeltaPercentB
| COQS_SFwdDeltaA
| COQS_SFwdDeltaAPips
| COQS_SFwdDeltaB
| COQS_SFwdDeltaBPips
| COQS_SGammaPercentA
| COQS_SGammaPercentB
| COQS_SGammaA
| COQS_SGammaAPips
| COQS_SGammaB
| COQS_SGammaBPips
| COQS_SLVPricePercentA
| COQS_SLVPricePercentB
| COQS_SLVPriceA
| COQS_SLVPriceAPips
| COQS_SLVPriceB
| COQS_SLVPriceBPips
| COQS_SpreadPercentA
| COQS_SpreadPercentB
| COQS_SpreadA
| COQS_SpreadAPips
| COQS_SpreadB
| COQS_SpreadBPips
| COQS_SPricePercentA
| COQS_SPricePercentB
| COQS_SPriceA
| COQS_SPriceAPips
| COQS_SPriceB
| COQS_SPriceBPips
| COQS_SSDeltaPercentA
| COQS_SSDeltaPercentB
| COQS_SSDeltaA
| COQS_SSDeltaAPips
| COQS_SSDeltaB
| COQS_SSDeltaBPips
| COQS_SSGammaPercentA
| COQS_SSGammaPercentB
| COQS_SSGammaA
| COQS_SSGammaAPips
| COQS_SSGammaB
| COQS_SSGammaBPips
| COQS_SVDupirePercentA
| COQS_SVDupirePercentB
| COQS_SVDupireA
| COQS_SVDupireAPips
| COQS_SVDupireB
| COQS_SVDupireBPips
| COQS_SVegaPercentA
| COQS_SVegaPercentB
| COQS_SVegaA
| COQS_SVegaAPips
| COQS_SVegaB
| COQS_SVegaBPips
| COQS_SVegaBSpread
| COQS_SVegaWeightedPercentA
| COQS_SVegaWeightedPercentB
| COQS_SVegaWeightedA
| COQS_SVegaWeightedAPips
| COQS_SVegaWeightedB
| COQS_SVegaWeightedBPips
| COQS_SVPdePercentA
| COQS_SVPdePercentB
| COQS_SVPdeA
| COQS_SVPdeAPips
| COQS_SVPdeB
| COQS_SVPdeBPips
| COQS_SVPlusSPricePercentA
| COQS_SVPlusSPricePercentB
| COQS_SVPlusSPriceA
| COQS_SVPlusSPriceAPips
| COQS_SVPlusSPriceB
| COQS_SVPlusSPriceBPips
| COQS_SVPricePercentA
| COQS_SVPricePercentB
| COQS_SVPriceA
| COQS_SVPriceAPips
| COQS_SVPriceB
| COQS_SVPriceBPips
| COQS_SVSPricePercentA
| COQS_SVSPricePercentB
| COQS_SVSPriceA
| COQS_SVSPriceAPips
| COQS_SVSPriceB
| COQS_SVSPriceBPips
| COQS_SVvolPercentA
| COQS_SVvolPercentB
| COQS_SVvolA
| COQS_SVvolAPips
| COQS_SVvolB
| COQS_SVvolBPips
| COQS_VegaPercentA
| COQS_VegaPercentB
| COQS_VegaA
| COQS_VegaAPips
| COQS_VegaB
| COQS_VegaBPips
| COQS_VegaWeightedPercentA
| COQS_VegaWeightedPercentB
| COQS_VegaWeightedA
| COQS_VegaWeightedAPips
| COQS_VegaWeightedB
| COQS_VegaWeightedBPips
| COQS_VRDeltaPercentA
| COQS_VRDeltaPercentB
| COQS_VRDeltaA
| COQS_VRDeltaAPips
| COQS_VRDeltaB
| COQS_VRDeltaBPips
| COQS_VRGammaPercentA
| COQS_VRGammaPercentB
| COQS_VRGammaA
| COQS_VRGammaAPips
| COQS_VRGammaB
| COQS_VRGammaBPips
| COQS_VvolPercentA
| COQS_VvolPercentB
| COQS_VvolA
| COQS_VvolAPips
| COQS_VvolB
| COQS_VvolBPips
| CoqsDrillDownOutputs
| CoqsOutputs
| CP
| CrossDeltaBaseRhoExp
| CrossGrannaExp
| CrossVegaBaseRhoExp
| CrossVegaDealtRhoExp
| Cut
| DealtDepo
| DealtDepoToPayOut
| DealtDepoToStandardDelivery
| DealtDepoToT1
| DealtDepoToT2
| DealtNotional
| DealtRhoBucketed
| DealtRhoExp
| DealtRhoExpPercentBase
| DealtRhoExpPercentDealt
| DealtRhoExpAssetPips
| DealtRhoExpPips
| DealtRhoPayout
| DealtRhoToT1
| DealtRhoToT1PercentBase
| DealtRhoToT1PercentDealt
| DealtRhoToT1AssetPips
| DealtRhoToT1Pips
| DealtRhoToT2
| DealtRhoToT2PercentBase
| DealtRhoToT2PercentDealt
| DealtRhoToT2AssetPips
| DealtRhoToT2Pips
| DealtZCRhoBucketed
| DealtZCRhoExp
| DealtZCRhoToT1
| DealtZCRhoToT2
| DealtZero
| DealtZeroToPayOut
| DealtZeroToStandardDelivery
| DealtZeroToT1
| DealtZeroToT2
| DefaultSPrice
| DeliveryDate
| DeliveryDateToT1
| DeliveryDateToT2
| Delta
| DeltaPercentBase
| DeltaPercentDealt
| DeltaAssetPips
| DeltaBase
| DeltaDealt
| DeltaGapsA
| DeltaGapsB
| DeltaJumps
| DeltaMatrix
| DeltaPips
| Deltas
| DeltaWithStopLoss
| Derive
| DerivePercentBase
| DerivePercentDealt
| DeriveAssetPips
| DeriveBase
| DeriveDealt
| DerivePips
| Description
| DigitalJumpDown
| DigitalJumpUp
| DVega25DeltaRegaB
| DVega25DeltaSegaB
| DVegaAtHitBounds
| DvegaAtHitShifts
| DVegaAtHitSPrice
| DVegaDFwd
| DVegaDFwdT1
| DVegaDVol
| DVegaDVolToT1
| DVegaDVolToT2
| DVegaExpDSpot
| DVegaImplemented
| DVegaSDelta
| DVegaSPrice
| DVegaT1DFwd
| DVegaT1DFwdT1
| DVegaT1DSpot
| DVegaT2DFwdT2
| DVegaToT1DVol
| DVegaToT1DVolToT1
| DVegaToT1DVolToT2
| DVegaToT2DSpot
| DVegaToT2DVol
| DVegaToT2DVolToT1
| DVegaToT2DVolToT2
| EventInfo
| Events
| ExpiryDate
| ExpiryDateToT1
| ExpiryDateToT2
| Factors
| Fixings
| FlexBarType
| Forward
| FullBarrierInfo
| FullBarrierInfoDll
| Fwd
| FwdDelta
| FwdDeltaBucketed
| FwdDeltaBucketedSpread
| FwdDeltaExpiry
| FwdDeltaPayout
| FwdDeltaToT1
| FwdDeltaToT2
| FwdStrike
| FwdToStandardDelivery
| FwdToT1
| FwdToT2
| Gamma
| GammaPercentBase
| GammaPercentDealt
| GammaAssetPips
| GammaBase
| GammaDealt
| GammaPips
| Gearing
| GSVParameters
| GSVVolErrors
| GusSPrice
| Hedges
| HestonHedge
| HestonProbOfCrossing
| HitDate
| HurdleDown
| HurdleDownToT1
| HurdleUp
| HurdleUpToT1
| InputDescription
| Inputs
| IntrinsicValue
| InvAlphaPrice
| InvBaseRhoExp
| InvBaseRhoToT1
| InvBaseRhoToT2
| InvDealtRhoExp
| InvDealtRhoToT1
| InvDealtRhoToT2
| InvDelta
| InvDeltaWithStopLoss
| InvDerive
| InverseBidAsk
| InvGamma
| InvIntrinsicValue
| InvSDelta
| InvSGamma
| InvSmileDeltaWithStopLoss
| InvSmileValueWithStopLoss
| InvSSDelta
| InvSSGamma
| InvThetaExp
| InvTotalDerive
| InvTotalSmileDerive
| InvTotalSmileVvol
| InvTotalVega
| InvTotalVvol
| InvValue
| InvValueWithStopLoss
| InvVega
| InvVegaExp
| InvVegaToT1
| InvVegaToT2
| InvVegaWeighted
| InvVRDelta
| InvVRGamma
| InvVvol
| IsExerciseAllowed
| IsExpiryAllowed
| IsKoAllowed
| IsMultiHurdle
| Lifetime
| Lifetimes
| LimitClass
| LimitInfo
| LookUpHestonPoint
| MarketData
| MarketVols
| MinBarDays
| MinMax
| MoneyZero
| NextBarrierInfo
| NextPinRisk
| Notional
| NotionalAndPayOut
| NotionalB
| NumBarDays
| OneDTheta
| OneDThetaPercentBase
| OneDThetaPercentDealt
| OneDThetaAssetPips
| OneDThetaBase
| OneDThetaDealt
| OneDThetaPips
| OptimisedVolSurface
| OptionDetails
| OptionInfo
| OptionType
| PassportNotionalStep
| PassportPL
| PassportPosition
| PayOutDate
| PinRisk
| PinRiskDrillDown
| PosAdjustments
| Positions
| PredictedDelta
| PredictedDerive
| PredictedGamma
| PredictedTotalDerive
| PricePercentBase
| PricePercentDealt
| PriceAssetPips
| PriceBase
| PriceDealt
| PricePips
| QuantoDeltas
| QuantoDerives
| QuantoGammas
| QuantoPrice
| QuantoSDeltas
| QuantoSGammas
| QuantoSPrice
| QuantoSVegas
| QuantoVegas
| QuantoVvols
| RealSTheta
| RealTheta
| RR25
| SCcyFormulaExecutionMode
| SDelta
| SGamma
| Shifts
| SLVVolErrors
| SmileAnnualisedOneDTheta
| SmileAtmVegaBucketed
| SmileAtmVegaBucketedSpread
| SmileBaseRhoBucketed
| SmileBaseZCRhoBucketed
| SmileDealtRhoBucketed
| SmileDealtZCRhoBucketed
| SmileDeltaGapsA
| SmileDeltaGapsB
| SmileDeltaWithStopLoss
| SmileFwdDelta
| SmileFwdDeltaBucketed
| SmileFwdDeltaBucketedSpread
| SmileFwdDeltaExpiry
| SmileFwdDeltaPayout
| SmileFwdDeltaToT1
| SmileFwdDeltaToT2
| SmileOneDTheta
| SmileRega10DeltaBucketed
| SmileRega10DeltaBucketedSpread
| SmileRega25DeltaBucketed
| SmileRega25DeltaBucketedSpread
| SmileSega10DeltaBucketed
| SmileSega10DeltaBucketedSpread
| SmileSega25DeltaBucketed
| SmileSega25DeltaBucketedSpread
| SmileSpotDelta
| SmileTotalBaseRho
| SmileTotalDealtRho
| SmileValueWithStopLoss
| SmileVegaBucketed
| SmileVegaBucketedSpread
| SpotDate
| SpotDelta
| SSDelta
| SSGamma
| Strike
| SVDupireParameters
| SVDupireVolErrors
| SVHedge
| SVMCVanillaError
| SVParameters
| SVPdePrice
| SVPDEVanillaError
| SVPrice
| SVProbOfCrossing
| SVSPrice
| SVVanillaError
| SVVolErrors
| SVVols
| ThetaExp
| ThetaExpPercentBase
| ThetaExpPercentDealt
| ThetaExpAssetPips
| ThetaExpPips
| TotalBaseRho
| TotalBaseRhoPercentBase
| TotalBaseRhoPercentDealt
| TotalBaseRhoAssetPips
| TotalDealtRho
| TotalDealtRhoPercentBase
| TotalDealtRhoPercentDealt
| TotalDealtRhoAssetPips
| TotalDerive
| TotalDerivePercentBase
| TotalDerivePercentDealt
| TotalDeriveAssetPips
| TotalDerivePips
| TotalDeriveWeighted
| TotalFwdDerive
| TotalLooks
| TotalSmileDerive
| TotalSmileVvol
| TotalVega
| TotalVegaPercentBase
| TotalVegaPercentDealt
| TotalVegaAssetPips
| TotalVegaBase
| TotalVegaDealt
| TotalVegaPips
| TotalVvol
| TotalVvolPercentBase
| TotalVvolPercentDealt
| TotalVvolAssetPips
| TotalVvolPips
| TradeDate
| TRFPinRisk
| TRFPinRiskDrillDown
| TypeAndParams
| Types
| UnderlyingPrice
| UsingNumericalMethod
| Value
| ValueBase
| ValueDealt
| ValueWithStopLoss
| Vega
| VegaPercentBase
| VegaPercentDealt
| VegaAssetPips
| VegaBase
| VegaBucketed
| VegaDealt
| VegaExp
| VegaExpPercentBase
| VegaExpPercentDealt
| VegaExpAssetPips
| VegaExpPips
| VegaPips
| VegaT1PercentBase
| VegaT1PercentDealt
| VegaT1AssetPips
| VegaT1Pips
| VegaT2PercentBase
| VegaT2PercentDealt
| VegaT2AssetPips
| VegaT2Pips
| VegaToT1
| VegaToT2
| VegaWeighted
| Vol
| VolToT1
| VolToT2
| VRDelta
| VRGamma
| Vvol
| VvolPercentBase
| VvolPercentDealt
| VvolAssetPips
| VVolBase
| VvolDealt
| VvolPips
| ZeroOrNegativeStrike
];

(*  Needs explicit implementation as we have a types like AlphaPricePercentBase which needs to generate "AlphaPrice%Base" *)

value string_of_option_output: option_output -> string =
    fun oo ->
      match oo with
      [
        AccruedNotional -> "AccruedNotional"
      | AlphaPrice -> "AlphaPrice"
      | AlphaPricePercentBase -> "AlphaPrice%Base"
      | AlphaPricePercentDealt -> "AlphaPrice%Dealt"
      | AlphaPriceAssetPips -> "AlphaPriceAssetPips"
      | AlphaPricePips -> "AlphaPricePips"
      | AnalyticVegas -> "AnalyticVegas?"
      | AssetZero -> "AssetZero"
      | Bar1 -> "Bar1"
      | Bar1ToT1 -> "Bar1ToT1"
      | Bar1ToT2 -> "Bar1ToT2"
      | Bar2 -> "Bar2"
      | Bar2ToT1 -> "Bar2ToT1"
      | Bar2ToT2 -> "Bar2ToT2"
      | BarCompCP -> "BarCompCP"
      | BarCompStrike -> "BarCompStrike"
      | BarCompStrikeIsAsset -> "BarCompStrikeIsAsset"
      | BarMinMax -> "BarMinMax"
      | BarrierDate -> "BarrierDate"
      | BarrierDateToT2 -> "BarrierDateToT2"
      | BarrierInfo -> "BarrierInfo"
      | BarrierTable -> "BarrierTable"
      | BaseDepo -> "BaseDepo"
      | BaseDepoToPayOut -> "BaseDepoToPayOut"
      | BaseDepoToStandardDelivery -> "BaseDepoToStandardDelivery"
      | BaseDepoToT1 -> "BaseDepoToT1"
      | BaseDepoToT2 -> "BaseDepoToT2"
      | BaseGrannaExp -> "BaseGrannaExp"
      | BaseNotional -> "BaseNotional"
      | BaseRhoBucketed -> "BaseRhoBucketed"
      | BaseRhoExp -> "BaseRhoExp"
      | BaseRhoExpPercentBase -> "BaseRhoExp%Base"
      | BaseRhoExpPercentDealt -> "BaseRhoExp%Dealt"
      | BaseRhoExpAssetPips -> "BaseRhoExpAssetPips"
      | BaseRhoExpPips -> "BaseRhoExpPips"
      | BaseRhoPayout -> "BaseRhoPayout"
      | BaseRhoToT1 -> "BaseRhoToT1"
      | BaseRhoToT1PercentBase -> "BaseRhoToT1%Base"
      | BaseRhoToT1PercentDealt -> "BaseRhoToT1%Dealt"
      | BaseRhoToT1AssetPips -> "BaseRhoToT1AssetPips"
      | BaseRhoToT1Pips -> "BaseRhoToT1Pips"
      | BaseRhoToT2 -> "BaseRhoToT2"
      | BaseRhoToT2PercentBase -> "BaseRhoToT2%Base"
      | BaseRhoToT2PercentDealt -> "BaseRhoToT2%Dealt"
      | BaseRhoToT2AssetPips -> "BaseRhoToT2AssetPips"
      | BaseRhoToT2Pips -> "BaseRhoToT2Pips"
      | BaseZCRhoBucketed -> "BaseZCRhoBucketed"
      | BaseZCRhoExp -> "BaseZCRhoExp"
      | BaseZCRhoToT1 -> "BaseZCRhoToT1"
      | BaseZCRhoToT2 -> "BaseZCRhoToT2"
      | BaseZero -> "BaseZero"
      | BaseZeroToPayOut -> "BaseZeroToPayOut"
      | BaseZeroToStandardDelivery -> "BaseZeroToStandardDelivery"
      | BaseZeroToT1 -> "BaseZeroToT1"
      | BaseZeroToT2 -> "BaseZeroToT2"
      | BidAsk -> "BidAsk"
      | BidAskDetails -> "BidAskDetails"
      | BidAskDetailsDrillDown -> "BidAskDetailsDrillDown"
      | BidAskVolPrice -> "BidAskVolPrice"
      | BreakEven -> "BreakEven"
      | CanHaveNegativeValue -> "CanHaveNegativeValue"
      | CheckFixings -> "CheckFixings"
      | CompStrike -> "CompStrike"
      | COQS_10DeltaRegaPercentA -> "COQS_10DeltaRega%A"
      | COQS_10DeltaRegaPercentB -> "COQS_10DeltaRega%B"
      | COQS_10DeltaRegaA -> "COQS_10DeltaRegaA"
      | COQS_10DeltaRegaAPips -> "COQS_10DeltaRegaAPips"
      | COQS_10DeltaRegaB -> "COQS_10DeltaRegaB"
      | COQS_10DeltaRegaBPips -> "COQS_10DeltaRegaBPips"
      | COQS_10DeltaSegaPercentA -> "COQS_10DeltaSega%A"
      | COQS_10DeltaSegaPercentB -> "COQS_10DeltaSega%B"
      | COQS_10DeltaSegaA -> "COQS_10DeltaSegaA"
      | COQS_10DeltaSegaAPips -> "COQS_10DeltaSegaAPips"
      | COQS_10DeltaSegaB -> "COQS_10DeltaSegaB"
      | COQS_10DeltaSegaBPips -> "COQS_10DeltaSegaBPips"
      | COQS_1DThetaPercentA -> "COQS_1DTheta%A"
      | COQS_1DThetaPercentB -> "COQS_1DTheta%B"
      | COQS_1DThetaA -> "COQS_1DThetaA"
      | COQS_1DThetaAPips -> "COQS_1DThetaAPips"
      | COQS_1DThetaB -> "COQS_1DThetaB"
      | COQS_1DThetaBPips -> "COQS_1DThetaBPips"
      | COQS_25DeltaRegaPercentA -> "COQS_25DeltaRega%A"
      | COQS_25DeltaRegaPercentB -> "COQS_25DeltaRega%B"
      | COQS_25DeltaRegaA -> "COQS_25DeltaRegaA"
      | COQS_25DeltaRegaAPips -> "COQS_25DeltaRegaAPips"
      | COQS_25DeltaRegaB -> "COQS_25DeltaRegaB"
      | COQS_25DeltaRegaBPips -> "COQS_25DeltaRegaBPips"
      | COQS_25DeltaSegaPercentA -> "COQS_25DeltaSega%A"
      | COQS_25DeltaSegaPercentB -> "COQS_25DeltaSega%B"
      | COQS_25DeltaSegaA -> "COQS_25DeltaSegaA"
      | COQS_25DeltaSegaAPips -> "COQS_25DeltaSegaAPips"
      | COQS_25DeltaSegaB -> "COQS_25DeltaSegaB"
      | COQS_25DeltaSegaBPips -> "COQS_25DeltaSegaBPips"
      | COQS_AlphaPercentA -> "COQS_Alpha%A"
      | COQS_AlphaPercentB -> "COQS_Alpha%B"
      | COQS_AlphaA -> "COQS_AlphaA"
      | COQS_AlphaAPips -> "COQS_AlphaAPips"
      | COQS_AlphaB -> "COQS_AlphaB"
      | COQS_AlphaBPips -> "COQS_AlphaBPips"
      | COQS_BidAskNetDetailsPercentA -> "COQS_BidAskNetDetails%A"
      | COQS_BidAskNetDetailsPercentB -> "COQS_BidAskNetDetails%B"
      | COQS_BidAskNetDetailsA -> "COQS_BidAskNetDetailsA"
      | COQS_BidAskNetDetailsAPips -> "COQS_BidAskNetDetailsAPips"
      | COQS_BidAskNetDetailsB -> "COQS_BidAskNetDetailsB"
      | COQS_BidAskNetDetailsBPips -> "COQS_BidAskNetDetailsBPips"
      | COQS_BidAskSpreadPercentA -> "COQS_BidAskSpread%A"
      | COQS_BidAskSpreadPercentB -> "COQS_BidAskSpread%B"
      | COQS_BidAskSpreadA -> "COQS_BidAskSpreadA"
      | COQS_BidAskSpreadAPips -> "COQS_BidAskSpreadAPips"
      | COQS_BidAskSpreadB -> "COQS_BidAskSpreadB"
      | COQS_BidAskSpreadBPips -> "COQS_BidAskSpreadBPips"
      | COQS_BidAskSPricePercentA -> "COQS_BidAskSPrice%A"
      | COQS_BidAskSPricePercentB -> "COQS_BidAskSPrice%B"
      | COQS_BidAskSPriceA -> "COQS_BidAskSPriceA"
      | COQS_BidAskSPriceAPips -> "COQS_BidAskSPriceAPips"
      | COQS_BidAskSPriceB -> "COQS_BidAskSPriceB"
      | COQS_BidAskSPriceBPips -> "COQS_BidAskSPriceBPips"
      | COQS_CreditRiskPercentA -> "COQS_CreditRisk%A"
      | COQS_CreditRiskPercentB -> "COQS_CreditRisk%B"
      | COQS_CreditRiskA -> "COQS_CreditRiskA"
      | COQS_CreditRiskAPips -> "COQS_CreditRiskAPips"
      | COQS_CreditRiskB -> "COQS_CreditRiskB"
      | COQS_CreditRiskBPips -> "COQS_CreditRiskBPips"
      | COQS_DefaultSPricePercentA -> "COQS_DefaultSPrice%A"
      | COQS_DefaultSPricePercentB -> "COQS_DefaultSPrice%B"
      | COQS_DefaultSPriceA -> "COQS_DefaultSPriceA"
      | COQS_DefaultSPriceAPips -> "COQS_DefaultSPriceAPips"
      | COQS_DefaultSPriceB -> "COQS_DefaultSPriceB"
      | COQS_DefaultSPriceBPips -> "COQS_DefaultSPriceBPips"
      | COQS_DeltaPercentA -> "COQS_Delta%A"
      | COQS_DeltaPercentB -> "COQS_Delta%B"
      | COQS_DeltaA -> "COQS_DeltaA"
      | COQS_DeltaAPips -> "COQS_DeltaAPips"
      | COQS_DeltaB -> "COQS_DeltaB"
      | COQS_DeltaBPips -> "COQS_DeltaBPips"
      | COQS_DeltaGapDownPercentA -> "COQS_DeltaGapDown%A"
      | COQS_DeltaGapDownPercentB -> "COQS_DeltaGapDown%B"
      | COQS_DeltaGapDownA -> "COQS_DeltaGapDownA"
      | COQS_DeltaGapDownAPips -> "COQS_DeltaGapDownAPips"
      | COQS_DeltaGapDownB -> "COQS_DeltaGapDownB"
      | COQS_DeltaGapDownBPips -> "COQS_DeltaGapDownBPips"
      | COQS_DeltaGapUpPercentA -> "COQS_DeltaGapUp%A"
      | COQS_DeltaGapUpPercentB -> "COQS_DeltaGapUp%B"
      | COQS_DeltaGapUpA -> "COQS_DeltaGapUpA"
      | COQS_DeltaGapUpAPips -> "COQS_DeltaGapUpAPips"
      | COQS_DeltaGapUpB -> "COQS_DeltaGapUpB"
      | COQS_DeltaGapUpBPips -> "COQS_DeltaGapUpBPips"
      | COQS_DerivePercentA -> "COQS_Derive%A"
      | COQS_DerivePercentB -> "COQS_Derive%B"
      | COQS_DeriveA -> "COQS_DeriveA"
      | COQS_DeriveAPips -> "COQS_DeriveAPips"
      | COQS_DeriveB -> "COQS_DeriveB"
      | COQS_DeriveBPips -> "COQS_DeriveBPips"
      | COQS_DVega15DSPricePercentA -> "COQS_DVega15DSPrice%A"
      | COQS_DVega15DSPricePercentB -> "COQS_DVega15DSPrice%B"
      | COQS_DVega15DSPriceA -> "COQS_DVega15DSPriceA"
      | COQS_DVega15DSPriceAPips -> "COQS_DVega15DSPriceAPips"
      | COQS_DVega15DSPriceB -> "COQS_DVega15DSPriceB"
      | COQS_DVega15DSPriceBPips -> "COQS_DVega15DSPriceBPips"
      | COQS_DVegaAtHit15DSPricePercentA -> "COQS_DVegaAtHit15DSPrice%A"
      | COQS_DVegaAtHit15DSPricePercentB -> "COQS_DVegaAtHit15DSPrice%B"
      | COQS_DVegaAtHit15DSPriceA -> "COQS_DVegaAtHit15DSPriceA"
      | COQS_DVegaAtHit15DSPriceAPips -> "COQS_DVegaAtHit15DSPriceAPips"
      | COQS_DVegaAtHit15DSPriceB -> "COQS_DVegaAtHit15DSPriceB"
      | COQS_DVegaAtHit15DSPriceBPips -> "COQS_DVegaAtHit15DSPriceBPips"
      | COQS_DVegaAtHitSPricePercentA -> "COQS_DVegaAtHitSPrice%A"
      | COQS_DVegaAtHitSPricePercentB -> "COQS_DVegaAtHitSPrice%B"
      | COQS_DVegaAtHitSPriceA -> "COQS_DVegaAtHitSPriceA"
      | COQS_DVegaAtHitSPriceAPips -> "COQS_DVegaAtHitSPriceAPips"
      | COQS_DVegaAtHitSPriceB -> "COQS_DVegaAtHitSPriceB"
      | COQS_DVegaAtHitSPriceBPips -> "COQS_DVegaAtHitSPriceBPips"
      | COQS_DVegaSDeltaPercentA -> "COQS_DVegaSDelta%A"
      | COQS_DVegaSDeltaPercentB -> "COQS_DVegaSDelta%B"
      | COQS_DVegaSDeltaA -> "COQS_DVegaSDeltaA"
      | COQS_DVegaSDeltaAPips -> "COQS_DVegaSDeltaAPips"
      | COQS_DVegaSDeltaB -> "COQS_DVegaSDeltaB"
      | COQS_DVegaSDeltaBPips -> "COQS_DVegaSDeltaBPips"
      | COQS_DVegaSPricePercentA -> "COQS_DVegaSPrice%A"
      | COQS_DVegaSPricePercentB -> "COQS_DVegaSPrice%B"
      | COQS_DVegaSPriceA -> "COQS_DVegaSPriceA"
      | COQS_DVegaSPriceAPips -> "COQS_DVegaSPriceAPips"
      | COQS_DVegaSPriceB -> "COQS_DVegaSPriceB"
      | COQS_DVegaSPriceBPips -> "COQS_DVegaSPriceBPips"
      | COQS_DVegaSVegaPercentA -> "COQS_DVegaSVega%A"
      | COQS_DVegaSVegaPercentB -> "COQS_DVegaSVega%B"
      | COQS_DVegaSVegaA -> "COQS_DVegaSVegaA"
      | COQS_DVegaSVegaAPips -> "COQS_DVegaSVegaAPips"
      | COQS_DVegaSVegaB -> "COQS_DVegaSVegaB"
      | COQS_DVegaSVegaBPips -> "COQS_DVegaSVegaBPips"
      | COQS_FwdDeltaPercentA -> "COQS_FwdDelta%A"
      | COQS_FwdDeltaPercentB -> "COQS_FwdDelta%B"
      | COQS_FwdDeltaA -> "COQS_FwdDeltaA"
      | COQS_FwdDeltaAPips -> "COQS_FwdDeltaAPips"
      | COQS_FwdDeltaB -> "COQS_FwdDeltaB"
      | COQS_FwdDeltaBPips -> "COQS_FwdDeltaBPips"
      | COQS_GammaPercentA -> "COQS_Gamma%A"
      | COQS_GammaPercentB -> "COQS_Gamma%B"
      | COQS_GammaA -> "COQS_GammaA"
      | COQS_GammaAPips -> "COQS_GammaAPips"
      | COQS_GammaB -> "COQS_GammaB"
      | COQS_GammaBPips -> "COQS_GammaBPips"
      | COQS_GusSPricePercentA -> "COQS_GusSPrice%A"
      | COQS_GusSPricePercentB -> "COQS_GusSPrice%B"
      | COQS_GusSPriceA -> "COQS_GusSPriceA"
      | COQS_GusSPriceAPips -> "COQS_GusSPriceAPips"
      | COQS_GusSPriceB -> "COQS_GusSPriceB"
      | COQS_GusSPriceBPips -> "COQS_GusSPriceBPips"
      | COQS_OfficialPercentA -> "COQS_Official%A"
      | COQS_OfficialPercentB -> "COQS_Official%B"
      | COQS_OfficialA -> "COQS_OfficialA"
      | COQS_OfficialAPips -> "COQS_OfficialAPips"
      | COQS_OfficialB -> "COQS_OfficialB"
      | COQS_OfficialBPips -> "COQS_OfficialBPips"
      | COQS_PricePercentA -> "COQS_Price%A"
      | COQS_PricePercentB -> "COQS_Price%B"
      | COQS_PriceA -> "COQS_PriceA"
      | COQS_PriceAPips -> "COQS_PriceAPips"
      | COQS_PriceB -> "COQS_PriceB"
      | COQS_PriceBPips -> "COQS_PriceBPips"
      | COQS_RealSThetaPercentA -> "COQS_RealSTheta%A"
      | COQS_RealSThetaPercentB -> "COQS_RealSTheta%B"
      | COQS_RealSThetaA -> "COQS_RealSThetaA"
      | COQS_RealSThetaAPips -> "COQS_RealSThetaAPips"
      | COQS_RealSThetaB -> "COQS_RealSThetaB"
      | COQS_RealSThetaBPips -> "COQS_RealSThetaBPips"
      | COQS_RealThetaPercentA -> "COQS_RealTheta%A"
      | COQS_RealThetaPercentB -> "COQS_RealTheta%B"
      | COQS_RealThetaA -> "COQS_RealThetaA"
      | COQS_RealThetaAPips -> "COQS_RealThetaAPips"
      | COQS_RealThetaB -> "COQS_RealThetaB"
      | COQS_RealThetaBPips -> "COQS_RealThetaBPips"
      | COQS_ReplSPricePercentA -> "COQS_ReplSPrice%A"
      | COQS_ReplSPricePercentB -> "COQS_ReplSPrice%B"
      | COQS_ReplSPriceA -> "COQS_ReplSPriceA"
      | COQS_ReplSPriceAPips -> "COQS_ReplSPriceAPips"
      | COQS_ReplSPriceB -> "COQS_ReplSPriceB"
      | COQS_ReplSPriceBPips -> "COQS_ReplSPriceBPips"
      | COQS_RhoAPercentA -> "COQS_RhoA%A"
      | COQS_RhoAPercentB -> "COQS_RhoA%B"
      | COQS_RhoAA -> "COQS_RhoAA"
      | COQS_RhoAAPips -> "COQS_RhoAAPips"
      | COQS_RhoAB -> "COQS_RhoAB"
      | COQS_RhoABPips -> "COQS_RhoABPips"
      | COQS_RhoBPercentA -> "COQS_RhoB%A"
      | COQS_RhoBPercentB -> "COQS_RhoB%B"
      | COQS_RhoBA -> "COQS_RhoBA"
      | COQS_RhoBAPips -> "COQS_RhoBAPips"
      | COQS_RhoBB -> "COQS_RhoBB"
      | COQS_RhoBBPips -> "COQS_RhoBBPips"
      | COQS_S10DeltaRegaPercentA -> "COQS_S10DeltaRega%A"
      | COQS_S10DeltaRegaPercentB -> "COQS_S10DeltaRega%B"
      | COQS_S10DeltaRegaA -> "COQS_S10DeltaRegaA"
      | COQS_S10DeltaRegaAPips -> "COQS_S10DeltaRegaAPips"
      | COQS_S10DeltaRegaB -> "COQS_S10DeltaRegaB"
      | COQS_S10DeltaRegaBPips -> "COQS_S10DeltaRegaBPips"
      | COQS_S10DeltaSegaPercentA -> "COQS_S10DeltaSega%A"
      | COQS_S10DeltaSegaPercentB -> "COQS_S10DeltaSega%B"
      | COQS_S10DeltaSegaA -> "COQS_S10DeltaSegaA"
      | COQS_S10DeltaSegaAPips -> "COQS_S10DeltaSegaAPips"
      | COQS_S10DeltaSegaB -> "COQS_S10DeltaSegaB"
      | COQS_S10DeltaSegaBPips -> "COQS_S10DeltaSegaBPips"
      | COQS_S25DeltaRegaPercentA -> "COQS_S25DeltaRega%A"
      | COQS_S25DeltaRegaPercentB -> "COQS_S25DeltaRega%B"
      | COQS_S25DeltaRegaA -> "COQS_S25DeltaRegaA"
      | COQS_S25DeltaRegaAPips -> "COQS_S25DeltaRegaAPips"
      | COQS_S25DeltaRegaB -> "COQS_S25DeltaRegaB"
      | COQS_S25DeltaRegaBPips -> "COQS_S25DeltaRegaBPips"
      | COQS_S25DeltaSegaPercentA -> "COQS_S25DeltaSega%A"
      | COQS_S25DeltaSegaPercentB -> "COQS_S25DeltaSega%B"
      | COQS_S25DeltaSegaA -> "COQS_S25DeltaSegaA"
      | COQS_S25DeltaSegaAPips -> "COQS_S25DeltaSegaAPips"
      | COQS_S25DeltaSegaB -> "COQS_S25DeltaSegaB"
      | COQS_S25DeltaSegaBPips -> "COQS_S25DeltaSegaBPips"
      | COQS_SDeltaPercentA -> "COQS_SDelta%A"
      | COQS_SDeltaPercentB -> "COQS_SDelta%B"
      | COQS_SDeltaA -> "COQS_SDeltaA"
      | COQS_SDeltaAPips -> "COQS_SDeltaAPips"
      | COQS_SDeltaB -> "COQS_SDeltaB"
      | COQS_SDeltaBPips -> "COQS_SDeltaBPips"
      | COQS_SDeltaGapDownPercentA -> "COQS_SDeltaGapDown%A"
      | COQS_SDeltaGapDownPercentB -> "COQS_SDeltaGapDown%B"
      | COQS_SDeltaGapDownA -> "COQS_SDeltaGapDownA"
      | COQS_SDeltaGapDownAPips -> "COQS_SDeltaGapDownAPips"
      | COQS_SDeltaGapDownB -> "COQS_SDeltaGapDownB"
      | COQS_SDeltaGapDownBPips -> "COQS_SDeltaGapDownBPips"
      | COQS_SDeltaGapUpPercentA -> "COQS_SDeltaGapUp%A"
      | COQS_SDeltaGapUpPercentB -> "COQS_SDeltaGapUp%B"
      | COQS_SDeltaGapUpA -> "COQS_SDeltaGapUpA"
      | COQS_SDeltaGapUpAPips -> "COQS_SDeltaGapUpAPips"
      | COQS_SDeltaGapUpB -> "COQS_SDeltaGapUpB"
      | COQS_SDeltaGapUpBPips -> "COQS_SDeltaGapUpBPips"
      | COQS_SDerivePercentA -> "COQS_SDerive%A"
      | COQS_SDerivePercentB -> "COQS_SDerive%B"
      | COQS_SDeriveA -> "COQS_SDeriveA"
      | COQS_SDeriveAPips -> "COQS_SDeriveAPips"
      | COQS_SDeriveB -> "COQS_SDeriveB"
      | COQS_SDeriveBPips -> "COQS_SDeriveBPips"
      | COQS_SFwdDeltaPercentA -> "COQS_SFwdDelta%A"
      | COQS_SFwdDeltaPercentB -> "COQS_SFwdDelta%B"
      | COQS_SFwdDeltaA -> "COQS_SFwdDeltaA"
      | COQS_SFwdDeltaAPips -> "COQS_SFwdDeltaAPips"
      | COQS_SFwdDeltaB -> "COQS_SFwdDeltaB"
      | COQS_SFwdDeltaBPips -> "COQS_SFwdDeltaBPips"
      | COQS_SGammaPercentA -> "COQS_SGamma%A"
      | COQS_SGammaPercentB -> "COQS_SGamma%B"
      | COQS_SGammaA -> "COQS_SGammaA"
      | COQS_SGammaAPips -> "COQS_SGammaAPips"
      | COQS_SGammaB -> "COQS_SGammaB"
      | COQS_SGammaBPips -> "COQS_SGammaBPips"
      | COQS_SLVPricePercentA -> "COQS_SLVPrice%A"
      | COQS_SLVPricePercentB -> "COQS_SLVPrice%B"
      | COQS_SLVPriceA -> "COQS_SLVPriceA"
      | COQS_SLVPriceAPips -> "COQS_SLVPriceAPips"
      | COQS_SLVPriceB -> "COQS_SLVPriceB"
      | COQS_SLVPriceBPips -> "COQS_SLVPriceBPips"
      | COQS_SpreadPercentA -> "COQS_Spread%A"
      | COQS_SpreadPercentB -> "COQS_Spread%B"
      | COQS_SpreadA -> "COQS_SpreadA"
      | COQS_SpreadAPips -> "COQS_SpreadAPips"
      | COQS_SpreadB -> "COQS_SpreadB"
      | COQS_SpreadBPips -> "COQS_SpreadBPips"
      | COQS_SPricePercentA -> "COQS_SPrice%A"
      | COQS_SPricePercentB -> "COQS_SPrice%B"
      | COQS_SPriceA -> "COQS_SPriceA"
      | COQS_SPriceAPips -> "COQS_SPriceAPips"
      | COQS_SPriceB -> "COQS_SPriceB"
      | COQS_SPriceBPips -> "COQS_SPriceBPips"
      | COQS_SSDeltaPercentA -> "COQS_SSDelta%A"
      | COQS_SSDeltaPercentB -> "COQS_SSDelta%B"
      | COQS_SSDeltaA -> "COQS_SSDeltaA"
      | COQS_SSDeltaAPips -> "COQS_SSDeltaAPips"
      | COQS_SSDeltaB -> "COQS_SSDeltaB"
      | COQS_SSDeltaBPips -> "COQS_SSDeltaBPips"
      | COQS_SSGammaPercentA -> "COQS_SSGamma%A"
      | COQS_SSGammaPercentB -> "COQS_SSGamma%B"
      | COQS_SSGammaA -> "COQS_SSGammaA"
      | COQS_SSGammaAPips -> "COQS_SSGammaAPips"
      | COQS_SSGammaB -> "COQS_SSGammaB"
      | COQS_SSGammaBPips -> "COQS_SSGammaBPips"
      | COQS_SVDupirePercentA -> "COQS_SVDupire%A"
      | COQS_SVDupirePercentB -> "COQS_SVDupire%B"
      | COQS_SVDupireA -> "COQS_SVDupireA"
      | COQS_SVDupireAPips -> "COQS_SVDupireAPips"
      | COQS_SVDupireB -> "COQS_SVDupireB"
      | COQS_SVDupireBPips -> "COQS_SVDupireBPips"
      | COQS_SVegaPercentA -> "COQS_SVega%A"
      | COQS_SVegaPercentB -> "COQS_SVega%B"
      | COQS_SVegaA -> "COQS_SVegaA"
      | COQS_SVegaAPips -> "COQS_SVegaAPips"
      | COQS_SVegaB -> "COQS_SVegaB"
      | COQS_SVegaBPips -> "COQS_SVegaBPips"
      | COQS_SVegaBSpread -> "COQS_SVegaBSpread"
      | COQS_SVegaWeightedPercentA -> "COQS_SVegaWeighted%A"
      | COQS_SVegaWeightedPercentB -> "COQS_SVegaWeighted%B"
      | COQS_SVegaWeightedA -> "COQS_SVegaWeightedA"
      | COQS_SVegaWeightedAPips -> "COQS_SVegaWeightedAPips"
      | COQS_SVegaWeightedB -> "COQS_SVegaWeightedB"
      | COQS_SVegaWeightedBPips -> "COQS_SVegaWeightedBPips"
      | COQS_SVPdePercentA -> "COQS_SVPde%A"
      | COQS_SVPdePercentB -> "COQS_SVPde%B"
      | COQS_SVPdeA -> "COQS_SVPdeA"
      | COQS_SVPdeAPips -> "COQS_SVPdeAPips"
      | COQS_SVPdeB -> "COQS_SVPdeB"
      | COQS_SVPdeBPips -> "COQS_SVPdeBPips"
      | COQS_SVPlusSPricePercentA -> "COQS_SVPlusSPrice%A"
      | COQS_SVPlusSPricePercentB -> "COQS_SVPlusSPrice%B"
      | COQS_SVPlusSPriceA -> "COQS_SVPlusSPriceA"
      | COQS_SVPlusSPriceAPips -> "COQS_SVPlusSPriceAPips"
      | COQS_SVPlusSPriceB -> "COQS_SVPlusSPriceB"
      | COQS_SVPlusSPriceBPips -> "COQS_SVPlusSPriceBPips"
      | COQS_SVPricePercentA -> "COQS_SVPrice%A"
      | COQS_SVPricePercentB -> "COQS_SVPrice%B"
      | COQS_SVPriceA -> "COQS_SVPriceA"
      | COQS_SVPriceAPips -> "COQS_SVPriceAPips"
      | COQS_SVPriceB -> "COQS_SVPriceB"
      | COQS_SVPriceBPips -> "COQS_SVPriceBPips"
      | COQS_SVSPricePercentA -> "COQS_SVSPrice%A"
      | COQS_SVSPricePercentB -> "COQS_SVSPrice%B"
      | COQS_SVSPriceA -> "COQS_SVSPriceA"
      | COQS_SVSPriceAPips -> "COQS_SVSPriceAPips"
      | COQS_SVSPriceB -> "COQS_SVSPriceB"
      | COQS_SVSPriceBPips -> "COQS_SVSPriceBPips"
      | COQS_SVvolPercentA -> "COQS_SVvol%A"
      | COQS_SVvolPercentB -> "COQS_SVvol%B"
      | COQS_SVvolA -> "COQS_SVvolA"
      | COQS_SVvolAPips -> "COQS_SVvolAPips"
      | COQS_SVvolB -> "COQS_SVvolB"
      | COQS_SVvolBPips -> "COQS_SVvolBPips"
      | COQS_VegaPercentA -> "COQS_Vega%A"
      | COQS_VegaPercentB -> "COQS_Vega%B"
      | COQS_VegaA -> "COQS_VegaA"
      | COQS_VegaAPips -> "COQS_VegaAPips"
      | COQS_VegaB -> "COQS_VegaB"
      | COQS_VegaBPips -> "COQS_VegaBPips"
      | COQS_VegaWeightedPercentA -> "COQS_VegaWeighted%A"
      | COQS_VegaWeightedPercentB -> "COQS_VegaWeighted%B"
      | COQS_VegaWeightedA -> "COQS_VegaWeightedA"
      | COQS_VegaWeightedAPips -> "COQS_VegaWeightedAPips"
      | COQS_VegaWeightedB -> "COQS_VegaWeightedB"
      | COQS_VegaWeightedBPips -> "COQS_VegaWeightedBPips"
      | COQS_VRDeltaPercentA -> "COQS_VRDelta%A"
      | COQS_VRDeltaPercentB -> "COQS_VRDelta%B"
      | COQS_VRDeltaA -> "COQS_VRDeltaA"
      | COQS_VRDeltaAPips -> "COQS_VRDeltaAPips"
      | COQS_VRDeltaB -> "COQS_VRDeltaB"
      | COQS_VRDeltaBPips -> "COQS_VRDeltaBPips"
      | COQS_VRGammaPercentA -> "COQS_VRGamma%A"
      | COQS_VRGammaPercentB -> "COQS_VRGamma%B"
      | COQS_VRGammaA -> "COQS_VRGammaA"
      | COQS_VRGammaAPips -> "COQS_VRGammaAPips"
      | COQS_VRGammaB -> "COQS_VRGammaB"
      | COQS_VRGammaBPips -> "COQS_VRGammaBPips"
      | COQS_VvolPercentA -> "COQS_Vvol%A"
      | COQS_VvolPercentB -> "COQS_Vvol%B"
      | COQS_VvolA -> "COQS_VvolA"
      | COQS_VvolAPips -> "COQS_VvolAPips"
      | COQS_VvolB -> "COQS_VvolB"
      | COQS_VvolBPips -> "COQS_VvolBPips"
      | CoqsDrillDownOutputs -> "CoqsDrillDownOutputs"
      | CoqsOutputs -> "CoqsOutputs"
      | CP -> "CP"
      | CrossDeltaBaseRhoExp -> "CrossDeltaBaseRhoExp"
      | CrossGrannaExp -> "CrossGrannaExp"
      | CrossVegaBaseRhoExp -> "CrossVegaBaseRhoExp"
      | CrossVegaDealtRhoExp -> "CrossVegaDealtRhoExp"
      | Cut -> "Cut"
      | DealtDepo -> "DealtDepo"
      | DealtDepoToPayOut -> "DealtDepoToPayOut"
      | DealtDepoToStandardDelivery -> "DealtDepoToStandardDelivery"
      | DealtDepoToT1 -> "DealtDepoToT1"
      | DealtDepoToT2 -> "DealtDepoToT2"
      | DealtNotional -> "DealtNotional"
      | DealtRhoBucketed -> "DealtRhoBucketed"
      | DealtRhoExp -> "DealtRhoExp"
      | DealtRhoExpPercentBase -> "DealtRhoExp%Base"
      | DealtRhoExpPercentDealt -> "DealtRhoExp%Dealt"
      | DealtRhoExpAssetPips -> "DealtRhoExpAssetPips"
      | DealtRhoExpPips -> "DealtRhoExpPips"
      | DealtRhoPayout -> "DealtRhoPayout"
      | DealtRhoToT1 -> "DealtRhoToT1"
      | DealtRhoToT1PercentBase -> "DealtRhoToT1%Base"
      | DealtRhoToT1PercentDealt -> "DealtRhoToT1%Dealt"
      | DealtRhoToT1AssetPips -> "DealtRhoToT1AssetPips"
      | DealtRhoToT1Pips -> "DealtRhoToT1Pips"
      | DealtRhoToT2 -> "DealtRhoToT2"
      | DealtRhoToT2PercentBase -> "DealtRhoToT2%Base"
      | DealtRhoToT2PercentDealt -> "DealtRhoToT2%Dealt"
      | DealtRhoToT2AssetPips -> "DealtRhoToT2AssetPips"
      | DealtRhoToT2Pips -> "DealtRhoToT2Pips"
      | DealtZCRhoBucketed -> "DealtZCRhoBucketed"
      | DealtZCRhoExp -> "DealtZCRhoExp"
      | DealtZCRhoToT1 -> "DealtZCRhoToT1"
      | DealtZCRhoToT2 -> "DealtZCRhoToT2"
      | DealtZero -> "DealtZero"
      | DealtZeroToPayOut -> "DealtZeroToPayOut"
      | DealtZeroToStandardDelivery -> "DealtZeroToStandardDelivery"
      | DealtZeroToT1 -> "DealtZeroToT1"
      | DealtZeroToT2 -> "DealtZeroToT2"
      | DefaultSPrice -> "DefaultSPrice"
      | DeliveryDate -> "DeliveryDate"
      | DeliveryDateToT1 -> "DeliveryDateToT1"
      | DeliveryDateToT2 -> "DeliveryDateToT2"
      | Delta -> "Delta"
      | DeltaPercentBase -> "Delta%Base"
      | DeltaPercentDealt -> "Delta%Dealt"
      | DeltaAssetPips -> "DeltaAssetPips"
      | DeltaBase -> "DeltaBase"
      | DeltaDealt -> "DeltaDealt"
      | DeltaGapsA -> "DeltaGapsA"
      | DeltaGapsB -> "DeltaGapsB"
      | DeltaJumps -> "DeltaJumps"
      | DeltaMatrix -> "DeltaMatrix"
      | DeltaPips -> "DeltaPips"
      | Deltas -> "Deltas"
      | DeltaWithStopLoss -> "DeltaWithStopLoss"
      | Derive -> "Derive"
      | DerivePercentBase -> "Derive%Base"
      | DerivePercentDealt -> "Derive%Dealt"
      | DeriveAssetPips -> "DeriveAssetPips"
      | DeriveBase -> "DeriveBase"
      | DeriveDealt -> "DeriveDealt"
      | DerivePips -> "DerivePips"
      | Description -> "Description"
      | DigitalJumpDown -> "DigitalJumpDown"
      | DigitalJumpUp -> "DigitalJumpUp"
      | DVega25DeltaRegaB -> "DVega25DeltaRegaB"
      | DVega25DeltaSegaB -> "DVega25DeltaSegaB"
      | DVegaAtHitBounds -> "DVegaAtHitBounds"
      | DvegaAtHitShifts -> "DvegaAtHitShifts"
      | DVegaAtHitSPrice -> "DVegaAtHitSPrice"
      | DVegaDFwd -> "DVegaDFwd"
      | DVegaDFwdT1 -> "DVegaDFwdT1"
      | DVegaDVol -> "DVegaDVol"
      | DVegaDVolToT1 -> "DVegaDVolToT1"
      | DVegaDVolToT2 -> "DVegaDVolToT2"
      | DVegaExpDSpot -> "DVegaExpDSpot"
      | DVegaImplemented -> "DVegaImplemented"
      | DVegaSDelta -> "DVegaSDelta"
      | DVegaSPrice -> "DVegaSPrice"
      | DVegaT1DFwd -> "DVegaT1DFwd"
      | DVegaT1DFwdT1 -> "DVegaT1DFwdT1"
      | DVegaT1DSpot -> "DVegaT1DSpot"
      | DVegaT2DFwdT2 -> "DVegaT2DFwdT2"
      | DVegaToT1DVol -> "DVegaToT1DVol"
      | DVegaToT1DVolToT1 -> "DVegaToT1DVolToT1"
      | DVegaToT1DVolToT2 -> "DVegaToT1DVolToT2"
      | DVegaToT2DSpot -> "DVegaToT2DSpot"
      | DVegaToT2DVol -> "DVegaToT2DVol"
      | DVegaToT2DVolToT1 -> "DVegaToT2DVolToT1"
      | DVegaToT2DVolToT2 -> "DVegaToT2DVolToT2"
      | EventInfo -> "EventInfo"
      | Events -> "Events"
      | ExpiryDate -> "ExpiryDate"
      | ExpiryDateToT1 -> "ExpiryDateToT1"
      | ExpiryDateToT2 -> "ExpiryDateToT2"
      | Factors -> "Factors"
      | Fixings -> "Fixings"
      | FlexBarType -> "FlexBarType"
      | Forward -> "Forward"
      | FullBarrierInfo -> "FullBarrierInfo"
      | FullBarrierInfoDll -> "FullBarrierInfoDll"
      | Fwd -> "Fwd"
      | FwdDelta -> "FwdDelta"
      | FwdDeltaBucketed -> "FwdDeltaBucketed"
      | FwdDeltaBucketedSpread -> "FwdDeltaBucketedSpread"
      | FwdDeltaExpiry -> "FwdDeltaExpiry"
      | FwdDeltaPayout -> "FwdDeltaPayout"
      | FwdDeltaToT1 -> "FwdDeltaToT1"
      | FwdDeltaToT2 -> "FwdDeltaToT2"
      | FwdStrike -> "FwdStrike"
      | FwdToStandardDelivery -> "FwdToStandardDelivery"
      | FwdToT1 -> "FwdToT1"
      | FwdToT2 -> "FwdToT2"
      | Gamma -> "Gamma"
      | GammaPercentBase -> "Gamma%Base"
      | GammaPercentDealt -> "Gamma%Dealt"
      | GammaAssetPips -> "GammaAssetPips"
      | GammaBase -> "GammaBase"
      | GammaDealt -> "GammaDealt"
      | GammaPips -> "GammaPips"
      | Gearing -> "Gearing"
      | GSVParameters -> "GSVParameters"
      | GSVVolErrors -> "GSVVolErrors"
      | GusSPrice -> "GusSPrice"
      | Hedges -> "Hedges"
      | HestonHedge -> "HestonHedge"
      | HestonProbOfCrossing -> "HestonProbOfCrossing"
      | HitDate -> "HitDate"
      | HurdleDown -> "HurdleDown"
      | HurdleDownToT1 -> "HurdleDownToT1"
      | HurdleUp -> "HurdleUp"
      | HurdleUpToT1 -> "HurdleUpToT1"
      | InputDescription -> "InputDescription"
      | Inputs -> "Inputs"
      | IntrinsicValue -> "IntrinsicValue"
      | InvAlphaPrice -> "InvAlphaPrice"
      | InvBaseRhoExp -> "InvBaseRhoExp"
      | InvBaseRhoToT1 -> "InvBaseRhoToT1"
      | InvBaseRhoToT2 -> "InvBaseRhoToT2"
      | InvDealtRhoExp -> "InvDealtRhoExp"
      | InvDealtRhoToT1 -> "InvDealtRhoToT1"
      | InvDealtRhoToT2 -> "InvDealtRhoToT2"
      | InvDelta -> "InvDelta"
      | InvDeltaWithStopLoss -> "InvDeltaWithStopLoss"
      | InvDerive -> "InvDerive"
      | InverseBidAsk -> "InverseBidAsk"
      | InvGamma -> "InvGamma"
      | InvIntrinsicValue -> "InvIntrinsicValue"
      | InvSDelta -> "InvSDelta"
      | InvSGamma -> "InvSGamma"
      | InvSmileDeltaWithStopLoss -> "InvSmileDeltaWithStopLoss"
      | InvSmileValueWithStopLoss -> "InvSmileValueWithStopLoss"
      | InvSSDelta -> "InvSSDelta"
      | InvSSGamma -> "InvSSGamma"
      | InvThetaExp -> "InvThetaExp"
      | InvTotalDerive -> "InvTotalDerive"
      | InvTotalSmileDerive -> "InvTotalSmileDerive"
      | InvTotalSmileVvol -> "InvTotalSmileVvol"
      | InvTotalVega -> "InvTotalVega"
      | InvTotalVvol -> "InvTotalVvol"
      | InvValue -> "InvValue"
      | InvValueWithStopLoss -> "InvValueWithStopLoss"
      | InvVega -> "InvVega"
      | InvVegaExp -> "InvVegaExp"
      | InvVegaToT1 -> "InvVegaToT1"
      | InvVegaToT2 -> "InvVegaToT2"
      | InvVegaWeighted -> "InvVegaWeighted"
      | InvVRDelta -> "InvVRDelta"
      | InvVRGamma -> "InvVRGamma"
      | InvVvol -> "InvVvol"
      | IsExerciseAllowed -> "IsExerciseAllowed"
      | IsExpiryAllowed -> "IsExpiryAllowed"
      | IsKoAllowed -> "IsKoAllowed"
      | IsMultiHurdle -> "IsMultiHurdle"
      | Lifetime -> "Lifetime"
      | Lifetimes -> "Lifetimes"
      | LimitClass -> "LimitClass"
      | LimitInfo -> "LimitInfo"
      | LookUpHestonPoint -> "LookUpHestonPoint"
      | MarketData -> "MarketData"
      | MarketVols -> "MarketVols"
      | MinBarDays -> "MinBarDays"
      | MinMax -> "MinMax"
      | MoneyZero -> "MoneyZero"
      | NextBarrierInfo -> "NextBarrierInfo"
      | NextPinRisk -> "NextPinRisk"
      | Notional -> "Notional"
      | NotionalAndPayOut -> "NotionalAndPayOut"
      | NotionalB -> "NotionalB"
      | NumBarDays -> "NumBarDays"
      | OneDTheta -> "OneDTheta"
      | OneDThetaPercentBase -> "OneDTheta%Base"
      | OneDThetaPercentDealt -> "OneDTheta%Dealt"
      | OneDThetaAssetPips -> "OneDThetaAssetPips"
      | OneDThetaBase -> "OneDThetaBase"
      | OneDThetaDealt -> "OneDThetaDealt"
      | OneDThetaPips -> "OneDThetaPips"
      | OptimisedVolSurface -> "OptimisedVolSurface"
      | OptionDetails -> "OptionDetails"
      | OptionInfo -> "OptionInfo"
      | OptionType -> "OptionType"
      | PassportNotionalStep -> "PassportNotionalStep"
      | PassportPL -> "PassportPL"
      | PassportPosition -> "PassportPosition"
      | PayOutDate -> "PayOutDate"
      | PinRisk -> "PinRisk"
      | PinRiskDrillDown -> "PinRiskDrillDown"
      | PosAdjustments -> "PosAdjustments"
      | Positions -> "Positions"
      | PredictedDelta -> "PredictedDelta"
      | PredictedDerive -> "PredictedDerive"
      | PredictedGamma -> "PredictedGamma"
      | PredictedTotalDerive -> "PredictedTotalDerive"
      | PricePercentBase -> "Price%Base"
      | PricePercentDealt -> "Price%Dealt"
      | PriceAssetPips -> "PriceAssetPips"
      | PriceBase -> "PriceBase"
      | PriceDealt -> "PriceDealt"
      | PricePips -> "PricePips"
      | QuantoDeltas -> "QuantoDeltas"
      | QuantoDerives -> "QuantoDerives"
      | QuantoGammas -> "QuantoGammas"
      | QuantoPrice -> "QuantoPrice"
      | QuantoSDeltas -> "QuantoSDeltas"
      | QuantoSGammas -> "QuantoSGammas"
      | QuantoSPrice -> "QuantoSPrice"
      | QuantoSVegas -> "QuantoSVegas"
      | QuantoVegas -> "QuantoVegas"
      | QuantoVvols -> "QuantoVvols"
      | RealSTheta -> "RealSTheta"
      | RealTheta -> "RealTheta"
      | RR25 -> "RR25"
      | SCcyFormulaExecutionMode -> "SCcyFormulaExecutionMode"
      | SDelta -> "SDelta"
      | SGamma -> "SGamma"
      | Shifts -> "Shifts"
      | SLVVolErrors -> "SLVVolErrors"
      | SmileAnnualisedOneDTheta -> "SmileAnnualisedOneDTheta"
      | SmileAtmVegaBucketed -> "SmileAtmVegaBucketed"
      | SmileAtmVegaBucketedSpread -> "SmileAtmVegaBucketedSpread"
      | SmileBaseRhoBucketed -> "SmileBaseRhoBucketed"
      | SmileBaseZCRhoBucketed -> "SmileBaseZCRhoBucketed"
      | SmileDealtRhoBucketed -> "SmileDealtRhoBucketed"
      | SmileDealtZCRhoBucketed -> "SmileDealtZCRhoBucketed"
      | SmileDeltaGapsA -> "SmileDeltaGapsA"
      | SmileDeltaGapsB -> "SmileDeltaGapsB"
      | SmileDeltaWithStopLoss -> "SmileDeltaWithStopLoss"
      | SmileFwdDelta -> "SmileFwdDelta"
      | SmileFwdDeltaBucketed -> "SmileFwdDeltaBucketed"
      | SmileFwdDeltaBucketedSpread -> "SmileFwdDeltaBucketedSpread"
      | SmileFwdDeltaExpiry -> "SmileFwdDeltaExpiry"
      | SmileFwdDeltaPayout -> "SmileFwdDeltaPayout"
      | SmileFwdDeltaToT1 -> "SmileFwdDeltaToT1"
      | SmileFwdDeltaToT2 -> "SmileFwdDeltaToT2"
      | SmileOneDTheta -> "SmileOneDTheta"
      | SmileRega10DeltaBucketed -> "SmileRega10DeltaBucketed"
      | SmileRega10DeltaBucketedSpread -> "SmileRega10DeltaBucketedSpread"
      | SmileRega25DeltaBucketed -> "SmileRega25DeltaBucketed"
      | SmileRega25DeltaBucketedSpread -> "SmileRega25DeltaBucketedSpread"
      | SmileSega10DeltaBucketed -> "SmileSega10DeltaBucketed"
      | SmileSega10DeltaBucketedSpread -> "SmileSega10DeltaBucketedSpread"
      | SmileSega25DeltaBucketed -> "SmileSega25DeltaBucketed"
      | SmileSega25DeltaBucketedSpread -> "SmileSega25DeltaBucketedSpread"
      | SmileSpotDelta -> "SmileSpotDelta"
      | SmileTotalBaseRho -> "SmileTotalBaseRho"
      | SmileTotalDealtRho -> "SmileTotalDealtRho"
      | SmileValueWithStopLoss -> "SmileValueWithStopLoss"
      | SmileVegaBucketed -> "SmileVegaBucketed"
      | SmileVegaBucketedSpread -> "SmileVegaBucketedSpread"
      | SpotDate -> "SpotDate"
      | SpotDelta -> "SpotDelta"
      | SSDelta -> "SSDelta"
      | SSGamma -> "SSGamma"
      | Strike -> "Strike"
      | SVDupireParameters -> "SVDupireParameters"
      | SVDupireVolErrors -> "SVDupireVolErrors"
      | SVHedge -> "SVHedge"
      | SVMCVanillaError -> "SVMCVanillaError"
      | SVParameters -> "SVParameters"
      | SVPdePrice -> "SVPdePrice"
      | SVPDEVanillaError -> "SVPDEVanillaError"
      | SVPrice -> "SVPrice"
      | SVProbOfCrossing -> "SVProbOfCrossing"
      | SVSPrice -> "SVSPrice"
      | SVVanillaError -> "SVVanillaError"
      | SVVolErrors -> "SVVolErrors"
      | SVVols -> "SVVols"
      | ThetaExp -> "ThetaExp"
      | ThetaExpPercentBase -> "ThetaExp%Base"
      | ThetaExpPercentDealt -> "ThetaExp%Dealt"
      | ThetaExpAssetPips -> "ThetaExpAssetPips"
      | ThetaExpPips -> "ThetaExpPips"
      | TotalBaseRho -> "TotalBaseRho"
      | TotalBaseRhoPercentBase -> "TotalBaseRho%Base"
      | TotalBaseRhoPercentDealt -> "TotalBaseRho%Dealt"
      | TotalBaseRhoAssetPips -> "TotalBaseRhoAssetPips"
      | TotalDealtRho -> "TotalDealtRho"
      | TotalDealtRhoPercentBase -> "TotalDealtRho%Base"
      | TotalDealtRhoPercentDealt -> "TotalDealtRho%Dealt"
      | TotalDealtRhoAssetPips -> "TotalDealtRhoAssetPips"
      | TotalDerive -> "TotalDerive"
      | TotalDerivePercentBase -> "TotalDerive%Base"
      | TotalDerivePercentDealt -> "TotalDerive%Dealt"
      | TotalDeriveAssetPips -> "TotalDeriveAssetPips"
      | TotalDerivePips -> "TotalDerivePips"
      | TotalDeriveWeighted -> "TotalDeriveWeighted"
      | TotalFwdDerive -> "TotalFwdDerive"
      | TotalLooks -> "TotalLooks"
      | TotalSmileDerive -> "TotalSmileDerive"
      | TotalSmileVvol -> "TotalSmileVvol"
      | TotalVega -> "TotalVega"
      | TotalVegaPercentBase -> "TotalVega%Base"
      | TotalVegaPercentDealt -> "TotalVega%Dealt"
      | TotalVegaAssetPips -> "TotalVegaAssetPips"
      | TotalVegaBase -> "TotalVegaBase"
      | TotalVegaDealt -> "TotalVegaDealt"
      | TotalVegaPips -> "TotalVegaPips"
      | TotalVvol -> "TotalVvol"
      | TotalVvolPercentBase -> "TotalVvol%Base"
      | TotalVvolPercentDealt -> "TotalVvol%Dealt"
      | TotalVvolAssetPips -> "TotalVvolAssetPips"
      | TotalVvolPips -> "TotalVvolPips"
      | TradeDate -> "TradeDate"
      | TRFPinRisk -> "TRFPinRisk"
      | TRFPinRiskDrillDown -> "TRFPinRiskDrillDown"
      | TypeAndParams -> "TypeAndParams"
      | Types -> "Types"
      | UnderlyingPrice -> "UnderlyingPrice"
      | UsingNumericalMethod -> "UsingNumericalMethod"
      | Value -> "Value"
      | ValueBase -> "ValueBase"
      | ValueDealt -> "ValueDealt"
      | ValueWithStopLoss -> "ValueWithStopLoss"
      | Vega -> "Vega"
      | VegaPercentBase -> "Vega%Base"
      | VegaPercentDealt -> "Vega%Dealt"
      | VegaAssetPips -> "VegaAssetPips"
      | VegaBase -> "VegaBase"
      | VegaBucketed -> "VegaBucketed"
      | VegaDealt -> "VegaDealt"
      | VegaExp -> "VegaExp"
      | VegaExpPercentBase -> "VegaExp%Base"
      | VegaExpPercentDealt -> "VegaExp%Dealt"
      | VegaExpAssetPips -> "VegaExpAssetPips"
      | VegaExpPips -> "VegaExpPips"
      | VegaPips -> "VegaPips"
      | VegaT1PercentBase -> "VegaT1%Base"
      | VegaT1PercentDealt -> "VegaT1%Dealt"
      | VegaT1AssetPips -> "VegaT1AssetPips"
      | VegaT1Pips -> "VegaT1Pips"
      | VegaT2PercentBase -> "VegaT2%Base"
      | VegaT2PercentDealt -> "VegaT2%Dealt"
      | VegaT2AssetPips -> "VegaT2AssetPips"
      | VegaT2Pips -> "VegaT2Pips"
      | VegaToT1 -> "VegaToT1"
      | VegaToT2 -> "VegaToT2"
      | VegaWeighted -> "VegaWeighted"
      | Vol -> "Vol"
      | VolToT1 -> "VolToT1"
      | VolToT2 -> "VolToT2"
      | VRDelta -> "VRDelta"
      | VRGamma -> "VRGamma"
      | Vvol -> "Vvol"
      | VvolPercentBase -> "Vvol%Base"
      | VvolPercentDealt -> "Vvol%Dealt"
      | VvolAssetPips -> "VvolAssetPips"
      | VVolBase -> "VVolBase"
      | VvolDealt -> "VvolDealt"
      | VvolPips -> "VvolPips"
      | ZeroOrNegativeStrike -> "ZeroOrNegativeStrike"
      ];


value option_output_of_string: string -> option_output =
    fun s ->
      match String.lowercase s with
      [
        "accruednotional" -> AccruedNotional
      | "alphaprice" -> AlphaPrice
      | "alphaprice%base" -> AlphaPricePercentBase
      | "alphaprice%dealt" -> AlphaPricePercentDealt
      | "alphapriceassetpips" -> AlphaPriceAssetPips
      | "alphapricepips" -> AlphaPricePips
      | "analyticvegas?" -> AnalyticVegas
      | "assetzero" -> AssetZero
      | "bar1" -> Bar1
      | "bar1tot1" -> Bar1ToT1
      | "bar1tot2" -> Bar1ToT2
      | "bar2" -> Bar2
      | "bar2tot1" -> Bar2ToT1
      | "bar2tot2" -> Bar2ToT2
      | "barcompcp" -> BarCompCP
      | "barcompstrike" -> BarCompStrike
      | "barcompstrikeisasset" -> BarCompStrikeIsAsset
      | "barminmax" -> BarMinMax
      | "barrierdate" -> BarrierDate
      | "barrierdatetot2" -> BarrierDateToT2
      | "barrierinfo" -> BarrierInfo
      | "barriertable" -> BarrierTable
      | "basedepo" -> BaseDepo
      | "basedepotopayout" -> BaseDepoToPayOut
      | "basedepotostandarddelivery" -> BaseDepoToStandardDelivery
      | "basedepotot1" -> BaseDepoToT1
      | "basedepotot2" -> BaseDepoToT2
      | "basegrannaexp" -> BaseGrannaExp
      | "basenotional" -> BaseNotional
      | "baserhobucketed" -> BaseRhoBucketed
      | "baserhoexp" -> BaseRhoExp
      | "baserhoexp%base" -> BaseRhoExpPercentBase
      | "baserhoexp%dealt" -> BaseRhoExpPercentDealt
      | "baserhoexpassetpips" -> BaseRhoExpAssetPips
      | "baserhoexppips" -> BaseRhoExpPips
      | "baserhopayout" -> BaseRhoPayout
      | "baserhotot1" -> BaseRhoToT1
      | "baserhotot1%base" -> BaseRhoToT1PercentBase
      | "baserhotot1%dealt" -> BaseRhoToT1PercentDealt
      | "baserhotot1assetpips" -> BaseRhoToT1AssetPips
      | "baserhotot1pips" -> BaseRhoToT1Pips
      | "baserhotot2" -> BaseRhoToT2
      | "baserhotot2%base" -> BaseRhoToT2PercentBase
      | "baserhotot2%dealt" -> BaseRhoToT2PercentDealt
      | "baserhotot2assetpips" -> BaseRhoToT2AssetPips
      | "baserhotot2pips" -> BaseRhoToT2Pips
      | "basezcrhobucketed" -> BaseZCRhoBucketed
      | "basezcrhoexp" -> BaseZCRhoExp
      | "basezcrhotot1" -> BaseZCRhoToT1
      | "basezcrhotot2" -> BaseZCRhoToT2
      | "basezero" -> BaseZero
      | "basezerotopayout" -> BaseZeroToPayOut
      | "basezerotostandarddelivery" -> BaseZeroToStandardDelivery
      | "basezerotot1" -> BaseZeroToT1
      | "basezerotot2" -> BaseZeroToT2
      | "bidask" -> BidAsk
      | "bidaskdetails" -> BidAskDetails
      | "bidaskdetailsdrilldown" -> BidAskDetailsDrillDown
      | "bidaskvolprice" -> BidAskVolPrice
      | "breakeven" -> BreakEven
      | "canhavenegativevalue" -> CanHaveNegativeValue
      | "checkfixings" -> CheckFixings
      | "compstrike" -> CompStrike
      | "coqs_10deltarega%a" -> COQS_10DeltaRegaPercentA
      | "coqs_10deltarega%b" -> COQS_10DeltaRegaPercentB
      | "coqs_10deltaregaa" -> COQS_10DeltaRegaA
      | "coqs_10deltaregaapips" -> COQS_10DeltaRegaAPips
      | "coqs_10deltaregab" -> COQS_10DeltaRegaB
      | "coqs_10deltaregabpips" -> COQS_10DeltaRegaBPips
      | "coqs_10deltasega%a" -> COQS_10DeltaSegaPercentA
      | "coqs_10deltasega%b" -> COQS_10DeltaSegaPercentB
      | "coqs_10deltasegaa" -> COQS_10DeltaSegaA
      | "coqs_10deltasegaapips" -> COQS_10DeltaSegaAPips
      | "coqs_10deltasegab" -> COQS_10DeltaSegaB
      | "coqs_10deltasegabpips" -> COQS_10DeltaSegaBPips
      | "coqs_1dtheta%a" -> COQS_1DThetaPercentA
      | "coqs_1dtheta%b" -> COQS_1DThetaPercentB
      | "coqs_1dthetaa" -> COQS_1DThetaA
      | "coqs_1dthetaapips" -> COQS_1DThetaAPips
      | "coqs_1dthetab" -> COQS_1DThetaB
      | "coqs_1dthetabpips" -> COQS_1DThetaBPips
      | "coqs_25deltarega%a" -> COQS_25DeltaRegaPercentA
      | "coqs_25deltarega%b" -> COQS_25DeltaRegaPercentB
      | "coqs_25deltaregaa" -> COQS_25DeltaRegaA
      | "coqs_25deltaregaapips" -> COQS_25DeltaRegaAPips
      | "coqs_25deltaregab" -> COQS_25DeltaRegaB
      | "coqs_25deltaregabpips" -> COQS_25DeltaRegaBPips
      | "coqs_25deltasega%a" -> COQS_25DeltaSegaPercentA
      | "coqs_25deltasega%b" -> COQS_25DeltaSegaPercentB
      | "coqs_25deltasegaa" -> COQS_25DeltaSegaA
      | "coqs_25deltasegaapips" -> COQS_25DeltaSegaAPips
      | "coqs_25deltasegab" -> COQS_25DeltaSegaB
      | "coqs_25deltasegabpips" -> COQS_25DeltaSegaBPips
      | "coqs_alpha%a" -> COQS_AlphaPercentA
      | "coqs_alpha%b" -> COQS_AlphaPercentB
      | "coqs_alphaa" -> COQS_AlphaA
      | "coqs_alphaapips" -> COQS_AlphaAPips
      | "coqs_alphab" -> COQS_AlphaB
      | "coqs_alphabpips" -> COQS_AlphaBPips
      | "coqs_bidasknetdetails%a" -> COQS_BidAskNetDetailsPercentA
      | "coqs_bidasknetdetails%b" -> COQS_BidAskNetDetailsPercentB
      | "coqs_bidasknetdetailsa" -> COQS_BidAskNetDetailsA
      | "coqs_bidasknetdetailsapips" -> COQS_BidAskNetDetailsAPips
      | "coqs_bidasknetdetailsb" -> COQS_BidAskNetDetailsB
      | "coqs_bidasknetdetailsbpips" -> COQS_BidAskNetDetailsBPips
      | "coqs_bidaskspread%a" -> COQS_BidAskSpreadPercentA
      | "coqs_bidaskspread%b" -> COQS_BidAskSpreadPercentB
      | "coqs_bidaskspreada" -> COQS_BidAskSpreadA
      | "coqs_bidaskspreadapips" -> COQS_BidAskSpreadAPips
      | "coqs_bidaskspreadb" -> COQS_BidAskSpreadB
      | "coqs_bidaskspreadbpips" -> COQS_BidAskSpreadBPips
      | "coqs_bidasksprice%a" -> COQS_BidAskSPricePercentA
      | "coqs_bidasksprice%b" -> COQS_BidAskSPricePercentB
      | "coqs_bidaskspricea" -> COQS_BidAskSPriceA
      | "coqs_bidaskspriceapips" -> COQS_BidAskSPriceAPips
      | "coqs_bidaskspriceb" -> COQS_BidAskSPriceB
      | "coqs_bidaskspricebpips" -> COQS_BidAskSPriceBPips
      | "coqs_creditrisk%a" -> COQS_CreditRiskPercentA
      | "coqs_creditrisk%b" -> COQS_CreditRiskPercentB
      | "coqs_creditriska" -> COQS_CreditRiskA
      | "coqs_creditriskapips" -> COQS_CreditRiskAPips
      | "coqs_creditriskb" -> COQS_CreditRiskB
      | "coqs_creditriskbpips" -> COQS_CreditRiskBPips
      | "coqs_defaultsprice%a" -> COQS_DefaultSPricePercentA
      | "coqs_defaultsprice%b" -> COQS_DefaultSPricePercentB
      | "coqs_defaultspricea" -> COQS_DefaultSPriceA
      | "coqs_defaultspriceapips" -> COQS_DefaultSPriceAPips
      | "coqs_defaultspriceb" -> COQS_DefaultSPriceB
      | "coqs_defaultspricebpips" -> COQS_DefaultSPriceBPips
      | "coqs_delta%a" -> COQS_DeltaPercentA
      | "coqs_delta%b" -> COQS_DeltaPercentB
      | "coqs_deltaa" -> COQS_DeltaA
      | "coqs_deltaapips" -> COQS_DeltaAPips
      | "coqs_deltab" -> COQS_DeltaB
      | "coqs_deltabpips" -> COQS_DeltaBPips
      | "coqs_deltagapdown%a" -> COQS_DeltaGapDownPercentA
      | "coqs_deltagapdown%b" -> COQS_DeltaGapDownPercentB
      | "coqs_deltagapdowna" -> COQS_DeltaGapDownA
      | "coqs_deltagapdownapips" -> COQS_DeltaGapDownAPips
      | "coqs_deltagapdownb" -> COQS_DeltaGapDownB
      | "coqs_deltagapdownbpips" -> COQS_DeltaGapDownBPips
      | "coqs_deltagapup%a" -> COQS_DeltaGapUpPercentA
      | "coqs_deltagapup%b" -> COQS_DeltaGapUpPercentB
      | "coqs_deltagapupa" -> COQS_DeltaGapUpA
      | "coqs_deltagapupapips" -> COQS_DeltaGapUpAPips
      | "coqs_deltagapupb" -> COQS_DeltaGapUpB
      | "coqs_deltagapupbpips" -> COQS_DeltaGapUpBPips
      | "coqs_derive%a" -> COQS_DerivePercentA
      | "coqs_derive%b" -> COQS_DerivePercentB
      | "coqs_derivea" -> COQS_DeriveA
      | "coqs_deriveapips" -> COQS_DeriveAPips
      | "coqs_deriveb" -> COQS_DeriveB
      | "coqs_derivebpips" -> COQS_DeriveBPips
      | "coqs_dvega15dsprice%a" -> COQS_DVega15DSPricePercentA
      | "coqs_dvega15dsprice%b" -> COQS_DVega15DSPricePercentB
      | "coqs_dvega15dspricea" -> COQS_DVega15DSPriceA
      | "coqs_dvega15dspriceapips" -> COQS_DVega15DSPriceAPips
      | "coqs_dvega15dspriceb" -> COQS_DVega15DSPriceB
      | "coqs_dvega15dspricebpips" -> COQS_DVega15DSPriceBPips
      | "coqs_dvegaathit15dsprice%a" -> COQS_DVegaAtHit15DSPricePercentA
      | "coqs_dvegaathit15dsprice%b" -> COQS_DVegaAtHit15DSPricePercentB
      | "coqs_dvegaathit15dspricea" -> COQS_DVegaAtHit15DSPriceA
      | "coqs_dvegaathit15dspriceapips" -> COQS_DVegaAtHit15DSPriceAPips
      | "coqs_dvegaathit15dspriceb" -> COQS_DVegaAtHit15DSPriceB
      | "coqs_dvegaathit15dspricebpips" -> COQS_DVegaAtHit15DSPriceBPips
      | "coqs_dvegaathitsprice%a" -> COQS_DVegaAtHitSPricePercentA
      | "coqs_dvegaathitsprice%b" -> COQS_DVegaAtHitSPricePercentB
      | "coqs_dvegaathitspricea" -> COQS_DVegaAtHitSPriceA
      | "coqs_dvegaathitspriceapips" -> COQS_DVegaAtHitSPriceAPips
      | "coqs_dvegaathitspriceb" -> COQS_DVegaAtHitSPriceB
      | "coqs_dvegaathitspricebpips" -> COQS_DVegaAtHitSPriceBPips
      | "coqs_dvegasdelta%a" -> COQS_DVegaSDeltaPercentA
      | "coqs_dvegasdelta%b" -> COQS_DVegaSDeltaPercentB
      | "coqs_dvegasdeltaa" -> COQS_DVegaSDeltaA
      | "coqs_dvegasdeltaapips" -> COQS_DVegaSDeltaAPips
      | "coqs_dvegasdeltab" -> COQS_DVegaSDeltaB
      | "coqs_dvegasdeltabpips" -> COQS_DVegaSDeltaBPips
      | "coqs_dvegasprice%a" -> COQS_DVegaSPricePercentA
      | "coqs_dvegasprice%b" -> COQS_DVegaSPricePercentB
      | "coqs_dvegaspricea" -> COQS_DVegaSPriceA
      | "coqs_dvegaspriceapips" -> COQS_DVegaSPriceAPips
      | "coqs_dvegaspriceb" -> COQS_DVegaSPriceB
      | "coqs_dvegaspricebpips" -> COQS_DVegaSPriceBPips
      | "coqs_dvegasvega%a" -> COQS_DVegaSVegaPercentA
      | "coqs_dvegasvega%b" -> COQS_DVegaSVegaPercentB
      | "coqs_dvegasvegaa" -> COQS_DVegaSVegaA
      | "coqs_dvegasvegaapips" -> COQS_DVegaSVegaAPips
      | "coqs_dvegasvegab" -> COQS_DVegaSVegaB
      | "coqs_dvegasvegabpips" -> COQS_DVegaSVegaBPips
      | "coqs_fwddelta%a" -> COQS_FwdDeltaPercentA
      | "coqs_fwddelta%b" -> COQS_FwdDeltaPercentB
      | "coqs_fwddeltaa" -> COQS_FwdDeltaA
      | "coqs_fwddeltaapips" -> COQS_FwdDeltaAPips
      | "coqs_fwddeltab" -> COQS_FwdDeltaB
      | "coqs_fwddeltabpips" -> COQS_FwdDeltaBPips
      | "coqs_gamma%a" -> COQS_GammaPercentA
      | "coqs_gamma%b" -> COQS_GammaPercentB
      | "coqs_gammaa" -> COQS_GammaA
      | "coqs_gammaapips" -> COQS_GammaAPips
      | "coqs_gammab" -> COQS_GammaB
      | "coqs_gammabpips" -> COQS_GammaBPips
      | "coqs_gussprice%a" -> COQS_GusSPricePercentA
      | "coqs_gussprice%b" -> COQS_GusSPricePercentB
      | "coqs_gusspricea" -> COQS_GusSPriceA
      | "coqs_gusspriceapips" -> COQS_GusSPriceAPips
      | "coqs_gusspriceb" -> COQS_GusSPriceB
      | "coqs_gusspricebpips" -> COQS_GusSPriceBPips
      | "coqs_official%a" -> COQS_OfficialPercentA
      | "coqs_official%b" -> COQS_OfficialPercentB
      | "coqs_officiala" -> COQS_OfficialA
      | "coqs_officialapips" -> COQS_OfficialAPips
      | "coqs_officialb" -> COQS_OfficialB
      | "coqs_officialbpips" -> COQS_OfficialBPips
      | "coqs_price%a" -> COQS_PricePercentA
      | "coqs_price%b" -> COQS_PricePercentB
      | "coqs_pricea" -> COQS_PriceA
      | "coqs_priceapips" -> COQS_PriceAPips
      | "coqs_priceb" -> COQS_PriceB
      | "coqs_pricebpips" -> COQS_PriceBPips
      | "coqs_realstheta%a" -> COQS_RealSThetaPercentA
      | "coqs_realstheta%b" -> COQS_RealSThetaPercentB
      | "coqs_realsthetaa" -> COQS_RealSThetaA
      | "coqs_realsthetaapips" -> COQS_RealSThetaAPips
      | "coqs_realsthetab" -> COQS_RealSThetaB
      | "coqs_realsthetabpips" -> COQS_RealSThetaBPips
      | "coqs_realtheta%a" -> COQS_RealThetaPercentA
      | "coqs_realtheta%b" -> COQS_RealThetaPercentB
      | "coqs_realthetaa" -> COQS_RealThetaA
      | "coqs_realthetaapips" -> COQS_RealThetaAPips
      | "coqs_realthetab" -> COQS_RealThetaB
      | "coqs_realthetabpips" -> COQS_RealThetaBPips
      | "coqs_replsprice%a" -> COQS_ReplSPricePercentA
      | "coqs_replsprice%b" -> COQS_ReplSPricePercentB
      | "coqs_replspricea" -> COQS_ReplSPriceA
      | "coqs_replspriceapips" -> COQS_ReplSPriceAPips
      | "coqs_replspriceb" -> COQS_ReplSPriceB
      | "coqs_replspricebpips" -> COQS_ReplSPriceBPips
      | "coqs_rhoa%a" -> COQS_RhoAPercentA
      | "coqs_rhoa%b" -> COQS_RhoAPercentB
      | "coqs_rhoaa" -> COQS_RhoAA
      | "coqs_rhoaapips" -> COQS_RhoAAPips
      | "coqs_rhoab" -> COQS_RhoAB
      | "coqs_rhoabpips" -> COQS_RhoABPips
      | "coqs_rhob%a" -> COQS_RhoBPercentA
      | "coqs_rhob%b" -> COQS_RhoBPercentB
      | "coqs_rhoba" -> COQS_RhoBA
      | "coqs_rhobapips" -> COQS_RhoBAPips
      | "coqs_rhobb" -> COQS_RhoBB
      | "coqs_rhobbpips" -> COQS_RhoBBPips
      | "coqs_s10deltarega%a" -> COQS_S10DeltaRegaPercentA
      | "coqs_s10deltarega%b" -> COQS_S10DeltaRegaPercentB
      | "coqs_s10deltaregaa" -> COQS_S10DeltaRegaA
      | "coqs_s10deltaregaapips" -> COQS_S10DeltaRegaAPips
      | "coqs_s10deltaregab" -> COQS_S10DeltaRegaB
      | "coqs_s10deltaregabpips" -> COQS_S10DeltaRegaBPips
      | "coqs_s10deltasega%a" -> COQS_S10DeltaSegaPercentA
      | "coqs_s10deltasega%b" -> COQS_S10DeltaSegaPercentB
      | "coqs_s10deltasegaa" -> COQS_S10DeltaSegaA
      | "coqs_s10deltasegaapips" -> COQS_S10DeltaSegaAPips
      | "coqs_s10deltasegab" -> COQS_S10DeltaSegaB
      | "coqs_s10deltasegabpips" -> COQS_S10DeltaSegaBPips
      | "coqs_s25deltarega%a" -> COQS_S25DeltaRegaPercentA
      | "coqs_s25deltarega%b" -> COQS_S25DeltaRegaPercentB
      | "coqs_s25deltaregaa" -> COQS_S25DeltaRegaA
      | "coqs_s25deltaregaapips" -> COQS_S25DeltaRegaAPips
      | "coqs_s25deltaregab" -> COQS_S25DeltaRegaB
      | "coqs_s25deltaregabpips" -> COQS_S25DeltaRegaBPips
      | "coqs_s25deltasega%a" -> COQS_S25DeltaSegaPercentA
      | "coqs_s25deltasega%b" -> COQS_S25DeltaSegaPercentB
      | "coqs_s25deltasegaa" -> COQS_S25DeltaSegaA
      | "coqs_s25deltasegaapips" -> COQS_S25DeltaSegaAPips
      | "coqs_s25deltasegab" -> COQS_S25DeltaSegaB
      | "coqs_s25deltasegabpips" -> COQS_S25DeltaSegaBPips
      | "coqs_sdelta%a" -> COQS_SDeltaPercentA
      | "coqs_sdelta%b" -> COQS_SDeltaPercentB
      | "coqs_sdeltaa" -> COQS_SDeltaA
      | "coqs_sdeltaapips" -> COQS_SDeltaAPips
      | "coqs_sdeltab" -> COQS_SDeltaB
      | "coqs_sdeltabpips" -> COQS_SDeltaBPips
      | "coqs_sdeltagapdown%a" -> COQS_SDeltaGapDownPercentA
      | "coqs_sdeltagapdown%b" -> COQS_SDeltaGapDownPercentB
      | "coqs_sdeltagapdowna" -> COQS_SDeltaGapDownA
      | "coqs_sdeltagapdownapips" -> COQS_SDeltaGapDownAPips
      | "coqs_sdeltagapdownb" -> COQS_SDeltaGapDownB
      | "coqs_sdeltagapdownbpips" -> COQS_SDeltaGapDownBPips
      | "coqs_sdeltagapup%a" -> COQS_SDeltaGapUpPercentA
      | "coqs_sdeltagapup%b" -> COQS_SDeltaGapUpPercentB
      | "coqs_sdeltagapupa" -> COQS_SDeltaGapUpA
      | "coqs_sdeltagapupapips" -> COQS_SDeltaGapUpAPips
      | "coqs_sdeltagapupb" -> COQS_SDeltaGapUpB
      | "coqs_sdeltagapupbpips" -> COQS_SDeltaGapUpBPips
      | "coqs_sderive%a" -> COQS_SDerivePercentA
      | "coqs_sderive%b" -> COQS_SDerivePercentB
      | "coqs_sderivea" -> COQS_SDeriveA
      | "coqs_sderiveapips" -> COQS_SDeriveAPips
      | "coqs_sderiveb" -> COQS_SDeriveB
      | "coqs_sderivebpips" -> COQS_SDeriveBPips
      | "coqs_sfwddelta%a" -> COQS_SFwdDeltaPercentA
      | "coqs_sfwddelta%b" -> COQS_SFwdDeltaPercentB
      | "coqs_sfwddeltaa" -> COQS_SFwdDeltaA
      | "coqs_sfwddeltaapips" -> COQS_SFwdDeltaAPips
      | "coqs_sfwddeltab" -> COQS_SFwdDeltaB
      | "coqs_sfwddeltabpips" -> COQS_SFwdDeltaBPips
      | "coqs_sgamma%a" -> COQS_SGammaPercentA
      | "coqs_sgamma%b" -> COQS_SGammaPercentB
      | "coqs_sgammaa" -> COQS_SGammaA
      | "coqs_sgammaapips" -> COQS_SGammaAPips
      | "coqs_sgammab" -> COQS_SGammaB
      | "coqs_sgammabpips" -> COQS_SGammaBPips
      | "coqs_slvprice%a" -> COQS_SLVPricePercentA
      | "coqs_slvprice%b" -> COQS_SLVPricePercentB
      | "coqs_slvpricea" -> COQS_SLVPriceA
      | "coqs_slvpriceapips" -> COQS_SLVPriceAPips
      | "coqs_slvpriceb" -> COQS_SLVPriceB
      | "coqs_slvpricebpips" -> COQS_SLVPriceBPips
      | "coqs_spread%a" -> COQS_SpreadPercentA
      | "coqs_spread%b" -> COQS_SpreadPercentB
      | "coqs_spreada" -> COQS_SpreadA
      | "coqs_spreadapips" -> COQS_SpreadAPips
      | "coqs_spreadb" -> COQS_SpreadB
      | "coqs_spreadbpips" -> COQS_SpreadBPips
      | "coqs_sprice%a" -> COQS_SPricePercentA
      | "coqs_sprice%b" -> COQS_SPricePercentB
      | "coqs_spricea" -> COQS_SPriceA
      | "coqs_spriceapips" -> COQS_SPriceAPips
      | "coqs_spriceb" -> COQS_SPriceB
      | "coqs_spricebpips" -> COQS_SPriceBPips
      | "coqs_ssdelta%a" -> COQS_SSDeltaPercentA
      | "coqs_ssdelta%b" -> COQS_SSDeltaPercentB
      | "coqs_ssdeltaa" -> COQS_SSDeltaA
      | "coqs_ssdeltaapips" -> COQS_SSDeltaAPips
      | "coqs_ssdeltab" -> COQS_SSDeltaB
      | "coqs_ssdeltabpips" -> COQS_SSDeltaBPips
      | "coqs_ssgamma%a" -> COQS_SSGammaPercentA
      | "coqs_ssgamma%b" -> COQS_SSGammaPercentB
      | "coqs_ssgammaa" -> COQS_SSGammaA
      | "coqs_ssgammaapips" -> COQS_SSGammaAPips
      | "coqs_ssgammab" -> COQS_SSGammaB
      | "coqs_ssgammabpips" -> COQS_SSGammaBPips
      | "coqs_svdupire%a" -> COQS_SVDupirePercentA
      | "coqs_svdupire%b" -> COQS_SVDupirePercentB
      | "coqs_svdupirea" -> COQS_SVDupireA
      | "coqs_svdupireapips" -> COQS_SVDupireAPips
      | "coqs_svdupireb" -> COQS_SVDupireB
      | "coqs_svdupirebpips" -> COQS_SVDupireBPips
      | "coqs_svega%a" -> COQS_SVegaPercentA
      | "coqs_svega%b" -> COQS_SVegaPercentB
      | "coqs_svegaa" -> COQS_SVegaA
      | "coqs_svegaapips" -> COQS_SVegaAPips
      | "coqs_svegab" -> COQS_SVegaB
      | "coqs_svegabpips" -> COQS_SVegaBPips
      | "coqs_svegabspread" -> COQS_SVegaBSpread
      | "coqs_svegaweighted%a" -> COQS_SVegaWeightedPercentA
      | "coqs_svegaweighted%b" -> COQS_SVegaWeightedPercentB
      | "coqs_svegaweighteda" -> COQS_SVegaWeightedA
      | "coqs_svegaweightedapips" -> COQS_SVegaWeightedAPips
      | "coqs_svegaweightedb" -> COQS_SVegaWeightedB
      | "coqs_svegaweightedbpips" -> COQS_SVegaWeightedBPips
      | "coqs_svpde%a" -> COQS_SVPdePercentA
      | "coqs_svpde%b" -> COQS_SVPdePercentB
      | "coqs_svpdea" -> COQS_SVPdeA
      | "coqs_svpdeapips" -> COQS_SVPdeAPips
      | "coqs_svpdeb" -> COQS_SVPdeB
      | "coqs_svpdebpips" -> COQS_SVPdeBPips
      | "coqs_svplussprice%a" -> COQS_SVPlusSPricePercentA
      | "coqs_svplussprice%b" -> COQS_SVPlusSPricePercentB
      | "coqs_svplusspricea" -> COQS_SVPlusSPriceA
      | "coqs_svplusspriceapips" -> COQS_SVPlusSPriceAPips
      | "coqs_svplusspriceb" -> COQS_SVPlusSPriceB
      | "coqs_svplusspricebpips" -> COQS_SVPlusSPriceBPips
      | "coqs_svprice%a" -> COQS_SVPricePercentA
      | "coqs_svprice%b" -> COQS_SVPricePercentB
      | "coqs_svpricea" -> COQS_SVPriceA
      | "coqs_svpriceapips" -> COQS_SVPriceAPips
      | "coqs_svpriceb" -> COQS_SVPriceB
      | "coqs_svpricebpips" -> COQS_SVPriceBPips
      | "coqs_svsprice%a" -> COQS_SVSPricePercentA
      | "coqs_svsprice%b" -> COQS_SVSPricePercentB
      | "coqs_svspricea" -> COQS_SVSPriceA
      | "coqs_svspriceapips" -> COQS_SVSPriceAPips
      | "coqs_svspriceb" -> COQS_SVSPriceB
      | "coqs_svspricebpips" -> COQS_SVSPriceBPips
      | "coqs_svvol%a" -> COQS_SVvolPercentA
      | "coqs_svvol%b" -> COQS_SVvolPercentB
      | "coqs_svvola" -> COQS_SVvolA
      | "coqs_svvolapips" -> COQS_SVvolAPips
      | "coqs_svvolb" -> COQS_SVvolB
      | "coqs_svvolbpips" -> COQS_SVvolBPips
      | "coqs_vega%a" -> COQS_VegaPercentA
      | "coqs_vega%b" -> COQS_VegaPercentB
      | "coqs_vegaa" -> COQS_VegaA
      | "coqs_vegaapips" -> COQS_VegaAPips
      | "coqs_vegab" -> COQS_VegaB
      | "coqs_vegabpips" -> COQS_VegaBPips
      | "coqs_vegaweighted%a" -> COQS_VegaWeightedPercentA
      | "coqs_vegaweighted%b" -> COQS_VegaWeightedPercentB
      | "coqs_vegaweighteda" -> COQS_VegaWeightedA
      | "coqs_vegaweightedapips" -> COQS_VegaWeightedAPips
      | "coqs_vegaweightedb" -> COQS_VegaWeightedB
      | "coqs_vegaweightedbpips" -> COQS_VegaWeightedBPips
      | "coqs_vrdelta%a" -> COQS_VRDeltaPercentA
      | "coqs_vrdelta%b" -> COQS_VRDeltaPercentB
      | "coqs_vrdeltaa" -> COQS_VRDeltaA
      | "coqs_vrdeltaapips" -> COQS_VRDeltaAPips
      | "coqs_vrdeltab" -> COQS_VRDeltaB
      | "coqs_vrdeltabpips" -> COQS_VRDeltaBPips
      | "coqs_vrgamma%a" -> COQS_VRGammaPercentA
      | "coqs_vrgamma%b" -> COQS_VRGammaPercentB
      | "coqs_vrgammaa" -> COQS_VRGammaA
      | "coqs_vrgammaapips" -> COQS_VRGammaAPips
      | "coqs_vrgammab" -> COQS_VRGammaB
      | "coqs_vrgammabpips" -> COQS_VRGammaBPips
      | "coqs_vvol%a" -> COQS_VvolPercentA
      | "coqs_vvol%b" -> COQS_VvolPercentB
      | "coqs_vvola" -> COQS_VvolA
      | "coqs_vvolapips" -> COQS_VvolAPips
      | "coqs_vvolb" -> COQS_VvolB
      | "coqs_vvolbpips" -> COQS_VvolBPips
      | "coqsdrilldownoutputs" -> CoqsDrillDownOutputs
      | "coqsoutputs" -> CoqsOutputs
      | "cp" -> CP
      | "crossdeltabaserhoexp" -> CrossDeltaBaseRhoExp
      | "crossgrannaexp" -> CrossGrannaExp
      | "crossvegabaserhoexp" -> CrossVegaBaseRhoExp
      | "crossvegadealtrhoexp" -> CrossVegaDealtRhoExp
      | "cut" -> Cut
      | "dealtdepo" -> DealtDepo
      | "dealtdepotopayout" -> DealtDepoToPayOut
      | "dealtdepotostandarddelivery" -> DealtDepoToStandardDelivery
      | "dealtdepotot1" -> DealtDepoToT1
      | "dealtdepotot2" -> DealtDepoToT2
      | "dealtnotional" -> DealtNotional
      | "dealtrhobucketed" -> DealtRhoBucketed
      | "dealtrhoexp" -> DealtRhoExp
      | "dealtrhoexp%base" -> DealtRhoExpPercentBase
      | "dealtrhoexp%dealt" -> DealtRhoExpPercentDealt
      | "dealtrhoexpassetpips" -> DealtRhoExpAssetPips
      | "dealtrhoexppips" -> DealtRhoExpPips
      | "dealtrhopayout" -> DealtRhoPayout
      | "dealtrhotot1" -> DealtRhoToT1
      | "dealtrhotot1%base" -> DealtRhoToT1PercentBase
      | "dealtrhotot1%dealt" -> DealtRhoToT1PercentDealt
      | "dealtrhotot1assetpips" -> DealtRhoToT1AssetPips
      | "dealtrhotot1pips" -> DealtRhoToT1Pips
      | "dealtrhotot2" -> DealtRhoToT2
      | "dealtrhotot2%base" -> DealtRhoToT2PercentBase
      | "dealtrhotot2%dealt" -> DealtRhoToT2PercentDealt
      | "dealtrhotot2assetpips" -> DealtRhoToT2AssetPips
      | "dealtrhotot2pips" -> DealtRhoToT2Pips
      | "dealtzcrhobucketed" -> DealtZCRhoBucketed
      | "dealtzcrhoexp" -> DealtZCRhoExp
      | "dealtzcrhotot1" -> DealtZCRhoToT1
      | "dealtzcrhotot2" -> DealtZCRhoToT2
      | "dealtzero" -> DealtZero
      | "dealtzerotopayout" -> DealtZeroToPayOut
      | "dealtzerotostandarddelivery" -> DealtZeroToStandardDelivery
      | "dealtzerotot1" -> DealtZeroToT1
      | "dealtzerotot2" -> DealtZeroToT2
      | "defaultsprice" -> DefaultSPrice
      | "deliverydate" -> DeliveryDate
      | "deliverydatetot1" -> DeliveryDateToT1
      | "deliverydatetot2" -> DeliveryDateToT2
      | "delta" -> Delta
      | "delta%base" -> DeltaPercentBase
      | "delta%dealt" -> DeltaPercentDealt
      | "deltaassetpips" -> DeltaAssetPips
      | "deltabase" -> DeltaBase
      | "deltadealt" -> DeltaDealt
      | "deltagapsa" -> DeltaGapsA
      | "deltagapsb" -> DeltaGapsB
      | "deltajumps" -> DeltaJumps
      | "deltamatrix" -> DeltaMatrix
      | "deltapips" -> DeltaPips
      | "deltas" -> Deltas
      | "deltawithstoploss" -> DeltaWithStopLoss
      | "derive" -> Derive
      | "derive%base" -> DerivePercentBase
      | "derive%dealt" -> DerivePercentDealt
      | "deriveassetpips" -> DeriveAssetPips
      | "derivebase" -> DeriveBase
      | "derivedealt" -> DeriveDealt
      | "derivepips" -> DerivePips
      | "description" -> Description
      | "digitaljumpdown" -> DigitalJumpDown
      | "digitaljumpup" -> DigitalJumpUp
      | "dvega25deltaregab" -> DVega25DeltaRegaB
      | "dvega25deltasegab" -> DVega25DeltaSegaB
      | "dvegaathitbounds" -> DVegaAtHitBounds
      | "dvegaathitshifts" -> DvegaAtHitShifts
      | "dvegaathitsprice" -> DVegaAtHitSPrice
      | "dvegadfwd" -> DVegaDFwd
      | "dvegadfwdt1" -> DVegaDFwdT1
      | "dvegadvol" -> DVegaDVol
      | "dvegadvoltot1" -> DVegaDVolToT1
      | "dvegadvoltot2" -> DVegaDVolToT2
      | "dvegaexpdspot" -> DVegaExpDSpot
      | "dvegaimplemented" -> DVegaImplemented
      | "dvegasdelta" -> DVegaSDelta
      | "dvegasprice" -> DVegaSPrice
      | "dvegat1dfwd" -> DVegaT1DFwd
      | "dvegat1dfwdt1" -> DVegaT1DFwdT1
      | "dvegat1dspot" -> DVegaT1DSpot
      | "dvegat2dfwdt2" -> DVegaT2DFwdT2
      | "dvegatot1dvol" -> DVegaToT1DVol
      | "dvegatot1dvoltot1" -> DVegaToT1DVolToT1
      | "dvegatot1dvoltot2" -> DVegaToT1DVolToT2
      | "dvegatot2dspot" -> DVegaToT2DSpot
      | "dvegatot2dvol" -> DVegaToT2DVol
      | "dvegatot2dvoltot1" -> DVegaToT2DVolToT1
      | "dvegatot2dvoltot2" -> DVegaToT2DVolToT2
      | "eventinfo" -> EventInfo
      | "events" -> Events
      | "expirydate" -> ExpiryDate
      | "expirydatetot1" -> ExpiryDateToT1
      | "expirydatetot2" -> ExpiryDateToT2
      | "factors" -> Factors
      | "fixings" -> Fixings
      | "flexbartype" -> FlexBarType
      | "forward" -> Forward
      | "fullbarrierinfo" -> FullBarrierInfo
      | "fullbarrierinfodll" -> FullBarrierInfoDll
      | "fwd" -> Fwd
      | "fwddelta" -> FwdDelta
      | "fwddeltabucketed" -> FwdDeltaBucketed
      | "fwddeltabucketedspread" -> FwdDeltaBucketedSpread
      | "fwddeltaexpiry" -> FwdDeltaExpiry
      | "fwddeltapayout" -> FwdDeltaPayout
      | "fwddeltatot1" -> FwdDeltaToT1
      | "fwddeltatot2" -> FwdDeltaToT2
      | "fwdstrike" -> FwdStrike
      | "fwdtostandarddelivery" -> FwdToStandardDelivery
      | "fwdtot1" -> FwdToT1
      | "fwdtot2" -> FwdToT2
      | "gamma" -> Gamma
      | "gamma%base" -> GammaPercentBase
      | "gamma%dealt" -> GammaPercentDealt
      | "gammaassetpips" -> GammaAssetPips
      | "gammabase" -> GammaBase
      | "gammadealt" -> GammaDealt
      | "gammapips" -> GammaPips
      | "gearing" -> Gearing
      | "gsvparameters" -> GSVParameters
      | "gsvvolerrors" -> GSVVolErrors
      | "gussprice" -> GusSPrice
      | "hedges" -> Hedges
      | "hestonhedge" -> HestonHedge
      | "hestonprobofcrossing" -> HestonProbOfCrossing
      | "hitdate" | "hit-date" -> HitDate
      | "hurdledown" -> HurdleDown
      | "hurdledowntot1" -> HurdleDownToT1
      | "hurdleup" -> HurdleUp
      | "hurdleuptot1" -> HurdleUpToT1
      | "inputdescription" -> InputDescription
      | "inputs" -> Inputs
      | "intrinsicvalue" -> IntrinsicValue
      | "invalphaprice" -> InvAlphaPrice
      | "invbaserhoexp" -> InvBaseRhoExp
      | "invbaserhotot1" -> InvBaseRhoToT1
      | "invbaserhotot2" -> InvBaseRhoToT2
      | "invdealtrhoexp" -> InvDealtRhoExp
      | "invdealtrhotot1" -> InvDealtRhoToT1
      | "invdealtrhotot2" -> InvDealtRhoToT2
      | "invdelta" -> InvDelta
      | "invdeltawithstoploss" -> InvDeltaWithStopLoss
      | "invderive" -> InvDerive
      | "inversebidask" -> InverseBidAsk
      | "invgamma" -> InvGamma
      | "invintrinsicvalue" -> InvIntrinsicValue
      | "invsdelta" -> InvSDelta
      | "invsgamma" -> InvSGamma
      | "invsmiledeltawithstoploss" -> InvSmileDeltaWithStopLoss
      | "invsmilevaluewithstoploss" -> InvSmileValueWithStopLoss
      | "invssdelta" -> InvSSDelta
      | "invssgamma" -> InvSSGamma
      | "invthetaexp" -> InvThetaExp
      | "invtotalderive" -> InvTotalDerive
      | "invtotalsmilederive" -> InvTotalSmileDerive
      | "invtotalsmilevvol" -> InvTotalSmileVvol
      | "invtotalvega" -> InvTotalVega
      | "invtotalvvol" -> InvTotalVvol
      | "invvalue" -> InvValue
      | "invvaluewithstoploss" -> InvValueWithStopLoss
      | "invvega" -> InvVega
      | "invvegaexp" -> InvVegaExp
      | "invvegatot1" -> InvVegaToT1
      | "invvegatot2" -> InvVegaToT2
      | "invvegaweighted" -> InvVegaWeighted
      | "invvrdelta" -> InvVRDelta
      | "invvrgamma" -> InvVRGamma
      | "invvvol" -> InvVvol
      | "isexerciseallowed" -> IsExerciseAllowed
      | "isexpiryallowed" -> IsExpiryAllowed
      | "iskoallowed" -> IsKoAllowed
      | "ismultihurdle" -> IsMultiHurdle
      | "lifetime" -> Lifetime
      | "lifetimes" -> Lifetimes
      | "limitclass" -> LimitClass
      | "limitinfo" -> LimitInfo
      | "lookuphestonpoint" -> LookUpHestonPoint
      | "marketdata" -> MarketData
      | "marketvols" -> MarketVols
      | "minbardays" -> MinBarDays
      | "minmax" -> MinMax
      | "moneyzero" -> MoneyZero
      | "nextbarrierinfo" -> NextBarrierInfo
      | "nextpinrisk" -> NextPinRisk
      | "notional" -> Notional
      | "notionalandpayout" -> NotionalAndPayOut
      | "notionalb" -> NotionalB
      | "numbardays" -> NumBarDays
      | "onedtheta" -> OneDTheta
      | "onedtheta%base" -> OneDThetaPercentBase
      | "onedtheta%dealt" -> OneDThetaPercentDealt
      | "onedthetaassetpips" -> OneDThetaAssetPips
      | "onedthetabase" -> OneDThetaBase
      | "onedthetadealt" -> OneDThetaDealt
      | "onedthetapips" -> OneDThetaPips
      | "optimisedvolsurface" -> OptimisedVolSurface
      | "optiondetails" -> OptionDetails
      | "optioninfo" -> OptionInfo
      | "optiontype" -> OptionType
      | "passportnotionalstep" -> PassportNotionalStep
      | "passportpl" -> PassportPL
      | "passportposition" -> PassportPosition
      | "payoutdate" -> PayOutDate
      | "pinrisk" -> PinRisk
      | "pinriskdrilldown" -> PinRiskDrillDown
      | "posadjustments" -> PosAdjustments
      | "positions" -> Positions
      | "predicteddelta" -> PredictedDelta
      | "predictedderive" -> PredictedDerive
      | "predictedgamma" -> PredictedGamma
      | "predictedtotalderive" -> PredictedTotalDerive
      | "price%base" -> PricePercentBase
      | "price%dealt" -> PricePercentDealt
      | "priceassetpips" -> PriceAssetPips
      | "pricebase" -> PriceBase
      | "pricedealt" -> PriceDealt
      | "pricepips" -> PricePips
      | "quantodeltas" -> QuantoDeltas
      | "quantoderives" -> QuantoDerives
      | "quantogammas" -> QuantoGammas
      | "quantoprice" -> QuantoPrice
      | "quantosdeltas" -> QuantoSDeltas
      | "quantosgammas" -> QuantoSGammas
      | "quantosprice" -> QuantoSPrice
      | "quantosvegas" -> QuantoSVegas
      | "quantovegas" -> QuantoVegas
      | "quantovvols" -> QuantoVvols
      | "realstheta" -> RealSTheta
      | "realtheta" -> RealTheta
      | "rr25" -> RR25
      | "sccyformulaexecutionmode" -> SCcyFormulaExecutionMode
      | "sdelta" -> SDelta
      | "sgamma" -> SGamma
      | "shifts" -> Shifts
      | "slvvolerrors" -> SLVVolErrors
      | "smileannualisedonedtheta" -> SmileAnnualisedOneDTheta
      | "smileatmvegabucketed" -> SmileAtmVegaBucketed
      | "smileatmvegabucketedspread" -> SmileAtmVegaBucketedSpread
      | "smilebaserhobucketed" -> SmileBaseRhoBucketed
      | "smilebasezcrhobucketed" -> SmileBaseZCRhoBucketed
      | "smiledealtrhobucketed" -> SmileDealtRhoBucketed
      | "smiledealtzcrhobucketed" -> SmileDealtZCRhoBucketed
      | "smiledeltagapsa" -> SmileDeltaGapsA
      | "smiledeltagapsb" -> SmileDeltaGapsB
      | "smiledeltawithstoploss" -> SmileDeltaWithStopLoss
      | "smilefwddelta" -> SmileFwdDelta
      | "smilefwddeltabucketed" -> SmileFwdDeltaBucketed
      | "smilefwddeltabucketedspread" -> SmileFwdDeltaBucketedSpread
      | "smilefwddeltaexpiry" -> SmileFwdDeltaExpiry
      | "smilefwddeltapayout" -> SmileFwdDeltaPayout
      | "smilefwddeltatot1" -> SmileFwdDeltaToT1
      | "smilefwddeltatot2" -> SmileFwdDeltaToT2
      | "smileonedtheta" -> SmileOneDTheta
      | "smilerega10deltabucketed" -> SmileRega10DeltaBucketed
      | "smilerega10deltabucketedspread" -> SmileRega10DeltaBucketedSpread
      | "smilerega25deltabucketed" -> SmileRega25DeltaBucketed
      | "smilerega25deltabucketedspread" -> SmileRega25DeltaBucketedSpread
      | "smilesega10deltabucketed" -> SmileSega10DeltaBucketed
      | "smilesega10deltabucketedspread" -> SmileSega10DeltaBucketedSpread
      | "smilesega25deltabucketed" -> SmileSega25DeltaBucketed
      | "smilesega25deltabucketedspread" -> SmileSega25DeltaBucketedSpread
      | "smilespotdelta" -> SmileSpotDelta
      | "smiletotalbaserho" -> SmileTotalBaseRho
      | "smiletotaldealtrho" -> SmileTotalDealtRho
      | "smilevaluewithstoploss" -> SmileValueWithStopLoss
      | "smilevegabucketed" -> SmileVegaBucketed
      | "smilevegabucketedspread" -> SmileVegaBucketedSpread
      | "spotdate" -> SpotDate
      | "spotdelta" -> SpotDelta
      | "ssdelta" -> SSDelta
      | "ssgamma" -> SSGamma
      | "strike" -> Strike
      | "svdupireparameters" -> SVDupireParameters
      | "svdupirevolerrors" -> SVDupireVolErrors
      | "svhedge" -> SVHedge
      | "svmcvanillaerror" -> SVMCVanillaError
      | "svparameters" -> SVParameters
      | "svpdeprice" -> SVPdePrice
      | "svpdevanillaerror" -> SVPDEVanillaError
      | "svprice" -> SVPrice
      | "svprobofcrossing" -> SVProbOfCrossing
      | "svsprice" -> SVSPrice
      | "svvanillaerror" -> SVVanillaError
      | "svvolerrors" -> SVVolErrors
      | "svvols" -> SVVols
      | "thetaexp" -> ThetaExp
      | "thetaexp%base" -> ThetaExpPercentBase
      | "thetaexp%dealt" -> ThetaExpPercentDealt
      | "thetaexpassetpips" -> ThetaExpAssetPips
      | "thetaexppips" -> ThetaExpPips
      | "totalbaserho" -> TotalBaseRho
      | "totalbaserho%base" -> TotalBaseRhoPercentBase
      | "totalbaserho%dealt" -> TotalBaseRhoPercentDealt
      | "totalbaserhoassetpips" -> TotalBaseRhoAssetPips
      | "totaldealtrho" -> TotalDealtRho
      | "totaldealtrho%base" -> TotalDealtRhoPercentBase
      | "totaldealtrho%dealt" -> TotalDealtRhoPercentDealt
      | "totaldealtrhoassetpips" -> TotalDealtRhoAssetPips
      | "totalderive" -> TotalDerive
      | "totalderive%base" -> TotalDerivePercentBase
      | "totalderive%dealt" -> TotalDerivePercentDealt
      | "totalderiveassetpips" -> TotalDeriveAssetPips
      | "totalderivepips" -> TotalDerivePips
      | "totalderiveweighted" -> TotalDeriveWeighted
      | "totalfwdderive" -> TotalFwdDerive
      | "totallooks" -> TotalLooks
      | "totalsmilederive" -> TotalSmileDerive
      | "totalsmilevvol" -> TotalSmileVvol
      | "totalvega" -> TotalVega
      | "totalvega%base" -> TotalVegaPercentBase
      | "totalvega%dealt" -> TotalVegaPercentDealt
      | "totalvegaassetpips" -> TotalVegaAssetPips
      | "totalvegabase" -> TotalVegaBase
      | "totalvegadealt" -> TotalVegaDealt
      | "totalvegapips" -> TotalVegaPips
      | "totalvvol" -> TotalVvol
      | "totalvvol%base" -> TotalVvolPercentBase
      | "totalvvol%dealt" -> TotalVvolPercentDealt
      | "totalvvolassetpips" -> TotalVvolAssetPips
      | "totalvvolpips" -> TotalVvolPips
      | "tradedate" -> TradeDate
      | "trfpinrisk" -> TRFPinRisk
      | "trfpinriskdrilldown" -> TRFPinRiskDrillDown
      | "typeandparams" -> TypeAndParams
      | "types" -> Types
      | "underlyingprice" -> UnderlyingPrice
      | "usingnumericalmethod" -> UsingNumericalMethod
      | "value" -> Value
      | "valuebase" -> ValueBase
      | "valuedealt" -> ValueDealt
      | "valuewithstoploss" -> ValueWithStopLoss
      | "vega" -> Vega
      | "vega%base" -> VegaPercentBase
      | "vega%dealt" -> VegaPercentDealt
      | "vegaassetpips" -> VegaAssetPips
      | "vegabase" -> VegaBase
      | "vegabucketed" -> VegaBucketed
      | "vegadealt" -> VegaDealt
      | "vegaexp" -> VegaExp
      | "vegaexp%base" -> VegaExpPercentBase
      | "vegaexp%dealt" -> VegaExpPercentDealt
      | "vegaexpassetpips" -> VegaExpAssetPips
      | "vegaexppips" -> VegaExpPips
      | "vegapips" -> VegaPips
      | "vegat1%base" -> VegaT1PercentBase
      | "vegat1%dealt" -> VegaT1PercentDealt
      | "vegat1assetpips" -> VegaT1AssetPips
      | "vegat1pips" -> VegaT1Pips
      | "vegat2%base" -> VegaT2PercentBase
      | "vegat2%dealt" -> VegaT2PercentDealt
      | "vegat2assetpips" -> VegaT2AssetPips
      | "vegat2pips" -> VegaT2Pips
      | "vegatot1" -> VegaToT1
      | "vegatot2" -> VegaToT2
      | "vegaweighted" -> VegaWeighted
      | "vol" -> Vol
      | "voltot1" -> VolToT1
      | "voltot2" -> VolToT2
      | "vrdelta" -> VRDelta
      | "vrgamma" -> VRGamma
      | "vvol" -> Vvol
      | "vvol%base" -> VvolPercentBase
      | "vvol%dealt" -> VvolPercentDealt
      | "vvolassetpips" -> VvolAssetPips
      | "vvolbase" -> VVolBase
      | "vvoldealt" -> VvolDealt
      | "vvolpips" -> VvolPips
      | "zeroornegativestrike" -> ZeroOrNegativeStrike
      | s -> failwith (Printf.sprintf "option_output_of_string: %s is not a recognised output" s)
      ];
