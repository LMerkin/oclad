
(* 
   generate_file input tag replacement output
   
   Reads in the input file and replaces the tag with replacement in the output file
*)
value generate_file: string -> list string -> list string  -> string -> unit;


(* Adds spaces and | to a list of strings to create a formatted ocaml type definition *)
value typedef_of_list: list string -> string;

(* 
   file_entries file regexp1 regexp2

   matches first occurance of regexp1 in the file and then returns all the matches from regexp2 in the first match
 *)
value file_entries: string -> string -> string -> list string;
