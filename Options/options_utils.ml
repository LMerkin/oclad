
type option_info =
{
  short_code: string;
  category: string;
  required: string;
  constructor: string;
  params: string
}
with string_of, parse;


exception OptionConstruction of string;


type option_parameters =
{
  ccypair: Ccys.ccypair;
  startdate: int;
  parameters: Hashtbl.t Argument.argument_type Argument.argument
};


value string_of_option_parameters: option_parameters -> string =
fun p ->
  let string_of_p : Argument.argument_type ->
                    Argument.argument -> 
                    Buffer.t -> Buffer.t =
    fun k v b -> 
      let key    : string = Argument.string_of_argument_type k in
      let svalue : string = Argument.string_of_argument v in
      do
      {
        Buffer.add_string b ( Printf.sprintf " {%s: %s}" key svalue );
        b
      }
  in
  let params_str: string = 
    Buffer.contents ( Hashtbl.fold string_of_p p.parameters ( Buffer.create 50 ) )
  in
    (Ccys.string_of_ccypair p.ccypair) ^ params_str;


value list_of_optional_parameters: option_parameters -> list Argument.optional_argument =
fun p ->
  Hashtbl.fold (fun _k v l -> match v with 
                             [
                               Argument.Optional o -> [o :: l]
                             | _ -> l
                             ]) p.parameters [];


value contains : option_parameters -> string -> bool = 
fun op name ->
  Hashtbl.mem op.parameters ( Argument.argument_type_of_string name );
  

value add_optional_parameters: 
  Hashtbl.t Argument.argument_type Argument.argument -> 
  list Argument.optional_argument -> unit =

fun p l ->
  List.iter (fun a -> Hashtbl.add p (Argument.type_of_argument (Argument.Optional a)) (Argument.Optional a)) l;


value get_required_parameters: 
  Hashtbl.t Argument.argument_type Argument.argument -> list Argument.required_argument =

fun p ->
  Hashtbl.fold (fun _k v l -> match v with 
                             [
                               Argument.Required r -> [r :: l]
                             | _ -> l
                             ]) p [];

value add_required_parameters: 
  Hashtbl.t Argument.argument_type Argument.argument -> 
  list Argument.required_argument -> unit =

fun p l ->
  List.iter (fun a -> Hashtbl.add p (Argument.type_of_argument (Argument.Required a)) (Argument.Required a)) l;


exception ArgumentNotFound of string;


value get_argument: 
  Hashtbl.t Argument.argument_type Argument.argument -> 
  Argument.argument_type -> Argument.required_argument =

fun p a_type ->
  try
    let result: Argument.argument = Hashtbl.find p a_type in
    match result with
    [
      Argument.Required r -> r
    | _ -> raise Not_found
    ]
  with
      [ Not_found -> raise (ArgumentNotFound (Argument.string_of_argument_type a_type)) ];


value get_cp_argument: 
  Hashtbl.t Argument.argument_type Argument.argument -> 
  ( Argument.argument_type * Argument.argument_type ) -> Argument.required_argument =

fun p a_type ->
  let cp : Argument.required_argument = get_argument p Argument.T_CP in
  match cp with
  [
    Argument.CP Argument.Call -> get_argument p (fst a_type)
  | Argument.CP Argument.Put  -> get_argument p (snd a_type)
  | _ -> raise (ArgumentNotFound "get_cp_argument")
  ];


value add_argument:
  Hashtbl.t Argument.argument_type Argument.argument -> 
  Argument.required_argument -> unit =

fun p a ->
  Hashtbl.add p (Argument.type_of_argument (Argument.Required a)) (Argument.Required a);


value default_required_params: list ( string * bool ) -> option_parameters =
fun req_params ->
  let params : Hashtbl.t Argument.argument_type Argument.argument =
    Hashtbl.create 20
  in do
   {
     List.iter 
       (
         fun ( name, is_required ) -> 
           if is_required then
             let t : Argument.argument_type =
               Argument.argument_type_of_string name
             in
               Hashtbl.replace params t ( Argument.default_of_argument t )
           else
             ()
       ) req_params;
     { ccypair = Ccys.make_ccypair Ccys.EUR Ccys.USD; startdate = 40000; parameters = params }
   };
