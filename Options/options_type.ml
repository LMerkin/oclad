
type fx_category =
[
  C_Accrual
| C_AmericanDigital
| C_AverageOption
| C_BarrierOption
| C_EuropeanDigital
| C_ExoticKnockIn
| C_Forward
| C_ForwardStartDigital
| C_ForwardStartOption
| C_HurdleOption
| C_Lookback
| C_Miscellaneous
| C_Resettable
| C_SimpleOption
| C_TransatlanticOption
| C_VolatilityProduct
| C_WindowAmericanDigital
| C_WindowBarrierOption
| C_WindowEuropeanDigital
]
with string_of, parse;


type fx_option_type =
[
  T_ABF
| T_ABFKO
| T_ABFKOMBAR
| T_ABFMBAR
| T_ADBF
| T_AVGDKO
| T_AVGDOUI
| T_AVGHDKO
| T_AVGHDOUI
| T_AVGHUODI
| T_AVGKHLDKO
| T_AVGKLDKO
| T_AVGRATE
| T_AVGRATEH
| T_AVGRATEHI
| T_AVGRATEI
| T_AVGRATEK
| T_AVGSTRIKE
| T_AVGSTRIKEH
| T_AVGUODI
| T_BOX
| T_CASH
| T_CDNTA
| T_CDNTB
| T_CHOOSER
| T_COMPOUND
| T_CRAA
| T_CRAB
| T_CSBTLF
| T_CSTLF
| T_DARO
| T_DAROH
| T_DAROHI
| T_DAROI
| T_DAROK
| T_DAROKH
| T_DAROKHI
| T_DAROKI
| T_DBDOA
| T_DBDOB
| T_DBEDOA
| T_DBEDOB
| T_DBEUOA
| T_DBEUOB
| T_DBLDOA
| T_DBLDOB
| T_DBLUOA
| T_DBLUOB
| T_DBUOA
| T_DBUOB
| T_DDEA
| T_DDEB
| T_DEA
| T_DEAMH
| T_DEB
| T_DEBMH
| T_DEDKOA
| T_DEDKOB
| T_DEEDKOA
| T_DEEDKOB
| T_DELDKOA
| T_DELDKOB
| T_DEPOA
| T_DEPOB
| T_DHFWD
| T_DHFWDA
| T_DHFWDB
| T_DHITAE
| T_DHITBE
| T_DI
| T_DKI
| T_DKO
| T_DKOCRAA
| T_DKOCRAB
| T_DKODH
| T_DKODHFWD
| T_DKODIE
| T_DKODOE
| T_DKOELOOKR
| T_DKOELOOKRDE
| T_DKOHD
| T_DKOHU
| T_DKOI
| T_DKOK
| T_DKOLOOKR
| T_DKOLOOKS
| T_DKOLOOKSK
| T_DKOUIE
| T_DKOUOE
| T_DNTA
| T_DNTB
| T_DNTDHA
| T_DNTDHB
| T_DNTDOTAE
| T_DNTDOTBE
| T_DO
| T_DOE
| T_DOEK
| T_DOEUI
| T_DOEUO
| T_DOHD
| T_DOHU
| T_DOLOOKR
| T_DOLOOKS
| T_DOLOOKSK
| T_DOLUI
| T_DOLUO
| T_DOTAE
| T_DOTAH
| T_DOTBE
| T_DOTBH
| T_DOUI
| T_DOUIDKO
| T_DOUIDOE
| T_DOUIDOUIKOTAE
| T_DOUIDOUIKOTBE
| T_DOUIKOTAE
| T_DOUIKOTBE
| T_DOUILDKO
| T_DOUILKI
| T_DOUIUODIKOTAE
| T_DOUIUODIKOTBE
| T_DOUIUOE
| T_DOUO
| T_EARLYFADER
| T_EARLYFADERDKI
| T_EARLYFADERDOUI
| T_EARLYFADERUODI
| T_EARLYSHOUT
| T_EARLYSPEAK
| T_EDI
| T_EDKI
| T_EDKO
| T_EDKODH
| T_EDO
| T_EDOUI
| T_EDOUIDKO
| T_EFADERDOE
| T_EFADERDOEK
| T_EFADERMBAR
| T_EFADEROEN
| T_EFADERUOE
| T_EFADERUOEK
| T_EKIKO
| T_EKOKI
| T_EMDNTMHA
| T_EMDNTMHB
| T_EORA
| T_EORB
| T_ERBA
| T_ERBB
| T_EUI
| T_EUO
| T_EUODI
| T_EUODIDKO
| T_FADER
| T_FADERMBAR
| T_FAVGRATEK
| T_FIXEDA
| T_FIXEDAMH
| T_FIXEDB
| T_FIXEDBMH
| T_FLOATA
| T_FLOATB
| T_FODEA
| T_FODEB
| T_FODKO
| T_FODNTA
| T_FODNTB
| T_FOLDKO
| T_FOOE
| T_FRBDKI
| T_FRBDKO
| T_FRDEA
| T_FRDEB
| T_FRDKI
| T_FRDKO
| T_FRDKODH
| T_FRDNTA
| T_FRDNTB
| T_FRDOTAE
| T_FRDOTAH
| T_FRDOTBE
| T_FRDOTBH
| T_FRDOUI
| T_FRDOUIH
| T_FRFWD
| T_FRFWDH
| T_FRKIKO
| T_FRKOKI
| T_FRLDNTA
| T_FRLDNTB
| T_FRLDOTAE
| T_FRLDOTAH
| T_FRLDOTBE
| T_FRLDOTBH
| T_FROE
| T_FROEDH
| T_FROEH
| T_FROEK
| T_FROEM
| T_FROEMDKI
| T_FROEMDKO
| T_FROEW
| T_FROEWDKI
| T_FROEWDKO
| T_FRUODI
| T_FRUODIH
| T_FVAA
| T_FVAB
| T_FWD
| T_FXCAP2A
| T_FXCAPA
| T_FXSWAP
| T_HITAE
| T_HITBE
| T_INSTALLOPT
| T_IRFIXED
| T_IRFLOAT
| T_IRSWAP
| T_KIFWD
| T_KIKO
| T_KOFWD
| T_KOKI
| T_KOTAE
| T_KOTAH
| T_KOTBE
| T_KOTBH
| T_KOTDNTA
| T_KOTDNTB
| T_KOTFRDNTA
| T_KOTFRDNTB
| T_KOTKOTAE
| T_KOTKOTBE
| T_LDI
| T_LDKI
| T_LDKIMH
| T_LDKO
| T_LDKOMH
| T_LDNTA
| T_LDNTB
| T_LDO
| T_LDOTAE
| T_LDOTAH
| T_LDOTBE
| T_LDOTBH
| T_LDOUI
| T_LKIKO
| T_LKOKI
| T_LKOTAE
| T_LKOTBE
| T_LNMH
| T_LNTA
| T_LNTB
| T_LOAS
| T_LOAX
| T_LOOKR
| T_LOOKS
| T_LOTAE
| T_LOTAH
| T_LOTBE
| T_LOTBH
| T_LUI
| T_LUO
| T_LUODI
| T_MDKO
| T_MDKO3
| T_MDKO3FWD
| T_MDKODH
| T_MDKODHFWDA
| T_MDKODHFWDB
| T_MDKOM
| T_MDKOMFWD
| T_MDKOMH
| T_MDNTA
| T_MDNTB
| T_MDNTDHA
| T_MDNTDHB
| T_MDNTM
| T_MDNTMH
| T_MKOTAE
| T_MKOTBE
| T_MMDNTMHB
| T_N_A
| T_NTA
| T_NTB
| T_OA
| T_OAS
| T_OAX
| T_OE
| T_OEF
| T_OEI
| T_OEK
| T_OEMH
| T_OEN
| T_OPAYOFF of string
| T_OTAE
| T_OTAH
| T_OTBE
| T_OTBH
| T_PASSPORT
| T_PASSPORTPROXY
| T_PASSPORTPROXYI
| T_PDNT3A
| T_PDNT3B
| T_PDNTA
| T_PDNTB
| T_PREMIUMA
| T_PREMIUMB
| T_PRTFADER
| T_RAA
| T_RAB
| T_RADDA
| T_RADDB
| T_RADKOA
| T_RADKOB
| T_RAMBOA
| T_RAMBOB
| T_RAMDKOA
| T_RAMDKOB
| T_RAVGDKO
| T_RAVGHDKO
| T_RDNTA
| T_RDNTB
| T_RPRTFADER
| T_SA
| T_SCCYFORMULA of string
| T_SHOUT
| T_SHOUTI
| T_SPEAK
| T_SPOT
| T_STEPBOX
| T_STLF
| T_STRIP of fx_option_type
| T_TARNA
| T_TARNB
| T_TARNOE
| T_TARNOEK
| T_TLF
| T_TLF2
| T_TRF
| T_TRFE
| T_TRFH
| T_UI
| T_UO
| T_UODI
| T_UODIDKO
| T_UODIDOE
| T_UODIDOUIKOTAE
| T_UODIDOUIKOTBE
| T_UODIKOTAE
| T_UODIKOTBE
| T_UODILDKO
| T_UODILKI
| T_UODIUODIKOTAE
| T_UODIUODIKOTBE
| T_UODIUOE
| T_UODO
| T_UOE
| T_UOEDI
| T_UOEDO
| T_UOEK
| T_UOHD
| T_UOHU
| T_UOLDI
| T_UOLDO
| T_UOLOOKR
| T_UOLOOKS
| T_UOLOOKSK
| T_VARSWAPA
| T_VARSWAPB
| T_VNTLF
| T_VOLSWAPA
| T_VOLSWAPB
| T_WTLF
];


value string_of_fx_option_type: fx_option_type -> string =
fun t ->
  match t with
  [
    T_ABF -> "ABF"
  | T_ABFKO -> "ABFKO"
  | T_ABFKOMBAR -> "ABFKOMBAR"
  | T_ABFMBAR -> "ABFMBAR"
  | T_ADBF -> "ADBF"
  | T_AVGDKO -> "AVGDKO"
  | T_AVGDOUI -> "AVGDOUI"
  | T_AVGHDKO -> "AVGHDKO"
  | T_AVGHDOUI -> "AVGHDOUI"
  | T_AVGHUODI -> "AVGHUODI"
  | T_AVGKHLDKO -> "AVGKHLDKO"
  | T_AVGKLDKO -> "AVGKLDKO"
  | T_AVGRATE -> "AVGRATE"
  | T_AVGRATEH -> "AVGRATEH"
  | T_AVGRATEHI -> "AVGRATEHI"
  | T_AVGRATEI -> "AVGRATEI"
  | T_AVGRATEK -> "AVGRATEK"
  | T_AVGSTRIKE -> "AVGSTRIKE"
  | T_AVGSTRIKEH -> "AVGSTRIKEH"
  | T_AVGUODI -> "AVGUODI"
  | T_BOX -> "BOX"
  | T_CASH -> "CASH"
  | T_CDNTA -> "CDNTA"
  | T_CDNTB -> "CDNTB"
  | T_CHOOSER -> "CHOOSER"
  | T_COMPOUND -> "COMPOUND"
  | T_CRAA -> "CRAA"
  | T_CRAB -> "CRAB"
  | T_CSBTLF -> "CSBTLF"
  | T_CSTLF -> "CSTLF"
  | T_DARO -> "DARO"
  | T_DAROH -> "DAROH"
  | T_DAROHI -> "DAROHI"
  | T_DAROI -> "DAROI"
  | T_DAROK -> "DAROK"
  | T_DAROKH -> "DAROKH"
  | T_DAROKHI -> "DAROKHI"
  | T_DAROKI -> "DAROKI"
  | T_DBDOA -> "DBDOA"
  | T_DBDOB -> "DBDOB"
  | T_DBEDOA -> "DBEDOA"
  | T_DBEDOB -> "DBEDOB"
  | T_DBEUOA -> "DBEUOA"
  | T_DBEUOB -> "DBEUOB"
  | T_DBLDOA -> "DBLDOA"
  | T_DBLDOB -> "DBLDOB"
  | T_DBLUOA -> "DBLUOA"
  | T_DBLUOB -> "DBLUOB"
  | T_DBUOA -> "DBUOA"
  | T_DBUOB -> "DBUOB"
  | T_DDEA -> "DDEA"
  | T_DDEB -> "DDEB"
  | T_DEA -> "DEA"
  | T_DEAMH -> "DEAMH"
  | T_DEB -> "DEB"
  | T_DEBMH -> "DEBMH"
  | T_DEDKOA -> "DEDKOA"
  | T_DEDKOB -> "DEDKOB"
  | T_DEEDKOA -> "DEEDKOA"
  | T_DEEDKOB -> "DEEDKOB"
  | T_DELDKOA -> "DELDKOA"
  | T_DELDKOB -> "DELDKOB"
  | T_DEPOA -> "DEPOA"
  | T_DEPOB -> "DEPOB"
  | T_DHFWD -> "DHFWD"
  | T_DHFWDA -> "DHFWDA"
  | T_DHFWDB -> "DHFWDB"
  | T_DHITAE -> "DHITAE"
  | T_DHITBE -> "DHITBE"
  | T_DI -> "DI"
  | T_DKI -> "DKI"
  | T_DKO -> "DKO"
  | T_DKOCRAA -> "DKOCRAA"
  | T_DKOCRAB -> "DKOCRAB"
  | T_DKODH -> "DKODH"
  | T_DKODHFWD -> "DKODHFWD"
  | T_DKODIE -> "DKODIE"
  | T_DKODOE -> "DKODOE"
  | T_DKOELOOKR -> "DKOELOOKR"
  | T_DKOELOOKRDE -> "DKOELOOKRDE"
  | T_DKOHD -> "DKOHD"
  | T_DKOHU -> "DKOHU"
  | T_DKOI -> "DKOI"
  | T_DKOK -> "DKOK"
  | T_DKOLOOKR -> "DKOLOOKR"
  | T_DKOLOOKS -> "DKOLOOKS"
  | T_DKOLOOKSK -> "DKOLOOKSK"
  | T_DKOUIE -> "DKOUIE"
  | T_DKOUOE -> "DKOUOE"
  | T_DNTA -> "DNTA"
  | T_DNTB -> "DNTB"
  | T_DNTDHA -> "DNTDHA"
  | T_DNTDHB -> "DNTDHB"
  | T_DNTDOTAE -> "DNTDOTAE"
  | T_DNTDOTBE -> "DNTDOTBE"
  | T_DO -> "DO"
  | T_DOE -> "DOE"
  | T_DOEK -> "DOEK"
  | T_DOEUI -> "DOEUI"
  | T_DOEUO -> "DOEUO"
  | T_DOHD -> "DOHD"
  | T_DOHU -> "DOHU"
  | T_DOLOOKR -> "DOLOOKR"
  | T_DOLOOKS -> "DOLOOKS"
  | T_DOLOOKSK -> "DOLOOKSK"
  | T_DOLUI -> "DOLUI"
  | T_DOLUO -> "DOLUO"
  | T_DOTAE -> "DOTAE"
  | T_DOTAH -> "DOTAH"
  | T_DOTBE -> "DOTBE"
  | T_DOTBH -> "DOTBH"
  | T_DOUI -> "DOUI"
  | T_DOUIDKO -> "DOUIDKO"
  | T_DOUIDOE -> "DOUIDOE"
  | T_DOUIDOUIKOTAE -> "DOUIDOUIKOTAE"
  | T_DOUIDOUIKOTBE -> "DOUIDOUIKOTBE"
  | T_DOUIKOTAE -> "DOUIKOTAE"
  | T_DOUIKOTBE -> "DOUIKOTBE"
  | T_DOUILDKO -> "DOUILDKO"
  | T_DOUILKI -> "DOUILKI"
  | T_DOUIUODIKOTAE -> "DOUIUODIKOTAE"
  | T_DOUIUODIKOTBE -> "DOUIUODIKOTBE"
  | T_DOUIUOE -> "DOUIUOE"
  | T_DOUO -> "DOUO"
  | T_EARLYFADER -> "EARLYFADER"
  | T_EARLYFADERDKI -> "EARLYFADERDKI"
  | T_EARLYFADERDOUI -> "EARLYFADERDOUI"
  | T_EARLYFADERUODI -> "EARLYFADERUODI"
  | T_EARLYSHOUT -> "EARLYSHOUT"
  | T_EARLYSPEAK -> "EARLYSPEAK"
  | T_EDI -> "EDI"
  | T_EDKI -> "EDKI"
  | T_EDKO -> "EDKO"
  | T_EDKODH -> "EDKODH"
  | T_EDO -> "EDO"
  | T_EDOUI -> "EDOUI"
  | T_EDOUIDKO -> "EDOUIDKO"
  | T_EFADERDOE -> "EFADERDOE"
  | T_EFADERDOEK -> "EFADERDOEK"
  | T_EFADERMBAR -> "EFADERMBAR"
  | T_EFADEROEN -> "EFADEROEN"
  | T_EFADERUOE -> "EFADERUOE"
  | T_EFADERUOEK -> "EFADERUOEK"
  | T_EKIKO -> "EKIKO"
  | T_EKOKI -> "EKOKI"
  | T_EMDNTMHA -> "EMDNTMHA"
  | T_EMDNTMHB -> "EMDNTMHB"
  | T_EORA -> "EORA"
  | T_EORB -> "EORB"
  | T_ERBA -> "ERBA"
  | T_ERBB -> "ERBB"
  | T_EUI -> "EUI"
  | T_EUO -> "EUO"
  | T_EUODI -> "EUODI"
  | T_EUODIDKO -> "EUODIDKO"
  | T_FADER -> "FADER"
  | T_FADERMBAR -> "FADERMBAR"
  | T_FAVGRATEK -> "FAVGRATEK"
  | T_FIXEDA -> "FIXEDA"
  | T_FIXEDAMH -> "FIXEDAMH"
  | T_FIXEDB -> "FIXEDB"
  | T_FIXEDBMH -> "FIXEDBMH"
  | T_FLOATA -> "FLOATA"
  | T_FLOATB -> "FLOATB"
  | T_FODEA -> "FODEA"
  | T_FODEB -> "FODEB"
  | T_FODKO -> "FODKO"
  | T_FODNTA -> "FODNTA"
  | T_FODNTB -> "FODNTB"
  | T_FOLDKO -> "FOLDKO"
  | T_FOOE -> "FOOE"
  | T_FRBDKI -> "FRBDKI"
  | T_FRBDKO -> "FRBDKO"
  | T_FRDEA -> "FRDEA"
  | T_FRDEB -> "FRDEB"
  | T_FRDKI -> "FRDKI"
  | T_FRDKO -> "FRDKO"
  | T_FRDKODH -> "FRDKODH"
  | T_FRDNTA -> "FRDNTA"
  | T_FRDNTB -> "FRDNTB"
  | T_FRDOTAE -> "FRDOTAE"
  | T_FRDOTAH -> "FRDOTAH"
  | T_FRDOTBE -> "FRDOTBE"
  | T_FRDOTBH -> "FRDOTBH"
  | T_FRDOUI -> "FRDOUI"
  | T_FRDOUIH -> "FRDOUIH"
  | T_FRFWD -> "FRFWD"
  | T_FRFWDH -> "FRFWDH"
  | T_FRKIKO -> "FRKIKO"
  | T_FRKOKI -> "FRKOKI"
  | T_FRLDNTA -> "FRLDNTA"
  | T_FRLDNTB -> "FRLDNTB"
  | T_FRLDOTAE -> "FRLDOTAE"
  | T_FRLDOTAH -> "FRLDOTAH"
  | T_FRLDOTBE -> "FRLDOTBE"
  | T_FRLDOTBH -> "FRLDOTBH"
  | T_FROE -> "FROE"
  | T_FROEDH -> "FROEDH"
  | T_FROEH -> "FROEH"
  | T_FROEK -> "FROEK"
  | T_FROEM -> "FROEM"
  | T_FROEMDKI -> "FROEMDKI"
  | T_FROEMDKO -> "FROEMDKO"
  | T_FROEW -> "FROEW"
  | T_FROEWDKI -> "FROEWDKI"
  | T_FROEWDKO -> "FROEWDKO"
  | T_FRUODI -> "FRUODI"
  | T_FRUODIH -> "FRUODIH"
  | T_FVAA -> "FVAA"
  | T_FVAB -> "FVAB"
  | T_FWD -> "FWD"
  | T_FXCAP2A -> "FXCAP2A"
  | T_FXCAPA -> "FXCAPA"
  | T_FXSWAP -> "FXSWAP"
  | T_HITAE -> "HITAE"
  | T_HITBE -> "HITBE"
  | T_INSTALLOPT -> "INSTALLOPT"
  | T_IRFIXED -> "IRFIXED"
  | T_IRFLOAT -> "IRFLOAT"
  | T_IRSWAP -> "IRSWAP"
  | T_KIFWD -> "KIFWD"
  | T_KIKO -> "KIKO"
  | T_KOFWD -> "KOFWD"
  | T_KOKI -> "KOKI"
  | T_KOTAE -> "KOTAE"
  | T_KOTAH -> "KOTAH"
  | T_KOTBE -> "KOTBE"
  | T_KOTBH -> "KOTBH"
  | T_KOTDNTA -> "KOTDNTA"
  | T_KOTDNTB -> "KOTDNTB"
  | T_KOTFRDNTA -> "KOTFRDNTA"
  | T_KOTFRDNTB -> "KOTFRDNTB"
  | T_KOTKOTAE -> "KOTKOTAE"
  | T_KOTKOTBE -> "KOTKOTBE"
  | T_LDI -> "LDI"
  | T_LDKI -> "LDKI"
  | T_LDKIMH -> "LDKIMH"
  | T_LDKO -> "LDKO"
  | T_LDKOMH -> "LDKOMH"
  | T_LDNTA -> "LDNTA"
  | T_LDNTB -> "LDNTB"
  | T_LDO -> "LDO"
  | T_LDOTAE -> "LDOTAE"
  | T_LDOTAH -> "LDOTAH"
  | T_LDOTBE -> "LDOTBE"
  | T_LDOTBH -> "LDOTBH"
  | T_LDOUI -> "LDOUI"
  | T_LKIKO -> "LKIKO"
  | T_LKOKI -> "LKOKI"
  | T_LKOTAE -> "LKOTAE"
  | T_LKOTBE -> "LKOTBE"
  | T_LNMH -> "LNMH"
  | T_LNTA -> "LNTA"
  | T_LNTB -> "LNTB"
  | T_LOAS -> "LOAS"
  | T_LOAX -> "LOAX"
  | T_LOOKR -> "LOOKR"
  | T_LOOKS -> "LOOKS"
  | T_LOTAE -> "LOTAE"
  | T_LOTAH -> "LOTAH"
  | T_LOTBE -> "LOTBE"
  | T_LOTBH -> "LOTBH"
  | T_LUI -> "LUI"
  | T_LUO -> "LUO"
  | T_LUODI -> "LUODI"
  | T_MDKO -> "MDKO"
  | T_MDKO3 -> "MDKO3"
  | T_MDKO3FWD -> "MDKO3FWD"
  | T_MDKODH -> "MDKODH"
  | T_MDKODHFWDA -> "MDKODHFWDA"
  | T_MDKODHFWDB -> "MDKODHFWDB"
  | T_MDKOM -> "MDKOM"
  | T_MDKOMFWD -> "MDKOMFWD"
  | T_MDKOMH -> "MDKOMH"
  | T_MDNTA -> "MDNTA"
  | T_MDNTB -> "MDNTB"
  | T_MDNTDHA -> "MDNTDHA"
  | T_MDNTDHB -> "MDNTDHB"
  | T_MDNTM -> "MDNTM"
  | T_MDNTMH -> "MDNTMH"
  | T_MKOTAE -> "MKOTAE"
  | T_MKOTBE -> "MKOTBE"
  | T_MMDNTMHB -> "MMDNTMHB"
  | T_N_A -> "N_A"
  | T_NTA -> "NTA"
  | T_NTB -> "NTB"
  | T_OA -> "OA"
  | T_OAS -> "OAS"
  | T_OAX -> "OAX"
  | T_OE -> "OE"
  | T_OEF -> "OEF"
  | T_OEI -> "OEI"
  | T_OEK -> "OEK"
  | T_OEMH -> "OEMH"
  | T_OEN -> "OEN"
  | T_OPAYOFF s -> "OPAYOFF(" ^ s ^ ")"
  | T_OTAE -> "OTAE"
  | T_OTAH -> "OTAH"
  | T_OTBE -> "OTBE"
  | T_OTBH -> "OTBH"
  | T_PASSPORT -> "PASSPORT"
  | T_PASSPORTPROXY -> "PASSPORTPROXY"
  | T_PASSPORTPROXYI -> "PASSPORTPROXYI"
  | T_PDNT3A -> "PDNT3A"
  | T_PDNT3B -> "PDNT3B"
  | T_PDNTA -> "PDNTA"
  | T_PDNTB -> "PDNTB"
  | T_PREMIUMA -> "PREMIUMA"
  | T_PREMIUMB -> "PREMIUMB"
  | T_PRTFADER -> "PRTFADER"
  | T_RAA -> "RAA"
  | T_RAB -> "RAB"
  | T_RADDA -> "RADDA"
  | T_RADDB -> "RADDB"
  | T_RADKOA -> "RADKOA"
  | T_RADKOB -> "RADKOB"
  | T_RAMBOA -> "RAMBOA"
  | T_RAMBOB -> "RAMBOB"
  | T_RAMDKOA -> "RAMDKOA"
  | T_RAMDKOB -> "RAMDKOB"
  | T_RAVGDKO -> "RAVGDKO"
  | T_RAVGHDKO -> "RAVGHDKO"
  | T_RDNTA -> "RDNTA"
  | T_RDNTB -> "RDNTB"
  | T_RPRTFADER -> "RPRTFADER"
  | T_SA -> "SA"
  | T_SCCYFORMULA s -> "SCCYFORMULA(" ^ s ^ ")"
  | T_SHOUT -> "SHOUT"
  | T_SHOUTI -> "SHOUTI"
  | T_SPEAK -> "SPEAK"
  | T_SPOT -> "SPOT"
  | T_STEPBOX -> "STEPBOX"
  | T_STLF -> "STLF"
  | T_STRIP _ -> "STRIP"
  | T_TARNA -> "TARNA"
  | T_TARNB -> "TARNB"
  | T_TARNOE -> "TARNOE"
  | T_TARNOEK -> "TARNOEK"
  | T_TLF -> "TLF"
  | T_TLF2 -> "TLF2"
  | T_TRF -> "TRF"
  | T_TRFE -> "TRFE"
  | T_TRFH -> "TRFH"
  | T_UI -> "UI"
  | T_UO -> "UO"
  | T_UODI -> "UODI"
  | T_UODIDKO -> "UODIDKO"
  | T_UODIDOE -> "UODIDOE"
  | T_UODIDOUIKOTAE -> "UODIDOUIKOTAE"
  | T_UODIDOUIKOTBE -> "UODIDOUIKOTBE"
  | T_UODIKOTAE -> "UODIKOTAE"
  | T_UODIKOTBE -> "UODIKOTBE"
  | T_UODILDKO -> "UODILDKO"
  | T_UODILKI -> "UODILKI"
  | T_UODIUODIKOTAE -> "UODIUODIKOTAE"
  | T_UODIUODIKOTBE -> "UODIUODIKOTBE"
  | T_UODIUOE -> "UODIUOE"
  | T_UODO -> "UODO"
  | T_UOE -> "UOE"
  | T_UOEDI -> "UOEDI"
  | T_UOEDO -> "UOEDO"
  | T_UOEK -> "UOEK"
  | T_UOHD -> "UOHD"
  | T_UOHU -> "UOHU"
  | T_UOLDI -> "UOLDI"
  | T_UOLDO -> "UOLDO"
  | T_UOLOOKR -> "UOLOOKR"
  | T_UOLOOKS -> "UOLOOKS"
  | T_UOLOOKSK -> "UOLOOKSK"
  | T_VARSWAPA -> "VARSWAPA"
  | T_VARSWAPB -> "VARSWAPB"
  | T_VNTLF -> "VNTLF"
  | T_VOLSWAPA -> "VOLSWAPA"
  | T_VOLSWAPB -> "VOLSWAPB"
  | T_WTLF -> "WTLF"
  ];


value rec fx_option_type_of_string: string -> fx_option_type =
fun s->
  let r : string -> Pcre.regexp = 
    fun s -> Pcre.regexp ~{ flags=[`CASELESS] } (s ^ "[(](\w+)[)]")
  in
  let sccy : Pcre.regexp = r "SCCYFORMULA" in
  let opayoff : Pcre.regexp = r "OPAYOFF" in
  let strip : Pcre.regexp = r "STRIP" in
  let match_r = fun e s -> Pcre.pmatch ~{ rex=e } s in
  let get_type : Pcre.regexp -> string -> string = 
    fun e s -> String.uppercase (Pcre.get_substring (Pcre.exec ~{ rex=e } s) 1)
  in
    match String.lowercase s with
    [
      "abf" -> T_ABF
    | "abfko" -> T_ABFKO
    | "abfkombar" -> T_ABFKOMBAR
    | "abfmbar" -> T_ABFMBAR
    | "adbf" -> T_ADBF
    | "avgdko" -> T_AVGDKO
    | "avgdoui" -> T_AVGDOUI
    | "avghdko" -> T_AVGHDKO
    | "avghdoui" -> T_AVGHDOUI
    | "avghuodi" -> T_AVGHUODI
    | "avgkhldko" -> T_AVGKHLDKO
    | "avgkldko" -> T_AVGKLDKO
    | "avgrate" -> T_AVGRATE
    | "avgrateh" -> T_AVGRATEH
    | "avgratehi" -> T_AVGRATEHI
    | "avgratei" -> T_AVGRATEI
    | "avgratek" -> T_AVGRATEK
    | "avgstrike" -> T_AVGSTRIKE
    | "avgstrikeh" -> T_AVGSTRIKEH
    | "avguodi" -> T_AVGUODI
    | "box" -> T_BOX
    | "cash" -> T_CASH
    | "cdnta" -> T_CDNTA
    | "cdntb" -> T_CDNTB
    | "chooser" -> T_CHOOSER
    | "compound" -> T_COMPOUND
    | "craa" -> T_CRAA
    | "crab" -> T_CRAB
    | "csbtlf" -> T_CSBTLF
    | "cstlf" -> T_CSTLF
    | "daro" -> T_DARO
    | "daroh" -> T_DAROH
    | "darohi" -> T_DAROHI
    | "daroi" -> T_DAROI
    | "darok" -> T_DAROK
    | "darokh" -> T_DAROKH
    | "darokhi" -> T_DAROKHI
    | "daroki" -> T_DAROKI
    | "dbdoa" -> T_DBDOA
    | "dbdob" -> T_DBDOB
    | "dbedoa" -> T_DBEDOA
    | "dbedob" -> T_DBEDOB
    | "dbeuoa" -> T_DBEUOA
    | "dbeuob" -> T_DBEUOB
    | "dbldoa" -> T_DBLDOA
    | "dbldob" -> T_DBLDOB
    | "dbluoa" -> T_DBLUOA
    | "dbluob" -> T_DBLUOB
    | "dbuoa" -> T_DBUOA
    | "dbuob" -> T_DBUOB
    | "ddea" -> T_DDEA
    | "ddeb" -> T_DDEB
    | "dea" -> T_DEA
    | "deamh" -> T_DEAMH
    | "deb" -> T_DEB
    | "debmh" -> T_DEBMH
    | "dedkoa" -> T_DEDKOA
    | "dedkob" -> T_DEDKOB
    | "deedkoa" -> T_DEEDKOA
    | "deedkob" -> T_DEEDKOB
    | "deldkoa" -> T_DELDKOA
    | "deldkob" -> T_DELDKOB
    | "depoa" -> T_DEPOA
    | "depob" -> T_DEPOB
    | "dhfwd" -> T_DHFWD
    | "dhfwda" -> T_DHFWDA
    | "dhfwdb" -> T_DHFWDB
    | "dhitae" -> T_DHITAE
    | "dhitbe" -> T_DHITBE
    | "di" -> T_DI
    | "dki" -> T_DKI
    | "dko" -> T_DKO
    | "dkocraa" -> T_DKOCRAA
    | "dkocrab" -> T_DKOCRAB
    | "dkodh" -> T_DKODH
    | "dkodhfwd" -> T_DKODHFWD
    | "dkodie" -> T_DKODIE
    | "dkodoe" -> T_DKODOE
    | "dkoelookr" -> T_DKOELOOKR
    | "dkoelookrde" -> T_DKOELOOKRDE
    | "dkohd" -> T_DKOHD
    | "dkohu" -> T_DKOHU
    | "dkoi" -> T_DKOI
    | "dkok" -> T_DKOK
    | "dkolookr" -> T_DKOLOOKR
    | "dkolooks" -> T_DKOLOOKS
    | "dkolooksk" -> T_DKOLOOKSK
    | "dkouie" -> T_DKOUIE
    | "dkouoe" -> T_DKOUOE
    | "dnta" -> T_DNTA
    | "dntb" -> T_DNTB
    | "dntdha" -> T_DNTDHA
    | "dntdhb" -> T_DNTDHB
    | "dntdotae" -> T_DNTDOTAE
    | "dntdotbe" -> T_DNTDOTBE
    | "do" -> T_DO
    | "doe" -> T_DOE
    | "doek" -> T_DOEK
    | "doeui" -> T_DOEUI
    | "doeuo" -> T_DOEUO
    | "dohd" -> T_DOHD
    | "dohu" -> T_DOHU
    | "dolookr" -> T_DOLOOKR
    | "dolooks" -> T_DOLOOKS
    | "dolooksk" -> T_DOLOOKSK
    | "dolui" -> T_DOLUI
    | "doluo" -> T_DOLUO
    | "dotae" -> T_DOTAE
    | "dotah" -> T_DOTAH
    | "dotbe" -> T_DOTBE
    | "dotbh" -> T_DOTBH
    | "doui" -> T_DOUI
    | "douidko" -> T_DOUIDKO
    | "douidoe" -> T_DOUIDOE
    | "douidouikotae" -> T_DOUIDOUIKOTAE
    | "douidouikotbe" -> T_DOUIDOUIKOTBE
    | "douikotae" -> T_DOUIKOTAE
    | "douikotbe" -> T_DOUIKOTBE
    | "douildko" -> T_DOUILDKO
    | "douilki" -> T_DOUILKI
    | "douiuodikotae" -> T_DOUIUODIKOTAE
    | "douiuodikotbe" -> T_DOUIUODIKOTBE
    | "douiuoe" -> T_DOUIUOE
    | "douo" -> T_DOUO
    | "earlyfader" -> T_EARLYFADER
    | "earlyfaderdki" -> T_EARLYFADERDKI
    | "earlyfaderdoui" -> T_EARLYFADERDOUI
    | "earlyfaderuodi" -> T_EARLYFADERUODI
    | "earlyshout" -> T_EARLYSHOUT
    | "earlyspeak" -> T_EARLYSPEAK
    | "edi" -> T_EDI
    | "edki" -> T_EDKI
    | "edko" -> T_EDKO
    | "edkodh" -> T_EDKODH
    | "edo" -> T_EDO
    | "edoui" -> T_EDOUI
    | "edouidko" -> T_EDOUIDKO
    | "efaderdoe" -> T_EFADERDOE
    | "efaderdoek" -> T_EFADERDOEK
    | "efadermbar" -> T_EFADERMBAR
    | "efaderoen" -> T_EFADEROEN
    | "efaderuoe" -> T_EFADERUOE
    | "efaderuoek" -> T_EFADERUOEK
    | "ekiko" -> T_EKIKO
    | "ekoki" -> T_EKOKI
    | "emdntmha" -> T_EMDNTMHA
    | "emdntmhb" -> T_EMDNTMHB
    | "eora" -> T_EORA
    | "eorb" -> T_EORB
    | "erba" -> T_ERBA
    | "erbb" -> T_ERBB
    | "eui" -> T_EUI
    | "euo" -> T_EUO
    | "euodi" -> T_EUODI
    | "euodidko" -> T_EUODIDKO
    | "fader" -> T_FADER
    | "fadermbar" -> T_FADERMBAR
    | "favgratek" -> T_FAVGRATEK
    | "fixeda" -> T_FIXEDA
    | "fixedamh" -> T_FIXEDAMH
    | "fixedb" -> T_FIXEDB
    | "fixedbmh" -> T_FIXEDBMH
    | "floata" -> T_FLOATA
    | "floatb" -> T_FLOATB
    | "fodea" -> T_FODEA
    | "fodeb" -> T_FODEB
    | "fodko" -> T_FODKO
    | "fodnta" -> T_FODNTA
    | "fodntb" -> T_FODNTB
    | "foldko" -> T_FOLDKO
    | "fooe" -> T_FOOE
    | "frbdki" -> T_FRBDKI
    | "frbdko" -> T_FRBDKO
    | "frdea" -> T_FRDEA
    | "frdeb" -> T_FRDEB
    | "frdki" -> T_FRDKI
    | "frdko" -> T_FRDKO
    | "frdkodh" -> T_FRDKODH
    | "frdnta" -> T_FRDNTA
    | "frdntb" -> T_FRDNTB
    | "frdotae" -> T_FRDOTAE
    | "frdotah" -> T_FRDOTAH
    | "frdotbe" -> T_FRDOTBE
    | "frdotbh" -> T_FRDOTBH
    | "frdoui" -> T_FRDOUI
    | "frdouih" -> T_FRDOUIH
    | "frfwd" -> T_FRFWD
    | "frfwdh" -> T_FRFWDH
    | "frkiko" -> T_FRKIKO
    | "frkoki" -> T_FRKOKI
    | "frldnta" -> T_FRLDNTA
    | "frldntb" -> T_FRLDNTB
    | "frldotae" -> T_FRLDOTAE
    | "frldotah" -> T_FRLDOTAH
    | "frldotbe" -> T_FRLDOTBE
    | "frldotbh" -> T_FRLDOTBH
    | "froe" -> T_FROE
    | "froedh" -> T_FROEDH
    | "froeh" -> T_FROEH
    | "froek" -> T_FROEK
    | "froem" -> T_FROEM
    | "froemdki" -> T_FROEMDKI
    | "froemdko" -> T_FROEMDKO
    | "froew" -> T_FROEW
    | "froewdki" -> T_FROEWDKI
    | "froewdko" -> T_FROEWDKO
    | "fruodi" -> T_FRUODI
    | "fruodih" -> T_FRUODIH
    | "fvaa" -> T_FVAA
    | "fvab" -> T_FVAB
    | "fwd" -> T_FWD
    | "fxcap2a" -> T_FXCAP2A
    | "fxcapa" -> T_FXCAPA
    | "fxswap" -> T_FXSWAP
    | "hitae" -> T_HITAE
    | "hitbe" -> T_HITBE
    | "installopt" -> T_INSTALLOPT
    | "irfixed" -> T_IRFIXED
    | "irfloat" -> T_IRFLOAT
    | "irswap" -> T_IRSWAP
    | "kifwd" -> T_KIFWD
    | "kiko" -> T_KIKO
    | "kofwd" -> T_KOFWD
    | "koki" -> T_KOKI
    | "kotae" -> T_KOTAE
    | "kotah" -> T_KOTAH
    | "kotbe" -> T_KOTBE
    | "kotbh" -> T_KOTBH
    | "kotdnta" -> T_KOTDNTA
    | "kotdntb" -> T_KOTDNTB
    | "kotfrdnta" -> T_KOTFRDNTA
    | "kotfrdntb" -> T_KOTFRDNTB
    | "kotkotae" -> T_KOTKOTAE
    | "kotkotbe" -> T_KOTKOTBE
    | "ldi" -> T_LDI
    | "ldki" -> T_LDKI
    | "ldkimh" -> T_LDKIMH
    | "ldko" -> T_LDKO
    | "ldkomh" -> T_LDKOMH
    | "ldnta" -> T_LDNTA
    | "ldntb" -> T_LDNTB
    | "ldo" -> T_LDO
    | "ldotae" -> T_LDOTAE
    | "ldotah" -> T_LDOTAH
    | "ldotbe" -> T_LDOTBE
    | "ldotbh" -> T_LDOTBH
    | "ldoui" -> T_LDOUI
    | "lkiko" -> T_LKIKO
    | "lkoki" -> T_LKOKI
    | "lkotae" -> T_LKOTAE
    | "lkotbe" -> T_LKOTBE
    | "lnmh" -> T_LNMH
    | "lnta" -> T_LNTA
    | "lntb" -> T_LNTB
    | "loas" -> T_LOAS
    | "loax" -> T_LOAX
    | "lookr" -> T_LOOKR
    | "looks" -> T_LOOKS
    | "lotae" -> T_LOTAE
    | "lotah" -> T_LOTAH
    | "lotbe" -> T_LOTBE
    | "lotbh" -> T_LOTBH
    | "lui" -> T_LUI
    | "luo" -> T_LUO
    | "luodi" -> T_LUODI
    | "mdko" -> T_MDKO
    | "mdko3" -> T_MDKO3
    | "mdko3fwd" -> T_MDKO3FWD
    | "mdkodh" -> T_MDKODH
    | "mdkodhfwda" -> T_MDKODHFWDA
    | "mdkodhfwdb" -> T_MDKODHFWDB
    | "mdkom" -> T_MDKOM
    | "mdkomfwd" -> T_MDKOMFWD
    | "mdkomh" -> T_MDKOMH
    | "mdnta" -> T_MDNTA
    | "mdntb" -> T_MDNTB
    | "mdntdha" -> T_MDNTDHA
    | "mdntdhb" -> T_MDNTDHB
    | "mdntm" -> T_MDNTM
    | "mdntmh" -> T_MDNTMH
    | "mkotae" -> T_MKOTAE
    | "mkotbe" -> T_MKOTBE
    | "mmdntmhb" -> T_MMDNTMHB
    | "n_a" -> T_N_A
    | "nta" -> T_NTA
    | "ntb" -> T_NTB
    | "oa" -> T_OA
    | "oas" -> T_OAS
    | "oax" -> T_OAX
    | "oe" -> T_OE
    | "oef" -> T_OEF
    | "oei" -> T_OEI
    | "oek" -> T_OEK
    | "oemh" -> T_OEMH
    | "oen" -> T_OEN
    | "otae" -> T_OTAE
    | "otah" -> T_OTAH
    | "otbe" -> T_OTBE
    | "otbh" -> T_OTBH
    | "passport" -> T_PASSPORT
    | "passportproxy" -> T_PASSPORTPROXY
    | "passportproxyi" -> T_PASSPORTPROXYI
    | "pdnt3a" -> T_PDNT3A
    | "pdnt3b" -> T_PDNT3B
    | "pdnta" -> T_PDNTA
    | "pdntb" -> T_PDNTB
    | "premiuma" -> T_PREMIUMA
    | "premiumb" -> T_PREMIUMB
    | "prtfader" -> T_PRTFADER
    | "raa" -> T_RAA
    | "rab" -> T_RAB
    | "radda" -> T_RADDA
    | "raddb" -> T_RADDB
    | "radkoa" -> T_RADKOA
    | "radkob" -> T_RADKOB
    | "ramboa" -> T_RAMBOA
    | "rambob" -> T_RAMBOB
    | "ramdkoa" -> T_RAMDKOA
    | "ramdkob" -> T_RAMDKOB
    | "ravgdko" -> T_RAVGDKO
    | "ravghdko" -> T_RAVGHDKO
    | "rdnta" -> T_RDNTA
    | "rdntb" -> T_RDNTB
    | "rprtfader" -> T_RPRTFADER
    | "sa" -> T_SA
    | "shout" -> T_SHOUT
    | "shouti" -> T_SHOUTI
    | "speak" -> T_SPEAK
    | "spot" -> T_SPOT
    | "stepbox" -> T_STEPBOX
    | "stlf" -> T_STLF
    | "tarna" -> T_TARNA
    | "tarnb" -> T_TARNB
    | "tarnoe" -> T_TARNOE
    | "tarnoek" -> T_TARNOEK
    | "tlf" -> T_TLF
    | "tlf2" -> T_TLF2
    | "trf" -> T_TRF
    | "trfe" -> T_TRFE
    | "trfh" -> T_TRFH
    | "ui" -> T_UI
    | "uo" -> T_UO
    | "uodi" -> T_UODI
    | "uodidko" -> T_UODIDKO
    | "uodidoe" -> T_UODIDOE
    | "uodidouikotae" -> T_UODIDOUIKOTAE
    | "uodidouikotbe" -> T_UODIDOUIKOTBE
    | "uodikotae" -> T_UODIKOTAE
    | "uodikotbe" -> T_UODIKOTBE
    | "uodildko" -> T_UODILDKO
    | "uodilki" -> T_UODILKI
    | "uodiuodikotae" -> T_UODIUODIKOTAE
    | "uodiuodikotbe" -> T_UODIUODIKOTBE
    | "uodiuoe" -> T_UODIUOE
    | "uodo" -> T_UODO
    | "uoe" -> T_UOE
    | "uoedi" -> T_UOEDI
    | "uoedo" -> T_UOEDO
    | "uoek" -> T_UOEK
    | "uohd" -> T_UOHD
    | "uohu" -> T_UOHU
    | "uoldi" -> T_UOLDI
    | "uoldo" -> T_UOLDO
    | "uolookr" -> T_UOLOOKR
    | "uolooks" -> T_UOLOOKS
    | "uolooksk" -> T_UOLOOKSK
    | "varswapa" -> T_VARSWAPA
    | "varswapb" -> T_VARSWAPB
    | "vntlf" -> T_VNTLF
    | "volswapa" -> T_VOLSWAPA
    | "volswapb" -> T_VOLSWAPB
    | "wtlf" -> T_WTLF
    | s when (match_r sccy s)    -> T_SCCYFORMULA (get_type sccy s)
    | s when (match_r opayoff s) -> T_OPAYOFF (get_type opayoff s)
    | s when (match_r strip s)   -> T_STRIP (fx_option_type_of_string (get_type strip s))
    | e -> failwith (Printf.sprintf "fx_option_type_of_string: %s is not a recognised option type" e)
    ];


value parse_fx_option_type: Stream.t char -> fx_option_type =
fun s ->
  let string_of_stream: Stream.t char -> string = 
    fun s ->
      let buffer = Buffer.create 10 in
      do
      {
        Stream.iter (Buffer.add_char buffer) s;
        Buffer.contents buffer
      }
  in
    fx_option_type_of_string (string_of_stream s);

