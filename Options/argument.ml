
type collection_item = 
[
  BidAskCollection of string
| FxVolSurfaces of string
| HestonParameters of string
| HestonDupireParameters of string
| HestonPlusParameters of string
| OneWayInterpTable of string
| ReltaVeltaTable of string
| SlvModelParameters of string
| TwoWayInterpTable of string
]
with string_of, parse;


value string_of_stream: Stream.t char -> string = 
fun s ->
  let buffer = Buffer.create 16 in
  do
  {
    Stream.iter (Buffer.add_char buffer) s;
    Buffer.contents buffer
  };


type positive_float = float with string_of;

value parse_float: Stream.t char -> float =
fun s ->
  float_of_string (string_of_stream s);


value parse_positive_float: Stream.t char -> positive_float = parse_float;


type collection_item_or_num =
[
  Coll of collection_item
| Num of float
]
with string_of, parse;


type library_date =
[
  DateString of string
| DateValue of float
]
with string_of, parse;


type min_max =
[
  MinMaxSpot of positive_float
| MinMaxTable of collection_item
]
with string_of, parse;


type call_put = 
[
  Call
| Put
]
with string_of, parse;


value parse_bool: Stream.t char -> bool =
fun s ->
  bool_of_string (string_of_stream s);


type argument_type =
[
  T_Bar1
| T_Bar1ToT1
| T_Bar1ToT2
| T_Bar2
| T_Bar2ToT1
| T_Bar2ToT2
| T_BarrierDate
| T_BarrierDateToT2
| T_BarrierTable
| T_BaseDepo
| T_BaseDepoToT1
| T_BaseDepoToT2
| T_BucketedRhoMaturity
| T_CalibDate
| T_CompCP
| T_CompStrike
| T_CompStrikeIsAsset
| T_COQS_SVegaB
| T_Corr
| T_CP
| T_Cut
| T_DealtDepo
| T_DealtDepoToT1
| T_DealtDepoToT2
| T_DeliveryDate
| T_DeliveryDateToT1
| T_DVegaHedgeDelta
| T_DVegaPrefs
| T_DVegaScaleFactor
| T_ExpiryDate
| T_Fixings
| T_FwdDeltaBucketed
| T_Gearing
| T_HestonDupirePoint
| T_Heston
| T_HestonPlusPoint
| T_HurdleDown
| T_HurdleDownToT1
| T_HurdleUp
| T_HurdleUpToT1
| T_IRVol
| T_LookbackInc1
| T_LookbackInc2
| T_LookbackStd
| T_MarkovChain
| T_MasterNotional
| T_MCPaths
| T_MersenneSeed
| T_MinBarDays
| T_MinMax
| T_NoBarDays
| T_Notional
| T_NotionalB
| T_NumMethodPrefs
| T_PassportNotionalStep
| T_PayOutDate
| T_QuantoCcy
| T_QuantoDepo
| T_Relta
| T_ReltaVelta
| T_RhoIncFactor
| T_SetBeforeCutOffTime
| T_SlvModel
| T_SLVPDENbX
| T_SLVPDENbY
| T_SmileAtmVegaBucketed
| T_SmileDeltaMethod
| T_SmileFwdDeltaBucketed
| T_SmilePriceSurface
| T_SmilePricingMethod
| T_SmileRega10DeltaBucketed
| T_SmileRega25DeltaBucketed
| T_SmileSega10DeltaBucketed
| T_SmileSega25DeltaBucketed
| T_SmileVegaBucketed
| T_SpotIncFactor
| T_SpotShockMode
| T_StartDate
| T_StdDelDate
| T_Strike
| T_TradeDate
| T_Type
| T_UseArbitraryScale
| T_UseSobol
| T_Velta
| T_Vol
| T_VolIncFactor
| T_VolToT1
| T_VolToT2
| T_STRIP_Param of argument_type
  (* Stuff for the new PDE: *)
| T_DiffusionType
| T_BIMethod
| T_UseUniformAxes
| T_SpotN
| T_VolM
| T_VolUp
| T_TimeStepDays
| T_ThreadsNo
]
with string_of, parse;


type argument_category =
[
  C_Market
| C_Optional
| C_Required
];


type required_argument =
[
  CP of call_put
| Strike of float
| BarrierDateToT2 of library_date
| BarrierDate of library_date
| Gearing of float
| CompStrikeIsAsset of bool
| Corr of float
| HurdleUpToT1 of positive_float
| HurdleUp of positive_float
| Fixings of collection_item
| MinBarDays of float
| CompCP of call_put
| TradeDate of library_date
| ExpiryDate of library_date
| MinMax of min_max
| Bar1ToT1 of float
| Bar1ToT2 of positive_float
| Bar2ToT1 of positive_float
| HurdleDownToT1 of positive_float
| Bar2ToT2 of float
| Bar1 of float
| Bar2 of positive_float
| PassportNotionalStep of float
| HurdleDown of positive_float
| PayOutDate of library_date
| CompStrike of float
| NoBarDays of positive_float
| BarrierTable of collection_item
| IRVol of positive_float
| StripParam of strip_param
| Type of Options_type.fx_option_type
] 
with string_of, parse
and strip_param =
[
  StdArg of required_argument
| FormulaArg of ( argument_type * string )
]
with string_of, parse;


value rec type_of_required_argument: required_argument -> argument_type =
fun a ->
  match a with
  [
    CP _ -> T_CP
  | Strike _ -> T_Strike
  | BarrierDateToT2 _ -> T_BarrierDateToT2
  | BarrierDate _ -> T_BarrierDate
  | Gearing _ -> T_Gearing
  | CompStrikeIsAsset _ -> T_CompStrikeIsAsset
  | Corr _ -> T_Corr
  | ExpiryDate _ -> T_ExpiryDate
  | HurdleUpToT1 _ -> T_HurdleUpToT1
  | HurdleUp _ -> T_HurdleUp
  | Fixings _ -> T_Fixings
  | MinBarDays _ -> T_MinBarDays
  | CompCP _ -> T_CompCP
  | TradeDate _ -> T_TradeDate
  | MinMax _ -> T_MinMax
  | Bar1ToT1 _ -> T_Bar1ToT1
  | Bar1ToT2 _ -> T_Bar1ToT2
  | Bar2ToT1 _ -> T_Bar2ToT1
  | HurdleDownToT1 _ -> T_HurdleDownToT1
  | Bar2ToT2 _ -> T_Bar2ToT2
  | Bar1 _ -> T_Bar1
  | Bar2 _ -> T_Bar2
  | PassportNotionalStep _ -> T_PassportNotionalStep
  | HurdleDown _ -> T_HurdleDown
  | PayOutDate _ -> T_PayOutDate
  | CompStrike _ -> T_CompStrike
  | NoBarDays _ -> T_NoBarDays
  | BarrierTable _ -> T_BarrierTable
  | IRVol _ -> T_IRVol
  | StripParam s -> match s with
                    [
                      StdArg sa -> type_of_required_argument sa
                    | FormulaArg ( t , _ ) -> t
                    ]
  | Type _t -> T_Type
  ];  


type optional_argument =
[
  BucketedRhoMaturity of string
| CalibDate of library_date
| COQS_SVegaB of float
| Cut of string
| DeliveryDate of library_date
| DeliveryDateToT1 of float
| DVegaHedgeDelta of positive_float
| DVegaPrefs of string
| DVegaScaleFactor of positive_float
| FwdDeltaBucketed of collection_item
| HestonDupirePoint of collection_item
| Heston of collection_item
| HestonPlusPoint of collection_item
| LookbackInc1 of positive_float
| LookbackInc2 of positive_float
| LookbackStd of positive_float
| MarkovChain of bool
| MasterNotional of float
| MCPaths of positive_float
| MersenneSeed of positive_float
| Notional of float
| NotionalB of float
| NumMethodPrefs of string
| QuantoCcy of string
| Relta of float
| ReltaVelta of collection_item
| RhoIncFactor of positive_float
| SetBeforeCutOffTime of bool
| SlvModel of collection_item
| SLVPDENbX of positive_float
| SLVPDENbY of positive_float
| SmileAtmVegaBucketed of collection_item
| SmileDeltaMethod of string
| SmileFwdDeltaBucketed of collection_item
| SmilePricingMethod of string
| SmileRega10DeltaBucketed of collection_item
| SmileRega25DeltaBucketed of collection_item
| SmileSega10DeltaBucketed of collection_item
| SmileSega25DeltaBucketed of collection_item
| SmileVegaBucketed of collection_item
| SpotIncFactor of positive_float
| SpotShockMode of bool
| StartDate of float
| StdDelDate of float
| UseArbitraryScale of bool
| UseSobol of bool
| Velta of float
| VolIncFactor of positive_float
  (* The following block of parameters is to support the new PDE Engine:     *)
| DiffusionType  of string         (* Diffusion Type                         *)
| BIMethod       of string         (* Backward Induction Method to be used   *)
| UseUniformAxes of bool           (* Make a uniform grid?                   *)
| SpotN          of positive_float (* Number of intervals in the Spot    dim *)
| VolM           of positive_float (* Number of intervals in the StocVol dim *)
| VolUp          of positive_float (* Upper boundary for the StocVol         *)
| TimeStepDays   of positive_float (* Step for time integration, in days     *)
| ThreadsNo      of positive_float (* Number of Threads for the BI Engine    *)
]
with string_of, parse;


value type_of_optional_argument: optional_argument -> argument_type =
fun a ->
  match a with
  [
    BucketedRhoMaturity _ -> T_BucketedRhoMaturity
  | CalibDate _ -> T_CalibDate
  | COQS_SVegaB _ -> T_COQS_SVegaB
  | Cut _ -> T_Cut
  | DeliveryDate _ -> T_DeliveryDate
  | DeliveryDateToT1 _ -> T_DeliveryDateToT1
  | DVegaHedgeDelta _ -> T_DVegaHedgeDelta
  | DVegaPrefs _ -> T_DVegaPrefs
  | DVegaScaleFactor _ -> T_DVegaScaleFactor
  | FwdDeltaBucketed _ -> T_FwdDeltaBucketed
  | HestonDupirePoint _ -> T_HestonDupirePoint
  | Heston _ -> T_Heston
  | HestonPlusPoint _ -> T_HestonPlusPoint
  | LookbackInc1 _ -> T_LookbackInc1
  | LookbackInc2 _ -> T_LookbackInc2
  | LookbackStd _ -> T_LookbackStd
  | MarkovChain _ -> T_MarkovChain
  | MasterNotional _ -> T_MasterNotional
  | MCPaths _ -> T_MCPaths
  | MersenneSeed _ -> T_MersenneSeed
  | Notional _ -> T_Notional
  | NotionalB _ -> T_NotionalB
  | NumMethodPrefs _ -> T_NumMethodPrefs
  | QuantoCcy _ -> T_QuantoCcy
  | Relta _ -> T_Relta
  | ReltaVelta _ -> T_ReltaVelta
  | RhoIncFactor _ -> T_RhoIncFactor
  | SetBeforeCutOffTime _ -> T_SetBeforeCutOffTime
  | SlvModel _ -> T_SlvModel
  | SLVPDENbX _ -> T_SLVPDENbX
  | SLVPDENbY _ -> T_SLVPDENbY
  | SmileAtmVegaBucketed _ -> T_SmileAtmVegaBucketed
  | SmileDeltaMethod _ -> T_SmileDeltaMethod
  | SmileFwdDeltaBucketed _ -> T_SmileFwdDeltaBucketed
  | SmilePricingMethod _ -> T_SmilePricingMethod
  | SmileRega10DeltaBucketed _ -> T_SmileRega10DeltaBucketed
  | SmileRega25DeltaBucketed _ -> T_SmileRega25DeltaBucketed
  | SmileSega10DeltaBucketed _ -> T_SmileSega10DeltaBucketed
  | SmileSega25DeltaBucketed _ -> T_SmileSega25DeltaBucketed
  | SmileVegaBucketed _ -> T_SmileVegaBucketed
  | SpotIncFactor _ -> T_SpotIncFactor
  | SpotShockMode _ -> T_SpotShockMode
  | StartDate _ -> T_StartDate
  | StdDelDate _ -> T_StdDelDate
  | UseArbitraryScale _ -> T_UseArbitraryScale
  | UseSobol _ -> T_UseSobol
  | Velta _ -> T_Velta
  | VolIncFactor  _ -> T_VolIncFactor
    (* New PDE stuff: *)
  | DiffusionType  _ -> T_DiffusionType
  | BIMethod       _ -> T_BIMethod
  | UseUniformAxes _ -> T_UseUniformAxes
  | SpotN          _ -> T_SpotN
  | VolM           _ -> T_VolM
  | VolUp         _ -> T_VolUp
  | TimeStepDays  _ -> T_TimeStepDays
  | ThreadsNo     _ -> T_ThreadsNo
  ];


type market_argument =
[
  BaseDepo of collection_item_or_num
| BaseDepoToT1 of float
| BaseDepoToT2 of float
| DealtDepo of collection_item_or_num
| DealtDepoToT1 of float
| DealtDepoToT2 of float
| QuantoDepo of collection_item
| SmilePriceSurface of collection_item
| Vol of collection_item_or_num
| VolToT1 of positive_float
| VolToT2 of positive_float
]
with string_of, parse;


value type_of_market_argument: market_argument -> argument_type = 
fun a ->
  match a with
  [
    BaseDepo _ -> T_BaseDepo
  | BaseDepoToT1 _ -> T_BaseDepoToT1
  | BaseDepoToT2 _ -> T_BaseDepoToT2
  | DealtDepo _ -> T_DealtDepo
  | DealtDepoToT1 _ -> T_DealtDepoToT1
  | DealtDepoToT2 _ -> T_DealtDepoToT2
  | QuantoDepo _ -> T_QuantoDepo
  | SmilePriceSurface _ -> T_SmilePriceSurface
  | Vol _ -> T_Vol
  | VolToT1 _ -> T_VolToT1
  | VolToT2 _ -> T_VolToT2
  ];


type argument =
[
  Required of required_argument
| Optional of optional_argument
| Market of market_argument
]
with string_of, parse;


value type_of_argument: argument -> argument_type =
fun a ->
  match a with
  [
    Required r -> type_of_required_argument r
  | Optional o -> type_of_optional_argument o
  | Market m   -> type_of_market_argument m
  ];

value rec argument_category_of_type: argument_type -> argument_category =
fun t ->
      match t with
      [
        (* Required args: *)
        T_Bar1
      | T_Bar1ToT1
      | T_Bar1ToT2
      | T_Bar2
      | T_Bar2ToT1
      | T_Bar2ToT2
      | T_BarrierDate
      | T_BarrierDateToT2
      | T_BarrierTable
      | T_CompCP
      | T_CompStrike
      | T_CompStrikeIsAsset
      | T_Corr
      | T_CP
      | T_ExpiryDate
      | T_Fixings
      | T_Gearing
      | T_HurdleDown
      | T_HurdleDownToT1
      | T_HurdleUp
      | T_HurdleUpToT1
      | T_IRVol
      | T_MinBarDays
      | T_MinMax
      | T_NoBarDays
      | T_PassportNotionalStep
      | T_PayOutDate
      | T_Strike
      | T_TradeDate
      | T_STRIP_Param _ 
      | T_Type -> C_Required

        (* Optional args: *)
      | T_BucketedRhoMaturity
      | T_CalibDate
      | T_COQS_SVegaB
      | T_Cut
      | T_DeliveryDate
      | T_DeliveryDateToT1
      | T_DVegaHedgeDelta
      | T_DVegaPrefs
      | T_DVegaScaleFactor
      | T_FwdDeltaBucketed
      | T_Heston
      | T_HestonDupirePoint
      | T_HestonPlusPoint
      | T_LookbackInc1
      | T_LookbackInc2
      | T_LookbackStd
      | T_MarkovChain
      | T_MasterNotional
      | T_MCPaths
      | T_MersenneSeed
      | T_Notional
      | T_NotionalB
      | T_NumMethodPrefs
      | T_QuantoCcy
      | T_Relta
      | T_ReltaVelta
      | T_RhoIncFactor
      | T_SetBeforeCutOffTime
      | T_SlvModel
      | T_SLVPDENbX
      | T_SLVPDENbY
      | T_SmileAtmVegaBucketed
      | T_SmileDeltaMethod
      | T_SmileFwdDeltaBucketed
      | T_SmilePricingMethod
      | T_SmileRega10DeltaBucketed
      | T_SmileRega25DeltaBucketed
      | T_SmileSega10DeltaBucketed
      | T_SmileSega25DeltaBucketed
      | T_SmileVegaBucketed
      | T_SpotIncFactor
      | T_SpotShockMode
      | T_StartDate
      | T_StdDelDate
      | T_UseArbitraryScale
      | T_UseSobol
      | T_Velta
      | T_VolIncFactor
        (* New PDE stuff: *)
      | T_DiffusionType
      | T_BIMethod
      | T_UseUniformAxes
      | T_SpotN
      | T_VolM
      | T_VolUp
      | T_TimeStepDays
      | T_ThreadsNo     -> C_Optional

        (* Market args: *)
      | T_BaseDepo
      | T_BaseDepoToT1
      | T_BaseDepoToT2
      | T_DealtDepo
      | T_DealtDepoToT1
      | T_DealtDepoToT2
      | T_QuantoDepo
      | T_SmilePriceSurface
      | T_Vol
      | T_VolToT1
      | T_VolToT2 -> C_Market
      ];

exception UnrecognisedArgumentName of string;

value argument_type_of_string: ?otype:Options_type.fx_option_type -> string -> argument_type =
fun ?{ otype = Options_type.T_N_A }  s ->
  let atype = match String.lowercase s with
  [
    "bar1" -> T_Bar1
  | "bar1tot1" -> T_Bar1ToT1
  | "bar1tot2" -> T_Bar1ToT2
  | "bar2" -> T_Bar2
  | "bar2tot1" -> T_Bar2ToT1
  | "bar2tot2" -> T_Bar2ToT2
  | "barrierdate" -> T_BarrierDate
  | "barrierdatetot2" -> T_BarrierDateToT2
  | "barriertable" -> T_BarrierTable
  | "basedepo" -> T_BaseDepo
  | "basedepotot1" -> T_BaseDepoToT1
  | "basedepotot2" -> T_BaseDepoToT2
  | "bucketedrhomaturity" -> T_BucketedRhoMaturity
  | "calibdate" -> T_CalibDate
  | "compcp" -> T_CompCP
  | "compstrike" -> T_CompStrike
  | "compstrikeisasset" -> T_CompStrikeIsAsset
  | "coqs_svegab" -> T_COQS_SVegaB
  | "corr" -> T_Corr
  | "cp" -> T_CP
  | "cut" -> T_Cut
  | "dealtdepo" -> T_DealtDepo
  | "dealtdepotot1" -> T_DealtDepoToT1
  | "dealtdepotot2" -> T_DealtDepoToT2
  | "deliverydate" -> T_DeliveryDate
  | "deliverydatetot1" -> T_DeliveryDateToT1
  | "dvegahedgedelta" -> T_DVegaHedgeDelta
  | "dvegaprefs" -> T_DVegaPrefs
  | "dvegascalefactor" -> T_DVegaScaleFactor
  | "expirydate" -> T_ExpiryDate
  | "fixings" -> T_Fixings
  | "fwddeltabucketed" -> T_FwdDeltaBucketed
  | "gearing" -> T_Gearing
  | "hestondupirepoint" -> T_HestonDupirePoint
  | "hestonparameters" -> T_Heston
  | "hestonpluspoint" -> T_HestonPlusPoint
  | "hurdledown" -> T_HurdleDown
  | "hurdledowntot1" -> T_HurdleDownToT1
  | "hurdleup" -> T_HurdleUp
  | "hurdleuptot1" -> T_HurdleUpToT1
  | "irvol" -> T_IRVol
  | "lookbackinc1" -> T_LookbackInc1
  | "lookbackinc2" -> T_LookbackInc2
  | "lookbackstd" -> T_LookbackStd
  | "markovchain" -> T_MarkovChain
  | "masternotional" -> T_MasterNotional
  | "mcpaths" -> T_MCPaths
  | "mersenneseed" -> T_MersenneSeed
  | "minbardays" -> T_MinBarDays
  | "minmax" -> T_MinMax
  | "nobardays" -> T_NoBarDays
  | "notional" -> T_Notional
  | "notionalb" -> T_NotionalB
  | "nummethodprefs" -> T_NumMethodPrefs
  | "passportnotionalstep" -> T_PassportNotionalStep
  | "payoutdate" -> T_PayOutDate
  | "quantoccy" -> T_QuantoCcy
  | "quantodepo" -> T_QuantoDepo
  | "relta" -> T_Relta
  | "reltaveltatable" -> T_ReltaVelta
  | "rhoincfactor" -> T_RhoIncFactor
  | "setbeforecutofftime" -> T_SetBeforeCutOffTime
  | "slvmodel" -> T_SlvModel
  | "slvpdenbx" -> T_SLVPDENbX
  | "slvpdenby" -> T_SLVPDENbY
  | "smileatmvegabucketed" -> T_SmileAtmVegaBucketed
  | "smiledeltamethod" -> T_SmileDeltaMethod
  | "smilefwddeltabucketed" -> T_SmileFwdDeltaBucketed
  | "smilepricesurface" -> T_SmilePriceSurface
  | "smilepricingmethod" -> T_SmilePricingMethod
  | "smilerega10deltabucketed" -> T_SmileRega10DeltaBucketed
  | "smilerega25deltabucketed" -> T_SmileRega25DeltaBucketed
  | "smilesega10deltabucketed" -> T_SmileSega10DeltaBucketed
  | "smilesega25deltabucketed" -> T_SmileSega25DeltaBucketed
  | "smilevegabucketed" -> T_SmileVegaBucketed
  | "spotincfactor" -> T_SpotIncFactor
  | "spotshockmode" -> T_SpotShockMode
  | "startdate" -> T_StartDate
  | "stddeldate" -> T_StdDelDate
  | "strike" -> T_Strike
  | "tradedate" -> T_TradeDate
  | "type" -> T_Type
  | "usearbitraryscale" -> T_UseArbitraryScale
  | "usesobol" -> T_UseSobol
  | "velta" -> T_Velta
  | "vol" -> T_Vol
  | "volincfactor" -> T_VolIncFactor
  | "voltot1" -> T_VolToT1
  | "voltot2" -> T_VolToT2
    (* New PDE stuff: *)
  | "diffusiontype"  -> T_DiffusionType
  | "bimethod"       -> T_BIMethod
  | "useuniformaxes" -> T_UseUniformAxes
  | "spotn"          -> T_SpotN
  | "volm"           -> T_VolM
  | "volup"          -> T_VolUp
  | "timestepdays"   -> T_TimeStepDays
  | "threadsno"      -> T_ThreadsNo
  | _ -> raise (UnrecognisedArgumentName s)
  ]
  in
    match otype with
    [
      Options_type.T_STRIP _ when (argument_category_of_type atype) = C_Required -> T_STRIP_Param atype
    | _        -> atype
    ];


value rec string_of_argument_type: argument_type -> string =
fun t ->
  match t with
  [
    T_Bar1 -> "Bar1"
  | T_Bar1ToT1 -> "Bar1ToT1"
  | T_Bar1ToT2 -> "Bar1ToT2"
  | T_Bar2 -> "Bar2"
  | T_Bar2ToT1 -> "Bar2ToT1"
  | T_Bar2ToT2 -> "Bar2ToT2"
  | T_BarrierDate -> "BarrierDate"
  | T_BarrierDateToT2 -> "BarrierDateToT2"
  | T_BarrierTable -> "BarrierTable"
  | T_BaseDepo -> "BaseDepo"
  | T_BaseDepoToT1 -> "BaseDepoToT1"
  | T_BaseDepoToT2 -> "BaseDepoToT2"
  | T_BucketedRhoMaturity -> "BucketedRhoMaturity"
  | T_CalibDate -> "CalibDate"
  | T_CompCP -> "CompCP"
  | T_CompStrike -> "CompStrike"
  | T_CompStrikeIsAsset -> "CompStrikeIsAsset"
  | T_COQS_SVegaB -> "COQS_SVegaB"
  | T_Corr -> "Corr"
  | T_CP -> "CP"
  | T_Cut -> "Cut"
  | T_DealtDepo -> "DealtDepo"
  | T_DealtDepoToT1 -> "DealtDepoToT1"
  | T_DealtDepoToT2 -> "DealtDepoToT2"
  | T_DeliveryDate -> "DeliveryDate"
  | T_DeliveryDateToT1 -> "DeliveryDateToT1"
  | T_DVegaHedgeDelta -> "DVegaHedgeDelta"
  | T_DVegaPrefs -> "DVegaPrefs"
  | T_DVegaScaleFactor -> "DVegaScaleFactor"
  | T_ExpiryDate -> "ExpiryDate"
  | T_Fixings -> "Fixings"
  | T_FwdDeltaBucketed -> "FwdDeltaBucketed"
  | T_Gearing -> "Gearing"
  | T_HestonDupirePoint -> "HestonDupirePoint"
  | T_Heston -> "HestonParameters"
  | T_HestonPlusPoint -> "HestonPlusPoint"
  | T_HurdleDown -> "HurdleDown"
  | T_HurdleDownToT1 -> "HurdleDownToT1"
  | T_HurdleUp -> "HurdleUp"
  | T_HurdleUpToT1 -> "HurdleUpToT1"
  | T_IRVol -> "IRVol"
  | T_LookbackInc1 -> "LookbackInc1"
  | T_LookbackInc2 -> "LookbackInc2"
  | T_LookbackStd -> "LookbackStd"
  | T_MarkovChain -> "MarkovChain"
  | T_MasterNotional -> "MasterNotional"
  | T_MCPaths -> "MCPaths"
  | T_MersenneSeed -> "MersenneSeed"
  | T_MinBarDays -> "MinBarDays"
  | T_MinMax -> "MinMax"
  | T_NoBarDays -> "NoBarDays"
  | T_Notional -> "Notional"
  | T_NotionalB -> "NotionalB"
  | T_NumMethodPrefs -> "NumMethodPrefs"
  | T_PassportNotionalStep -> "PassportNotionalStep"
  | T_PayOutDate -> "PayOutDate"
  | T_QuantoCcy -> "QuantoCcy"
  | T_QuantoDepo -> "QuantoDepo"
  | T_Relta -> "Relta"
  | T_ReltaVelta -> "ReltaVeltaTable"
  | T_RhoIncFactor -> "RhoIncFactor"
  | T_SetBeforeCutOffTime -> "SetBeforeCutOffTime"
  | T_SlvModel -> "SlvModel"
  | T_SLVPDENbX -> "SLVPDENbX"
  | T_SLVPDENbY -> "SLVPDENbY"
  | T_SmileAtmVegaBucketed -> "SmileAtmVegaBucketed"
  | T_SmileDeltaMethod -> "SmileDeltaMethod"
  | T_SmileFwdDeltaBucketed -> "SmileFwdDeltaBucketed"
  | T_SmilePriceSurface -> "SmilePriceSurface"
  | T_SmilePricingMethod -> "SmilePricingMethod"
  | T_SmileRega10DeltaBucketed -> "SmileRega10DeltaBucketed"
  | T_SmileRega25DeltaBucketed -> "SmileRega25DeltaBucketed"
  | T_SmileSega10DeltaBucketed -> "SmileSega10DeltaBucketed"
  | T_SmileSega25DeltaBucketed -> "SmileSega25DeltaBucketed"
  | T_SmileVegaBucketed -> "SmileVegaBucketed"
  | T_SpotIncFactor -> "SpotIncFactor"
  | T_SpotShockMode -> "SpotShockMode"
  | T_StartDate -> "StartDate"
  | T_StdDelDate -> "StdDelDate"
  | T_Strike -> "Strike"
  | T_TradeDate -> "TradeDate"
  | T_Type -> "Type"
  | T_UseArbitraryScale -> "UseArbitraryScale"
  | T_UseSobol -> "UseSobol"
  | T_Velta -> "Velta"
  | T_Vol -> "Vol"
  | T_VolIncFactor -> "VolIncFactor"
  | T_VolToT1 -> "VolToT1"
  | T_VolToT2 -> "VolToT2"
    (* New PDE stuff: *)
  | T_DiffusionType  -> "DiffusionType"
  | T_BIMethod       -> "BIMethod"
  | T_UseUniformAxes -> "UseUniformAxes"
  | T_SpotN          -> "SpotN"
  | T_VolM           -> "VolM"
  | T_VolUp          -> "VolUp"
  | T_TimeStepDays   -> "TimeStepDays"
  | T_ThreadsNo      -> "ThreadsNo"

  | T_STRIP_Param a -> string_of_argument_type a
  ];


value rec default_of_argument : argument_type -> argument =
fun t ->
  match argument_category_of_type t with
  [
    C_Required ->
      match t with
      [
        T_CP                       -> Required ( CP Call )
      | T_Strike                   -> Required ( Strike 1.0 )
      | T_BarrierDateToT2          -> Required ( BarrierDateToT2 ( DateString "3m" ) )
      | T_BarrierDate              -> Required ( BarrierDate ( DateString "1m" ) )
      | T_Gearing                  -> Required ( Gearing 1.0 )
      | T_CompStrikeIsAsset        -> Required ( CompStrikeIsAsset True )
      | T_Corr                     -> Required ( Corr 0.0 )
      | T_ExpiryDate               -> Required ( ExpiryDate ( DateString "1y" ) )
      | T_HurdleUpToT1             -> Required ( HurdleUpToT1 0.1 )
      | T_HurdleUp                 -> Required ( HurdleUp 0.1 )
      | T_Fixings                  -> Required ( Fixings ( TwoWayInterpTable "Dummy" ) )
      | T_MinBarDays               -> Required ( MinBarDays 1.0 )
      | T_CompCP                   -> Required ( CompCP Call )
      | T_TradeDate                -> Required ( TradeDate ( DateString "1d" ) )
      | T_MinMax                   -> Required ( MinMax ( MinMaxSpot 1.0 ) )
      | T_Bar1ToT1                 -> Required ( Bar1ToT1 0.1 )
      | T_Bar1ToT2                 -> Required ( Bar1ToT2 0.1 )
      | T_Bar2ToT1                 -> Required ( Bar2ToT1 10000.0 )
      | T_HurdleDownToT1           -> Required ( HurdleDownToT1 0.1 )
      | T_Bar2ToT2                 -> Required ( Bar2ToT2 10000.0 )
      | T_Bar1                     -> Required ( Bar1 0.1 )
      | T_Bar2                     -> Required ( Bar2 10000.0 )
      | T_PassportNotionalStep     -> Required ( PassportNotionalStep 1.0 )
      | T_HurdleDown               -> Required ( HurdleDown 10000.0 )
      | T_PayOutDate               -> Required ( PayOutDate ( DateString "2y" ) )
      | T_CompStrike               -> Required ( CompStrike 1.0 )
      | T_NoBarDays                -> Required ( NoBarDays 1.0 )
      | T_BarrierTable             -> Required ( BarrierTable ( TwoWayInterpTable "Dummy" ) )
      | T_IRVol                    -> Required ( IRVol 0.1 )
      | T_STRIP_Param a            -> default_of_argument a
      | T_Type                     -> Required ( Type Options_type.T_N_A )
      | unknown                    -> failwith ( Printf.sprintf "Unrecognised required argument: %s" ( string_of_argument_type unknown ) )
      ]
  | _          -> failwith "Default only available for Required Arguments"
  ];
